SELECT top 10
ex.ID, t.Name, ExpenceDate, ExpenceValue, '000.01', 0, 
ex.ContactID, ISNULL(c.id, ''), ISNULL(c.name, ''), ex.EmplID, ISNULL(e.id, ''), ISNULL(e.name, '')
FROM Expences ex
JOIN ExpencesTypes t
ON ex.ExpencesTypesID=t.ID
LEFT JOIN Contacts c
on ex.ContactID=c.id
LEFT JOIN Empl e
ON ex.EmplID=e.id
ORDER BY ID DESC
