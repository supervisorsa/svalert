SELECT TOP 30 
o.ID, o.ID, o.date, o.Status, o.TotalValue, 0, 
o.customerid, ISNULL(c.euro, ''), ISNULL(c.name, ''), 1, 0, IsOrder, 1,
o.EmplID, ISNULL(e.id, ''), ISNULL(e.name, ''), 
od.ID, od.StockID, ISNULL(s.euro, ''), ISNULL(s.name, ''), od.Value, 
od.Value-(CASE WHEN od.Quan=0 THEN 0 ELSE od.DiscValue/od.Quan END), od.Quan, od.DiscPerc, od.DiscValue, 0, 0
FROM Offers o
JOIN OffersDetails od
on od.OfferID=o.id
LEFT JOIN Empl e
on o.EmplID=e.ID
LEFT JOIN Contacts c
on o.CustomerID=c.ID
LEFT JOIN Stock s
ON od.StockID=s.id
ORDER BY o.ID DESC