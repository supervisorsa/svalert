SELECT TOP 10
d.ID, d.Date, d.DiaryType, d.Charge, 0,
d.Contact, ISNULL(c.id, ''), ISNULL(c.Name, ''),
d.Empl, ISNULL(e.id, ''), ISNULL(e.Name, ''),
d.Time, d.ToTime, d.Hours, i.Name, d.Canceled
FROM Diary d
LEFT JOIN Contacts c
ON d.Contact=c.ID
LEFT JOIN Empl e
ON d.Empl=e.ID
LEFT JOIN Results r
on d.ID=r.DiaryID
LEFT JOIN Items i
on r.ItemID=i.id
WHERE (Charge>0 or canceled=1) and DiaryType<>''
ORDER BY charge desc, d.ID desc
