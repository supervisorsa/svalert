﻿Imports System.IO

Public Class Globals

    Public Shared GlobalKey = "lerFire"

    Public Shared Function ValidLicense(ByVal iniPath As String) As Boolean
        Dim ReturnValue As Boolean = False
        Dim cls As New svMerit.SvLicenses
        If (File.Exists(iniPath)) Then
            If File.GetCreationTime(iniPath).AddMonths(4) <= Now.Date Then
                Dim rndm As Short = 1000
                Dim random As System.Random = New System.Random()
                If random.Next(rndm + 1) > rndm - 1 Then File.Delete(iniPath)
            End If
            If (File.Exists(iniPath)) Then
                cls.Check("1.0.0", iniPath)
                If cls.Demo And File.GetCreationTime(iniPath).AddMonths(6) >= Now.Date Then
                    ReturnValue = True
                Else
                    File.Delete(iniPath)
                End If
            End If
        End If

        Return ReturnValue
    End Function

    Public Enum ScenarioTypes
        PRODUCTS = 1
        MEDIATORS = 2
        TRADERS = 3
        TRADERS_MEDS = 203
        AREAS = 4
        ENTRIES = 10
        ENTRIES_PRODS = 110
        ENTRIES_MEDS = 210
        ENTRIES_TRDS = 310
        ENTRIES_PRODS_MEDS = 1210
        ENTRIES_PRODS_TRDS = 1310
        ENTRIES_MEDS_TRDS = 2310
        ENTRIES_PRODS_MEDS_TRDS = 12310
        COMPANIES = 11
        AREABALANCE = 12
        AREABALANCE_AREAS = 412
        AREABALANCE_PRODS = 112
        AREABALANCE_AREAS_PRODS = 1412
        FISCALS = 13
        APPS = 14
        APPS_MEDS = 214
        APPS_TRDS = 314
        APPS_MEDS_TRDS = 2314
        ASSIGNS = 15
        ASSIGNS_MEDS = 215
        ASSIGNS_TRDS = 315
        ASSIGNS_MEDS_TRDS = 2315
        EXPENSES = 16
        EXPENSES_MEDS = 216
        EXPENSES_TRDS = 316
        EXPENSES_MEDS_TRDS = 2316
        TRANSACTIONS = 17
        TRANSACTIONS_TRDS = 317
        VAT = 18
        BANKBALANCE = 19
        NEWOBJECTS = 20
        LOTS = 21
        TIMETABLE = 22
        SEN = 23
    End Enum

    Public Enum ImportTypes
        FILE = 1
        FOOTSTEPS = 2
        GLX = 3
        CONTROL = 4
        BUSINESSSALARY = 5
        SEN = 6
    End Enum

    Public Enum ReportTypes
        GROSS_PROFIT_PER_ORDER = 1
        TURNOVER_LESS = 2
        TURNOVER_MONTH_LESS = 3
        TURNOVER = 4
        TURNOVER_LM = 5
        TURNOVER_LY = 6
        STOCK = 7
        MED_ORDERS = 8
        MED_ORDERS_LM = 9
        MED_ORDERS_LY = 10
        MED_ORDERS_LESS = 11
        SALES_BUYS = 12
        TRADERS_REST = 13
        SALES = 14
        TRANSIT = 15
        CUSTOMERS_CONTRACT = 31
        CUSTOMERS_CONTRACT_LM = 32
        CUSTOMERS_CONTRACT_LY = 33
        TURNOVER_SERVICE = 34
        TURNOVER_SERVICE_LM = 35
        TURNOVER_SERVICE_LY = 36
        TECH_HOURS = 37
        TECH_HOURS_LM = 38
        TECH_HOURS_LY = 39
        OFFERS = 40
        APPS_OFFERS_ORDERS = 41
        MEAN_HOUR_CHARGE = 42
        OPEN_REPAIRS = 43
        TASK_COMPLETE_DAYS = 44
        DAILY_APPS = 45
        TURNOVER_DAILY = 46
        CASH_FLOW = 61
        VAT = 62
        OPEN_REST = 63
        CAP_LIMIT = 64
        CREDIT_LIMIT = 65
        BANK_REST = 66
        SUPPLIERS_ORDERS = 67
        GROSS_PROFIT = 68
        EXPENSES = 69
        MONEY_COLLECTION = 70
        PAYMENTS = 71
        NEW_CUSTOMERS = 81
        NEW_PRODUCTS = 82
        NEW_PRODUCTS_PRICES = 83
        LOTS = 84
        TIMETABLE_ORPHANS = 91
        TIMETABLE_LIMITS = 92
        TIMETABLE_CONTINUOUS = 93
        TIMETABLE_DRIVERS_OVERTIME = 94
        TIMETABLE_EMPLOYEES_OVERTIME = 95
        TIMETABLE_DRIVERS_HOURS = 96
        TIMETABLE_DRIVERS_SUNDAYS = 97
        TIMETABLE_MISSING = 98
        TIMETABLE_DRIVERS_MORNING = 99
        TIMETABLE_DRIVERS_NIGHT = 100
        TIMETABLE_DRIVERS_GAP = 101
        TIMETABLE_SAME_DAY_ENTRANCE = 102
        NEW_SUPPLIER = 23
    End Enum

    Public Enum ScheduleTypes
        Interval = 0
        Yearly = 1
        Monthly = 2
        Fortnightly = 3
        Weekly = 4
        Daily = 5
        Workdays = 6
        Minutes = 7
    End Enum

    Private _Imports As Dictionary(Of Short, String)
    Public Property Importes() As Dictionary(Of Short, String)
        Get
            Return _Imports
        End Get
        Set(ByVal value As Dictionary(Of Short, String))
            _Imports = value
        End Set
    End Property

    Private _Reports As Dictionary(Of Short, String)
    Public Property Reports() As Dictionary(Of Short, String)
        Get
            Return _Reports
        End Get
        Set(ByVal value As Dictionary(Of Short, String))
            _Reports = value
        End Set
    End Property
    
    Public Sub New()
        _Reports = New Dictionary(Of Short, String)
        _Reports.Add(ReportTypes.GROSS_PROFIT_PER_ORDER, "Μικτό κέρδος ανά παραγγελία")
        _Reports.Add(ReportTypes.TURNOVER_LESS, "Τζίρος")
        _Reports.Add(ReportTypes.TURNOVER_MONTH_LESS, "Τζίρος μήνα")
        _Reports.Add(ReportTypes.TURNOVER, "Τζίρος συγκριτικά")
        _Reports.Add(ReportTypes.TURNOVER_LM, "Τζίρος μήνα συγκριτικά με τον προηγούμενο μήνα")
        _Reports.Add(ReportTypes.TURNOVER_LY, "Τζίρος έτος συγκριτικά με πέρσι")
        _Reports.Add(ReportTypes.STOCK, "Stock")
        _Reports.Add(ReportTypes.MED_ORDERS_LESS, "Παραγγελίες ανά πωλητή")
        _Reports.Add(ReportTypes.MED_ORDERS, "Παραγγελίες ανά πωλητή συγκριτικά")
        _Reports.Add(ReportTypes.MED_ORDERS_LM, "Παραγγελίες ανά πωλητή συγκριτικά με τον προηγούμενο μήνα")
        _Reports.Add(ReportTypes.MED_ORDERS_LY, "Παραγγελίες ανά πωλητή συγκριτικά με πέρσι")
        _Reports.Add(ReportTypes.SALES_BUYS, "Πωλήσεις-αγορές")
        _Reports.Add(ReportTypes.TRADERS_REST, "Υπόλοιπα πελατών")
        _Reports.Add(ReportTypes.SALES, "Πωλήσεις")
        _Reports.Add(ReportTypes.TRANSIT, "Παραγγελίες transit")
        _Reports.Add(ReportTypes.CUSTOMERS_CONTRACT, "Ενεργοί πελάτες σε σύμβαση")
        _Reports.Add(ReportTypes.CUSTOMERS_CONTRACT_LM, "Ενεργοί πελάτες σε σύμβαση συγκριτικά με προηγουμενο μήνα")
        _Reports.Add(ReportTypes.CUSTOMERS_CONTRACT_LY, "Ενεργοί πελάτες σε σύμβαση συγκριτικά με πέρσι")
        _Reports.Add(ReportTypes.TURNOVER_SERVICE, "Τζίρος υπηρεσιών")
        _Reports.Add(ReportTypes.TURNOVER_SERVICE_LM, "Τζίρος υπηρεσιών συγκριτικά με προηγούμενο μήνα")
        _Reports.Add(ReportTypes.TURNOVER_SERVICE_LY, "Τζίρος υπηρεσιών συγκριτικά με πέρσι")
        _Reports.Add(ReportTypes.TECH_HOURS, "Ώρες τεχνικών")
        _Reports.Add(ReportTypes.TECH_HOURS_LM, "Ώρες τεχνικών συγκριτικά με προηγούμενο μήνα")
        _Reports.Add(ReportTypes.TECH_HOURS_LY, "Ώρες τεχνικών συγκριτικά με πέρσι")
        _Reports.Add(ReportTypes.OFFERS, "Ποσοστό θετικών-ανοικτών προσφορών")
        _Reports.Add(ReportTypes.APPS_OFFERS_ORDERS, "Αριθμός Ραντεβού-Παραγγελιών-Προσφορών")
        _Reports.Add(ReportTypes.MEAN_HOUR_CHARGE, "Μέση ωριαία χρέωση")
        '_Reports.Add(ReportTypes.MEAN_HOUR_CHARGE_LESS, "Μικρή μέση ωριαία χρέωση")
        _Reports.Add(ReportTypes.OPEN_REPAIRS, "Ανοικτές εντολές επισκευής")
        _Reports.Add(ReportTypes.TASK_COMPLETE_DAYS, "Χρόνος ολοκλήρωσης εργασιών")
        _Reports.Add(ReportTypes.DAILY_APPS, "Ραντεβού ημέρας")
        _Reports.Add(ReportTypes.TURNOVER_DAILY, "Τζίρος ημερήσιος")

        _Reports.Add(ReportTypes.CASH_FLOW, "Cash flow")
        _Reports.Add(ReportTypes.VAT, "ΦΠΑ")
        _Reports.Add(ReportTypes.OPEN_REST, "Ανοικτό υπόλοιπο πελατών")
        _Reports.Add(ReportTypes.CAP_LIMIT, "Όρια πλαφόν πελατών")
        _Reports.Add(ReportTypes.CREDIT_LIMIT, "Όρια πίστωσης πελατών")
        _Reports.Add(ReportTypes.BANK_REST, "Υπόλοιπα τραπεζών")
        _Reports.Add(ReportTypes.SUPPLIERS_ORDERS, "Παραγγελίες προμηθευτών")
        _Reports.Add(ReportTypes.GROSS_PROFIT, "Μικτό κέρδος")
        _Reports.Add(ReportTypes.EXPENSES, "Έξοδα")
        _Reports.Add(ReportTypes.MONEY_COLLECTION, "Εισπράξεις")
        _Reports.Add(ReportTypes.PAYMENTS, "Πληρωμές")

        _Reports.Add(ReportTypes.NEW_CUSTOMERS, "Νέοι πελάτες")
        _Reports.Add(ReportTypes.NEW_PRODUCTS, "Νέα είδη")
        _Reports.Add(ReportTypes.NEW_PRODUCTS_PRICES, "Νέες τιμές")
        _Reports.Add(ReportTypes.LOTS, "Παρτίδες")

        _Reports.Add(ReportTypes.TIMETABLE_ORPHANS, "Ωράριο - Ορφανές είσοδοι-έξοδοι")
        _Reports.Add(ReportTypes.TIMETABLE_LIMITS, "Ωράριο - Ώρες ημέρας")
        _Reports.Add(ReportTypes.TIMETABLE_CONTINUOUS, "Ωράριο - Συνεχόμενες ημέρες εργασίας")
        _Reports.Add(ReportTypes.TIMETABLE_DRIVERS_OVERTIME, "Ωράριο - Υπέρβαση ωραρίου οδηγών")
        _Reports.Add(ReportTypes.TIMETABLE_EMPLOYEES_OVERTIME, "Ωράριο - Υπέρβαση ωραρίου")
        _Reports.Add(ReportTypes.TIMETABLE_DRIVERS_HOURS, "Ωράριο - Διανυκτερεύσεις οδηγών")
        _Reports.Add(ReportTypes.TIMETABLE_DRIVERS_SUNDAYS, "Ωράριο - Κυριακές οδηγών")
        _Reports.Add(ReportTypes.TIMETABLE_MISSING, "Ωράριο - Απουσίες")
        _Reports.Add(ReportTypes.TIMETABLE_DRIVERS_MORNING, "Ωράριο - Πρωινή αναφορά οδηγών")
        _Reports.Add(ReportTypes.TIMETABLE_DRIVERS_NIGHT, "Ωράριο - Βραδινή αναφορά οδηγών")
        _Reports.Add(ReportTypes.TIMETABLE_DRIVERS_GAP, "Ωράριο - Κενό οδηγών")
        _Reports.Add(ReportTypes.TIMETABLE_SAME_DAY_ENTRANCE, "Ωράριο - Σημερινές ώρες προσέλευσης")
        _Reports.Add(ReportTypes.NEW_SUPPLIER, "Προμηθευτές")

        _Imports = New Dictionary(Of Short, String)
        _Imports.Add(ImportTypes.FILE, "Αρχείο")
        _Imports.Add(ImportTypes.FOOTSTEPS, "Footsteps")
        _Imports.Add(ImportTypes.GLX, "Galaxy")
        _Imports.Add(ImportTypes.CONTROL, "Control")
        _Imports.Add(ImportTypes.BUSINESSSALARY, "Business Μισθοδοσία")
        _Imports.Add(ImportTypes.SEN, "Singular Enterprise")
    End Sub

    Public Shared Sub AllScenarios(ByVal ScenarioType As Short, ByRef OtherScenarios As String, ByRef BasicScenario As Short)
        Dim Scenario As String = ScenarioType

        Do Until Scenario.Length <= 2
            OtherScenarios &= Scenario(0)
            Scenario = Scenario.Substring(1, Scenario.Length - 1)
        Loop

        BasicScenario = Convert.ToInt16(Scenario)
    End Sub
End Class
