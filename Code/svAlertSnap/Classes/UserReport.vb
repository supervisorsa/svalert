﻿Public Class UserReport
    Private _IsSelected As Boolean = False
    Public Property IsSelected() As Boolean
        Get
            Return _IsSelected
        End Get
        Set(ByVal value As Boolean)
            _IsSelected = value
        End Set
    End Property

    'Private _ReportName As String
    Public ReadOnly Property ReportName() As String
        Get
            If ReportsNames Is Nothing Then LoadReportNames()
            Return ReportsNames.Item(_ReportType)
        End Get
        'Set(ByVal value As String)
        '    _ReportName = value
        'End Set
    End Property

End Class
