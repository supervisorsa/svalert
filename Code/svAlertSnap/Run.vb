﻿Imports System.Threading
Imports System.Net.Mail
Imports System.Globalization
Imports svGlobal
Imports svAlertReportAndScenario.Globals


Public Class Run
    Private EnDecrypt As New svMerit.SvCrypto(svMerit.SvCrypto.SymmProvEnum.Rijndael)

    Public Sub New(ByVal ConnString As String)
        dataMain = New dataMainDataContext(ConnString)
        LoadReportNames()
    End Sub

    Private _IsService As Boolean = False
    Public WriteOnly Property IsService() As Boolean
        Set(ByVal value As Boolean)
            _IsService = value
        End Set
    End Property

    Private CloseIt As Boolean = False
    Private ConnectorParams As String

    Private ActiveProcessID As Integer
    Private CurrentProcessID As Integer
    Private UpdatedNextRun As Boolean = True

    Public Function RunSnapshot(ByVal DataConnString As String, ByVal ListID As Guid) As Boolean
        UpdateNextRun()

        Dim SuccessfulRun As Boolean = False
        Dim ErrorMessage As String = ""

        Dim CustomRun As Boolean
        CustomRun = Not ListID.Equals(Guid.Empty)

        SvRegistry.SetRegistryValue("SvAlert", "IsRunning", 1)

        Dim AllLists As IQueryable(Of TaskList)

        Try
            If CustomRun Then
                AllLists = From t In dataMain.TaskLists _
                           Where t.ID.Equals(ListID) _
                           Select t
            Else
                AllLists = From t In dataMain.TaskLists _
                           Where t.IsActive _
                           Order By t.NextRun _
                           Select t
            End If
        Catch exList As Exception
            SvSys.ResultsLog("***".PadRight(10) & SvSys.LeftFixedLength("ΑΠΟΤΥΧΙΑ", 20) & vbTab & "***", My.Application.Info.DirectoryPath)
            SvSys.ErrorLog(exList.Message, My.Application.Info.DirectoryPath)
            If Settings.GetBoolean("UnhandledExceptionManager/SendEmail") Then
                Dim MailError As String = svDespatch.SvMail.SendConnEmail(exList.Message, "")
                If MailError <> String.Empty Then SvSys.ErrorLog(MailError, My.Application.Info.DirectoryPath)
            End If
            Exit Function
        End Try
        Try
            Dim ci As CultureInfo = Thread.CurrentThread.CurrentCulture

            SvSys.ResultsLog("***".PadRight(10) & SvSys.LeftFixedLength("ΕΝΑΡΞΗ : " & Environment.UserName & vbTab & ci.DisplayName, 60) & vbTab & "***", My.Application.Info.DirectoryPath)

            UpdatedNextRun = False
            For Each lst In AllLists
                Try
                    'Dim NextOccurrence As Date
                    '                    Dim LastOccurance As Date

                    'NextOccurrence = GetNextRun(lst.ID, lst.LastRun)

                    'LastOccurance = Now

                    Dim SnapDate As DateTime = Now

                    If CustomRun OrElse _
                    (Not CustomRun AndAlso lst.NextRun <= SnapDate) Then
                        SvSys.ResultsLog("***".PadRight(10) & SvSys.LeftFixedLength(lst.ListName, 60) & vbTab & "***", My.Application.Info.DirectoryPath)

                        Dim SnapshotFail As Boolean = False

                        Dim SnapID As Guid = Guid.NewGuid

                        Dim StartDate As Date
                        Dim EndDate As Date

                        LoadPreviousDates(lst.Schedule, StartDate, EndDate)

                        Dim NewSnap As New Snapshot
                        NewSnap.ID = SnapID
                        NewSnap.SnapshotName = lst.ListName
                        NewSnap.TaskList = lst
                        NewSnap.IsMinutes = lst.Schedule.IsMinutes
                        NewSnap.ExecuteDate = SnapDate
                        NewSnap.DataStart = StartDate
                        NewSnap.DataEnd = EndDate
                        NewSnap.IsFailed = False
                        dataMain.Snapshots.InsertOnSubmit(NewSnap)

                        dataMain.SubmitChanges()

                        For Each tsk In lst.TaskListLines
                            Dim Importer As New svAlertData.Importer(DataConnString, StartDate, EndDate)



                            Dim CurrentScheduleType As Short = 0
                            If tsk.TaskList.Schedule.IsYearly Then
                                CurrentScheduleType = ScheduleTypes.Yearly
                            ElseIf tsk.TaskList.Schedule.IsMonthly Then
                                CurrentScheduleType = ScheduleTypes.Monthly
                            ElseIf tsk.TaskList.Schedule.IsFortnightly Then
                                CurrentScheduleType = ScheduleTypes.Fortnightly
                            ElseIf tsk.TaskList.Schedule.IsWeekly Then
                                CurrentScheduleType = ScheduleTypes.Weekly
                            ElseIf tsk.TaskList.Schedule.IsDaily Then
                                CurrentScheduleType = ScheduleTypes.Daily
                            ElseIf tsk.TaskList.Schedule.IsWorkdays Then
                                CurrentScheduleType = ScheduleTypes.Workdays
                            ElseIf tsk.TaskList.Schedule.IsMinutes Then
                                CurrentScheduleType = ScheduleTypes.Minutes
                            End If

                           

                            If Not Importer.Import(SnapID, _
                                                       CurrentScheduleType, _
                                                       tsk.TaskList.Schedule.Offset, _
                                                       tsk.Task.ImportType, _
                                                       tsk.Task.FilePath & If(tsk.Task.FileName Is Nothing OrElse tsk.Task.FileName = "", "", "\" & tsk.Task.FileName), _
                                                       False, _
                                                       tsk.Task.ScenarioType) Then


                                SnapshotFail = True
                                'Exit For
                            End If
                        Next
                        If SnapshotFail Then
                            NewSnap.IsFailed = True
                        Else
                            lst.LastRun = SnapDate
                            lst.NextRun = CalcNextRun(lst.ScheduleID, SnapDate, lst.Schedule.Offset)

                            For Each usr In lst.TaskListUsers
                                If usr.User.IsActive Then
                                    For Each rpt In usr.TaskListUserReports
                                        Dim NewReport As New SnapshotReport With { _
                                        .ID = Guid.NewGuid, _
                                        .SnapshotID = SnapID, _
                                        .UserID = usr.UserID, _
                                        .ReportType = rpt.ReportType}
                                        dataMain.SnapshotReports.InsertOnSubmit(NewReport)

                                        Try
                                            Dim IsTimetable As Boolean = False
                                            If rpt.ReportType = ReportTypes.TIMETABLE_ORPHANS Or _
                                            rpt.ReportType = ReportTypes.TIMETABLE_LIMITS Or _
                                            rpt.ReportType = ReportTypes.TIMETABLE_CONTINUOUS Or _
                                            rpt.ReportType = ReportTypes.TIMETABLE_EMPLOYEES_OVERTIME Or _
                                            rpt.ReportType = ReportTypes.TIMETABLE_DRIVERS_SUNDAYS Or _
                                            rpt.ReportType = ReportTypes.TIMETABLE_DRIVERS_OVERTIME Or _
                                            rpt.ReportType = ReportTypes.TIMETABLE_DRIVERS_HOURS Or _
                                            rpt.ReportType = ReportTypes.TIMETABLE_MISSING Or _
                                            rpt.ReportType = ReportTypes.TIMETABLE_DRIVERS_MORNING Or _
                                            rpt.ReportType = ReportTypes.TIMETABLE_DRIVERS_NIGHT Or _
                                            rpt.ReportType = ReportTypes.TIMETABLE_DRIVERS_GAP Or _
                                            rpt.ReportType = ReportTypes.TIMETABLE_SAME_DAY_ENTRANCE Then
                                                IsTimetable = True
                                            End If

                                            Dim EmplCodes As New List(Of String)
                                            If IsTimetable AndAlso usr.User.ExternalCode IsNot Nothing AndAlso usr.User.ExternalCode <> "" Then
                                                Dim AllEmpls = From t In dataMain.Users _
                                                               Join a In dataMain.UsersAssociates _
                                                               On t.ID Equals a.AssociateUserID _
                                                                Where a.SupervisorUserID.Equals(usr.User.ID) _
                                                                And t.IsActive = True _
                                                                Select t
                                                For Each item In AllEmpls
                                                    EmplCodes.Add(item.ExternalCode)
                                                Next
                                            End If

                                            If EmplCodes.Count > 0 Then
                                                EmplCodes.Add(usr.User.ExternalCode)
                                            End If

                                            Dim AlertTrigger As New svAlertData.ExporterData(DataConnString, SnapID)

                                            Dim TriggerString As String = ""

                                            If IsTimetable Then
                                                AlertTrigger.ExportPath = usr.User.ExportPath
                                                TriggerString = AlertTrigger.Data(rpt.ReportType, usr.User.LoginName, EmplCodes, GetExplainText(NewSnap))
                                            Else
                                                TriggerString = AlertTrigger.Data(rpt.ReportType, usr.User.LoginName, EmplCodes)
                                            End If
                                            'SvSys.ErrorLog(TriggerString, My.Application.Info.DirectoryPath)

                                            If TriggerString <> "" Then

                                                Dim Mail As New svDespatch.SvMail.SMTPClient
                                                Dim EmailFrom As String = IIf(rpt.ReportType = ReportTypes.NEW_SUPPLIER, Settings.GetString("UnhandledExceptionManager/SuppEmailFrom"), Settings.GetString("UnhandledExceptionManager/EmailFrom"))
                                                Dim SenderAddress As New MailAddress(EmailFrom) ', _CampaignSenders(SenderIndex).SenderName)
                                                Dim RecipientAddress As New MailAddress(usr.User.Email)

                                                'Mail.AuthUser = _CampaignSenders(SenderIndex).SenderEmail
                                                'Mail.AuthPassword = _CampaignSenders(SenderIndex).Password
                                                Mail.Port = 25

                                                Dim Body As String
                                                Body = SnapDate.ToString & "<br/>"
                                                If rpt.ReportType = ReportTypes.NEW_SUPPLIER Then
                                                    Body &= String.Format("Έχει δημιουργηθεί μια καινούργια αναφορά για το διάστημα {0}-{1}", Date.Today.ToShortDateString, Date.Today.ToShortDateString) & "<br/>"
                                                Else
                                                    Body &= String.Format("Έχει δημιουργηθεί μια καινούργια αναφορά για το διάστημα {0}-{1}", StartDate.ToShortDateString, EndDate.ToShortDateString) & "<br/>"
                                                End If
                                                Body &= "Λίστα : " & lst.ListName & "<br/>"
                                                Body &= "Αναφορά : " & NewReport.ReportName & "<br/>"

                                                'If TriggerString <> "" Then
                                                If Not IsTimetable Then
                                                    Body &= "<br/>" & TriggerString
                                                End If

                                                'Else
                                                'Body &= "<br/>" & "Αυτό το email είναι δοκιμαστικό, κανονικά δε θα έπρεπε να σταλεί"
                                                'End If

                                                Mail.Server = Settings.GetString("UnhandledExceptionManager/Server")

                                                Dim Attachments As New List(Of String)
                                                If IsTimetable Then
                                                    Attachments.Add(TriggerString)
                                                End If

                                                Mail.AuthUser = Settings.GetString("AuthUser")
                                                Mail.AuthPassword = EnDecrypt.Decrypting(Settings.GetString("AuthPassword"), svAlertReportAndScenario.Globals.GlobalKey, "-")


                                                ErrorMessage = Mail.SendSingleEmail(SenderAddress, _
                                                                                    RecipientAddress, _
                                                                                    "Αναφορά " & NewReport.ReportName, _
                                                                                    Body, _
                                                                                    Attachments, False, False)

                                                If ErrorMessage <> "" Then
                                                    If ErrorMessage.StartsWith("Mailing failed") Then

                                                    Else
                                                        Throw New Exception(ErrorMessage)
                                                    End If
                                                End If
                                            End If

                                        Catch ex As Exception
                                            SvSys.ErrorLog("RunSnapshot".PadRight(10) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
                                        End Try
                                    Next
                                End If
                            Next
                        End If

                        dataMain.SubmitChanges()

                    End If

                    SuccessfulRun = True

                    If _IsService Then
                        If IsNumeric(SvRegistry.GetRegistryValue("SvAlert", "ActiveProcess")) _
                        AndAlso SvRegistry.GetRegistryValue("SvAlert", "ActiveProcess") <> "0" Then
                            Exit For
                        End If
                    End If
                Catch ex As Exception
                    SuccessfulRun = False

                    SvSys.ResultsLog("***".PadRight(10) & SvSys.LeftFixedLength("ΑΠΟΤΥΧΙΑ", 20) & vbTab & "***", My.Application.Info.DirectoryPath)
                    SvSys.ErrorLog(ex.Message, My.Application.Info.DirectoryPath)
                    If Settings.GetBoolean("UnhandledExceptionManager/SendEmail") Then
                        Dim MailError As String = svDespatch.SvMail.SendConnEmail(ex.Message, "")
                        If MailError <> String.Empty Then SvSys.ErrorLog(MailError, My.Application.Info.DirectoryPath)
                    End If
                End Try
            Next
            UpdatedNextRun = True

            SvSys.ResultsLog("***".PadRight(10) & SvSys.LeftFixedLength("ΟΛΟΚΛΗΡΩΣΗ ΔΙΑΔΙΚΑΣΙΑΣ", 30) & vbTab & "***", My.Application.Info.DirectoryPath)

        Catch ex As Exception
            SuccessfulRun = False

            SvSys.ResultsLog("***".PadRight(10) & SvSys.LeftFixedLength("ΑΠΟΤΥΧΙΑ", 20) & vbTab & "***", My.Application.Info.DirectoryPath)
            SvSys.ErrorLog(ex.Message, My.Application.Info.DirectoryPath)
            If Settings.GetBoolean("UnhandledExceptionManager/SendEmail") Then
                Dim MailError As String = svDespatch.SvMail.SendConnEmail(ex.Message, "")
                If MailError <> String.Empty Then SvSys.ErrorLog(MailError, My.Application.Info.DirectoryPath)
            End If
        Finally
            Try
                CalcInterval()
            Catch ex As Exception
                SvSys.ErrorLog(ex.Message, My.Application.Info.DirectoryPath)
            End Try
        End Try
        SvRegistry.SetRegistryValue("SvAlert", "IsRunning", 0)
        Return SuccessfulRun

    End Function

    Public Function CalcInterval() As Integer
        Dim intv As Integer = 0
        Try
            Dim NextTaskRun = (From t In dataMain.TaskLists _
                          Order By t.NextRun _
                          Select t.NextRun).FirstOrDefault

            Dim tempOccurrence = NextTaskRun
            If tempOccurrence IsNot Nothing Then
                Dim NextOccurrence As Date = NextTaskRun
                If NextOccurrence < Now Then
                    intv = If(UpdatedNextRun, 10000, 1000000)
                Else
                    intv = DateDiff(DateInterval.Second, TimeValue(Now), TimeValue(NextOccurrence)) * 1000
                End If
                If intv < 0 Then
                    intv += 86400000
                End If
                If intv = 0 Then
                    intv = 10002
                End If
                'lblNextRun.Text = FormatDateTime(DateAdd(DateInterval.Second, (intv / 1000), Now), DateFormat.GeneralDate).ToString
            End If
        Catch ex As Exception
            intv = 1000004
            SvSys.ErrorLog(ex.Message, My.Application.Info.DirectoryPath)
        End Try
        Return intv
    End Function

    'Private Function GetNextRun(ByVal ListID As Guid, ByVal LastRun As DateTime) As DateTime
    '    Dim CurrentList = (From t In dataMain.TaskLists _
    '                       Where t.ID.Equals(ListID) _
    '                       Select t).FirstOrDefault
    '    If CurrentList.NextRun IsNot Nothing Then
    '        Return CurrentList.NextRun
    '    Else
    '        Return CalcNextRun(CurrentList.ScheduleID, If(CurrentList.LastRun IsNot Nothing, CurrentList.LastRun, Now))
    '    End If
    'End Function

    Private Sub UpdateNextRun()
        Dim AllLists = From t In dataMain.TaskLists _
                     Where t.NextRun Is Nothing _
                     Select t
        For Each lst In AllLists
            lst.NextRun = CalcNextRun(lst.ScheduleID, If(lst.LastRun IsNot Nothing, lst.LastRun, Now), lst.Schedule.Offset)
        Next
        dataMain.SubmitChanges()
    End Sub

    Public Function CalcNextRun(ByVal ScheduleID As Guid, ByVal LastRun As DateTime, ByVal Offset As Short) As DateTime
        Dim CurrentSchedule = (From t In dataMain.Schedules _
                              Where t.ID.Equals(ScheduleID) _
                              Select t).FirstOrDefault

        Dim ExecutionTime = CDate(CurrentSchedule.ExecutionTime)

        Dim NextRun As DateTime = Nothing

        If CurrentSchedule Is Nothing Then
        Else

            If CurrentSchedule.IsYearly Then
                Dim NextYear = Now.Year + 1
                NextRun = New DateTime(NextYear, 1, 1, 0, 0, 0)
            ElseIf CurrentSchedule.IsMonthly Then
                Dim NextYear As Short = Now.Year
                Dim NextMonth = Now.Month + 1
                CorrectMonth(NextMonth, NextYear)
                'If NextMonth = 13 Then
                '    NextYear += 1
                '    NextMonth = 1
                'End If
                NextRun = New DateTime(NextYear, NextMonth, 1, 0, 0, 0)
            ElseIf CurrentSchedule.IsFortnightly Then
                Dim NextDay As Short
                Dim NextYear As Short = Now.Year
                Dim NextMonth

                If Now.Day < 14 Then
                    If Now.Month = 2 Then
                        NextDay = 14
                    Else
                        NextDay = 15
                    End If
                    NextMonth = Now.Month
                Else
                    NextDay = 1
                    NextMonth = Now.Month + 1
                End If
                CorrectMonth(NextMonth, NextYear)
                NextRun = New DateTime(NextYear, NextMonth, NextDay, 0, 0, 0)

            ElseIf CurrentSchedule.IsWeekly Then
                NextRun = LastRun.Date.AddDays(1)
                Do Until NextRun.DayOfWeek = DayOfWeek.Sunday
                    NextRun = NextRun.Date.AddDays(1)
                Loop
            ElseIf CurrentSchedule.IsDaily Then
                NextRun = LastRun.Date.AddDays(1)
            ElseIf CurrentSchedule.IsWorkdays Then
                NextRun = LastRun.Date.AddDays(1)
                Do Until NextRun.DayOfWeek <> DayOfWeek.Sunday And NextRun.DayOfWeek <> DayOfWeek.Monday
                    NextRun = NextRun.Date.AddDays(1)
                Loop
            ElseIf CurrentSchedule.IsRepeated Then
                NextRun = LastRun.Date.AddDays(CurrentSchedule.IntervalDays)
            ElseIf CurrentSchedule.IsMinutes Then
                NextRun = LastRun.AddMinutes(CurrentSchedule.IntervalMinutes)
            Else
                For Each line In CurrentSchedule.ScheduleLines
                    Dim LineDate = CDate(line.ScheduleDate & "/" & Now.Year)
                    If LineDate < Now Then LineDate = LineDate.AddYears(1)
                    If NextRun = Date.MinValue Then
                        NextRun = LineDate
                    Else
                        If LineDate < NextRun Then NextRun = LineDate
                    End If
                Next
            End If
        End If
        If NextRun = Date.MinValue Then
            Return Nothing
        Else
            If Not CurrentSchedule.IsMinutes Then
                Return NextRun.AddHours(ExecutionTime.Hour).AddMinutes(ExecutionTime.Minute)
                If Offset >= 0 Then
                    NextRun.AddDays(Offset)
                End If
            Else
                Return NextRun
            End If
        End If
    End Function

    Public Sub LoadPreviousDates(ByVal CurrentSchedule As Schedule, ByRef SnapStartDate As Date, ByRef SnapEndDate As Date)

        Dim PreviousYear As Short
        Dim PreviousMonth As Short

        Dim CurrentRunDate As Date = Now.Date.AddDays(-CurrentSchedule.Offset)

        If CurrentSchedule.IsYearly Then
            PreviousYear = CurrentRunDate.Year - 1
            PreviousMonth = 1
            SnapStartDate = New Date(PreviousYear, 1, 1)
            SnapEndDate = New Date(PreviousYear, 12, 31)
        ElseIf CurrentSchedule.IsMonthly Then
            PreviousYear = CurrentRunDate.Year
            PreviousMonth = CurrentRunDate.Month - 1
            CorrectMonth(PreviousMonth, PreviousYear)
            SnapStartDate = New Date(PreviousYear, PreviousMonth, 1)
            SnapEndDate = SnapStartDate.AddMonths(1).AddDays(-1)
        ElseIf CurrentSchedule.IsFortnightly Then
            Dim PreviousDay As Short
            PreviousYear = CurrentRunDate.Year

            If CurrentRunDate.Day < 14 Then
                PreviousMonth = CurrentRunDate.Month - 1
                If PreviousMonth = 2 Then
                    PreviousDay = 14
                Else
                    PreviousDay = 15
                End If
                CorrectMonth(PreviousMonth, PreviousYear)
                SnapStartDate = New Date(PreviousYear, PreviousMonth, PreviousDay)
                SnapEndDate = New Date(CurrentRunDate.Year, CurrentRunDate.Month, 1).AddDays(-1)
            Else
                PreviousDay = 1
                PreviousMonth = CurrentRunDate.Month
                SnapStartDate = New Date(PreviousYear, PreviousMonth, PreviousDay)
                SnapEndDate = New Date(CurrentRunDate.Year, CurrentRunDate.Month, If(CurrentRunDate.Month = 2, 14, 15)).AddDays(-1)
            End If
        ElseIf CurrentSchedule.IsWeekly Then
            SnapEndDate = CurrentRunDate.Date.AddDays(-1)
            Do Until SnapEndDate.DayOfWeek = DayOfWeek.Saturday
                SnapEndDate = SnapEndDate.Date.AddDays(-1)
            Loop
            SnapStartDate = SnapEndDate.AddDays(-6)
        ElseIf CurrentSchedule.IsDaily Then
            SnapEndDate = CurrentRunDate.Date.AddDays(-1)
            SnapStartDate = SnapEndDate
        ElseIf CurrentSchedule.IsWorkdays Then
            SnapEndDate = CurrentRunDate.Date.AddDays(-1)
            Do Until SnapEndDate.DayOfWeek <> DayOfWeek.Sunday And SnapEndDate.DayOfWeek <> DayOfWeek.Saturday
                SnapEndDate = SnapEndDate.Date.AddDays(-1)
            Loop
            SnapStartDate = SnapEndDate
        ElseIf CurrentSchedule.IsMinutes Then
            Dim CurrentSnap, PreviousSnap As Snapshot
            Dim PreviousDate = From t In dataMain.Snapshots _
                               Where t.IsMinutes = True _
                               And t.IsFailed = False _
                               Order By t.DataEnd Descending _
                               Select t.DataEnd
            If PreviousDate.Count = 0 Then
                SnapStartDate = New Date(2000, 1, 1)
            Else
                SnapStartDate = PreviousDate.FirstOrDefault
            End If

            SnapEndDate = Now
        End If
    End Sub

    Private Sub CorrectMonth(ByRef NextMonth As Short, ByRef NextYear As Short)
        If NextMonth = 13 Then
            NextYear += 1
            NextMonth = 1
        End If
    End Sub

    Private Function GetExplainText(ByVal NewSnap As Snapshot) As String
        Dim ExplainText As String = ""

        If NewSnap.DataStart = NewSnap.DataEnd Then
            ExplainText = String.Format("Αναφορά για {0}", NewSnap.DataStart.ToShortDateString)
        Else
            ExplainText = String.Format("Αναφορά για {0}-{1}", NewSnap.DataStart.ToShortDateString, NewSnap.DataEnd.ToShortDateString)
        End If

        Return ExplainText
    End Function
End Class
