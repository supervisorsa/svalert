﻿Imports svGlobal
Imports System.Diagnostics
Imports System.Linq
Imports svAlertSnap
Imports svAlertData
Imports System.Threading
Imports System.Globalization

Public Class SvAlert

    Private WithEvents tmr As New Timers.Timer 'With {.Interval = 10000}
    Private ConnStringMain As String
    Private ConnStringData As String

    Private TaskRuns As Boolean = False
    Private LastRun As Date = Now
    Private StopRequested As Boolean = False

    Protected Overrides Sub OnStart(ByVal args() As String)
        'Dim procInfo As ApplicationLoader.PROCESS_INFORMATION
        'Dim pProcess() As Process = System.Diagnostics.Process.GetProcessesByName("SvAlert")

        'Dim HasStart As Boolean = False
        'HasStart = (pProcess.Count > 0)

        'If Not HasStart AndAlso IO.File.Exists(My.Application.Info.DirectoryPath & "\SvAlert.exe") Then
        '    ApplicationLoader.StartProcessAndBypassUAC( _
        '            My.Application.Info.DirectoryPath & "\SvAlert.exe", procInfo)
        'End If

        tmr.Interval = 10000
        tmr.Enabled = True

        Thread.CurrentThread.CurrentCulture = New CultureInfo("el-GR")
        Thread.CurrentThread.CurrentUICulture = New CultureInfo("el-GR")
    End Sub

    Private Sub OnElapsedTime(ByVal source As Object, ByVal e As Timers.ElapsedEventArgs) Handles tmr.Elapsed
        tmr.Enabled = False
        If Not StopRequested Then

            Dim IntervalUpdated As Boolean = False

            Try
                Dim ProcessExists As Boolean = False
                Dim ActiveProcessID As Integer = 0
            If IsNumeric(SvRegistry.GetRegistryValue("SvAlert", "ActiveProcess")) Then
                ActiveProcessID = Convert.ToInt32(SvRegistry.GetRegistryValue("SvAlert", "ActiveProcess"))
                    If ActiveProcessID <> 0 Then
                        For i = 1 To 3
                            Try
                                Dim ActiveProcess As Process = Process.GetProcessById(ActiveProcessID)
                                ProcessExists = True
                                Exit For
                            Catch ex As Exception

                            End Try
                        Next
                    End If
                End If

                If ActiveProcessID <> 0 AndAlso ProcessExists Then
                    SvSys.WriteToLog(My.Application.Info.DirectoryPath, "Service.log", Now.ToString & " ActiveProcess:" & ActiveProcessID.ToString)
                    tmr.Interval = 1000000
                Else
                    Dim intv As Integer = 0
                Dim EnDecrypt As New svMerit.SvCrypto(svMerit.SvCrypto.SymmProvEnum.Rijndael)

                ConnStringMain = DbConn.ConstructConnString(Settings.GetString("MainServer"), _
                                                            Settings.GetString("MainUser"), _
                                                            EnDecrypt.Decrypting(Settings.GetString("MainPwd"), "lerFire", "-"), _
                                                            Settings.GetString("MainDB"), "")
                ConnStringData = DbConn.ConstructConnString(Settings.GetString("DataServer"), _
                                                            Settings.GetString("DataUser"), _
                                                            EnDecrypt.Decrypting(Settings.GetString("DataPwd"), "lerFire", "-"), _
                                                            Settings.GetString("DataDB") & Now.Date.Year, "")

                ' If modScheduler.ValidLicense(My.Application.Info.DirectoryPath & "\sv.lck") Then
                        TaskRuns = True
                        LastRun = Now
                SvRegistry.SetRegistryValue("SvAlert", "ActiveProcess", "0")

                Dim Snap As New svAlertSnap.Run(ConnStringMain)
                Snap.IsService = True


                If Snap.RunSnapshot(ConnStringData, Guid.Empty) Then IntervalUpdated = True
                intv = Snap.CalcInterval()
                If Not IntervalUpdated AndAlso intv < 1000000 Then intv = 1000001
                If intv = 0 Then intv = 1000002
                'Else
                'intv = 2222222
                'SvSys.WriteToLog(My.Application.Info.DirectoryPath, "Service.log", Now.ToString & " " & "License")
                'End If
                    tmr.Interval = intv
                End If
            Catch ex As Exception
                tmr.Interval = 1000003
                SvSys.WriteToLog(My.Application.Info.DirectoryPath, "Service.log", Now.ToString & " " & ex.Message)
                TaskRuns = False
            Finally
                Try
                    SvSys.WriteToLog(My.Application.Info.DirectoryPath, "Service.log", Now.ToString & " Interval: " & tmr.Interval.ToString)
                Catch ex As Exception
                    TaskRuns = False
                End Try
            End Try
        End If
        TaskRuns = False
        If Not StopRequested Then tmr.Enabled = True
    End Sub

    Private Function ActiveProcessExists() As Boolean
        Dim ProcessExists As Boolean = False
        Try
            Dim ActiveProcessID As String = SvRegistry.GetRegistryValue("SvAlert", "ActiveProcess")

            If IsNumeric(ActiveProcessID) Then
                If ActiveProcessID = 0 Then
                    ProcessExists = False
                Else
                    For i = 1 To 3
                        Try
                            Dim ActiveProcess As Process = Process.GetProcessById(Convert.ToInt32(ActiveProcessID))
                            ProcessExists = True
                            Exit For
                        Catch ex As Exception
                        End Try
                    Next
                End If
            End If
        Catch
            ProcessExists = False
        End Try
        Return ProcessExists
    End Function

    Protected Overrides Sub OnStop()
        tmr.Enabled = False
        StopRequested = True

        'In the non-shutdown case, RequestAdditionalTime() must be called more often than every 2 minutes:
        'Note that despite the name, the time parameter is the total shutdown time requested.

            If TaskRuns And DateDiff(DateInterval.Minute, LastRun, Now) < 20 Then
                Dim i As Integer = 1
                Do Until Not TaskRuns Or i > 15
                    'SvSys.WriteToLog(My.Application.Info.DirectoryPath, "Service.log", Now.ToString & " ServiceRuns: " & i.ToString)
                    RequestAdditionalTime(1000000)
                    Thread.Sleep(60000)
                    RequestAdditionalTime(1000000)
                    i += 1
                Loop
            End If
        If SvRegistry.GetRegistryValue("SvAlert", "IsRunning") = "1" _
        AndAlso Not ActiveProcessExists() Then
            SvRegistry.SetRegistryValue("SvAlert", "ActiveProcess", "0")
            SvRegistry.SetRegistryValue("SvAlert", "IsRunning", "0")
        End If
    End Sub

End Class
