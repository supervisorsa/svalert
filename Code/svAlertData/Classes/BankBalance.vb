﻿Public Class BankBalance
    Public Sub Import(ByVal DataList As List(Of String), ByVal SnapshotID As Guid)
        _ID = Guid.NewGuid
        _SnapshotID = SnapshotID
        _AccountingCode = ImproveData("String", DataList(0))
        _BankRest = ImproveData("Numeric", DataList(1))

        dataImported.BankBalances.InsertOnSubmit(Me)
        dataImported.SubmitChanges()
    End Sub

End Class
