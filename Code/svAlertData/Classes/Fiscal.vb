﻿Public Class Fiscal
    Public Sub Import(ByVal DataList As List(Of String), ByVal SnapshotID As Guid)
        _ID = Guid.NewGuid
        _SnapshotID = SnapshotID
        _FiscalID = ImproveData("String", DataList(0))
        _FiscalCode = ImproveData("String", DataList(1))
        _FiscalDescription = ImproveData("String", DataList(2))
        _StartDate = ImproveData("Date", DataList(3))
        _EndDate = ImproveData("Date", DataList(4))

        dataImported.Fiscals.InsertOnSubmit(Me)
        dataImported.SubmitChanges()
    End Sub

End Class
