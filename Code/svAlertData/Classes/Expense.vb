﻿Public Class Expense
    Public Sub Import(ByVal DataList As List(Of String), ByVal SnapshotID As Guid)
        _ID = Guid.NewGuid
        _SnapshotID = SnapshotID
        _ExpenseID = ImproveData("String", DataList(0))
        _ExpenseDescription = ImproveData("String", DataList(1))
        _ExpenseDate = ImproveData("Date", DataList(2))
        _ExpenseValue = ImproveData("Numeric", DataList(3))
        _ExpenseAccountingCode = ImproveData("String", DataList(4))
        _CompanyID = ImproveData("String", DataList(5))
        _TraderID = ImproveData("String", DataList(6))
        _TraderCode = ImproveData("String", DataList(7))
        _TraderDescription = ImproveData("String", DataList(8))
        _MediatorID = ImproveData("String", DataList(9))
        _MediatorCode = ImproveData("String", DataList(10))
        _MediatorDescription = ImproveData("String", DataList(11))

        dataImported.Expenses.InsertOnSubmit(Me)
        dataImported.SubmitChanges()
    End Sub

End Class
