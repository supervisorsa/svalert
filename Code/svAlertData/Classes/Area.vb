﻿Public Class Area
    Public Sub Import(ByVal DataList As List(Of String), ByVal SnapshotID As Guid, ByVal FullImport As Boolean)
        _ID = Guid.NewGuid
        _SnapshotID = SnapshotID
        _AreaID = ImproveData("String", DataList(0))
        _AreaCode = ImproveData("String", DataList(1))
        _AreaDescription = ImproveData("String", DataList(2))

        'If FullImport Then
        'Else
        'End If

        dataImported.Areas.InsertOnSubmit(Me)
        dataImported.SubmitChanges()
    End Sub

End Class
