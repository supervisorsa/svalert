﻿Public Class Trader
    Public Sub Import(ByVal DataList As List(Of String), ByVal SnapshotID As Guid, ByVal FullImport As Boolean)
        _ID = Guid.NewGuid
        _SnapshotID = SnapshotID
        _TraderID = ImproveData("String", DataList(0))
        _TraderCode = ImproveData("String", DataList(1))
        _TraderDescription = ImproveData("String", DataList(2))
        _CompanyID = ImproveData("String", DataList(3))

        If FullImport Then
            _MediatorID = ImproveData("String", DataList(4))
            _MediatorCode = ImproveData("String", DataList(5))
            _MediatorDescription = ImproveData("String", DataList(6))
            _Charge = ImproveData("Numeric", DataList(7))
            _Credit = ImproveData("Numeric", DataList(8))
            _Rest = ImproveData("Numeric", DataList(9))
            _CapLimit = ImproveData("Numeric", DataList(10))
            _CreditLimit = ImproveData("Numeric", DataList(11))
            _IsActive = ImproveData("Boolean", DataList(12))
            _InContract = ImproveData("Boolean", DataList(13))
            _IsCustomer = ImproveData("Boolean", DataList(14))
            _IsSupplier = ImproveData("Boolean", DataList(15))
            _IsDebtor = ImproveData("Boolean", DataList(16))
            _IsCreditor = ImproveData("Boolean", DataList(17))
            _UseInCommercial = If(DataList(18) Is Nothing OrElse DataList(18) = "", True, ImproveData("Boolean", DataList(18)))
            _UseInServices = If(DataList(19) Is Nothing OrElse DataList(19) = "", True, ImproveData("Boolean", DataList(19)))
            _UseInFinancial = If(DataList(20) Is Nothing OrElse DataList(20) = "", True, ImproveData("Boolean", DataList(20)))
            _NewAlert = If(DataList(21) Is Nothing OrElse DataList(21) = "", False, ImproveData("Boolean", DataList(21)))
        Else
            _Charge = 0
            _Credit = 0
            _Rest = 0
            _CapLimit = 0
            _CreditLimit = 0
            _IsActive = 1
            _InContract = 1
            _IsCustomer = False
            _IsSupplier = False
            _IsDebtor = False
            _IsCreditor = False
            _UseInCommercial = True
            _UseInServices = True
            _UseInFinancial = True
            _NewAlert = False
        End If

        dataImported.Traders.InsertOnSubmit(Me)
        dataImported.SubmitChanges()
    End Sub

End Class
