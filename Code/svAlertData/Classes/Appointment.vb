﻿Public Class Appointment
    Public Sub Import(ByVal DataList As List(Of String), ByVal SnapshotID As Guid)
        _ID = Guid.NewGuid
        _SnapshotID = SnapshotID
        _AppointmentID = ImproveData("String", DataList(0))
        _AppointmentDate = ImproveData("Date", DataList(1))
        _AppointmentType = ImproveData("String", DataList(2))
        _AppointmentValue = ImproveData("Numeric", DataList(3))
        _CompanyID = ImproveData("String", DataList(4))
        _TraderID = ImproveData("String", DataList(5))
        _TraderCode = ImproveData("String", DataList(6))
        _TraderDescription = ImproveData("String", DataList(7))
        _MediatorID = ImproveData("String", DataList(8))
        _MediatorCode = ImproveData("String", DataList(9))
        _MediatorDescription = ImproveData("String", DataList(10))
        _StartTime = ImproveDateData(ImproveData("Date", DataList(11)), _AppointmentDate)
        _EndTime = ImproveDateData(ImproveData("Date", DataList(12)), _AppointmentDate)
        _HoursSpent = ImproveData("Numeric", DataList(13))
        _ResultDescription = Left(ImproveData("String", DataList(14)), 100)
        _IsCanceled = ImproveData("Boolean", DataList(15))
        _IsService = ImproveData("Boolean", DataList(16))

        dataImported.Appointments.InsertOnSubmit(Me)
        dataImported.SubmitChanges()
    End Sub

    Public ReadOnly Property CalcHours() As Decimal
        Get
            If _HoursSpent > 0 Then
                Return Convert.ToDecimal(_HoursSpent)
            ElseIf _StartTime < _EndTime Then
                Return Convert.ToDecimal(DateDiff(DateInterval.Hour, CDate(_StartTime), CDate(_EndTime)))
            Else
                Return 0
            End If
        End Get
    End Property

End Class
