﻿Public Class Transaction
    Public Sub Import(ByVal DataList As List(Of String), ByVal SnapshotID As Guid)
        _ID = Guid.NewGuid
        _SnapshotID = SnapshotID
        _TransactionID = ImproveData("String", DataList(0))
        _TransactionDate = ImproveData("Date", DataList(1))
        _TransactionValue = ImproveData("Numeric", DataList(2))
        _TransactionType = Convert.ToInt16(ImproveData("Numeric", DataList(3)))
        _IsPayment = ImproveData("Boolean", DataList(4))
        _CompanyID = ImproveData("String", DataList(5))
        _TraderID = ImproveData("String", DataList(6))
        _TraderCode = ImproveData("String", DataList(7))
        _TraderDescription = ImproveData("String", DataList(8))
        _DocumentExpiryDate = ImproveData("Date", DataList(9))

        dataImported.Transactions.InsertOnSubmit(Me)
        dataImported.SubmitChanges()
    End Sub

End Class
