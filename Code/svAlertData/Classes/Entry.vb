﻿Public Class Entry
    Public Sub Import(ByVal DataListLines As List(Of List(Of String)), ByVal SnapshotID As Guid)
        Dim DataList As List(Of String) = DataListLines(0)
        _ID = Guid.NewGuid
        _SnapshotID = SnapshotID
        _EntryID = ImproveData("String", DataList(0))
        _EntryCode = ImproveData("String", DataList(1))
        _EntryDate = ImproveData("Date", DataList(2))
        _EntryStatus = ImproveData("String", DataList(3))
        _EntryFinalValue = ImproveData("Numeric", DataList(4))
        _CompanyID = ImproveData("String", DataList(5))
        _TraderID = ImproveData("String", DataList(6))
        _TraderCode = ImproveData("String", DataList(7))
        _TraderDescription = ImproveData("String", DataList(8))
        _MediatorID = ImproveData("String", DataList(9))
        _MediatorCode = ImproveData("String", DataList(10))
        _MediatorDescription = ImproveData("String", DataList(11))
        _OrderDate = ImproveData("Date", DataList(12))
        _IsSale = ImproveData("Boolean", DataList(13))
        _IsCharge = ImproveData("Boolean", DataList(14))
        _IsOrder = ImproveData("Boolean", DataList(15))
        _AfterOffer = ImproveData("Boolean", DataList(16))
        _IsDelivered = ImproveData("Boolean", DataList(17))
        _InOfferDateLimit = ImproveData("Boolean", DataList(18))
        _InOrderDateLimit = ImproveData("Boolean", DataList(19))

        dataImported.Entries.InsertOnSubmit(Me)

        For Each row In DataListLines
            Dim NewEntryLine As New EntryLine
            NewEntryLine.Import(row, SnapshotID)
        Next

        dataImported.SubmitChanges()
    End Sub

End Class
