﻿Public Class EntryLine
    Public Sub Import(ByVal DataList As List(Of String), ByVal SnapshotID As Guid)
        _ID = Guid.NewGuid
        _SnapshotID = SnapshotID
        _LineID = ImproveData("String", DataList(20))
        _EntryID = ImproveData("String", DataList(0))
        _ProductID = ImproveData("String", DataList(21))
        _ProductCode = ImproveData("String", DataList(22))
        _ProductDescription = ImproveData("String", DataList(23))
        _ProductPreValue = ImproveData("Numeric", DataList(24))
        _ProductFinalValue = ImproveData("Numeric", DataList(25))
        _Quantity = ImproveData("Numeric", DataList(26))
        _DiscountPercentage = ImproveData("Numeric", DataList(27))
        _LineDiscountValue = ImproveData("Numeric", DataList(28))
        _LineVatValue = ImproveData("Numeric", DataList(29))
        _LineDiscountVatValue = ImproveData("Numeric", DataList(30))

        dataImported.EntryLines.InsertOnSubmit(Me)
    End Sub

End Class
