﻿Public Class ProductLot
    Public Sub Import(ByVal DataList As List(Of String), ByVal SnapshotID As Guid)
        _ID = Guid.NewGuid
        _SnapshotID = SnapshotID
        _LotID = ImproveData("String", DataList(0))
        _LotCode = ImproveData("String", DataList(1))
        _LotExpiry = ImproveData("Date", DataList(2))
        _ProductID = ImproveData("String", DataList(3))

        dataImported.ProductLots.InsertOnSubmit(Me)
        dataImported.SubmitChanges()
    End Sub

End Class
