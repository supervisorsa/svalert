﻿Public Class Product
    Public Sub Import(ByVal DataList As List(Of String), ByVal SnapshotID As Guid, ByVal FullImport As Boolean)
        _ID = Guid.NewGuid
        _SnapshotID = SnapshotID
        _ProductID = ImproveData("String", DataList(0))
        _ProductCode = ImproveData("String", DataList(1))
        _ProductDescription = ImproveData("String", DataList(2))

        If FullImport Then
            _Price = ImproveData("Numeric", DataList(3))
            _Cost = ImproveData("Numeric", DataList(4))
            _StockQuantity = ImproveData("Numeric", DataList(5))
            _NewAlert = If(DataList(6) Is Nothing OrElse DataList(6) = "", False, ImproveData("Boolean", DataList(6)))
        Else
            _Price = 0
            _Cost = 0
            _StockQuantity = 0
            _NewAlert = False
        End If

        dataImported.Products.InsertOnSubmit(Me)
        dataImported.SubmitChanges()
    End Sub

End Class
