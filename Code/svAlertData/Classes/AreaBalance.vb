﻿Public Class AreaBalance
    Public Sub Import(ByVal DataList As List(Of String), ByVal SnapshotID As Guid)
        _ID = Guid.NewGuid
        _SnapshotID = SnapshotID
        _AreaID = ImproveData("String", DataList(0))
        _AreaCode = ImproveData("String", DataList(1))
        _AreaDescription = ImproveData("String", DataList(2))
        _ProductID = ImproveData("String", DataList(3))
        _ProductCode = ImproveData("String", DataList(4))
        _ProductDescription = ImproveData("String", DataList(5))
        _BalanceQuantity = ImproveData("Numeric", DataList(6))
        _BalanceValue = ImproveData("Numeric", DataList(7))

        dataImported.AreaBalances.InsertOnSubmit(Me)
        dataImported.SubmitChanges()
    End Sub

End Class
