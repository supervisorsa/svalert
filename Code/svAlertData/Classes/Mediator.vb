﻿Public Class Mediator
    Public Sub Import(ByVal DataList As List(Of String), ByVal SnapshotID As Guid, ByVal FullImport As Boolean)
        _ID = Guid.NewGuid
        _SnapshotID = SnapshotID
        _MediatorID = ImproveData("String", DataList(0))
        _MediatorCode = ImproveData("String", DataList(1))
        _MediatorDescription = ImproveData("String", DataList(2))

        If FullImport Then
            _Department = ImproveData("String", DataList(3))
            _IsSalesperson = ImproveData("Boolean", DataList(4))
            _IsCollector = ImproveData("Boolean", DataList(5))
            _IsPartner = ImproveData("Boolean", DataList(6))
            _IsTechnician = ImproveData("Boolean", DataList(7))
            _IsRepresentative = ImproveData("Boolean", DataList(8))
            _IsOther = ImproveData("Boolean", DataList(9))
        Else
            _Department = ""
            _IsSalesperson = False
            _IsCollector = False
            _IsPartner = False
            _IsTechnician = False
            _IsRepresentative = False
            _IsOther = False
        End If

        dataImported.Mediators.InsertOnSubmit(Me)
        dataImported.SubmitChanges()
    End Sub
End Class
