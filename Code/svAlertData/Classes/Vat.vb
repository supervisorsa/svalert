﻿Public Class Vat
    Public Sub Import(ByVal DataList As List(Of String), ByVal SnapshotID As Guid)
        _ID = Guid.NewGuid
        _SnapshotID = SnapshotID
        _VatDate = ImproveData("Date", DataList(0))
        _VatValue = ImproveData("Numeric", DataList(1))

        dataImported.Vats.InsertOnSubmit(Me)
        dataImported.SubmitChanges()
    End Sub

End Class
