﻿Public Class Timetable
    Public Sub Import(ByVal DataList As List(Of String), ByVal SnapshotID As Guid)
        _ID = Guid.NewGuid
        _SnapshotID = SnapshotID
        _EmployeeCode = ImproveData("String", DataList(0))
        _EmployeeName = ImproveData("String", DataList(1))
        _TimetableDate = ImproveData("Date", DataList(3))
        _TimeIn = ImproveData("Date", DataList(4))
        _TimeOut = ImproveData("Date", DataList(5))
        _IsDriver = ImproveData("Boolean", DataList(6))

        dataImported.Timetables.InsertOnSubmit(Me)
        dataImported.SubmitChanges()
    End Sub
End Class
