﻿Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports Oracle.DataAccess.Client

Public Class DBManager


    Public Shared _ConnectionString As String
    Public Shared Property ConnectionString() As String
        Get
            Return _ConnectionString
        End Get
        Set(ByVal value As String)
            _ConnectionString = value
        End Set
    End Property


    Public Shared _Connection As OracleConnection
    Public Shared Property Connection() As OracleConnection
        Get
            Return _Connection
        End Get
        Set(ByVal value As OracleConnection)
            _Connection = value
        End Set
    End Property

    Public Shared _OracleReader As OracleDataReader
    Public Shared Property OracleReader() As OracleDataReader
        Get
            Return _OracleReader
        End Get
        Set(ByVal value As OracleDataReader)
            _OracleReader = value
        End Set
    End Property
    Public cmd As OracleCommand

    Public Sub New(ByVal _ConnString As String)
        ConnectionString = _ConnString
    End Sub

    Public Function Connect() As Boolean

        Try
            Connection = New OracleConnection(ConnectionString)            
            Connection.Open()
            cmd = New OracleCommand()
            cmd.Connection = Connection
            cmd.CommandType = System.Data.CommandType.Text
        Catch ex As Exception
            Return False
            SvSys.ErrorLog("Connect " & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
            Throw New Exception(ex.Message)
        End Try
        Return True
    End Function

    Public Function isConnect() As Boolean
        Return (Connection.State = System.Data.ConnectionState.Open)

    End Function

    Public Function Disconnect() As Boolean
        Try
            Connection.Close()
            Connection.Dispose()
            Connection = Nothing
            Return True

        Catch ex As Exception
            Return False
            SvSys.ErrorLog("Disconnect " & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
            Throw New Exception(ex.Message)

        End Try

    End Function
    Public Sub ExecuteQuery(ByVal query As String)

        If (String.IsNullOrEmpty(query)) Then
            Throw New ArgumentException("query is null or empty.", "query")
        End If
        If (cmd Is Nothing) Then
            Throw New ArgumentException("Oracle Command is null.", "cmd")
        End If
        Try
            OracleReader = Nothing
            cmd.CommandText = query
            OracleReader = cmd.ExecuteReader()
        Catch ex As Exception           
            SvSys.ErrorLog("ExecuteQuery " & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
            Throw New Exception(ex.Message)

        End Try
    End Sub
End Class
