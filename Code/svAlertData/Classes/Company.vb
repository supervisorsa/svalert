﻿Public Class Company
    Public Sub Import(ByVal DataList As List(Of String), ByVal SnapshotID As Guid)
        _ID = Guid.NewGuid
        _SnapshotID = SnapshotID
        _CompanyID = ImproveData("String", DataList(0))
        _CompanyCode = ImproveData("String", DataList(1))
        _CompanyDescription = ImproveData("String", DataList(2))
        _IsDefault = ImproveData("Boolean", DataList(3))
        _IsActive = ImproveData("Boolean", DataList(4))

        dataImported.Companies.InsertOnSubmit(Me)
        dataImported.SubmitChanges()
    End Sub

End Class
