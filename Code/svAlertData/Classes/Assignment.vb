﻿Public Class Assignment
    Public Sub Import(ByVal DataList As List(Of String), ByVal SnapshotID As Guid)
        _ID = Guid.NewGuid
        _SnapshotID = SnapshotID
        _AssignmentID = ImproveData("String", DataList(0))
        _AssignmentDate = ImproveData("Date", DataList(1))
        _AssignmentType = ImproveData("String", DataList(2))
        _AssignmentStatus = ImproveData("String", DataList(3))
        _CompanyID = ImproveData("String", DataList(4))
        _TraderID = ImproveData("String", DataList(5))
        _TraderCode = ImproveData("String", DataList(6))
        _TraderDescription = ImproveData("String", DataList(7))
        _MediatorID = ImproveData("String", DataList(8))
        _MediatorCode = ImproveData("String", DataList(9))
        _MediatorDescription = ImproveData("String", DataList(10))
        _StartDate = ImproveDateData(ImproveData("Date", DataList(11)), _AssignmentDate)
        _EndDate = ImproveDateData(ImproveData("Date", DataList(12)), _AssignmentDate)
        _IsCompleted = ImproveData("Boolean", DataList(13))
        _IsRepair = ImproveData("Boolean", DataList(14))

        dataImported.Assignments.InsertOnSubmit(Me)
        dataImported.SubmitChanges()
    End Sub

End Class
