﻿

Public Class AlertTrigger
    Public Sub New(ByVal ConnString As String)
        dataImported = New dataImportedDataContext(ConnString)
    End Sub

    Public Function Trigger(ByVal ReportType As Short, ByVal SnapID As Guid, Optional ByVal ExportPath As String = "") As String
        Dim TriggerString As String = ""
        Dim DataList As New List(Of List(Of String))

        Dim NewGlobal As New svAlertReportAndScenario.Globals
        Dim ReportName As String = NewGlobal.Reports(ReportType)

        Select Case ReportType
            Case ReportTypes.GROSS_PROFIT_PER_ORDER
                DataList = GrossProfit(SnapID)
            Case ReportTypes.TURNOVER_LESS Or ReportTypes.TURNOVER_MONTH_LESS
                DataList = Turnover(SnapID)
            Case ReportTypes.TURNOVER Or ReportTypes.TURNOVER_LM Or ReportTypes.TURNOVER_LY
                Dim CurrentSnap, PreviousSnap As Snapshot
                Dim AllSnapshots = (From t In dataImported.Entries _
                Select t.SnapshotID).Distinct
                LoadSnapshots(SnapID, AllSnapshots.ToList, CurrentSnap, PreviousSnap)
                If PreviousSnap Is Nothing Then
                    DataList = Turnover(SnapID)
                Else
                    DataList = TurnoverCompare(CurrentSnap, PreviousSnap)
                End If
            Case ReportTypes.STOCK
                DataList = Stock(SnapID)
            Case ReportTypes.MED_ORDERS Or ReportTypes.MED_ORDERS_LM Or ReportTypes.MED_ORDERS_LY
                Dim CurrentSnap, PreviousSnap As Snapshot
                Dim AllSnapshots = (From t In dataImported.Entries _
                Select t.SnapshotID).Distinct
                LoadSnapshots(SnapID, AllSnapshots.ToList, CurrentSnap, PreviousSnap)
                If PreviousSnap Is Nothing Then
                    DataList = MediatorOrders(SnapID)
                Else
                    DataList = MediatorOrdersCompare(CurrentSnap, PreviousSnap)
                End If
            Case ReportTypes.MED_ORDERS_LESS
                DataList = MediatorOrders(SnapID)
            Case ReportTypes.SALES_BUYS
                DataList = SalesBuys(SnapID)
            Case ReportTypes.TRADERS_REST
                DataList = TraderRest(SnapID)
            Case ReportTypes.SALES
                DataList = Sales(SnapID)
            Case ReportTypes.TRANSIT
                DataList = Transit(SnapID)

            Case ReportTypes.CUSTOMERS_CONTRACT
                DataList = CustomerInContract(SnapID)
            Case ReportTypes.CUSTOMERS_CONTRACT_LY Or ReportTypes.CUSTOMERS_CONTRACT_LM
                Dim CurrentSnap, PreviousSnap As Snapshot
                Dim AllSnapshots = (From t In dataImported.Traders _
                Select t.SnapshotID).Distinct
                LoadSnapshots(SnapID, AllSnapshots.ToList, CurrentSnap, PreviousSnap)
                If PreviousSnap Is Nothing Then
                    DataList = CustomerInContract(SnapID)
                Else
                    DataList = CustomerInContract_Compare(CurrentSnap, PreviousSnap)
                End If
            Case ReportTypes.TURNOVER_SERVICE
                DataList = ServicesTurnover(SnapID)
            Case ReportTypes.TURNOVER_SERVICE_LY Or ReportTypes.TURNOVER_SERVICE_LM
                Dim CurrentSnap, PreviousSnap As Snapshot
                Dim AllSnapshots = (From t In dataImported.Appointments _
                Select t.SnapshotID).Distinct
                LoadSnapshots(SnapID, AllSnapshots.ToList, CurrentSnap, PreviousSnap)
                If PreviousSnap Is Nothing Then
                    DataList = ServicesTurnover(SnapID)
                Else
                    DataList = ServicesTurnover_Compare(CurrentSnap, PreviousSnap)
                End If
            Case ReportTypes.TECH_HOURS
                DataList = TechHours(SnapID)
            Case ReportTypes.TECH_HOURS_LY Or ReportTypes.TECH_HOURS_LM
                Dim CurrentSnap, PreviousSnap As Snapshot
                Dim AllSnapshots = (From t In dataImported.Appointments _
                Select t.SnapshotID).Distinct
                LoadSnapshots(SnapID, AllSnapshots.ToList, CurrentSnap, PreviousSnap)
                If PreviousSnap Is Nothing Then
                    DataList = TechHours(SnapID)
                Else
                    DataList = TechHours_Compare(CurrentSnap, PreviousSnap)
                End If
            Case ReportTypes.OFFERS
                DataList = Offers(SnapID)
            Case ReportTypes.APPS_OFFERS_ORDERS
                DataList = AppOrders(SnapID)
            Case ReportTypes.MEAN_HOUR_CHARGE
                DataList = MeanHours(SnapID)
            Case ReportTypes.OPEN_REPAIRS
                DataList = OpenTasks(SnapID)
            Case ReportTypes.TASK_COMPLETE_DAYS
                DataList = CompletionDays(SnapID)
            Case ReportTypes.DAILY_APPS
                DataList = DailyApps(SnapID)
            Case ReportTypes.TURNOVER_DAILY
                DataList = TurnOverDaily(SnapID)
        End Select

        If ExportPath = "" Then
            Dim ExporterFile As New ExporterFile
        'Exporter.ExcelExport(String.Format("{0}\{1}{2).xls", ExportPath, "apps", Format(Now, "yyyyMMdd.hhmmss")), DataList)
            ExporterFile.ExcelExport(String.Format("{0}\{1}_{2}.xls", ExportPath, ReportName, Format(Now, "yyyyMMdd.HHmmss")), DataList)
        'String.Format("{0}\{1}{2}.xls", ExportPath,"apps",Format(Now, "yyyyMMdd.hhmmss"))
            Return ""
        Else
            If DataList.Count = 1 Then
                TriggerString = DataList(0)(0)
            End If
            If TriggerString <> "" Then
                TriggerString = "ΠΡΟΣΟΧΗ : <br/>" & TriggerString
            End If
            Return TriggerString
        End If
    End Function

End Class

