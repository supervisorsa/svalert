﻿Imports svGlobal
Imports svAlertReportAndScenario.Globals
Imports svGlobal.Settings

Public Class ExporterData
    Private SnapID As Guid
    Private JustAlert As Boolean = True

    Private _ExportPath As String
    Public WriteOnly Property ExportPath() As String
        Set(ByVal value As String)
            _ExportPath = value
            JustAlert = False
        End Set
    End Property

    Public Sub New(ByVal ConnString As String, ByVal SnapshotID As Guid)
        dataImported = New dataImportedDataContext(ConnString)
        dataImported.CommandTimeout = 300
        SnapID = SnapshotID
    End Sub

    Public Function Data(ByVal ReportType As Short, ByVal UserID As String, _
                         ByVal EmplCodes As List(Of String), Optional ByVal ExplainText As String = "") As String
        Dim TriggerString As String = ""
        Dim DataList As New List(Of List(Of String))

        Dim NewGlobal As New svAlertReportAndScenario.Globals
        Dim ReportName As String = NewGlobal.Reports(ReportType)

        Select Case ReportType
            Case ReportTypes.GROSS_PROFIT_PER_ORDER
                DataList = GrossProfitPerOrder()
            Case ReportTypes.TURNOVER_LESS Or ReportTypes.TURNOVER_MONTH_LESS
                DataList = Turnover()
            Case ReportTypes.TURNOVER
                DataList = CompareTurnover()
            Case ReportTypes.TURNOVER_LM
                DataList = CompareTurnover()
            Case ReportTypes.TURNOVER_LY
                DataList = CompareTurnover()
            Case ReportTypes.STOCK
                DataList = Stock()
            Case ReportTypes.MED_ORDERS_LESS
                DataList = MediatorOrders()
            Case ReportTypes.MED_ORDERS
                DataList = CompareMediators()
            Case ReportTypes.MED_ORDERS_LM
                DataList = CompareMediators()
            Case ReportTypes.MED_ORDERS_LY
                DataList = CompareMediators()
            Case ReportTypes.SALES_BUYS
                DataList = SalesBuys()
            Case ReportTypes.TRADERS_REST
                DataList = TradersRest()
            Case ReportTypes.SALES
                DataList = Sales()
            Case ReportTypes.TRANSIT
                DataList = Transit()
            Case ReportTypes.CUSTOMERS_CONTRACT
                DataList = CustomersInContract()
            Case ReportTypes.CUSTOMERS_CONTRACT_LY
                DataList = CompareCustomers()
            Case ReportTypes.CUSTOMERS_CONTRACT_LM
                DataList = CompareCustomers()
            Case ReportTypes.TURNOVER_SERVICE
                DataList = ServicesTurnover()
            Case ReportTypes.TURNOVER_SERVICE_LY
                DataList = CompareTurnoverService()
            Case ReportTypes.TURNOVER_SERVICE_LM
                DataList = CompareTurnoverService()
            Case ReportTypes.TECH_HOURS
                DataList = TechHours()
            Case ReportTypes.TECH_HOURS_LY
                DataList = CompareTechhours()
            Case ReportTypes.TECH_HOURS_LM
                DataList = CompareTechhours()
            Case ReportTypes.OFFERS
                DataList = Offers()
            Case ReportTypes.APPS_OFFERS_ORDERS
                DataList = AppOrders()
            Case ReportTypes.MEAN_HOUR_CHARGE
                DataList = MeanHours()
            Case ReportTypes.OPEN_REPAIRS
                DataList = OpenTasks()
            Case ReportTypes.TASK_COMPLETE_DAYS
                DataList = CompletionDays()
            Case ReportTypes.DAILY_APPS
                DataList = DailyApps()
            Case ReportTypes.TURNOVER_DAILY
                DataList = TurnoverDaily()
            Case ReportTypes.NEW_CUSTOMERS
                DataList = NewCustomers()
            Case ReportTypes.NEW_PRODUCTS
                DataList = NewProducts()
            Case ReportTypes.NEW_PRODUCTS_PRICES
                DataList = NewProductsPrices()
            Case ReportTypes.LOTS
                DataList = Lots()
            Case ReportTypes.TIMETABLE_ORPHANS
                DataList = TimetableOrphans(EmplCodes)
            Case ReportTypes.TIMETABLE_LIMITS
                DataList = TimetableLimits(EmplCodes)
            Case ReportTypes.TIMETABLE_CONTINUOUS
                DataList = TimetableContinuous(EmplCodes)
            Case ReportTypes.TIMETABLE_DRIVERS_GAP
                DataList = TimetableDriversGap(EmplCodes)
            Case ReportTypes.TIMETABLE_DRIVERS_OVERTIME
                DataList = TimetableDriversOvertime(EmplCodes)
            Case ReportTypes.TIMETABLE_EMPLOYEES_OVERTIME
                DataList = TimetableEmployeesOvertime(EmplCodes)
            Case ReportTypes.TIMETABLE_DRIVERS_HOURS
                DataList = TimetableDriversNights(EmplCodes)
            Case ReportTypes.TIMETABLE_DRIVERS_SUNDAYS
                DataList = TimetableDriversSundays(EmplCodes)
            Case ReportTypes.TIMETABLE_MISSING
                DataList = TimetableMissing(EmplCodes)
            Case ReportTypes.TIMETABLE_DRIVERS_MORNING
                DataList = TimetableDriversReport(EmplCodes, False)
            Case ReportTypes.TIMETABLE_DRIVERS_NIGHT
                DataList = TimetableDriversReport(EmplCodes, True)
            Case ReportTypes.TIMETABLE_SAME_DAY_ENTRANCE
                DataList = TimetableSameDayEntrances(EmplCodes)
            Case ReportTypes.NEW_SUPPLIER
                DataList = NewSuppliers(True)
        End Select

        If JustAlert Then
            If DataList.Count = 1 Then
                TriggerString = DataList(0)(0)
            End If
            If TriggerString <> "" AndAlso ReportType <> ReportTypes.APPS_OFFERS_ORDERS Then
                TriggerString = "ΠΡΟΣΟΧΗ : <br/>" & TriggerString
            End If
            Return TriggerString
        Else
            Dim ExporterFile As New ExporterFile
            ExporterFile.ExplainText = ExplainText
            Dim Filepath As String = String.Format("{0}\{1}_{2}_{3}", _ExportPath, ReportName, UserID, Format(Now, "yyyyMMdd.HHmmss"))
            'Exporter.ExcelExport(String.Format("{0}\{1}{2).xls", ExportPath, "apps", Format(Now, "yyyyMMdd.hhmmss")), DataList)
            Return ExporterFile.ExcelExport(Filepath, DataList)
            'String.Format("{0}\{1}{2}.xls", ExportPath,"apps",Format(Now, "yyyyMMdd.hhmmss"))
        End If
    End Function

    Private Function CompareTurnover() As List(Of List(Of String))
        Dim CurrentSnap, PreviousSnap As Snapshot
        Dim AllSnapshots = (From t In dataImported.Entries _
        Select t.SnapshotID).Distinct
        LoadSnapshots(SnapID, AllSnapshots.ToList, CurrentSnap, PreviousSnap)
        If PreviousSnap Is Nothing Then
            Return Turnover()
        Else
            Return TurnoverCompare(CurrentSnap, PreviousSnap)
        End If
    End Function

    Private Function CompareTechhours() As List(Of List(Of String))
        Dim CurrentSnap, PreviousSnap As Snapshot
        Dim AllSnapshots = (From t In dataImported.Appointments _
        Select t.SnapshotID).Distinct
        LoadSnapshots(SnapID, AllSnapshots.ToList, CurrentSnap, PreviousSnap)
        If PreviousSnap Is Nothing Then
            Return TechHours()
        Else
            Return TechHoursCompare(CurrentSnap, PreviousSnap)
        End If
    End Function

    Private Function CompareCustomers() As List(Of List(Of String))
        Dim CurrentSnap, PreviousSnap As Snapshot
        Dim AllSnapshots = (From t In dataImported.Traders _
                            Where t.NewAlert = False _
                            Select t.SnapshotID).Distinct
        LoadSnapshots(SnapID, AllSnapshots.ToList, CurrentSnap, PreviousSnap)
        If PreviousSnap Is Nothing Then
            Return CustomersInContract()
        Else
            Return CustomersInContractCompare(CurrentSnap, PreviousSnap)
        End If
    End Function

    Private Function CompareTurnoverService() As List(Of List(Of String))
        Dim CurrentSnap, PreviousSnap As Snapshot
        Dim AllSnapshots = (From t In dataImported.Appointments _
        Select t.SnapshotID).Distinct
        LoadSnapshots(SnapID, AllSnapshots.ToList, CurrentSnap, PreviousSnap)
        If PreviousSnap Is Nothing Then
            Return ServicesTurnover()
        Else
            Return ServicesTurnoverCompare(CurrentSnap, PreviousSnap)
        End If
    End Function

    Private Function CompareMediators() As List(Of List(Of String))
        Dim CurrentSnap, PreviousSnap As Snapshot
        Dim AllSnapshots = (From t In dataImported.Entries _
        Select t.SnapshotID).Distinct
        LoadSnapshots(SnapID, AllSnapshots.ToList, CurrentSnap, PreviousSnap)
        If PreviousSnap Is Nothing Then
            Return MediatorOrders()
        Else
            Return MediatorOrdersCompare(CurrentSnap, PreviousSnap)
        End If
    End Function

#Region "Reports and Alerts"
#Region "Commercial"
    Private Function GrossProfitPerOrder() As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))
        Try
            Dim AllOrders = (From e In dataImported.Entries _
                             Join t In dataImported.Traders _
                             On e.TraderID Equals t.TraderID _
                             And e.SnapshotID Equals t.SnapshotID _
                             Where e.SnapshotID.Equals(SnapID) _
                             And e.IsOrder = True _
                             Select e.EntryID, t.TraderCode, t.TraderDescription, e.EntryDate, e.EntryFinalValue).Distinct

            If JustAlert Then
                Dim LineList As New List(Of String)
                Dim TriggerString As String = ""

                Dim TotalOrders As Integer = 0 'AllTasks.Count

                For Each ord In AllOrders
                    Dim OrderCost As Decimal = 0

                    Dim AllItems = From t In dataImported.EntryLines _
                                   Where t.EntryID.Equals(ord.EntryID) _
                                   And t.SnapshotID.Equals(SnapID) _
                                   Select t.ProductID, t.Quantity

                    For Each item In AllItems
                        Dim ProductCost = (From t In dataImported.Products _
                                        Where t.ProductID.Equals(item.ProductID) _
                                        And t.SnapshotID.Equals(SnapID) _
                                        Select t.Cost).FirstOrDefault
                        OrderCost += ProductCost * item.Quantity
                    Next


                    If ord.EntryFinalValue - OrderCost < GetString("GrossProfitPerOrderLimit") Then
                        TotalOrders += 1
                    End If

                Next

                If TotalOrders > 0 Then
                    TriggerString = String.Format("Υπάρχουν {0} παραγγελίες με μικτό κέρδος μικρότερο από {1}", _
                                                  TotalOrders, GetString("GrossProfitPerOrderLimit"))
                End If
                LineList.Add(TriggerString)
                DataList.Add(LineList)
            Else
                Dim HeaderRow As New List(Of String)
                HeaderRow.Add("ΚΩΔΙΚΟΣ ΠΕΛΑΤΗ")
                HeaderRow.Add("ΕΠΩΝΥΜΙΑ ΠΕΛΑΤΗ")
                HeaderRow.Add("ΗΜΕΡΟΜΗΝΙΑ")
                HeaderRow.Add("ΠΩΛΗΣΗ")
                HeaderRow.Add("ΚΟΣΤΟΣ")
                HeaderRow.Add("ΜΙΚΤΟ ΚΕΡΔΟΣ")
                DataList.Add(HeaderRow)

                DataList.Add(New List(Of String))

                For Each ord In AllOrders.OrderBy(Function(f) f.EntryDate)
                    Dim OrderCost As Decimal = 0

                    Dim AllItems = From t In dataImported.EntryLines _
                                   Where t.EntryID.Equals(ord.EntryID) _
                                   And t.SnapshotID.Equals(SnapID) _
                                   Select t.ProductID, t.Quantity

                    For Each item In AllItems
                        Dim ProductCost = (From t In dataImported.Products _
                                        Where t.ProductID.Equals(item.ProductID) _
                                        And t.SnapshotID.Equals(SnapID) _
                                        Select t.Cost).FirstOrDefault
                        OrderCost += ProductCost * item.Quantity
                    Next

                    Dim RowList As New List(Of String)
                    RowList.Add(ord.TraderCode)
                    RowList.Add(ord.TraderDescription)
                    RowList.Add(ord.EntryDate.ToShortDateString)
                    RowList.Add(Convert.ToDecimal(ord.EntryFinalValue).ToString("#.00"))
                    RowList.Add(Convert.ToDecimal(OrderCost).ToString("#.00"))
                    RowList.Add(Convert.ToDecimal(ord.EntryFinalValue - OrderCost).ToString("#.00"))

                    DataList.Add(RowList)
                Next
            End If
        Catch ex As Exception
            SvSys.ErrorLog("ExporterData.GrossProfitPerOrder".PadRight(10) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
        End Try
        Return DataList

    End Function

    Private Function Turnover() As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))
        Try
            Dim AllOrders = (From e In dataImported.Entries _
                          Join t In dataImported.Traders _
                          On e.TraderID Equals t.TraderID _
                          And e.SnapshotID Equals t.SnapshotID _
                          Where e.SnapshotID.Equals(SnapID) _
                          And e.IsOrder = True _
                          Select e.EntryID, t.TraderCode, t.TraderDescription, e.EntryDate, e.EntryFinalValue).Distinct

            If JustAlert Then
                Dim LineList As New List(Of String)
                Dim TriggerString As String = ""

                Dim OrdersTotal As Decimal = Convert.ToDecimal(AllOrders.Sum(Function(f) f.EntryFinalValue))

                Dim CurrentSnapshot = (From t In dataImported.Snapshots _
                                    Where t.ID.Equals(SnapID) _
                                    Select t).FirstOrDefault

                If CurrentSnapshot.IsMonthly Then
                    If OrdersTotal < GetString("TurnoverMonthLimit") Then
                        TriggerString = String.Format("Ο τζίρος έπεσε σε {0}", _
                                                      OrdersTotal)
                    End If
                Else
                    If OrdersTotal < GetString("TurnoverLimit") Then
                        TriggerString = String.Format("Ο τζίρος έπεσε σε {0}", _
                                                      OrdersTotal)

                    End If
                End If

                LineList.Add(TriggerString)
                DataList.Add(LineList)
            Else
                Dim Total As New List(Of String)
                Total.Add("ΣΥΝΟΛΟ ΤΖΙΡΟΥ")
                Total.Add(Convert.ToDecimal(AllOrders.Sum(Function(f) f.EntryFinalValue)).ToString("#.00"))
                DataList.Add(Total)

                DataList.Add(New List(Of String))

                Dim HeaderRow As New List(Of String)
                HeaderRow.Add("ΚΩΔΙΚΟΣ ΠΕΛΑΤΗ")
                HeaderRow.Add("ΕΠΩΝΥΜΙΑ ΠΕΛΑΤΗ")
                HeaderRow.Add("ΗΜΕΡΟΜΗΝΙΑ")
                HeaderRow.Add("ΠΩΛΗΣΗ")
                DataList.Add(HeaderRow)

                For Each ord In AllOrders.OrderBy(Function(f) f.EntryDate)
                    Dim RowList As New List(Of String)
                    RowList.Add(ord.TraderCode)
                    RowList.Add(ord.TraderDescription)
                    RowList.Add(ord.EntryDate.ToShortDateString)
                    RowList.Add(Convert.ToDecimal(ord.EntryFinalValue).ToString("#.00"))

                    DataList.Add(RowList)
                Next
            End If
        Catch ex As Exception
            SvSys.ErrorLog("ExporterData.Turnover".PadRight(10) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
        End Try
        Return DataList
    End Function

    Private Function TurnoverCompare(ByVal CurrentSnap As Snapshot, ByVal PreviousSnap As Snapshot) As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))
        Try
            Dim CurrentOrders = (From e In dataImported.Entries _
                          Join t In dataImported.Traders _
                          On e.TraderID Equals t.TraderID _
                          And e.SnapshotID Equals t.SnapshotID _
                          Where e.SnapshotID.Equals(CurrentSnap.ID) _
                          And e.IsOrder = True _
                          Select e.EntryID, t.TraderCode, t.TraderDescription, e.EntryDate, e.EntryFinalValue).Distinct

            Dim PreviousOrders = (From e In dataImported.Entries _
                          Join t In dataImported.Traders _
                          On e.TraderID Equals t.TraderID _
                          And e.SnapshotID Equals t.SnapshotID _
                          Where e.SnapshotID.Equals(CurrentSnap.ID) _
                          And e.IsOrder = True _
                          Select e.EntryID, t.TraderCode, t.TraderDescription, e.EntryDate, e.EntryFinalValue).Distinct

            If JustAlert Then
                Dim LineList As New List(Of String)
                Dim TriggerString As String = ""

                If CurrentOrders.Sum(Function(f) f.EntryFinalValue) < PreviousOrders.Sum(Function(f) f.EntryFinalValue) Then
                    TriggerString = String.Format("Ο τζίρος μειώθηκε από {0} σε {1}", _
                                                  PreviousOrders.Sum(Function(f) f.EntryFinalValue).ToString, _
                                                  CurrentOrders.Sum(Function(f) f.EntryFinalValue).ToString)
                End If

                LineList.Add(TriggerString)
                DataList.Add(LineList)
            Else
                Dim Total1 As New List(Of String)
                Total1.Add("ΣΥΝΟΛΟ ΤΖΙΡΟΥ")
                Total1.Add(CurrentSnap.ExecuteDate.Month & "/" & CurrentSnap.ExecuteDate.Year)
                Total1.Add(Convert.ToDecimal(CurrentOrders.Sum(Function(f) f.EntryFinalValue)).ToString("#.00"))
                DataList.Add(Total1)

                Dim Total2 As New List(Of String)
                Total2.Add("ΣΥΝΟΛΟ ΤΖΙΡΟΥ")
                Total2.Add(PreviousSnap.ExecuteDate.Month & "/" & PreviousSnap.ExecuteDate.Year)
                Total2.Add(Convert.ToDecimal(PreviousOrders.Sum(Function(f) f.EntryFinalValue)).ToString("#.00"))
                DataList.Add(Total2)

                DataList.Add(New List(Of String))

                Dim HeaderRow As New List(Of String)
                HeaderRow.Add("ΚΩΔΙΚΟΣ ΠΕΛΑΤΗ")
                HeaderRow.Add("ΕΠΩΝΥΜΙΑ ΠΕΛΑΤΗ")
                HeaderRow.Add("ΗΜΕΡΟΜΗΝΙΑ")
                HeaderRow.Add("ΠΩΛΗΣΗ")
                DataList.Add(HeaderRow)

                For Each ord In CurrentOrders.OrderBy(Function(f) f.EntryDate)
                    Dim RowList As New List(Of String)
                    RowList.Add(ord.TraderCode)
                    RowList.Add(ord.TraderDescription)
                    RowList.Add(ord.EntryDate.ToShortDateString)
                    RowList.Add(Convert.ToDecimal(ord.EntryFinalValue).ToString("#.00"))

                    DataList.Add(RowList)
                Next

                DataList.Add(New List(Of String))

                For Each ord In PreviousOrders.OrderBy(Function(f) f.EntryDate)
                    Dim RowList As New List(Of String)
                    RowList.Add(ord.TraderCode)
                    RowList.Add(ord.TraderDescription)
                    RowList.Add(ord.EntryDate.ToShortDateString)
                    RowList.Add(Convert.ToDecimal(ord.EntryFinalValue).ToString("#.00"))

                    DataList.Add(RowList)
                Next
            End If
        Catch ex As Exception
            SvSys.ErrorLog("ExporterData.TurnoverCompare".PadRight(10) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
        End Try
        Return DataList
    End Function

    Private Function MediatorOrders() As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))
        Try
            Dim AllOrders = (From e In dataImported.Entries _
                          Join t In dataImported.Traders _
                          On e.TraderID Equals t.TraderID _
                          And e.SnapshotID Equals t.SnapshotID _
                          Join m In dataImported.Mediators _
                          On e.MediatorID Equals m.MediatorID _
                          And e.SnapshotID Equals m.SnapshotID _
                          Where e.SnapshotID.Equals(SnapID) _
                          And e.IsOrder = True _
                          And e.InOrderDateLimit = True _
                          Select e.EntryID, t.TraderCode, t.TraderDescription, m.MediatorDescription, e.OrderDate, e.EntryFinalValue, e.IsOrder).Distinct

            If JustAlert Then
                Dim LineList As New List(Of String)
                Dim TriggerString As String = ""

                Dim CurrentMediator As String = ""

                For Each ord In AllOrders.OrderBy(Function(f) f.OrderDate).OrderBy(Function(p) p.MediatorDescription)
                    If CurrentMediator <> ord.MediatorDescription Then
                        CurrentMediator = ord.MediatorDescription

                        Dim OrdersTotal As Decimal = Convert.ToDecimal(AllOrders.Where(Function(p) p.MediatorDescription = CurrentMediator).Sum(Function(f) f.EntryFinalValue))

                        If OrdersTotal < GetString("SalesPersonLimit") Then
                            TriggerString &= String.Format("Ο τζίρος του πωλητή {0} έπεσε σε {1}", CurrentMediator, OrdersTotal) & "<br/>"
                        End If
                    End If
                Next

                LineList.Add(TriggerString)
                DataList.Add(LineList)
            Else
                Dim CurrentMediator As String = ""

                For Each ord In AllOrders.OrderBy(Function(f) f.OrderDate).OrderBy(Function(p) p.MediatorDescription)
                    If CurrentMediator <> ord.MediatorDescription Then
                        CurrentMediator = ord.MediatorDescription

                        DataList.Add(New List(Of String))
                        DataList.Add(New List(Of String))

                        Dim Total As New List(Of String)
                        Total.Add(CurrentMediator)
                        Total.Add("ΣΥΝΟΛΟ ΤΖΙΡΟΥ")
                        Total.Add(Convert.ToDecimal(AllOrders.Where(Function(p) p.MediatorDescription = CurrentMediator).Sum(Function(f) f.EntryFinalValue)).ToString("#.00"))
                        DataList.Add(Total)

                        DataList.Add(New List(Of String))

                        Dim HeaderRow As New List(Of String)
                        HeaderRow.Add("ΗΜΕΡΟΜΗΝΙΑ")
                        HeaderRow.Add("ΚΩΔΙΚΟΣ ΠΕΛΑΤΗ")
                        HeaderRow.Add("ΕΠΩΝΥΜΙΑ ΠΕΛΑΤΗ")
                        HeaderRow.Add("ΠΩΛΗΣΗ")
                        DataList.Add(HeaderRow)
                    End If

                    Dim RowList As New List(Of String)
                    RowList.Add(CDate(ord.OrderDate).ToShortDateString)
                    RowList.Add(ord.TraderCode)
                    RowList.Add(ord.TraderDescription)
                    RowList.Add(Convert.ToDecimal(ord.EntryFinalValue).ToString("#.00"))

                    DataList.Add(RowList)
                Next
            End If
        Catch ex As Exception
            SvSys.ErrorLog("ExporterData.MediatorOrders".PadRight(10) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
        End Try
        Return DataList
    End Function

    Private Function MediatorOrdersCompare(ByVal CurrentSnap As Snapshot, ByVal PreviousSnap As Snapshot) As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))
        Try
            Dim CurrentOrders = (From e In dataImported.Entries _
                          Join t In dataImported.Traders _
                          On e.TraderID Equals t.TraderID _
                          And e.SnapshotID Equals t.SnapshotID _
                          Join m In dataImported.Mediators _
                          On e.MediatorID Equals m.MediatorID _
                          And e.SnapshotID Equals m.SnapshotID _
                          Where e.SnapshotID.Equals(CurrentSnap.ID) _
                          And e.IsOrder = True _
                          And e.InOrderDateLimit = True _
                          Select e.EntryID, t.TraderCode, t.TraderDescription, m.MediatorDescription, e.EntryDate, e.OrderDate, e.EntryFinalValue, e.IsOrder).Distinct

            Dim LineList As New List(Of String)
            Dim TriggerString As String = ""

            'Dim PreviousOrders 'As IQueryable(Of Object)

            Dim CurrentMediator As String = ""

            Dim PreviousOrders = (From e In dataImported.Entries _
                                  Join t In dataImported.Traders _
                                  On e.TraderID Equals t.TraderID _
                                  And e.SnapshotID Equals t.SnapshotID _
                                  Join m In dataImported.Mediators _
                                  On e.MediatorID Equals m.MediatorID _
                                  And e.SnapshotID Equals m.SnapshotID _
                                  Where e.SnapshotID.Equals(PreviousSnap.ID) _
                                  And m.MediatorDescription = CurrentMediator _
                                  And e.IsOrder = True _
                                  And e.InOrderDateLimit = True _
                                  Select t.TraderCode, t.TraderDescription, m.MediatorDescription, e.EntryDate, e.OrderDate, e.EntryFinalValue, e.IsOrder).Distinct

            For Each ord In CurrentOrders.OrderBy(Function(f) f.OrderDate).OrderBy(Function(p) p.MediatorDescription)
                If CurrentMediator <> ord.MediatorDescription Then
                    If CurrentMediator <> "" AndAlso PreviousOrders IsNot Nothing Then
                        DataList.Add(New List(Of String))

                        Dim Total2 As New List(Of String)
                        Total2.Add(CurrentMediator)
                        Total2.Add("ΠΡΟΗΓΟΥΜΕΝΗ ΠΕΡΙΟΔΟΣ")
                        Total2.Add("ΣΥΝΟΛΟ ΤΖΙΡΟΥ")
                        Total2.Add(Convert.ToDecimal(PreviousOrders.Sum(Function(f) f.EntryFinalValue)).ToString("#.00"))
                        DataList.Add(Total2)

                        Dim HeaderRow As New List(Of String)
                        HeaderRow.Add("ΚΩΔΙΚΟΣ ΠΕΛΑΤΗ")
                        HeaderRow.Add("ΕΠΩΝΥΜΙΑ ΠΕΛΑΤΗ")
                        HeaderRow.Add("ΗΜΕΡΟΜΗΝΙΑ")
                        HeaderRow.Add("ΠΩΛΗΣΗ")
                        DataList.Add(HeaderRow)

                        For Each pord In PreviousOrders.OrderBy(Function(f) f.OrderDate).OrderBy(Function(p) p.MediatorDescription)
                            Dim pRowList As New List(Of String)
                            pRowList.Add(pord.TraderCode)
                            pRowList.Add(pord.TraderDescription)
                            pRowList.Add(pord.EntryDate.ToShortDateString)
                            pRowList.Add(Convert.ToDecimal(pord.EntryFinalValue).ToString("#.00"))

                            DataList.Add(pRowList)
                        Next
                    End If

                    CurrentMediator = ord.MediatorDescription

                    PreviousOrders = (From e In dataImported.Entries _
                          Join t In dataImported.Traders _
                          On e.TraderID Equals t.TraderID _
                          And e.SnapshotID Equals t.SnapshotID _
                          Join m In dataImported.Mediators _
                          On e.MediatorID Equals m.MediatorID _
                          And e.SnapshotID Equals m.SnapshotID _
                          Where e.SnapshotID.Equals(PreviousSnap.ID) _
                          And m.MediatorDescription = CurrentMediator _
                          And e.IsOrder = True _
                          And e.InOrderDateLimit = True _
                          Select t.TraderCode, t.TraderDescription, m.MediatorDescription, e.EntryDate, e.OrderDate, e.EntryFinalValue, e.IsOrder).Distinct

                    If JustAlert Then
                        If Convert.ToDecimal(CurrentOrders.Where(Function(p) p.MediatorDescription = CurrentMediator).Sum(Function(f) f.EntryFinalValue)) _
                        < Convert.ToDecimal(PreviousOrders.Sum(Function(f) f.EntryFinalValue)) Then
                            TriggerString &= String.Format("Ο τζίρος του πωλητή {0} μειώθηκε από {1} σε {2}", CurrentMediator, _
                                                          PreviousOrders.Sum(Function(f) f.EntryFinalValue).ToString, _
                                                          CurrentOrders.Sum(Function(f) f.EntryFinalValue).ToString) & "<br/>"
                        End If
                    Else
                        DataList.Add(New List(Of String))
                        DataList.Add(New List(Of String))


                        Dim Total1 As New List(Of String)
                        Total1.Add(CurrentMediator)
                        Total1.Add("ΤΡΕΧΟΥΣΑ ΠΕΡΙΟΔΟΣ")
                        Total1.Add("ΣΥΝΟΛΟ ΤΖΙΡΟΥ")
                        Total1.Add(Convert.ToDecimal(CurrentOrders.Where(Function(p) p.MediatorDescription = CurrentMediator).Sum(Function(f) f.EntryFinalValue)).ToString("#.00"))
                        DataList.Add(Total1)

                        Dim HeaderRow As New List(Of String)
                        HeaderRow.Add("ΚΩΔΙΚΟΣ ΠΕΛΑΤΗ")
                        HeaderRow.Add("ΕΠΩΝΥΜΙΑ ΠΕΛΑΤΗ")
                        HeaderRow.Add("ΗΜΕΡΟΜΗΝΙΑ")
                        HeaderRow.Add("ΠΩΛΗΣΗ")
                        DataList.Add(HeaderRow)
                    End If
                End If
                Dim RowList As New List(Of String)
                RowList.Add(ord.TraderCode)
                RowList.Add(ord.TraderDescription)
                RowList.Add(ord.EntryDate.ToShortDateString)
                RowList.Add(Convert.ToDecimal(ord.EntryFinalValue).ToString("#.00"))

                DataList.Add(RowList)

            Next

            If CurrentMediator <> "" AndAlso PreviousOrders IsNot Nothing Then
                DataList.Add(New List(Of String))

                Dim Total2 As New List(Of String)
                Total2.Add(CurrentMediator)
                Total2.Add("ΠΡΟΗΓΟΥΜΕΝΗ ΠΕΡΙΟΔΟΣ")
                Total2.Add("ΣΥΝΟΛΟ ΤΖΙΡΟΥ")
                Total2.Add(Convert.ToDecimal(PreviousOrders.Sum(Function(f) f.EntryFinalValue)).ToString("#.00"))
                DataList.Add(Total2)

                Dim HeaderRow As New List(Of String)
                HeaderRow.Add("ΚΩΔΙΚΟΣ ΠΕΛΑΤΗ")
                HeaderRow.Add("ΕΠΩΝΥΜΙΑ ΠΕΛΑΤΗ")
                HeaderRow.Add("ΗΜΕΡΟΜΗΝΙΑ")
                HeaderRow.Add("ΠΩΛΗΣΗ")
                DataList.Add(HeaderRow)

                For Each pord In PreviousOrders.OrderBy(Function(f) f.OrderDate).OrderBy(Function(p) p.MediatorDescription)
                    Dim pRowList As New List(Of String)
                    pRowList.Add(pord.TraderCode)
                    pRowList.Add(pord.TraderDescription)
                    pRowList.Add(pord.EntryDate.ToShortDateString)
                    pRowList.Add(Convert.ToDecimal(pord.EntryFinalValue).ToString("#.00"))

                    DataList.Add(pRowList)
                Next
            End If

            If JustAlert Then
                LineList.Add(TriggerString)
                DataList.Add(LineList)
            End If
        Catch ex As Exception
            SvSys.ErrorLog("ExporterData.MediatorOrdersCompare".PadRight(10) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
        End Try
        Return DataList
    End Function

    Private Function Stock() As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))
        Try
            Dim AllStock = (From p In dataImported.Products _
                            Where p.SnapshotID.Equals(SnapID) _
                            Select p.ProductID, p.ProductCode, p.ProductDescription, p.StockQuantity, p.Price).Distinct

            If JustAlert Then
                Dim LineList As New List(Of String)
                Dim TriggerString As String = ""

                Dim TotalProducts As Integer = 0

                For Each stk In AllStock
                    Dim TotalStock As Decimal = Convert.ToDecimal(stk.StockQuantity * stk.Price)

                    If TotalStock > GetString("StockLowerLimit") OrElse TotalStock < GetString("StockUpperLimit") Then
                        TotalProducts += 1
                    End If
                Next

                If TotalProducts > 0 Then
                    TriggerString = String.Format("Υπάρχουν {0} είδη με αξία stock ανάμεσα σε {1} και {2}", _
                                                  TotalProducts, GetString("StockLowerLimit"), GetString("StockUpperLimit"))
                End If

                LineList.Add(TriggerString)
                DataList.Add(LineList)
            Else
                Dim Total As New List(Of String)
                Total.Add("ΑΞΙΑ STOCK")
                Total.Add(Convert.ToDecimal(AllStock.Sum(Function(f) f.StockQuantity * f.Price)).ToString("#.00"))
                DataList.Add(Total)

                DataList.Add(New List(Of String))

                Dim HeaderRow As New List(Of String)
                HeaderRow.Add("ΚΩΔΙΚΟΣ ΕΙΔΟΥΣ")
                HeaderRow.Add("ΕΙΔΟΣ")
                HeaderRow.Add("ΠΟΣΟΤΗΤΑ")
                HeaderRow.Add("ΑΞΙΑ STOCK")
                DataList.Add(HeaderRow)

                For Each stk In AllStock.OrderBy(Function(f) f.ProductDescription)
                    Dim RowList As New List(Of String)
                    RowList.Add(stk.ProductCode)
                    RowList.Add(stk.ProductDescription)
                    RowList.Add(stk.StockQuantity)
                    RowList.Add(Convert.ToDecimal(stk.StockQuantity * stk.Price).ToString("#.00"))

                    DataList.Add(RowList)
                Next
            End If
        Catch ex As Exception
            SvSys.ErrorLog("ExporterData.Stock".PadRight(10) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
        End Try
        Return DataList
    End Function

    Private Function SalesBuys() As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))
        Try
            Dim AllOrders = (From e In dataImported.Entries _
                          Join t In dataImported.Traders _
                          On e.TraderID Equals t.TraderID _
                          And e.SnapshotID Equals t.SnapshotID _
                          Where e.SnapshotID.Equals(SnapID) _
                          And e.IsOrder = True _
                          Select e.EntryID, t.TraderCode, t.TraderDescription, e.EntryDate, e.EntryFinalValue, e.IsSale).Distinct

            If JustAlert Then
                Dim LineList As New List(Of String)
                Dim TriggerString As String = ""

                Dim SalesBuysAmount As Decimal = Convert.ToDecimal(AllOrders.Where(Function(p) p.IsSale = True).Sum(Function(f) f.EntryFinalValue)) _
                - Convert.ToDecimal(AllOrders.Where(Function(p) p.IsSale = False).Sum(Function(f) f.EntryFinalValue))

                If SalesBuysAmount < GetString("SalesBuysLimit") Then
                    TriggerString = String.Format("Πωλήσεις - Αγορές = {0}", _
                                                  SalesBuysAmount)
                End If

                LineList.Add(TriggerString)
                DataList.Add(LineList)
            Else
                Dim Total1 As New List(Of String)
                Total1.Add("ΠΩΛΗΣΕΙΣ")
                Total1.Add(Convert.ToDecimal(AllOrders.Where(Function(p) p.IsSale = True).Sum(Function(f) f.EntryFinalValue)).ToString("#.00"))
                DataList.Add(Total1)

                Dim Total2 As New List(Of String)
                Total2.Add("ΑΓΟΡΕΣ")
                Total2.Add(Convert.ToDecimal(AllOrders.Where(Function(p) p.IsSale = False).Sum(Function(f) f.EntryFinalValue)).ToString("#.00"))
                DataList.Add(Total2)


                DataList.Add(New List(Of String))

                Dim HeaderRow As New List(Of String)
                HeaderRow.Add("")
                HeaderRow.Add("ΚΩΔΙΚΟΣ ΠΕΛΑΤΗ")
                HeaderRow.Add("ΕΠΩΝΥΜΙΑ ΠΕΛΑΤΗ")
                HeaderRow.Add("ΗΜΕΡΟΜΗΝΙΑ")
                HeaderRow.Add("ΠΩΛΗΣΗ")
                DataList.Add(HeaderRow)

                For Each ord In AllOrders.OrderBy(Function(f) f.EntryDate)
                    Dim RowList As New List(Of String)
                    RowList.Add(If(ord.IsSale, "ΠΩΛΗΣΗ", "ΑΓΟΡΑ"))
                    RowList.Add(ord.TraderCode)
                    RowList.Add(ord.TraderDescription)
                    RowList.Add(ord.EntryDate.ToShortDateString)
                    RowList.Add(Convert.ToDecimal(ord.EntryFinalValue).ToString("#.00"))

                    DataList.Add(RowList)
                Next
            End If
        Catch ex As Exception
            SvSys.ErrorLog("ExporterData.SalesBuys".PadRight(10) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
        End Try
        Return DataList
    End Function

    Private Function Sales() As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))
        Try
            Dim AllOrders = (From e In dataImported.Entries _
                          Join t In dataImported.Traders _
                          On e.TraderID Equals t.TraderID _
                          And e.SnapshotID Equals t.SnapshotID _
                          Where e.SnapshotID.Equals(SnapID) _
                          And e.IsOrder = True _
                          And e.OrderDate IsNot Nothing _
                          And e.InOrderDateLimit = True _
                          Select e.EntryID, t.TraderCode, t.TraderDescription, e.EntryDate, e.OrderDate, e.EntryFinalValue).Distinct

            If JustAlert Then
                Dim LineList As New List(Of String)
                Dim TriggerString As String = ""

                Dim TotalSales As Decimal = Convert.ToDecimal(AllOrders.Sum(Function(f) f.EntryFinalValue))

                If TotalSales > GetString("SalesLowerLimit") OrElse TotalSales < GetString("SalesUpperLimit") Then
                    TriggerString = String.Format("Πωλήσεις : {0}", _
                                                  TotalSales)
                End If

                LineList.Add(TriggerString)
                DataList.Add(LineList)
            Else
                Dim Total As New List(Of String)
                Total.Add("ΠΩΛΗΣΕΙΣ")
                Total.Add(Convert.ToDecimal(AllOrders.Sum(Function(f) f.EntryFinalValue)).ToString("#.00"))
                DataList.Add(Total)


                DataList.Add(New List(Of String))

                Dim HeaderRow As New List(Of String)
                HeaderRow.Add("ΚΩΔΙΚΟΣ ΠΕΛΑΤΗ")
                HeaderRow.Add("ΕΠΩΝΥΜΙΑ ΠΕΛΑΤΗ")
                HeaderRow.Add("ΗΜΕΡΟΜΗΝΙΑ")
                HeaderRow.Add("ΗΜΕΡΟΜΗΝΙΑ ΠΑΡΑΓΓΕΛΙΑΣ")
                HeaderRow.Add("ΠΩΛΗΣΗ")
                DataList.Add(HeaderRow)

                For Each ord In AllOrders.OrderBy(Function(f) f.OrderDate)
                    Dim RowList As New List(Of String)
                    RowList.Add(ord.TraderCode)
                    RowList.Add(ord.TraderDescription)
                    RowList.Add(ord.EntryDate.ToShortDateString)
                    RowList.Add(CDate(ord.OrderDate).ToShortDateString)
                    RowList.Add(Convert.ToDecimal(ord.EntryFinalValue).ToString("#.00"))

                    DataList.Add(RowList)
                Next
            End If
        Catch ex As Exception
            SvSys.ErrorLog("ExporterData.Sales".PadRight(10) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
        End Try
        Return DataList
    End Function

    Private Function Transit() As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))
        Try
            Dim AllOrders = (From e In dataImported.Entries _
                          Join t In dataImported.Traders _
                          On e.TraderID Equals t.TraderID _
                          And e.SnapshotID Equals t.SnapshotID _
                          Where e.SnapshotID.Equals(SnapID) _
                          And e.IsOrder = True _
                          And e.IsDelivered = False _
                          Select e.EntryID, t.TraderCode, t.TraderDescription, e.EntryDate, e.EntryFinalValue).Distinct

            If JustAlert Then
                Dim LineList As New List(Of String)
                Dim TriggerString As String = ""

                Dim TotalTransit As Decimal = Convert.ToDecimal(AllOrders.Sum(Function(f) f.EntryFinalValue))

                If TotalTransit > GetString("SalesLowerLimit") Then
                    TriggerString = String.Format("Παραγγελίες transit : {0}", _
                                                  TotalTransit)
                End If

                LineList.Add(TriggerString)
                DataList.Add(LineList)
            Else
                Dim Total As New List(Of String)
                Total.Add("ΑΞΙΑ")
                Total.Add(Convert.ToDecimal(AllOrders.Sum(Function(f) f.EntryFinalValue)).ToString("#.00"))
                DataList.Add(Total)


                DataList.Add(New List(Of String))

                Dim HeaderRow As New List(Of String)
                HeaderRow.Add("ΚΩΔΙΚΟΣ ΠΕΛΑΤΗ")
                HeaderRow.Add("ΕΠΩΝΥΜΙΑ ΠΕΛΑΤΗ")
                HeaderRow.Add("ΗΜΕΡΟΜΗΝΙΑ")
                HeaderRow.Add("ΠΩΛΗΣΗ")
                DataList.Add(HeaderRow)

                For Each ord In AllOrders.OrderBy(Function(f) f.EntryDate)
                    Dim RowList As New List(Of String)
                    RowList.Add(ord.TraderCode)
                    RowList.Add(ord.TraderDescription)
                    RowList.Add(ord.EntryDate.ToShortDateString)
                    RowList.Add(Convert.ToDecimal(ord.EntryFinalValue).ToString("#.00"))

                    DataList.Add(RowList)
                Next
            End If
        Catch ex As Exception
            SvSys.ErrorLog("ExporterData.Transit".PadRight(10) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
        End Try
        Return DataList
    End Function

    Private Function TradersRest() As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))
        Try
            Dim AllRests = (From t In dataImported.Traders _
                         Where t.SnapshotID.Equals(SnapID) _
                         And t.TraderDescription <> "" _
                         And t.Rest > 0 _
                         Select t.TraderID, t.TraderCode, t.TraderDescription, t.Charge, t.Credit, t.Rest).Distinct

            If JustAlert Then
                Dim LineList As New List(Of String)
                Dim TriggerString As String = ""

                Dim TotalTraders As Integer = 0 'AllTasks.Count

                For Each rst In AllRests
                    If rst.Rest > GetString("TradersRestLimit") Then
                        TotalTraders += 1
                    End If
                Next

                If TotalTraders > 0 Then
                    TriggerString = String.Format("Υπάρχουν {0} πελάτες με υπόλοιπο πάνω από {1}", _
                                                  TotalTraders, GetString("TradersRestLimit"))
                End If
                LineList.Add(TriggerString)
                DataList.Add(LineList)
            Else
                Dim HeaderRow As New List(Of String)
                HeaderRow.Add("ΚΩΔΙΚΟΣ ΠΕΛΑΤΗ")
                HeaderRow.Add("ΕΠΩΝΥΜΙΑ ΠΕΛΑΤΗ")
                HeaderRow.Add("ΧΡΕΩΣΗ")
                HeaderRow.Add("ΠΙΣΤΩΣΗ")
                HeaderRow.Add("ΥΠΟΛΟΙΠΟ")
                DataList.Add(HeaderRow)

                For Each rest In AllRests.OrderBy(Function(f) f.TraderDescription)
                    Dim RowList As New List(Of String)
                    RowList.Add(rest.TraderCode)
                    RowList.Add(rest.TraderDescription)
                    RowList.Add(rest.Charge)
                    RowList.Add(rest.Credit)
                    RowList.Add(rest.Rest)
                    DataList.Add(RowList)
                Next
            End If
        Catch ex As Exception
            SvSys.ErrorLog("ExporterData.TradersRest".PadRight(10) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
        End Try
        Return DataList

    End Function
#End Region

#Region "Services"
    Private Function CustomersInContract() As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))
        Try
            Dim AllCustomers = (From t In dataImported.Traders _
                                Where t.SnapshotID.Equals(SnapID) _
                                And t.TraderDescription <> "" _
                                And t.InContract = True _
                                Select t.TraderID, t.TraderCode, t.TraderDescription, t.Charge, t.Credit, t.Rest).Distinct

            If JustAlert Then
                Dim LineList As New List(Of String)
                Dim TriggerString As String = ""

                If AllCustomers.Count < GetString("CustomerContractLimit") Then
                    TriggerString = String.Format("Οι ενεργοί πελάτες σε σύμβαση είναι {0}", AllCustomers.Count.ToString)
                End If
                LineList.Add(TriggerString)
                DataList.Add(LineList)
            Else
                Dim HeaderRow As New List(Of String)
                HeaderRow.Add("ΚΩΔΙΚΟΣ ΠΕΛΑΤΗ")
                HeaderRow.Add("ΕΠΩΝΥΜΙΑ ΠΕΛΑΤΗ")
                HeaderRow.Add("ΧΡΕΩΣΗ")
                HeaderRow.Add("ΠΙΣΤΩΣΗ")
                HeaderRow.Add("ΥΠΟΛΟΙΠΟ")
                DataList.Add(HeaderRow)

                For Each cus In AllCustomers.OrderBy(Function(f) f.TraderDescription)
                    Dim RowList As New List(Of String)
                    RowList.Add(cus.TraderCode)
                    RowList.Add(cus.TraderDescription)
                    RowList.Add(cus.Charge)
                    RowList.Add(cus.Credit)
                    RowList.Add(cus.Rest)
                    DataList.Add(RowList)
                Next
            End If
        Catch ex As Exception
            SvSys.ErrorLog("ExporterData.CustomersInContract".PadRight(10) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
        End Try
        Return DataList
    End Function

    Private Function CustomersInContractCompare(ByVal CurrentSnap As Snapshot, ByVal PreviousSnap As Snapshot) As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))
        Try
            Dim CurrentCustomers = (From t In dataImported.Traders _
                                    Where t.SnapshotID.Equals(CurrentSnap.ID) _
                                    And t.TraderDescription <> "" _
                                    And t.InContract = True _
                                    Select t.TraderID, t.TraderCode, t.TraderDescription, t.Charge, t.Credit, t.Rest).Distinct

            Dim PreviousCustomers = (From t In dataImported.Traders _
                                     Where t.SnapshotID.Equals(PreviousSnap.ID) _
                                     And t.TraderDescription <> "" _
                                     And t.InContract = True _
                                     Select t.TraderID, t.TraderCode, t.TraderDescription, t.Charge, t.Credit, t.Rest).Distinct

            If JustAlert Then
                Dim LineList As New List(Of String)
                Dim TriggerString As String = ""

                If CurrentCustomers.Count < PreviousCustomers.Count Then
                    TriggerString = String.Format("Οι ενεργοί πελάτες σε σύμβαση έχουν μειωθεί από {0} σε {1}", _
                                                  PreviousCustomers.Count, CurrentCustomers.Count)
                End If

                LineList.Add(TriggerString)
                DataList.Add(LineList)
            Else
                Dim Total1 As New List(Of String)
                Total1.Add(CurrentSnap.ExecuteDate.Month & "/" & CurrentSnap.ExecuteDate.Year)
                Total1.Add(CurrentCustomers.Count)
                DataList.Add(Total1)

                Dim Total2 As New List(Of String)
                Total2.Add(PreviousSnap.ExecuteDate.Month & "/" & PreviousSnap.ExecuteDate.Year)
                Total2.Add(PreviousCustomers.Count)
                DataList.Add(Total2)

                DataList.Add(New List(Of String))

                Dim HeaderRow As New List(Of String)
                HeaderRow.Add("ΚΩΔΙΚΟΣ ΠΕΛΑΤΗ")
                HeaderRow.Add("ΕΠΩΝΥΜΙΑ ΠΕΛΑΤΗ")
                HeaderRow.Add("ΧΡΕΩΣΗ")
                HeaderRow.Add("ΠΙΣΤΩΣΗ")
                HeaderRow.Add("ΥΠΟΛΟΙΠΟ")
                DataList.Add(HeaderRow)

                For Each cus In CurrentCustomers.OrderBy(Function(f) f.TraderDescription)
                    Dim RowList As New List(Of String)
                    RowList.Add(cus.TraderCode)
                    RowList.Add(cus.TraderDescription)
                    RowList.Add(cus.Charge)
                    RowList.Add(cus.Credit)
                    RowList.Add(cus.Rest)
                    DataList.Add(RowList)
                Next

                DataList.Add(New List(Of String))
                DataList.Add(New List(Of String))
                DataList.Add(New List(Of String))

                For Each cus In PreviousCustomers.OrderBy(Function(f) f.TraderDescription)
                    Dim RowList As New List(Of String)
                    RowList.Add(cus.TraderCode)
                    RowList.Add(cus.TraderDescription)
                    RowList.Add(cus.Charge)
                    RowList.Add(cus.Credit)
                    RowList.Add(cus.Rest)
                    DataList.Add(RowList)
                Next
            End If
        Catch ex As Exception
            SvSys.ErrorLog("ExporterData.CustomersInContractCompare".PadRight(10) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
        End Try
        Return DataList
    End Function

    Private Function DailyApps() As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))
        Try
            Dim AllApps = (From a In dataImported.Appointments _
                          Join t In dataImported.Traders _
                          On a.TraderID Equals t.TraderID _
                          And a.SnapshotID Equals t.SnapshotID _
                          Join m In dataImported.Mediators _
                          On a.MediatorID Equals m.MediatorID _
                          And a.SnapshotID Equals m.SnapshotID _
                          Where a.SnapshotID.Equals(SnapID) _
                          And a.IsCanceled = False _
                          And a.IsService = False _
                          Select a.AppointmentID, t.TraderCode, t.TraderDescription, m.MediatorDescription, a.AppointmentDate, a.StartTime, a.EndTime).Distinct

            If JustAlert Then
                Dim LineList As New List(Of String)
                Dim TriggerString As String = ""

                If AllApps.Count < GetString("DailyAppsLimit") Then
                    TriggerString = String.Format("Τα ημερήσια ραντεβού είναι {0}", AllApps.Count.ToString)
                End If
                LineList.Add(TriggerString)
                DataList.Add(LineList)
            Else
                Dim HeaderRow As New List(Of String)
                HeaderRow.Add("ΚΩΔΙΚΟΣ ΠΕΛΑΤΗ")
                HeaderRow.Add("ΕΠΩΝΥΜΙΑ ΠΕΛΑΤΗ")
                HeaderRow.Add("ΥΠΑΛΛΗΛΟΣ")
                HeaderRow.Add("ΗΜΕΡΟΜΗΝΙΑ")
                HeaderRow.Add("ΑΠΟ")
                HeaderRow.Add("ΕΩΣ")
                DataList.Add(HeaderRow)

                For Each app In AllApps.OrderBy(Function(f) f.StartTime)
                    Dim RowList As New List(Of String)
                    RowList.Add(app.TraderCode)
                    RowList.Add(app.TraderDescription)
                    RowList.Add(app.MediatorDescription)
                    RowList.Add(app.AppointmentDate.ToShortDateString)
                    RowList.Add(CDate(app.StartTime).ToShortTimeString)
                    RowList.Add(CDate(app.EndTime).ToShortTimeString)
                    DataList.Add(RowList)
                Next
            End If
        Catch ex As Exception
            SvSys.ErrorLog("ExporterData.DailyApps".PadRight(10) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
        End Try
        Return DataList
    End Function

    Private Function ServicesTurnover() As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))
        Try
            Dim AllCharges = (From a In dataImported.Appointments _
                          Join t In dataImported.Traders _
                          On a.TraderID Equals t.TraderID _
                          And a.SnapshotID Equals t.SnapshotID _
                          Join m In dataImported.Mediators _
                          On a.MediatorID Equals m.MediatorID _
                          And a.SnapshotID Equals m.SnapshotID _
                          Where a.SnapshotID.Equals(SnapID) _
                          And a.IsCanceled = False _
                          And a.IsService = True _
                          Select a.AppointmentID, t.TraderCode, t.TraderDescription, m.MediatorDescription, a.AppointmentDate, a.AppointmentValue).Distinct

            If JustAlert Then
                Dim LineList As New List(Of String)
                Dim TriggerString As String = ""

                If AllCharges.Sum(Function(f) f.AppointmentValue) < GetString("ServicesTurnoverLimit") Then
                    TriggerString = String.Format("Ο τζίρος από υπηρεσίες είναι {0}", AllCharges.Sum(Function(f) f.AppointmentValue).ToString)
                End If
                LineList.Add(TriggerString)
                DataList.Add(LineList)
            Else
                Dim HeaderRow As New List(Of String)
                HeaderRow.Add("ΚΩΔΙΚΟΣ ΠΕΛΑΤΗ")
                HeaderRow.Add("ΕΠΩΝΥΜΙΑ ΠΕΛΑΤΗ")
                HeaderRow.Add("ΥΠΑΛΛΗΛΟΣ")
                HeaderRow.Add("ΗΜΕΡΟΜΗΝΙΑ")
                HeaderRow.Add("ΧΡΕΩΣΗ")
                DataList.Add(HeaderRow)

                For Each chrg In AllCharges.OrderBy(Function(f) f.AppointmentDate)
                    Dim RowList As New List(Of String)
                    RowList.Add(chrg.TraderCode)
                    RowList.Add(chrg.TraderDescription)
                    RowList.Add(chrg.MediatorDescription)
                    RowList.Add(chrg.AppointmentDate.ToShortDateString)
                    RowList.Add(chrg.AppointmentValue.ToString("#.00"))
                    DataList.Add(RowList)
                Next
            End If
        Catch ex As Exception
            SvSys.ErrorLog("ExporterData.ServicesTurnover".PadRight(10) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
        End Try
        Return DataList

    End Function

    Private Function ServicesTurnoverCompare(ByVal CurrentSnap As Snapshot, ByVal PreviousSnap As Snapshot) As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))
        Try
            Dim CurrentCharges = (From a In dataImported.Appointments _
                          Join t In dataImported.Traders _
                          On a.TraderID Equals t.TraderID _
                          And a.SnapshotID Equals t.SnapshotID _
                          Join m In dataImported.Mediators _
                          On a.MediatorID Equals m.MediatorID _
                          And a.SnapshotID Equals m.SnapshotID _
                          Where a.SnapshotID.Equals(CurrentSnap.ID) _
                          And a.IsCanceled = False _
                          And a.IsService = True _
                          Select a.AppointmentID, t.TraderCode, t.TraderDescription, m.MediatorDescription, a.AppointmentDate, a.AppointmentValue).Distinct

            Dim PreviousCharges = (From a In dataImported.Appointments _
                          Join t In dataImported.Traders _
                          On a.TraderID Equals t.TraderID _
                          And a.SnapshotID Equals t.SnapshotID _
                          Join m In dataImported.Mediators _
                          On a.MediatorID Equals m.MediatorID _
                          And a.SnapshotID Equals m.SnapshotID _
                          Where a.SnapshotID.Equals(PreviousSnap.ID) _
                          And a.IsCanceled = False _
                          And a.IsService = True _
                          Select a.AppointmentID, t.TraderCode, t.TraderDescription, m.MediatorDescription, a.AppointmentDate, a.AppointmentValue).Distinct

            If JustAlert Then
                Dim LineList As New List(Of String)
                Dim TriggerString As String = ""

                If CurrentCharges.Sum(Function(f) f.AppointmentValue) < PreviousCharges.Sum(Function(f) f.AppointmentValue) Then
                    TriggerString = String.Format("Ο τζίρος από υπηρεσίες έχει μειωθεί από {0} σε {1}", _
                                                  PreviousCharges.Sum(Function(f) f.AppointmentValue).ToString, _
                                                  CurrentCharges.Sum(Function(f) f.AppointmentValue).ToString)
                End If
                LineList.Add(TriggerString)
                DataList.Add(LineList)
            Else
                Dim Total1 As New List(Of String)
                Total1.Add(CurrentSnap.ExecuteDate.Month & "/" & CurrentSnap.ExecuteDate.Year)
                If CurrentCharges.Count = 0 Then
                    Total1.Add("0")
                Else
                    Total1.Add(CurrentCharges.Sum(Function(f) f.AppointmentValue).ToString("#.00"))
                End If
                DataList.Add(Total1)

                Dim Total2 As New List(Of String)
                Total2.Add(PreviousSnap.ExecuteDate.Month & "/" & PreviousSnap.ExecuteDate.Year)
                If PreviousCharges.Count = 0 Then
                    Total2.Add("0")
                Else
                    Total2.Add(PreviousCharges.Sum(Function(f) f.AppointmentValue).ToString("#.00"))
                End If
                DataList.Add(Total2)

                DataList.Add(New List(Of String))

                Dim HeaderRow As New List(Of String)
                HeaderRow.Add("ΚΩΔΙΚΟΣ ΠΕΛΑΤΗ")
                HeaderRow.Add("ΕΠΩΝΥΜΙΑ ΠΕΛΑΤΗ")
                HeaderRow.Add("ΥΠΑΛΛΗΛΟΣ")
                HeaderRow.Add("ΗΜΕΡΟΜΗΝΙΑ")
                HeaderRow.Add("ΧΡΕΩΣΗ")
                DataList.Add(HeaderRow)

                For Each chrg In CurrentCharges.OrderBy(Function(f) f.AppointmentDate)
                    Dim RowList As New List(Of String)
                    RowList.Add(chrg.TraderCode)
                    RowList.Add(chrg.TraderDescription)
                    RowList.Add(chrg.MediatorDescription)
                    RowList.Add(chrg.AppointmentDate.ToShortDateString)
                    RowList.Add(chrg.AppointmentValue.ToString("#.00"))
                    DataList.Add(RowList)
                Next

                DataList.Add(New List(Of String))
                DataList.Add(New List(Of String))
                DataList.Add(New List(Of String))

                For Each chrg In PreviousCharges.OrderBy(Function(f) f.AppointmentDate)
                    Dim RowList As New List(Of String)
                    RowList.Add(chrg.TraderCode)
                    RowList.Add(chrg.TraderDescription)
                    RowList.Add(chrg.MediatorDescription)
                    RowList.Add(chrg.AppointmentDate.ToShortDateString)
                    RowList.Add(chrg.AppointmentValue.ToString("#.00"))
                    DataList.Add(RowList)
                Next
            End If
        Catch ex As Exception
            SvSys.ErrorLog("ExporterData.ServiceTurnoverCompare".PadRight(10) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
        End Try
        Return DataList
    End Function

    Private Function TechHours() As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))
        Try
            Dim AllHours = (From a In dataImported.Appointments _
                          Join t In dataImported.Traders _
                          On a.TraderID Equals t.TraderID _
                          And a.SnapshotID Equals t.SnapshotID _
                          Join m In dataImported.Mediators _
                          On a.MediatorID Equals m.MediatorID _
                          And a.SnapshotID Equals m.SnapshotID _
                          Where a.SnapshotID.Equals(SnapID) _
                          And a.IsCanceled = False _
                          And a.IsService = True _
                          Select a.AppointmentID, t.TraderCode, t.TraderDescription, m.MediatorDescription, a.AppointmentDate, a.StartTime, a.EndTime, a.CalcHours).Distinct

            If JustAlert Then
                Dim LineList As New List(Of String)
                Dim TriggerString As String = ""


                Dim TechHours1 As Decimal = 0
                For Each row In AllHours
                    TechHours1 += row.CalcHours
                Next

                If TechHours1 < GetString("TechHoursLimit") Then
                    TriggerString = String.Format("Οι ώρες των τεχνικών είναι {0}", TechHours1.ToString)
                End If
                LineList.Add(TriggerString)
                DataList.Add(LineList)
            Else
                Dim HeaderRow As New List(Of String)
                HeaderRow.Add("ΚΩΔΙΚΟΣ ΠΕΛΑΤΗ")
                HeaderRow.Add("ΕΠΩΝΥΜΙΑ ΠΕΛΑΤΗ")
                HeaderRow.Add("ΥΠΑΛΛΗΛΟΣ")
                HeaderRow.Add("ΗΜΕΡΟΜΗΝΙΑ")
                HeaderRow.Add("ΩΡΕΣ")
                DataList.Add(HeaderRow)

                For Each hrs In AllHours.OrderBy(Function(f) f.AppointmentDate)
                    Dim RowList As New List(Of String)
                    RowList.Add(hrs.TraderCode)
                    RowList.Add(hrs.TraderDescription)
                    RowList.Add(hrs.MediatorDescription)
                    RowList.Add(hrs.AppointmentDate.ToShortDateString)
                    RowList.Add(hrs.CalcHours.ToString("#.00"))

                    DataList.Add(RowList)
                Next
            End If
        Catch ex As Exception
            SvSys.ErrorLog("ExporterData.TechHours".PadRight(10) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
        End Try
        Return DataList
    End Function

    Private Function TechHoursCompare(ByVal CurrentSnap As Snapshot, ByVal PreviousSnap As Snapshot) As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))
        Try
            Dim CurrentHours = (From a In dataImported.Appointments _
                          Join t In dataImported.Traders _
                          On a.TraderID Equals t.TraderID _
                          And a.SnapshotID Equals t.SnapshotID _
                          Join m In dataImported.Mediators _
                          On a.MediatorID Equals m.MediatorID _
                          And a.SnapshotID Equals m.SnapshotID _
                          Where a.SnapshotID.Equals(CurrentSnap.ID) _
                          And a.IsCanceled = False _
                          And a.IsService = True _
                          Select a.AppointmentID, t.TraderCode, t.TraderDescription, m.MediatorDescription, a.AppointmentDate, a.StartTime, a.EndTime, a.CalcHours).Distinct

            Dim PreviousHours = (From a In dataImported.Appointments _
                          Join t In dataImported.Traders _
                          On a.TraderID Equals t.TraderID _
                          And a.SnapshotID Equals t.SnapshotID _
                          Join m In dataImported.Mediators _
                          On a.MediatorID Equals m.MediatorID _
                          And a.SnapshotID Equals m.SnapshotID _
                          Where a.SnapshotID.Equals(PreviousSnap.ID) _
                          And a.IsCanceled = False _
                          And a.IsService = True _
                          Select a.AppointmentID, t.TraderCode, t.TraderDescription, m.MediatorDescription, a.AppointmentDate, a.StartTime, a.EndTime, a.CalcHours).Distinct

            Dim TechHours1 As Decimal = 0
            For Each row In CurrentHours
                TechHours1 += row.CalcHours
            Next

            Dim TechHours2 As Decimal = 0
            For Each row In PreviousHours
                TechHours2 += row.CalcHours
            Next

            If JustAlert Then
                Dim LineList As New List(Of String)
                Dim TriggerString As String = ""

                If TechHours1 < TechHours2 Then
                    TriggerString = String.Format("Οι ώρες των τεχνικών μειώθηκαν από {0} σε {1}", _
                                                  TechHours2.ToString, _
                                                  TechHours1.ToString)
                End If
                LineList.Add(TriggerString)
                DataList.Add(LineList)
            Else
                Dim Total1 As New List(Of String)
                Total1.Add(CurrentSnap.ExecuteDate.Month & "/" & CurrentSnap.ExecuteDate.Year)
                If CurrentHours.Count = 0 Then
                    Total1.Add("0")
                Else
                    Total1.Add(TechHours1.ToString("#.00"))
                End If
                DataList.Add(Total1)

                Dim Total2 As New List(Of String)
                Total2.Add(PreviousSnap.ExecuteDate.Month & "/" & PreviousSnap.ExecuteDate.Year)
                If PreviousHours.Count = 0 Then
                    Total2.Add("0")
                Else
                    Total2.Add(TechHours2.ToString("#.00"))
                End If
                DataList.Add(Total2)

                DataList.Add(New List(Of String))


                Dim HeaderRow As New List(Of String)
                HeaderRow.Add("ΚΩΔΙΚΟΣ ΠΕΛΑΤΗ")
                HeaderRow.Add("ΕΠΩΝΥΜΙΑ ΠΕΛΑΤΗ")
                HeaderRow.Add("ΥΠΑΛΛΗΛΟΣ")
                HeaderRow.Add("ΗΜΕΡΟΜΗΝΙΑ")
                HeaderRow.Add("ΩΡΕΣ")
                DataList.Add(HeaderRow)


                For Each hrs In CurrentHours.OrderBy(Function(f) f.AppointmentDate)
                    Dim RowList As New List(Of String)
                    RowList.Add(hrs.TraderCode)
                    RowList.Add(hrs.TraderDescription)
                    RowList.Add(hrs.MediatorDescription)
                    RowList.Add(hrs.AppointmentDate.ToShortDateString)
                    RowList.Add(hrs.CalcHours.ToString("#.00"))

                    DataList.Add(RowList)
                Next

                DataList.Add(New List(Of String))
                DataList.Add(New List(Of String))
                DataList.Add(New List(Of String))

                For Each hrs In PreviousHours.OrderBy(Function(f) f.AppointmentDate)
                    Dim RowList As New List(Of String)
                    RowList.Add(hrs.TraderCode)
                    RowList.Add(hrs.TraderDescription)
                    RowList.Add(hrs.MediatorDescription)
                    RowList.Add(hrs.AppointmentDate.ToShortDateString)
                    RowList.Add(hrs.CalcHours.ToString("#.00"))

                    DataList.Add(RowList)
                Next
            End If
        Catch ex As Exception
            SvSys.ErrorLog("ExporterData.TechHoursCompare".PadRight(10) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
        End Try
        Return DataList
    End Function

    ''  pk
    ''  Check only offers which were created during the selected period (InOfferDateLimit = True)
    Private Function Offers() As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))
        Try
            Dim AllOffers = (From e In dataImported.Entries _
                          Join t In dataImported.Traders _
                          On e.TraderID Equals t.TraderID _
                          And e.SnapshotID Equals t.SnapshotID _
                          Join m In dataImported.Mediators _
                          On e.MediatorID Equals m.MediatorID _
                          And e.SnapshotID Equals m.SnapshotID _
                          Where e.SnapshotID.Equals(SnapID) _
                          And e.AfterOffer = True _
                          And e.InOfferDateLimit = True _
                          Select e.EntryID, t.TraderCode, t.TraderDescription, m.MediatorDescription, e.EntryDate, e.OrderDate, e.EntryFinalValue, e.IsOrder).Distinct

            If JustAlert Then
                Dim LineList As New List(Of String)
                Dim TriggerString As String = ""

                Dim OpenOffersPerc As Decimal

                If AllOffers.Count > 0 Then
                    Dim OpenOffers = AllOffers.Where(Function(f) f.IsOrder = False).Count

                    OpenOffersPerc = Convert.ToDecimal((OpenOffers / AllOffers.Count) * 100)

                    TriggerString = String.Format("Το ποσοστό των ανοικτών προσφορών είναι {0}", OpenOffersPerc.ToString("#.00"))
                End If
                LineList.Add(TriggerString)
                DataList.Add(LineList)
            Else
                Dim HeaderRow As New List(Of String)
                HeaderRow.Add("ΚΩΔΙΚΟΣ ΠΕΛΑΤΗ")
                HeaderRow.Add("ΕΠΩΝΥΜΙΑ ΠΕΛΑΤΗ")
                HeaderRow.Add("ΠΩΛΗΤΗΣ")
                HeaderRow.Add("ΗΜΕΡΟΜΗΝΙΑ")
                HeaderRow.Add("ΗΜΕΡΟΜΗΝΙΑ ΑΠΟΔΟΧΗΣ")
                HeaderRow.Add("ΑΞΙΑ")
                HeaderRow.Add("ΚΑΤΑΣΤΑΣΗ")
                DataList.Add(HeaderRow)

                For Each ofr In AllOffers.OrderBy(Function(f) f.EntryDate)
                    Dim RowList As New List(Of String)
                    RowList.Add(ofr.TraderCode)
                    RowList.Add(ofr.TraderDescription)
                    RowList.Add(ofr.MediatorDescription)
                    RowList.Add(ofr.EntryDate.ToShortDateString)
                    RowList.Add(If(ofr.OrderDate Is Nothing, "", CDate(ofr.OrderDate).ToShortDateString))
                    RowList.Add(Convert.ToDecimal(ofr.EntryFinalValue).ToString("#.00"))
                    If ofr.IsOrder Then
                        RowList.Add("ΑΠΟΔΟΧΗ")
                    Else
                        RowList.Add("ΕΚΚΡΕΜΕΙ")
                    End If
                    DataList.Add(RowList)
                Next

                If AllOffers.Count > 0 Then

                    DataList.Add(New List(Of String))

                    Dim ApprovedOffers = AllOffers.Where(Function(f) f.IsOrder = True).Count

                    Dim Total1 As New List(Of String)
                    Total1.Add("ΑΠΟΔΟΧΗ")
                    Total1.Add(ApprovedOffers.ToString("#"))
                    Total1.Add(Convert.ToDecimal((ApprovedOffers / AllOffers.Count) * 100).ToString("#.00") & "%")

                    DataList.Add(Total1)

                    Dim Total2 As New List(Of String)
                    Total2.Add("ΑΝΟΙΚΤΕΣ")
                    Total2.Add((AllOffers.Count - ApprovedOffers).ToString("#"))
                    Total2.Add(Convert.ToDecimal(((AllOffers.Count - ApprovedOffers) / AllOffers.Count) * 100).ToString("#.00") & "%")

                    DataList.Add(Total2)
                End If
            End If
        Catch ex As Exception
            SvSys.ErrorLog("ExporterData.Offers".PadRight(10) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
        End Try
        Return DataList

    End Function

    Private Function AppOrders() As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))
        Try
            Dim AllApps = (From a In dataImported.Appointments _
                           Where a.SnapshotID.Equals(SnapID) _
                           And a.IsService = False _
                           Select a).Distinct

            Dim AllEntries = (From e In dataImported.Entries _
                          Join t In dataImported.Traders _
                          On e.TraderID Equals t.TraderID _
                          And e.SnapshotID Equals t.SnapshotID _
                          Join m In dataImported.Mediators _
                          On e.MediatorID Equals m.MediatorID _
                          And e.SnapshotID Equals m.SnapshotID _
                          Where e.SnapshotID.Equals(SnapID) _
                          Select e).Distinct

            If JustAlert Then
                Dim LineList As New List(Of String)
                Dim TriggerString As String = ""

                TriggerString = String.Format("Ραντεβού {0}{1} Προσφορές {2}{3}Παραγγελίες {4}", _
                                              AllApps.Count.ToString("#"), "<br/>", _
                                              AllEntries.Where(Function(f) f.IsOrder = False And f.InOfferDateLimit = True).Count.ToString("#"), "<br/>", _
                                              AllEntries.Where(Function(f) f.IsOrder = True And f.InOrderDateLimit = True).Count.ToString("#"))

                LineList.Add(TriggerString)
                DataList.Add(LineList)
            Else
                Dim Total1 As New List(Of String)
                Total1.Add("ΡΑΝΤΕΒΟΥ")
                Total1.Add(AllApps.Count.ToString("#"))
                DataList.Add(Total1)

                Dim Total2 As New List(Of String)
                Total2.Add("ΠΡΟΣΦΟΡΕΣ")
                Total2.Add(AllEntries.Where(Function(f) f.IsOrder = False And f.InOfferDateLimit = True).Count.ToString("#"))
                DataList.Add(Total2)

                Dim Total3 As New List(Of String)
                Total3.Add("ΠΑΡΑΓΓΕΛΙΕΣ")
                Total3.Add(AllEntries.Where(Function(f) f.IsOrder = True And f.InOrderDateLimit = True).Count.ToString("#"))
                DataList.Add(Total3)
            End If
        Catch ex As Exception
            SvSys.ErrorLog("ExporterData.AppOrders".PadRight(10) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
        End Try
        Return DataList
    End Function

    Private Function MeanHours() As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))
        Try
            Dim AllHours = (From a In dataImported.Appointments _
                          Join t In dataImported.Traders _
                          On a.TraderID Equals t.TraderID _
                          And a.SnapshotID Equals t.SnapshotID _
                          Join m In dataImported.Mediators _
                          On a.MediatorID Equals m.MediatorID _
                          And a.SnapshotID Equals m.SnapshotID _
                          Where a.SnapshotID.Equals(SnapID) _
                          And a.IsCanceled = False _
                          And a.IsService = True _
                          Select a.AppointmentID, t.TraderCode, t.TraderDescription, m.MediatorDescription, a.AppointmentDate, a.StartTime, a.EndTime, a.CalcHours, a.AppointmentValue).Distinct

            If JustAlert Then
                Dim LineList As New List(Of String)
                Dim TriggerString As String = ""

                Dim TotalValue As Decimal = 0
                Dim TotalHours As Decimal = 0

                If AllHours.Count > 0 Then
                    TotalValue = AllHours.Sum(Function(f) f.AppointmentValue)

                    For Each row In AllHours
                        TotalHours += row.CalcHours
                    Next
                End If
                Dim MeanValue As Decimal = 0
                If TotalHours > 0 Then
                    MeanValue = TotalValue / TotalHours
                End If

                If MeanValue < GetString("MeanHoursLimit") Then
                    TriggerString = String.Format("Ή μέση ωριαία χρέωση είναι {0}", MeanValue.ToString("#.00"))
                End If
                LineList.Add(TriggerString)
                DataList.Add(LineList)
            Else
                Dim HeaderRow As New List(Of String)
                HeaderRow.Add("ΚΩΔΙΚΟΣ ΠΕΛΑΤΗ")
                HeaderRow.Add("ΕΠΩΝΥΜΙΑ ΠΕΛΑΤΗ")
                HeaderRow.Add("ΥΠΑΛΛΗΛΟΣ")
                HeaderRow.Add("ΗΜΕΡΟΜΗΝΙΑ")
                HeaderRow.Add("ΧΡΕΩΣΗ")
                HeaderRow.Add("ΩΡΕΣ")
                HeaderRow.Add("ΜΕΣΗ ΧΡΕΩΣΗ")
                DataList.Add(HeaderRow)

                For Each hrs In AllHours.OrderBy(Function(f) f.AppointmentDate)

                    Dim MeanCharge As Double = 0
                    If hrs.CalcHours <> 0 Then
                        MeanCharge = hrs.AppointmentValue / hrs.CalcHours
                    Else
                        MeanCharge = 0
                    End If

                    'If MaxLimit = 0 OrElse MeanCharge <= MaxLimit Then
                    Dim RowList As New List(Of String)
                    RowList.Add(hrs.TraderCode)
                    RowList.Add(hrs.TraderDescription)
                    RowList.Add(hrs.MediatorDescription)
                    RowList.Add(hrs.AppointmentDate.ToShortDateString)
                    RowList.Add(hrs.AppointmentValue.ToString("#.00"))
                    RowList.Add(hrs.CalcHours.ToString("#.00"))
                    RowList.Add(MeanCharge.ToString("#.00"))

                    DataList.Add(RowList)
                    'End If
                Next
            End If
        Catch ex As Exception
            SvSys.ErrorLog("ExporterData.MeanHours".PadRight(10) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
        End Try
        Return DataList

    End Function

    Private Function OpenTasks() As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))
        Try
            Dim SnapDate = (From t In dataImported.Snapshots _
                  Where t.ID.Equals(SnapID) _
                  Select t.ExecuteDate).FirstOrDefault

            Dim AllTasks = (From g In dataImported.Assignments _
                          Join t In dataImported.Traders _
                          On g.TraderID Equals t.TraderID _
                          And g.SnapshotID Equals t.SnapshotID _
                          Join m In dataImported.Mediators _
                          On g.MediatorID Equals m.MediatorID _
                          And g.SnapshotID Equals m.SnapshotID _
                          Where g.SnapshotID.Equals(SnapID) _
                          And g.IsCompleted = False _
                          Select g.AssignmentID, t.TraderCode, t.TraderDescription, m.MediatorDescription, g.StartDate).Distinct

            If JustAlert Then
                Dim LineList As New List(Of String)
                Dim TriggerString As String = ""

                Dim AllDaysPending As Integer = 0
                Dim TotalTasks As Integer = 0 'AllTasks.Count

                For Each tsk In AllTasks
                    Dim DaysPending As Short = DateDiff(DateInterval.Day, tsk.StartDate, SnapDate)
                    If DaysPending > GetString("OpenTasksLimit") Then
                        TotalTasks += 1
                    End If
                Next

                If TotalTasks > 0 Then
                    TriggerString = String.Format("Υπάρχουν {0} ανοικτές εντολές επισκευής πάνω από {1} ημέρες", _
                                                  TotalTasks, GetString("OpenTasksLimit"))
                End If
                LineList.Add(TriggerString)
                DataList.Add(LineList)
            Else
                Dim HeaderRow As New List(Of String)
                HeaderRow.Add("ΚΩΔΙΚΟΣ ΠΕΛΑΤΗ")
                HeaderRow.Add("ΕΠΩΝΥΜΙΑ ΠΕΛΑΤΗ")
                HeaderRow.Add("ΥΠΑΛΛΗΛΟΣ")
                HeaderRow.Add("ΗΜΕΡΟΜΗΝΙΑ ΕΝΑΡΞΗΣ")
                HeaderRow.Add("ΗΜΕΡΕΣ ΕΚΚΡΕΜΟΤΗΤΑΣ")
                DataList.Add(HeaderRow)

                For Each tsk In AllTasks.OrderBy(Function(f) f.StartDate)
                    Dim DaysPending As Short = DateDiff(DateInterval.Day, tsk.StartDate, SnapDate)
                    'If DaysPending >= MinLimit Then
                    Dim RowList As New List(Of String)
                    RowList.Add(tsk.TraderCode)
                    RowList.Add(tsk.TraderDescription)
                    RowList.Add(tsk.MediatorDescription)
                    RowList.Add(tsk.StartDate.ToShortDateString)
                    RowList.Add(DaysPending.ToString("#.00"))
                    DataList.Add(RowList)
                    'End If
                Next
            End If
        Catch ex As Exception
            SvSys.ErrorLog("ExporterData.OpenTasks".PadRight(10) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
        End Try
        Return DataList
    End Function

    Private Function CompletionDays() As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))
        Try
            Dim AllTasks = (From g In dataImported.Assignments _
                          Join t In dataImported.Traders _
                          On g.TraderID Equals t.TraderID _
                          And g.SnapshotID Equals t.SnapshotID _
                          Join m In dataImported.Mediators _
                          On g.MediatorID Equals m.MediatorID _
                          And g.SnapshotID Equals m.SnapshotID _
                          Where g.SnapshotID.Equals(SnapID) _
                          And g.IsCompleted = True _
                          Select g.AssignmentID, t.TraderCode, t.TraderDescription, m.MediatorDescription, g.StartDate, g.EndDate).Distinct

            If JustAlert Then
                Dim LineList As New List(Of String)
                Dim TriggerString As String = ""

                Dim AllDaysPending As Integer = 0
                Dim TotalTasks As Integer = 0 'AllTasks.Count

                For Each tsk In AllTasks
                    If tsk.EndDate IsNot Nothing Then
                        Dim EndDate As Date = tsk.EndDate
                        Dim DaysPending As Short = DateDiff(DateInterval.Day, tsk.StartDate, EndDate)
                        If DaysPending > GetString("CompletionDaysLimit") Then
                            TotalTasks += 1
                        End If
                    End If
                Next

                If TotalTasks > 0 Then
                    TriggerString = String.Format("Υπάρχουν {0} εργασίες που χρειάστηκαν πάνω από {1} ημέρες για να ολοκληρωθούν", _
                                                  TotalTasks, GetString("CompletionDaysLimit"))
                End If
                LineList.Add(TriggerString)
                DataList.Add(LineList)
            Else
                Dim HeaderRow As New List(Of String)
                HeaderRow.Add("ΚΩΔΙΚΟΣ ΠΕΛΑΤΗ")
                HeaderRow.Add("ΕΠΩΝΥΜΙΑ ΠΕΛΑΤΗ")
                HeaderRow.Add("ΥΠΑΛΛΗΛΟΣ")
                HeaderRow.Add("ΗΜΕΡΟΜΗΝΙΑ ΕΝΑΡΞΗΣ")
                HeaderRow.Add("ΗΜΕΡΟΜΗΝΙΑ ΛΗΞΗΣ")
                HeaderRow.Add("ΗΜΕΡΕΣ")
                DataList.Add(HeaderRow)

                For Each tsk In AllTasks.OrderBy(Function(f) f.StartDate)
                    If tsk.EndDate IsNot Nothing Then
                        Dim DaysPending As Short = DateDiff(DateInterval.Day, tsk.StartDate, CDate(tsk.EndDate))
                        If DaysPending >= 0 Then
                            Dim RowList As New List(Of String)
                            RowList.Add(tsk.TraderCode)
                            RowList.Add(tsk.TraderDescription)
                            RowList.Add(tsk.MediatorDescription)
                            RowList.Add(tsk.StartDate.ToShortDateString)
                            RowList.Add(CDate(tsk.EndDate).ToShortDateString)
                            RowList.Add(DaysPending.ToString("#.00"))
                            DataList.Add(RowList)
                        End If
                    End If
                Next
            End If
        Catch ex As Exception
            SvSys.ErrorLog("ExporterData.CompletionDays".PadRight(10) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
        End Try
        Return DataList

    End Function

    Private Function TurnoverDaily() As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))
        Try
            Dim AllOffers = (From e In dataImported.Entries _
                          Join t In dataImported.Traders _
                          On e.TraderID Equals t.TraderID _
                          And e.SnapshotID Equals t.SnapshotID _
                          Join m In dataImported.Mediators _
                          On e.MediatorID Equals m.MediatorID _
                          And e.SnapshotID Equals m.SnapshotID _
                          Where e.SnapshotID.Equals(SnapID) _
                          And e.IsOrder = True _
                          And e.InOrderDateLimit = True _
                          Select e.EntryID, t.TraderCode, t.TraderDescription, m.MediatorDescription, e.EntryDate, e.OrderDate, e.EntryFinalValue).Distinct

            If JustAlert Then
                Dim LineList As New List(Of String)
                Dim TriggerString As String = ""

                Dim OfferValue As Decimal = 0

                For Each ofr In AllOffers
                    OfferValue += Convert.ToDecimal(ofr.EntryFinalValue)
                Next

                If OfferValue < GetString("DailyAppsLimit") Then
                    TriggerString = String.Format("Ο ημερήσιος τζίρος είναι {0}", OfferValue.ToString("#.00"))
                End If

                LineList.Add(TriggerString)
                DataList.Add(LineList)
            Else
                Dim HeaderRow As New List(Of String)
                HeaderRow.Add("ΗΜΕΡΟΜΗΝΙΑ ΑΠΟΔΟΧΗΣ")
                HeaderRow.Add("ΚΩΔΙΚΟΣ ΠΕΛΑΤΗ")
                HeaderRow.Add("ΕΠΩΝΥΜΙΑ ΠΕΛΑΤΗ")
                HeaderRow.Add("ΠΩΛΗΤΗΣ")
                HeaderRow.Add("ΑΞΙΑ")
                DataList.Add(HeaderRow)

                For Each ofr In AllOffers.OrderBy(Function(f) f.OrderDate)
                    Dim RowList As New List(Of String)
                    RowList.Add(If(ofr.OrderDate Is Nothing, "", CDate(ofr.OrderDate).ToShortDateString))
                    RowList.Add(ofr.TraderCode)
                    RowList.Add(ofr.TraderDescription)
                    RowList.Add(ofr.MediatorDescription)
                    RowList.Add(Convert.ToDecimal(ofr.EntryFinalValue).ToString("#.00"))
                    DataList.Add(RowList)
                Next
            End If
        Catch ex As Exception
            SvSys.ErrorLog("ExporterData.TurnoverDaily".PadRight(10) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
        End Try
        Return DataList
    End Function
#End Region

#Region "Financial"
    Private Function CashFlow() As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))
        Try
            Dim AllRests = (From t In dataImported.Traders _
                         Where t.SnapshotID.Equals(SnapID) _
                         And t.TraderDescription <> "" _
                         And t.Rest > 0 _
                         Select t.TraderID, t.TraderCode, t.TraderDescription, t.Charge, t.Credit, t.Rest).Distinct

            If JustAlert Then
                Dim LineList As New List(Of String)
                Dim TriggerString As String = ""

                Dim TotalTraders As Integer = 0 'AllTasks.Count

                For Each rst In AllRests
                    If rst.Rest > GetString("OpenRestLimit") Then
                        TotalTraders += 1
                    End If
                Next

                If TotalTraders > 0 Then
                    TriggerString = String.Format("Υπάρχουν {0} πελάτες με υπόλοιπο πάνω από {1}", _
                                                  TotalTraders, GetString("TradersRestLimit"))
                End If
                LineList.Add(TriggerString)
                DataList.Add(LineList)
            Else
                Dim HeaderRow As New List(Of String)
                HeaderRow.Add("ΚΩΔΙΚΟΣ ΠΕΛΑΤΗ")
                HeaderRow.Add("ΕΠΩΝΥΜΙΑ ΠΕΛΑΤΗ")
                HeaderRow.Add("ΧΡΕΩΣΗ")
                HeaderRow.Add("ΠΙΣΤΩΣΗ")
                HeaderRow.Add("ΥΠΟΛΟΙΠΟ")
                DataList.Add(HeaderRow)

                For Each rest In AllRests.OrderBy(Function(f) f.TraderDescription)
                    Dim RowList As New List(Of String)
                    RowList.Add(rest.TraderCode)
                    RowList.Add(rest.TraderDescription)
                    RowList.Add(rest.Charge)
                    RowList.Add(rest.Credit)
                    RowList.Add(rest.Rest)
                    DataList.Add(RowList)
                Next
            End If
        Catch ex As Exception
            SvSys.ErrorLog("ExporterData.CashFlow".PadRight(10) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
        End Try
        Return DataList

    End Function

    Private Function Vat() As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))
        Try
            Dim AllVats = (From t In dataImported.Vats _
                         Where t.SnapshotID.Equals(SnapID) _
                         Select t.ID, t.VatDate, t.VatValue).Distinct

            If JustAlert Then
                Dim LineList As New List(Of String)
                Dim TriggerString As String = ""

                Dim TotalVat As Decimal = Convert.ToDecimal(AllVats.Sum(Function(f) f.VatValue))

                If TotalVat > GetString("VatLimit") Then
                    TriggerString = String.Format("Το ΦΠΑ προς απόδοση είναι TotalVat", TotalVat.ToString("#.00"))
                End If
                LineList.Add(TriggerString)
                DataList.Add(LineList)
            Else
                Dim Total As New List(Of String)
                Total.Add("ΣΥΝΟΛΟ ΦΠΑ")
                Total.Add(Convert.ToDecimal(AllVats.Sum(Function(f) f.VatValue)).ToString("#.00"))

                DataList.Add(Total)

                DataList.Add(New List(Of String))

                Dim HeaderRow As New List(Of String)
                HeaderRow.Add("ΗΜΕΡΟΜΗΝΙΑ")
                HeaderRow.Add("ΦΠΑ")
                DataList.Add(HeaderRow)

                For Each fpa In AllVats.OrderBy(Function(f) f.VatDate)
                    Dim RowList As New List(Of String)
                    RowList.Add(fpa.VatDate)
                    RowList.Add(fpa.VatValue)
                    DataList.Add(RowList)
                Next
            End If
        Catch ex As Exception
            SvSys.ErrorLog("ExporterData.Vat".PadRight(10) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
        End Try
        Return DataList

    End Function

    Private Function OpenTradersRest() As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))
        Try
            Dim AllRests = (From t In dataImported.Traders _
                         Where t.SnapshotID.Equals(SnapID) _
                         And t.TraderDescription <> "" _
                         And t.Rest > 0 _
                         Select t.TraderID, t.TraderCode, t.TraderDescription, t.Charge, t.Credit, t.Rest).Distinct

            If JustAlert Then
                Dim LineList As New List(Of String)
                Dim TriggerString As String = ""

                Dim TotalTraders As Integer = 0 'AllTasks.Count

                For Each rst In AllRests
                    If rst.Rest > GetString("OpenRestLimit") Then
                        TotalTraders += 1
                    End If
                Next

                If TotalTraders > 0 Then
                    TriggerString = String.Format("Υπάρχουν {0} πελάτες με υπόλοιπο πάνω από {1}", _
                                                  TotalTraders, GetString("TradersRestLimit"))
                End If
                LineList.Add(TriggerString)
                DataList.Add(LineList)
            Else
                Dim HeaderRow As New List(Of String)
                HeaderRow.Add("ΚΩΔΙΚΟΣ ΠΕΛΑΤΗ")
                HeaderRow.Add("ΕΠΩΝΥΜΙΑ ΠΕΛΑΤΗ")
                HeaderRow.Add("ΧΡΕΩΣΗ")
                HeaderRow.Add("ΠΙΣΤΩΣΗ")
                HeaderRow.Add("ΥΠΟΛΟΙΠΟ")
                DataList.Add(HeaderRow)

                For Each rest In AllRests.OrderBy(Function(f) f.TraderDescription)
                    Dim RowList As New List(Of String)
                    RowList.Add(rest.TraderCode)
                    RowList.Add(rest.TraderDescription)
                    RowList.Add(rest.Charge)
                    RowList.Add(rest.Credit)
                    RowList.Add(rest.Rest)
                    DataList.Add(RowList)
                Next
            End If
        Catch ex As Exception
            SvSys.ErrorLog("ExporterData.OpenTradersRest".PadRight(10) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
        End Try
        Return DataList

    End Function

    Private Function TradersCap() As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))
        Try
            Dim AllCaps = (From t In dataImported.Traders _
                            Where t.SnapshotID.Equals(SnapID) _
                            And t.TraderDescription <> "" _
                            And t.CapLimit > 0 _
                            Select t.TraderID, t.TraderCode, t.TraderDescription, t.CapLimit).Distinct

            If JustAlert Then
                Dim LineList As New List(Of String)
                Dim TriggerString As String = ""

                Dim TotalTraders1 As Integer = 0
                Dim TotalTraders2 As Integer = 0

                For Each trd In AllCaps
                    If trd.CapLimit > GetString("CapLowerLimit") Then
                        TotalTraders1 += 1
                    End If
                    If trd.CapLimit < GetString("CapUpperLimit") Then
                        TotalTraders2 += 1
                    End If
                Next

                If TotalTraders1 > 0 Then
                    TriggerString = String.Format("Υπάρχουν {0} πελάτες με πλαφόν πάνω από {1}", _
                                                  TotalTraders1, GetString("CapLowerLimit"))
                End If
                If TotalTraders2 > 0 Then
                    If TriggerString <> "" Then TriggerString = "</br>"
                    TriggerString &= String.Format("Υπάρχουν {0} πελάτες με πλαφόν κάτω από {1}", _
                                                  TotalTraders2, GetString("CapUpperLimit"))
                End If
                LineList.Add(TriggerString)
                DataList.Add(LineList)
            Else
                Dim HeaderRow As New List(Of String)
                HeaderRow.Add("ΚΩΔΙΚΟΣ ΠΕΛΑΤΗ")
                HeaderRow.Add("ΕΠΩΝΥΜΙΑ ΠΕΛΑΤΗ")
                HeaderRow.Add("ΠΛΑΦΟΝ")
                DataList.Add(HeaderRow)

                For Each cap In AllCaps.OrderBy(Function(f) f.TraderDescription)
                    Dim RowList As New List(Of String)
                    RowList.Add(cap.TraderCode)
                    RowList.Add(cap.TraderDescription)
                    RowList.Add(cap.CapLimit)
                    DataList.Add(RowList)
                Next
            End If
        Catch ex As Exception
            SvSys.ErrorLog("ExporterData.TradersCap".PadRight(10) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
        End Try
        Return DataList

    End Function

    Private Function TradersCredit() As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))
        Try
            Dim AllCredits = (From t In dataImported.Traders _
                            Where t.SnapshotID.Equals(SnapID) _
                            And t.TraderDescription <> "" _
                            And t.CreditLimit > 0 _
                            Select t.TraderID, t.TraderCode, t.TraderDescription, t.CreditLimit).Distinct

            If JustAlert Then
                Dim LineList As New List(Of String)
                Dim TriggerString As String = ""

                Dim TotalTraders1 As Integer = 0
                Dim TotalTraders2 As Integer = 0

                For Each trd In AllCredits
                    If trd.CreditLimit > GetString("CreditLowerLimit") Then
                        TotalTraders1 += 1
                    End If
                    If trd.CreditLimit < GetString("CreditUpperLimit") Then
                        TotalTraders2 += 1
                    End If
                Next

                If TotalTraders1 > 0 Then
                    TriggerString = String.Format("Υπάρχουν {0} πελάτες με πίστωση πάνω από {1}", _
                                                  TotalTraders1, GetString("CreditLowerLimit"))
                End If
                If TotalTraders2 > 0 Then
                    If TriggerString <> "" Then TriggerString = "</br>"
                    TriggerString &= String.Format("Υπάρχουν {0} πελάτες με πίστωση κάτω από {1}", _
                                                  TotalTraders2, GetString("CreditUpperLimit"))
                End If
                LineList.Add(TriggerString)
                DataList.Add(LineList)
            Else
                Dim HeaderRow As New List(Of String)
                HeaderRow.Add("ΚΩΔΙΚΟΣ ΠΕΛΑΤΗ")
                HeaderRow.Add("ΕΠΩΝΥΜΙΑ ΠΕΛΑΤΗ")
                HeaderRow.Add("ΠΙΣΤΩΣΗ")
                DataList.Add(HeaderRow)

                For Each crd In AllCredits.OrderBy(Function(f) f.TraderDescription)
                    Dim RowList As New List(Of String)
                    RowList.Add(crd.TraderCode)
                    RowList.Add(crd.TraderDescription)
                    RowList.Add(crd.CreditLimit)
                    DataList.Add(RowList)
                Next
            End If
        Catch ex As Exception
            SvSys.ErrorLog("ExporterData.TradersCredit".PadRight(10) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
        End Try
        Return DataList

    End Function

    Private Function BanksRest() As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))
        Try
            Dim AllBanks = (From t In dataImported.BankBalances _
                            Where t.SnapshotID.Equals(SnapID) _
                            Select t.AccountingCode, t.BankRest).Distinct

            If JustAlert Then
                Dim LineList As New List(Of String)
                Dim TriggerString As String = ""

                Dim TotalBanks1 As Integer = 0
                Dim TotalBanks2 As Integer = 0

                For Each trd In AllBanks
                    If trd.BankRest > GetString("BankRestLowerLimit") Then
                        TotalBanks1 += 1
                    End If
                    If trd.BankRest < GetString("BankRestUpperLimit") Then
                        TotalBanks2 += 1
                    End If
                Next

                If TotalBanks1 > 0 Then
                    TriggerString = String.Format("Υπάρχουν {0} τράπεζες με υπόλοιπο πάνω από {1}", _
                                                  TotalBanks1, GetString("BankRestLowerLimit"))
                End If
                If TotalBanks2 > 0 Then
                    If TriggerString <> "" Then TriggerString = "</br>"
                    TriggerString &= String.Format("Υπάρχουν {0} τράπεζες με υπόλοιπο κάτω από {1}", _
                                                  TotalBanks2, GetString("BankRestUpperLimit"))
                End If
                LineList.Add(TriggerString)
                DataList.Add(LineList)
            Else
                Dim HeaderRow As New List(Of String)
                HeaderRow.Add("ΤΡΑΠΕΖΑ")
                HeaderRow.Add("ΥΠΟΛΟΙΠΟ")
                DataList.Add(HeaderRow)

                For Each rest In AllBanks.OrderBy(Function(f) f.AccountingCode)
                    Dim RowList As New List(Of String)
                    RowList.Add(rest.AccountingCode)
                    RowList.Add(rest.BankRest)
                    DataList.Add(RowList)
                Next
            End If
        Catch ex As Exception
            SvSys.ErrorLog("ExporterData.BanksRest".PadRight(10) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
        End Try
        Return DataList

    End Function

    Private Function SuppliersOrders() As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))
        Try
            Dim AllOrders = (From e In dataImported.Entries _
                          Join t In dataImported.Traders _
                          On e.TraderID Equals t.TraderID _
                          And e.SnapshotID Equals t.SnapshotID _
                          Where e.SnapshotID.Equals(SnapID) _
                          And e.IsCharge = True _
                          Select e.EntryID, t.TraderCode, t.TraderDescription, e.EntryDate, e.OrderDate, e.EntryFinalValue).Distinct

            If JustAlert Then
                Dim LineList As New List(Of String)
                Dim TriggerString As String = ""

                Dim TotalSales As Decimal = Convert.ToDecimal(AllOrders.Sum(Function(f) f.EntryFinalValue))

                If TotalSales > GetString("SuppliersOrderLimit") Then
                    TriggerString = String.Format("Παραγγελίες σε προμηθευτές : {0}", _
                                                  TotalSales)
                End If

                LineList.Add(TriggerString)
                DataList.Add(LineList)
            Else
                Dim Total As New List(Of String)
                Total.Add("ΠΩΛΗΣΕΙΣ")
                Total.Add(Convert.ToDecimal(AllOrders.Sum(Function(f) f.EntryFinalValue)).ToString("#.00"))
                DataList.Add(Total)


                DataList.Add(New List(Of String))

                Dim HeaderRow As New List(Of String)
                HeaderRow.Add("ΚΩΔΙΚΟΣ ΠΕΛΑΤΗ")
                HeaderRow.Add("ΕΠΩΝΥΜΙΑ ΠΕΛΑΤΗ")
                HeaderRow.Add("ΗΜΕΡΟΜΗΝΙΑ")
                HeaderRow.Add("ΗΜΕΡΟΜΗΝΙΑ ΠΑΡΑΓΓΕΛΙΑΣ")
                HeaderRow.Add("ΑΓΟΡΑ")
                DataList.Add(HeaderRow)

                For Each ord In AllOrders.OrderBy(Function(f) f.OrderDate)
                    Dim RowList As New List(Of String)
                    RowList.Add(ord.TraderCode)
                    RowList.Add(ord.TraderDescription)
                    RowList.Add(ord.EntryDate.ToShortDateString)
                    RowList.Add(CDate(ord.OrderDate).ToShortDateString)
                    RowList.Add(Convert.ToDecimal(ord.EntryFinalValue).ToString("#.00"))

                    DataList.Add(RowList)
                Next
            End If
        Catch ex As Exception
            SvSys.ErrorLog("ExporterData.SuppliersOrders".PadRight(10) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
        End Try
        Return DataList

    End Function

    Private Function GrossProfit() As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))
        Try
            Dim AllOrders = (From e In dataImported.Entries _
                             Join t In dataImported.Traders _
                             On e.TraderID Equals t.TraderID _
                             And e.SnapshotID Equals t.SnapshotID _
                             Where e.SnapshotID.Equals(SnapID) _
                             And e.IsOrder = True _
                             Select e.EntryID, t.TraderCode, t.TraderDescription, e.EntryDate, e.EntryFinalValue).Distinct

            Dim TotalOrderProfit As Decimal = 0

            For Each ord In AllOrders
                Dim OrderCost As Decimal = 0

                Dim AllItems = From t In dataImported.EntryLines _
                               Where t.EntryID.Equals(ord.EntryID) _
                               And t.SnapshotID.Equals(SnapID) _
                               Select t.ProductID, t.Quantity

                For Each item In AllItems
                    Dim ProductCost = (From t In dataImported.Products _
                                    Where t.ProductID.Equals(item.ProductID) _
                                    And t.SnapshotID.Equals(SnapID) _
                                    Select t.Cost).FirstOrDefault
                    OrderCost += ProductCost * item.Quantity
                Next

                TotalOrderProfit += ord.EntryFinalValue - OrderCost

            Next

            If JustAlert Then
                Dim LineList As New List(Of String)
                Dim TriggerString As String = ""


                If TotalOrderProfit < GetString("GrossProfitLimit") Then
                    TriggerString = String.Format("Το μικτό κέρδος είναι {0}", TotalOrderProfit.ToString("#.00"))
                End If
                LineList.Add(TriggerString)
                DataList.Add(LineList)
            Else
                Dim Total As New List(Of String)
                Total.Add("ΜΙΚΤΟ ΚΕΡΔΟΣ")
                Total.Add(TotalOrderProfit.ToString("#.00"))
                DataList.Add(Total)

                DataList.Add(New List(Of String))

                Dim HeaderRow As New List(Of String)
                HeaderRow.Add("ΚΩΔΙΚΟΣ ΠΕΛΑΤΗ")
                HeaderRow.Add("ΕΠΩΝΥΜΙΑ ΠΕΛΑΤΗ")
                HeaderRow.Add("ΗΜΕΡΟΜΗΝΙΑ")
                HeaderRow.Add("ΠΩΛΗΣΗ")
                HeaderRow.Add("ΚΟΣΤΟΣ")
                HeaderRow.Add("ΜΙΚΤΟ ΚΕΡΔΟΣ")
                DataList.Add(HeaderRow)

                DataList.Add(New List(Of String))

                For Each ord In AllOrders.OrderBy(Function(f) f.EntryDate)
                    Dim OrderCost As Decimal = 0

                    Dim AllItems = From t In dataImported.EntryLines _
                                   Where t.EntryID.Equals(ord.EntryID) _
                                   And t.SnapshotID.Equals(SnapID) _
                                   Select t.ProductID, t.Quantity

                    For Each item In AllItems
                        Dim ProductCost = (From t In dataImported.Products _
                                        Where t.ProductID.Equals(item.ProductID) _
                                        And t.SnapshotID.Equals(SnapID) _
                                        Select t.Cost).FirstOrDefault
                        OrderCost += ProductCost * item.Quantity
                    Next

                    Dim RowList As New List(Of String)
                    RowList.Add(ord.TraderCode)
                    RowList.Add(ord.TraderDescription)
                    RowList.Add(ord.EntryDate.ToShortDateString)
                    RowList.Add(Convert.ToDecimal(ord.EntryFinalValue).ToString("#.00"))
                    RowList.Add(Convert.ToDecimal(OrderCost).ToString("#.00"))
                    RowList.Add(Convert.ToDecimal(ord.EntryFinalValue - OrderCost).ToString("#.00"))

                    DataList.Add(RowList)
                Next
            End If
        Catch ex As Exception
            SvSys.ErrorLog("ExporterData.GrossProfit".PadRight(10) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
        End Try
        Return DataList

    End Function

    Private Function Expenses() As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))
        Try
            Dim AllExpenses = (From t In dataImported.Expenses _
                            Where t.SnapshotID.Equals(SnapID) _
                            Select t.ExpenseID, t.ExpenseAccountingCode, t.ExpenseDate, t.ExpenseDescription, t.ExpenseValue).Distinct

            Dim TotalExpenses As Decimal = AllExpenses.Sum(Function(f) f.ExpenseValue)

            If JustAlert Then
                Dim LineList As New List(Of String)
                Dim TriggerString As String = ""

                If TotalExpenses > GetString("ExpensesLowerLimit") Or TotalExpenses < GetString("ExpensesUpperLimit") Then
                    TriggerString = String.Format("Τα έξοδα είναι {0}", TotalExpenses)
                End If

                LineList.Add(TriggerString)
                DataList.Add(LineList)
            Else
                Dim Total As New List(Of String)
                Total.Add("ΣΥΝΟΛΟ ΕΞΟΔΩΝ")
                Total.Add(TotalExpenses.ToString("#.00"))

                DataList.Add(Total)

                DataList.Add(New List(Of String))

                Dim HeaderRow As New List(Of String)
                HeaderRow.Add("ΗΜΕΡΟΜΗΝΙΑ")
                HeaderRow.Add("ΤΥΠΟΣ")
                HeaderRow.Add("ΕΞΟΔΟ")
                HeaderRow.Add("ΠΟΣΟ")
                DataList.Add(HeaderRow)

                For Each exp In AllExpenses.OrderBy(Function(f) f.ExpenseDate)
                    Dim RowList As New List(Of String)
                    RowList.Add(exp.ExpenseDate)
                    RowList.Add(exp.ExpenseAccountingCode)
                    RowList.Add(exp.ExpenseDescription)
                    RowList.Add(exp.ExpenseValue)
                    DataList.Add(RowList)
                Next
            End If
        Catch ex As Exception
            SvSys.ErrorLog("ExporterData.Expenses".PadRight(10) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
        End Try
        Return DataList

    End Function

    Private Function Collections() As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))
        Try
            Dim AllTransactions = (From t In dataImported.Transactions _
                                   Where t.SnapshotID.Equals(SnapID) _
                                   And t.IsPayment = False _
                                   Select t.TransactionID, t.TraderCode, t.TraderDescription, t.TransactionType, t.TransactionDate, t.TransactionValue).Distinct

            Dim TotalTransactions As Decimal = AllTransactions.Sum(Function(f) f.TransactionValue)

            If JustAlert Then
                Dim LineList As New List(Of String)
                Dim TriggerString As String = ""

                If TotalTransactions < GetString("MoneyCollectionLimit") Then
                    TriggerString = String.Format("Οι εισπράξεις είναι {0}", TotalTransactions)
                End If

                LineList.Add(TriggerString)
                DataList.Add(LineList)
            Else
                Dim Total As New List(Of String)
                Total.Add("ΣΥΝΟΛΟ ΕΙΣΠΡΑΞΕΩΝ")
                Total.Add(TotalTransactions.ToString("#.00"))

                DataList.Add(Total)

                DataList.Add(New List(Of String))

                Dim HeaderRow As New List(Of String)
                HeaderRow.Add("ΗΜΕΡΟΜΗΝΙΑ")
                HeaderRow.Add("ΤΥΠΟΣ")
                HeaderRow.Add("ΚΩΔΙΚΟΣ ΠΕΛΑΤΗ")
                HeaderRow.Add("ΠΕΛΑΤΗΣ")
                HeaderRow.Add("ΠΟΣΟ")
                DataList.Add(HeaderRow)

                For Each tns In AllTransactions.OrderBy(Function(f) f.TransactionDate)
                    Dim RowList As New List(Of String)
                    RowList.Add(tns.TransactionDate)
                    RowList.Add(tns.TransactionType)
                    RowList.Add(tns.TraderCode)
                    RowList.Add(tns.TraderDescription)
                    RowList.Add(tns.TransactionValue)
                    DataList.Add(RowList)
                Next
            End If
        Catch ex As Exception
            SvSys.ErrorLog("ExporterData.Collections".PadRight(10) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
        End Try
        Return DataList

    End Function

    Private Function Payments() As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))
        Try
            Dim AllTransactions = (From t In dataImported.Transactions _
                                   Where t.SnapshotID.Equals(SnapID) _
                                   And t.IsPayment = True _
                                   Select t.TransactionID, t.TraderCode, t.TraderDescription, t.TransactionType, t.TransactionDate, t.TransactionValue).Distinct

            Dim TotalTransactions As Decimal = AllTransactions.Sum(Function(f) f.TransactionValue)

            If JustAlert Then
                Dim LineList As New List(Of String)
                Dim TriggerString As String = ""

                If TotalTransactions > GetString("PaymentsLimit") Then
                    TriggerString = String.Format("Οι εισπράξεις είναι {0}", TotalTransactions)
                End If

                LineList.Add(TriggerString)
                DataList.Add(LineList)
            Else
                Dim Total As New List(Of String)
                Total.Add("ΣΥΝΟΛΟ ΕΙΣΠΡΑΞΕΩΝ")
                Total.Add(TotalTransactions.ToString("#.00"))

                DataList.Add(Total)

                DataList.Add(New List(Of String))

                Dim HeaderRow As New List(Of String)
                HeaderRow.Add("ΗΜΕΡΟΜΗΝΙΑ")
                HeaderRow.Add("ΤΥΠΟΣ")
                HeaderRow.Add("ΚΩΔΙΚΟΣ ΠΕΛΑΤΗ")
                HeaderRow.Add("ΠΕΛΑΤΗΣ")
                HeaderRow.Add("ΠΟΣΟ")
                DataList.Add(HeaderRow)

                For Each tns In AllTransactions.OrderBy(Function(f) f.TransactionDate)
                    Dim RowList As New List(Of String)
                    RowList.Add(tns.TransactionDate)
                    RowList.Add(tns.TransactionType)
                    RowList.Add(tns.TraderCode)
                    RowList.Add(tns.TraderDescription)
                    RowList.Add(tns.TransactionValue)
                    DataList.Add(RowList)
                Next
            End If
        Catch ex As Exception
            SvSys.ErrorLog("ExporterData.Payments".PadRight(10) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
        End Try
        Return DataList

    End Function

#End Region

    Private Function NewCustomers() As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))
        Try
            Dim AllCustomers = (From t In dataImported.Traders _
                                Where t.SnapshotID.Equals(SnapID) _
                                And t.TraderDescription <> "" _
                                Select t.TraderID, t.TraderCode, t.TraderDescription).Distinct

            If JustAlert Then
                Dim LineList As New List(Of String)
                Dim TriggerString As String = ""

                For Each cus In AllCustomers.OrderBy(Function(f) f.TraderDescription)
                    TriggerString &= String.Format("Πραγματοποιήθηκε εισαγωγή νέου Πελάτη : {0} - {1}", cus.TraderCode, cus.TraderDescription) & "<br/>"
                Next
                LineList.Add(TriggerString)
                DataList.Add(LineList)
            Else
                Dim HeaderRow As New List(Of String)
                HeaderRow.Add("ΚΩΔΙΚΟΣ ΠΕΛΑΤΗ")
                HeaderRow.Add("ΕΠΩΝΥΜΙΑ ΠΕΛΑΤΗ")
                DataList.Add(HeaderRow)

                For Each cus In AllCustomers.OrderBy(Function(f) f.TraderDescription)
                    Dim RowList As New List(Of String)
                    RowList.Add(cus.TraderCode)
                    RowList.Add(cus.TraderDescription)
                    DataList.Add(RowList)
                Next
            End If
        Catch ex As Exception
            SvSys.ErrorLog("ExporterData.NewCustomers".PadRight(10) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
        End Try
        Return DataList
    End Function

    Private Function NewProducts() As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))
        Try
            Dim AllProducts = (From t In dataImported.Products _
                                Where t.SnapshotID.Equals(SnapID) _
                                And t.ProductDescription <> "" _
                                And t.Price = 0 _
                                Select t.ProductID, t.ProductCode, t.ProductDescription).Distinct

            If JustAlert Then
                Dim LineList As New List(Of String)
                Dim TriggerString As String = ""

                For Each prd In AllProducts.OrderBy(Function(f) f.ProductDescription)
                    TriggerString &= String.Format("Πραγματοποιήθηκε εισαγωγή νέου Είδους : {0} - {1}", prd.ProductCode, prd.ProductDescription) & "<br/>"
                Next
                LineList.Add(TriggerString)
                DataList.Add(LineList)
            Else
                Dim HeaderRow As New List(Of String)
                HeaderRow.Add("ΚΩΔΙΚΟΣ ΕΙΔΟΥΣ")
                HeaderRow.Add("ΕΙΔΟΣ")
                DataList.Add(HeaderRow)

                For Each prd In AllProducts.OrderBy(Function(f) f.ProductDescription)
                    Dim RowList As New List(Of String)
                    RowList.Add(prd.ProductCode)
                    RowList.Add(prd.ProductDescription)
                    DataList.Add(RowList)
                Next
            End If
        Catch ex As Exception
            SvSys.ErrorLog("ExporterData.NewProducts".PadRight(10) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
        End Try
        Return DataList
    End Function

    Private Function NewProductsPrices() As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))
        Try
            Dim AllProducts = (From t In dataImported.Products _
                                Where t.SnapshotID.Equals(SnapID) _
                                And t.ProductDescription <> "" _
                                And t.Price <> 0 _
                                Select t.ProductID, t.ProductCode, t.ProductDescription, t.Price).Distinct

            If JustAlert Then
                Dim LineList As New List(Of String)
                Dim TriggerString As String = ""

                For Each prd In AllProducts.OrderBy(Function(f) f.ProductDescription)
                    TriggerString &= String.Format("Νέα τιμή : {0} - {1} : {2}", prd.ProductCode, prd.ProductDescription, prd.Price) & "<br/>"
                Next
                LineList.Add(TriggerString)
                DataList.Add(LineList)
            Else
                Dim HeaderRow As New List(Of String)
                HeaderRow.Add("ΚΩΔΙΚΟΣ ΕΙΔΟΥΣ")
                HeaderRow.Add("ΕΙΔΟΣ")
                HeaderRow.Add("ΤΙΜΗ")
                DataList.Add(HeaderRow)

                For Each prd In AllProducts.OrderBy(Function(f) f.ProductDescription)
                    Dim RowList As New List(Of String)
                    RowList.Add(prd.ProductCode)
                    RowList.Add(prd.ProductDescription)
                    RowList.Add(prd.Price)
                    DataList.Add(RowList)
                Next
            End If
        Catch ex As Exception
            SvSys.ErrorLog("ExporterData.NewProductsPrices".PadRight(10) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
        End Try
        Return DataList
    End Function

    Private Function Lots() As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))
        Try
            Dim MonthsLimit As Integer
            Try
                MonthsLimit = GetString("LotsMonthsLimit")
            Catch ex As Exception
                MonthsLimit = 0
            End Try

            If MonthsLimit = 0 Then MonthsLimit = 3

            Dim AllProducts = (From t In dataImported.ProductLots _
                               Join p In dataImported.Products _
                               On t.ProductID Equals p.ProductID _
                                Where t.SnapshotID.Equals(SnapID) _
                                And p.ProductDescription <> "" _
                                Select t.LotID, t.LotCode, p.ProductCode, p.ProductDescription, t.LotExpiry, p.StockQuantity).Distinct

            If JustAlert Then
                Dim LineList As New List(Of String)
                Dim TriggerString As String = ""

                For Each prd In AllProducts.OrderBy(Function(f) f.ProductDescription)
                    If Now.Date.AddMonths(MonthsLimit) > prd.LotExpiry AndAlso prd.StockQuantity > 0 Then
                        TriggerString &= String.Format("Λήξη παρτίδας {0} στις {1} : {2} - {3} - {4}, Υπόλοιπο:{5}", prd.LotCode, prd.LotExpiry.ToShortDateString, prd.ProductCode, prd.ProductDescription, Left(prd.ProductCode, 11), prd.StockQuantity) & "<br/>"
                    End If
                Next
                LineList.Add(TriggerString)
                DataList.Add(LineList)
            Else
                Dim HeaderRow As New List(Of String)
                HeaderRow.Add("ΠΑΡΤΙΔΑ")
                HeaderRow.Add("ΛΗΞΗ")
                HeaderRow.Add("ΚΩΔΙΚΟΣ ΕΙΔΟΥΣ")
                HeaderRow.Add("ΕΙΔΟΣ")
                HeaderRow.Add("ΥΠΟΛΟΙΠΟ")
                DataList.Add(HeaderRow)

                For Each prd In AllProducts.OrderBy(Function(f) f.LotExpiry)
                    If Now.Date.AddMonths(MonthsLimit) > prd.LotExpiry AndAlso prd.StockQuantity > 0 Then
                        Dim RowList As New List(Of String)
                        RowList.Add(prd.LotCode)
                        RowList.Add(prd.LotExpiry)
                        RowList.Add(prd.ProductCode)
                        RowList.Add(prd.ProductDescription)
                        RowList.Add(prd.StockQuantity)
                        DataList.Add(RowList)
                    End If
                Next
            End If
        Catch ex As Exception
            SvSys.ErrorLog("ExporterData.Lots".PadRight(10) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
        End Try
        Return DataList
    End Function
    Private Function NewSuppliers(ByVal JustAlert As Boolean) As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))
        Try
            Dim AllSuppliers = (From t In dataImported.Traders _
                                Where t.SnapshotID.Equals(SnapID) _
                                And t.IsSupplier = True _
                                Select t.TraderID, t.TraderCode, t.TraderDescription, t.CompanyID, t.MediatorID, t.MediatorCode).Distinct

            'DataList.Add(.OracleReader("TRACODE"))   'TraderID '0
            'DataList.Add(.OracleReader("LEENAME"))  'TraderCode '1
            'DataList.Add(.OracleReader("LEEAFM"))   'TraderDescription '2
            'DataList.Add(.OracleReader("PMTTITLE")) 'CompanyID '3
            'DataList.Add(.OracleReader("FULLNAME")) 'MediatorID '4
            If JustAlert Then
                Dim LineList As New List(Of String)
                Dim TriggerString As String = ""

                For Each sup In AllSuppliers.OrderBy(Function(f) f.TraderDescription)
                    TriggerString &= String.Format("Πραγματοποιήθηκε {5} Προμηθευτή : ΚΩΔΙΚΟΣ: {0} - ΕΠΩΝΥΜΙΑ: {1} - Α.Φ.Μ: {2} - ΤΡ.ΠΛΗΡΩΜΗΣ: {3} - ΟΝ.ΧΡΗΣΤΗ: {4}", sup.TraderID, sup.TraderCode, sup.TraderDescription, sup.CompanyID, sup.MediatorID, sup.MediatorCode) & "<br/>"
                Next
                LineList.Add(TriggerString)
                DataList.Add(LineList)
            Else
                Dim HeaderRow As New List(Of String)
                HeaderRow.Add("ΚΩΔΙΚΟΣ ΠΡΟΜΗΘΕΥΤΗ")                
                HeaderRow.Add("ΕΠΩΝΥΜΙΑ ΠΡΟΜΗΘΕΥΤΗ")
                HeaderRow.Add("Α.Φ.Μ. ΠΡΟΜΗΘΕΥΤΗ")
                HeaderRow.Add("ΤΡΟΠΟΣ ΠΛΗΡΩΜΗΣ")
                HeaderRow.Add("ΟΝΟΜΑ ΧΡΗΣΤΗ")
                DataList.Add(HeaderRow)

                For Each sup In AllSuppliers.OrderBy(Function(f) f.TraderDescription)
                    Dim RowList As New List(Of String)

                    RowList.Add(sup.TraderID)
                    RowList.Add(sup.TraderCode)
                    RowList.Add(sup.TraderDescription)
                    RowList.Add(sup.CompanyID)
                    RowList.Add(sup.MediatorID)
                    DataList.Add(RowList)
                Next
            End If

         
        Catch ex As Exception
            SvSys.ErrorLog("ExporterData.NewSuppliers".PadRight(10) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
        End Try
        Return DataList
    End Function
#Region "Timetable"

    'Case ReportTypes.TIMETABLE_CONTINUOUS
    '    DataList = TimetableContinuous
    'Case ReportTypes.TIMETABLE_DRIVERS_HOURS
    '    DataList = TimetableDriversHours
    'Case ReportTypes.TIMETABLE_DRIVERS_SUNDAYS = 97
    '    DataList = TimetableDriversSundays


    Private Function TimetableOrphans(ByVal EmplCodes As List(Of String)) As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))
        Try
            Dim AllTimetables = (From t In dataImported.Timetables _
                                Where t.SnapshotID.Equals(SnapID) _
                                And ((t.TimeIn Is Nothing AndAlso t.TimeOut IsNot Nothing) OrElse _
                                     (t.TimeIn IsNot Nothing AndAlso t.TimeOut Is Nothing)) _
                                Select t.EmployeeCode, t.EmployeeName, t.TimetableDate, t.TimeIn, t.TimeOut).Distinct

            If EmplCodes.Count > 0 Then
                AllTimetables = AllTimetables.Where(Function(f) EmplCodes.Contains(f.EmployeeCode))
            End If

            If JustAlert Then
                Dim LineList As New List(Of String)
                Dim TriggerString As String = ""

                For Each tmt In AllTimetables.OrderBy(Function(f) f.EmployeeName).OrderBy(Function(p) p.TimetableDate)
                    If tmt.TimeIn Is Nothing Then
                        TriggerString &= String.Format("Ορφανή είσοδος {0} : {1} - Έξοδος {2}", tmt.EmployeeName, CDate(tmt.TimetableDate).ToShortDateString, CDate(tmt.TimeOut).ToShortTimeString) & "<br/>"
                    ElseIf tmt.TimeOut Is Nothing Then
                        TriggerString &= String.Format("Ορφανή έξοδος {0} : {1} - Είσοδος {2}", tmt.EmployeeName, CDate(tmt.TimetableDate).ToShortDateString, CDate(tmt.TimeIn).ToShortTimeString) & "<br/>"
                    End If
                Next
                LineList.Add(TriggerString)
                DataList.Add(LineList)
            Else
                Dim HeaderRow As New List(Of String)
                HeaderRow.Add("ΥΠΑΛΛΗΛΟΣ")
                HeaderRow.Add("ΗΜΕΡΟΜΗΝΙΑ")
                HeaderRow.Add("ΕΙΣΟΔΟΣ")
                HeaderRow.Add("ΕΞΟΔΟΣ")
                DataList.Add(HeaderRow)

                For Each tmt In AllTimetables.OrderBy(Function(f) f.EmployeeName).OrderBy(Function(p) p.TimetableDate)
                    Dim RowList As New List(Of String)
                    RowList.Add(tmt.EmployeeName)
                    RowList.Add(tmt.TimetableDate)
                    RowList.Add(If(tmt.TimeIn Is Nothing, "", CDate(tmt.TimeIn).ToShortTimeString))
                    RowList.Add(If(tmt.TimeOut Is Nothing, "", CDate(tmt.TimeOut).ToShortTimeString))
                    DataList.Add(RowList)
                Next
            End If
        Catch ex As Exception
            SvSys.ErrorLog("ExporterData.TimetableOrphans".PadRight(10) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
        End Try
        Return DataList
    End Function

    Private Function TimetableMissing(ByVal EmplCodes As List(Of String)) As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))
        Try
            Dim AllTimetables = (From t In dataImported.Timetables _
                                Where t.SnapshotID.Equals(SnapID) _
                                And ((t.TimeIn Is Nothing AndAlso t.TimeOut Is Nothing)) _
                                Select t.EmployeeCode, t.EmployeeName, t.TimetableDate, t.TimeIn, t.TimeOut).Distinct

            If EmplCodes.Count > 0 Then
                AllTimetables = AllTimetables.Where(Function(f) EmplCodes.Contains(f.EmployeeCode))
            End If

            If JustAlert Then
                Dim LineList As New List(Of String)
                Dim TriggerString As String = ""

                For Each tmt In AllTimetables.OrderBy(Function(f) f.EmployeeName).OrderBy(Function(p) p.TimetableDate)

                    TriggerString &= String.Format("Απουσία {0} : {1}", tmt.EmployeeName, CDate(tmt.TimetableDate).ToShortDateString) & "<br/>"
                Next
                LineList.Add(TriggerString)
                DataList.Add(LineList)
            Else
                Dim HeaderRow As New List(Of String)
                HeaderRow.Add("ΥΠΑΛΛΗΛΟΣ")
                HeaderRow.Add("ΗΜΕΡΟΜΗΝΙΑ")
                HeaderRow.Add("ΑΙΤΙΟΛΟΓΙΑ")
                DataList.Add(HeaderRow)

                For Each tmt In AllTimetables.OrderBy(Function(f) f.EmployeeName).OrderBy(Function(p) p.TimetableDate)
                    Dim RowList As New List(Of String)
                    RowList.Add(tmt.EmployeeName)
                    RowList.Add(tmt.TimetableDate)
                    RowList.Add("")
                    DataList.Add(RowList)
                Next
            End If
        Catch ex As Exception
            SvSys.ErrorLog("ExporterData.TimetableMissing".PadRight(10) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
        End Try
        Return DataList
    End Function

    Private Function TimetableLimits(ByVal EmplCodes As List(Of String)) As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))
        Try
            Dim TimetableLowerLimit As Decimal = GetString("TimetableLowerLimit")
            Dim TimetableUpperLimit As Decimal = GetString("TimetableUpperLimit")

            Dim AllTimetables = (From t In dataImported.Timetables _
                                Where t.SnapshotID.Equals(SnapID) _
                                And (t.TimeIn IsNot Nothing And t.TimeOut IsNot Nothing) _
                                Select t.EmployeeCode, t.EmployeeName, t.TimetableDate, t.TimeIn, t.TimeOut).Distinct

            If EmplCodes.Count > 0 Then
                AllTimetables = AllTimetables.Where(Function(f) EmplCodes.Contains(f.EmployeeCode))
            End If

            If JustAlert Then
                Dim LineList As New List(Of String)
                Dim TriggerString As String = ""

                For Each tmt In AllTimetables.OrderBy(Function(f) f.EmployeeName).OrderBy(Function(p) p.TimetableDate)

                    Dim DateIn As DateTime = tmt.TimetableDate

                    Dim DateOut As DateTime
                    If tmt.TimeOut > tmt.TimetableDate Then
                        DateOut = CDate(tmt.TimetableDate).AddDays(1)
                    Else
                        DateOut = tmt.TimetableDate
                    End If

                    DateIn = DateIn.AddHours(CDate(tmt.TimeIn).Hour).AddMinutes(CDate(tmt.TimeIn).Minute)
                    DateOut = DateOut.AddHours(CDate(tmt.TimeOut).Hour).AddMinutes(CDate(tmt.TimeOut).Minute)

                    If DateDiff(DateInterval.Minute, DateIn, DateOut) < TimetableLowerLimit * 60 Then
                        TriggerString &= String.Format("Λιγότερο από {0} ώρες για {1} - {2} : {3}-{4}", _
                                                       TimetableLowerLimit, tmt.EmployeeName, _
                                                       CDate(tmt.TimetableDate).ToShortDateString, _
                                                       CDate(tmt.TimeIn).ToShortTimeString, CDate(tmt.TimeOut).ToShortTimeString) & "<br/>"
                    ElseIf DateDiff(DateInterval.Minute, DateIn, DateOut) > TimetableUpperLimit * 60 Then
                        TriggerString &= String.Format("Περισσότερο από {0} ώρες για {1} - {2} : {3}-{4}", _
                                                       TimetableUpperLimit, tmt.EmployeeName, _
                                                       CDate(tmt.TimetableDate).ToShortDateString, _
                                                       CDate(tmt.TimeIn).ToShortTimeString, CDate(tmt.TimeOut).ToShortTimeString) & "<br/>"
                    End If
                Next
                LineList.Add(TriggerString)
                DataList.Add(LineList)
            Else
                Dim HeaderRow As New List(Of String)
                HeaderRow.Add("ΥΠΑΛΛΗΛΟΣ")
                HeaderRow.Add("ΗΜΕΡΟΜΗΝΙΑ")
                HeaderRow.Add("ΕΙΣΟΔΟΣ")
                HeaderRow.Add("ΕΞΟΔΟΣ")
                HeaderRow.Add("ΩΡΕΣ")
                DataList.Add(HeaderRow)

                For Each tmt In AllTimetables.OrderBy(Function(f) f.EmployeeName).OrderBy(Function(p) p.TimetableDate)

                    Dim DateIn As DateTime = tmt.TimetableDate

                    Dim DateOut As DateTime
                    If tmt.TimeOut > tmt.TimetableDate Then
                        DateOut = CDate(tmt.TimetableDate).AddDays(1)
                    Else
                        DateOut = tmt.TimetableDate
                    End If

                    DateIn = DateIn.AddHours(CDate(tmt.TimeIn).Hour).AddMinutes(CDate(tmt.TimeIn).Minute)
                    DateOut = DateOut.AddHours(CDate(tmt.TimeOut).Hour).AddMinutes(CDate(tmt.TimeOut).Minute)

                    If DateDiff(DateInterval.Minute, DateIn, DateOut) < TimetableLowerLimit * 60 OrElse _
                    DateDiff(DateInterval.Minute, DateIn, DateOut) > TimetableUpperLimit * 60 Then
                        Dim RowList As New List(Of String)
                        RowList.Add(tmt.EmployeeName)
                        RowList.Add(tmt.TimetableDate)
                        RowList.Add(tmt.TimeIn)
                        RowList.Add(tmt.TimeOut)
                        RowList.Add(DurationDescription(tmt.TimeIn, tmt.TimeOut))
                        DataList.Add(RowList)
                    End If
                Next
            End If
        Catch ex As Exception
            SvSys.ErrorLog("ExporterData.TimetableLimits".PadRight(10) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
        End Try
        Return DataList
    End Function

    Private Function TimetableDriversOvertime(ByVal EmplCodes As List(Of String)) As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))
        Try
            Dim TimetableDriversLimit As Decimal = Convert.ToDecimal(GetString("TimetableDriversLimit"))
            If TimetableDriversLimit > 100 Then
                TimetableDriversLimit = Convert.ToDecimal(GetString("TimetableDriversLimit").Replace(".", ","))
            End If

            Dim AllTimetables = (From t In dataImported.Timetables _
                                Where t.SnapshotID.Equals(SnapID) _
                                And (t.TimeIn IsNot Nothing And t.TimeOut IsNot Nothing) _
                                And t.IsDriver = True _
                                Select t.EmployeeCode, t.EmployeeName, t.TimetableDate, t.TimeIn, t.TimeOut).Distinct

            If EmplCodes.Count > 0 Then
                AllTimetables = AllTimetables.Where(Function(f) EmplCodes.Contains(f.EmployeeCode))
            End If

            If JustAlert Then
                Dim LineList As New List(Of String)
                Dim TriggerString As String = ""

                For Each tmt In AllTimetables.OrderBy(Function(f) f.EmployeeName).OrderBy(Function(p) p.TimetableDate)

                    Dim DateIn As DateTime = tmt.TimetableDate

                    Dim DateOut As DateTime
                    If tmt.TimeOut > tmt.TimetableDate Then
                        DateOut = CDate(tmt.TimetableDate).AddDays(1)
                    Else
                        DateOut = tmt.TimetableDate
                    End If

                    DateIn = DateIn.AddHours(CDate(tmt.TimeIn).Hour).AddMinutes(CDate(tmt.TimeIn).Minute)
                    DateOut = DateOut.AddHours(CDate(tmt.TimeOut).Hour).AddMinutes(CDate(tmt.TimeOut).Minute)

                    If DateDiff(DateInterval.Minute, DateIn, DateOut) > TimetableDriversLimit * 60 Then
                        TriggerString &= String.Format("Περισσότερο από {0} ώρες για {1} - {2} : {3}-{4}", _
                                                       TimetableDriversLimit, tmt.EmployeeName, _
                                                       CDate(tmt.TimetableDate).ToShortDateString, _
                                                       CDate(tmt.TimeIn).ToShortTimeString, CDate(tmt.TimeOut).ToShortTimeString) & "<br/>"
                    End If
                Next
                LineList.Add(TriggerString)
                DataList.Add(LineList)
            Else
                Dim HeaderRow As New List(Of String)
                HeaderRow.Add("ΥΠΑΛΛΗΛΟΣ")
                HeaderRow.Add("ΗΜΕΡΟΜΗΝΙΑ")
                HeaderRow.Add("ΕΙΣΟΔΟΣ")
                HeaderRow.Add("ΕΞΟΔΟΣ")
                HeaderRow.Add("ΩΡΕΣ")
                DataList.Add(HeaderRow)

                For Each tmt In AllTimetables.OrderBy(Function(f) f.EmployeeName).OrderBy(Function(p) p.TimetableDate)

                    Dim DateIn As DateTime = tmt.TimetableDate

                    Dim DateOut As DateTime
                    If tmt.TimeOut > tmt.TimetableDate Then
                        DateOut = CDate(tmt.TimetableDate).AddDays(1)
                    Else
                        DateOut = tmt.TimetableDate
                    End If

                    DateIn = DateIn.AddHours(CDate(tmt.TimeIn).Hour).AddMinutes(CDate(tmt.TimeIn).Minute)
                    DateOut = DateOut.AddHours(CDate(tmt.TimeOut).Hour).AddMinutes(CDate(tmt.TimeOut).Minute)

                    If DateDiff(DateInterval.Minute, DateIn, DateOut) > TimetableDriversLimit * 60 Then
                        Dim RowList As New List(Of String)
                        RowList.Add(tmt.EmployeeName)
                        RowList.Add(tmt.TimetableDate)
                        RowList.Add(tmt.TimeIn)
                        RowList.Add(tmt.TimeOut)
                        RowList.Add(DurationDescription(tmt.TimeIn, tmt.TimeOut))
                        DataList.Add(RowList)
                    End If
                Next
            End If
        Catch ex As Exception
            SvSys.ErrorLog("ExporterData.TimetableDriversOvertime".PadRight(10) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
        End Try
        Return DataList
    End Function

    Private Function TimetableEmployeesOvertime(ByVal EmplCodes As List(Of String)) As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))
        Try
            Dim TimetableEmployeesLimit As Decimal = GetString("TimetableEmployeesLimit")

            Dim AllTimetables = (From t In dataImported.Timetables _
                                Where t.SnapshotID.Equals(SnapID) _
                                And (t.TimeIn IsNot Nothing And t.TimeOut IsNot Nothing) _
                                And t.IsDriver = False _
                                Select t.EmployeeCode, t.EmployeeName, t.TimetableDate, t.TimeIn, t.TimeOut).Distinct

            If EmplCodes.Count > 0 Then
                AllTimetables = AllTimetables.Where(Function(f) EmplCodes.Contains(f.EmployeeCode))
            End If

            If JustAlert Then
                Dim LineList As New List(Of String)
                Dim TriggerString As String = ""

                For Each tmt In AllTimetables.OrderBy(Function(f) f.EmployeeName).OrderBy(Function(p) p.TimetableDate)

                    Dim DateIn As DateTime = tmt.TimetableDate

                    Dim DateOut As DateTime
                    If tmt.TimeOut > tmt.TimetableDate Then
                        DateOut = CDate(tmt.TimetableDate).AddDays(1)
                    Else
                        DateOut = tmt.TimetableDate
                    End If

                    DateIn = DateIn.AddHours(CDate(tmt.TimeIn).Hour).AddMinutes(CDate(tmt.TimeIn).Minute)
                    DateOut = DateOut.AddHours(CDate(tmt.TimeOut).Hour).AddMinutes(CDate(tmt.TimeOut).Minute)

                    If DateDiff(DateInterval.Minute, DateIn, DateOut) > TimetableEmployeesLimit * 60 Then
                        TriggerString &= String.Format("Περισσότερο από {0} ώρες για {1} - {2} : {3}-{4}", _
                                                       TimetableEmployeesLimit, tmt.EmployeeName, _
                                                       CDate(tmt.TimetableDate).ToShortDateString, _
                                                       CDate(tmt.TimeIn).ToShortTimeString, CDate(tmt.TimeOut).ToShortTimeString) & "<br/>"
                    End If
                Next
                LineList.Add(TriggerString)
                DataList.Add(LineList)
            Else
                Dim HeaderRow As New List(Of String)
                HeaderRow.Add("ΥΠΑΛΛΗΛΟΣ")
                HeaderRow.Add("ΗΜΕΡΟΜΗΝΙΑ")
                HeaderRow.Add("ΕΙΣΟΔΟΣ")
                HeaderRow.Add("ΕΞΟΔΟΣ")
                HeaderRow.Add("ΩΡΕΣ")
                DataList.Add(HeaderRow)

                For Each tmt In AllTimetables.OrderBy(Function(f) f.EmployeeName).OrderBy(Function(p) p.TimetableDate)

                    Dim DateIn As DateTime = tmt.TimetableDate

                    Dim DateOut As DateTime
                    If tmt.TimeOut > tmt.TimetableDate Then
                        DateOut = CDate(tmt.TimetableDate).AddDays(1)
                    Else
                        DateOut = tmt.TimetableDate
                    End If

                    DateIn = DateIn.AddHours(CDate(tmt.TimeIn).Hour).AddMinutes(CDate(tmt.TimeIn).Minute)
                    DateOut = DateOut.AddHours(CDate(tmt.TimeOut).Hour).AddMinutes(CDate(tmt.TimeOut).Minute)

                    If DateDiff(DateInterval.Minute, DateIn, DateOut) > TimetableEmployeesLimit * 60 Then
                        Dim RowList As New List(Of String)
                        RowList.Add(tmt.EmployeeName)
                        RowList.Add(tmt.TimetableDate)
                        RowList.Add(tmt.TimeIn)
                        RowList.Add(tmt.TimeOut)
                        RowList.Add(DurationDescription(tmt.TimeIn, tmt.TimeOut))
                        DataList.Add(RowList)
                    End If
                Next
            End If
        Catch ex As Exception
            SvSys.ErrorLog("ExporterData.TimetableDriversOvertime".PadRight(10) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
        End Try
        Return DataList
    End Function

    Private Function TimetableDriversSundays(ByVal EmplCodes As List(Of String)) As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))
        Try
            Dim TimetableDriversSundayAmount As Decimal = GetString("TimetableDriversSundayAmount")

            Dim AllTimetables = (From t In dataImported.Timetables _
                                Where t.SnapshotID.Equals(SnapID) _
                                And (t.TimeIn IsNot Nothing And t.TimeOut IsNot Nothing) _
                                And t.IsDriver = True _
                                Select t.EmployeeCode, t.EmployeeName, t.TimetableDate, t.TimeIn, t.TimeOut).Distinct

            If EmplCodes.Count > 0 Then
                AllTimetables = AllTimetables.Where(Function(f) EmplCodes.Contains(f.EmployeeCode))
            End If

            If JustAlert Then
                Dim LineList As New List(Of String)
                Dim TriggerString As String = ""

                For Each tmt In AllTimetables.OrderBy(Function(f) f.EmployeeName).OrderBy(Function(p) p.TimetableDate)

                    If CDate(tmt.TimetableDate).DayOfWeek = DayOfWeek.Sunday Then
                        TriggerString &= String.Format("Εργασία Κυριακή για {0} - {1} : {2}€", _
                                                       tmt.EmployeeName, _
                                                       CDate(tmt.TimetableDate).ToShortDateString, _
                                                       TimetableDriversSundayAmount) & "<br/>"
                    End If
                Next
                LineList.Add(TriggerString)
                DataList.Add(LineList)
            Else
                Dim HeaderRow As New List(Of String)
                HeaderRow.Add("ΥΠΑΛΛΗΛΟΣ")
                HeaderRow.Add("ΗΜΕΡΟΜΗΝΙΑ")
                HeaderRow.Add("ΑΠΟΖΗΜΙΩΣΗ")
                DataList.Add(HeaderRow)

                For Each tmt In AllTimetables.OrderBy(Function(f) f.EmployeeName).OrderBy(Function(p) p.TimetableDate)

                    If CDate(tmt.TimetableDate).DayOfWeek = DayOfWeek.Sunday Then
                        Dim RowList As New List(Of String)
                        RowList.Add(tmt.EmployeeName)
                        RowList.Add(tmt.TimetableDate)
                        RowList.Add(TimetableDriversSundayAmount & "€")
                        DataList.Add(RowList)
                    End If

                Next
            End If
        Catch ex As Exception
            SvSys.ErrorLog("ExporterData.TimetableDriversSundays".PadRight(10) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
        End Try
        Return DataList
    End Function

    Private Function TimetableDriversNights(ByVal EmplCodes As List(Of String)) As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))
        Try
            Dim TimetableDriversNight1 As Decimal = GetString("TimetableDriversNight1")
            Dim TimetableDriversNight2 As Decimal = GetString("TimetableDriversNight2")
            Dim TimetableDriversNight1Amount As Decimal = GetString("TimetableDriversNight1Amount")
            Dim TimetableDriversNight2Amount As Decimal = GetString("TimetableDriversNight2Amount")

            Dim AllTimetables = (From t In dataImported.Timetables _
                                Where t.SnapshotID.Equals(SnapID) _
                                And (t.TimeIn IsNot Nothing) _
                                And t.IsDriver = True _
                                Select t.EmployeeCode, t.EmployeeName, t.TimetableDate, t.TimeIn, t.TimeOut).Distinct

            If EmplCodes.Count > 0 Then
                AllTimetables = AllTimetables.Where(Function(f) EmplCodes.Contains(f.EmployeeCode))
            End If

            If JustAlert Then
                Dim LineList As New List(Of String)
                Dim TriggerString As String = ""

                For Each tmt In AllTimetables.OrderBy(Function(f) f.EmployeeName).OrderBy(Function(p) p.TimetableDate)
                    If tmt.TimeOut IsNot Nothing Then
                        If tmt.TimeOut > tmt.TimetableDate AndAlso CDate(tmt.TimeOut).Hour >= TimetableDriversNight1 And CDate(tmt.TimeOut).Hour < TimetableDriversNight2 Then
                            TriggerString &= String.Format("Λήξη μετά τις 0{0}:00 για {1} - {2} : {3}, {4}€", _
                                                           TimetableDriversNight1, tmt.EmployeeName, _
                                                           CDate(tmt.TimetableDate).ToShortDateString, _
                                                           CDate(tmt.TimeOut).ToShortTimeString, TimetableDriversNight1Amount) & "<br/>"
                        ElseIf tmt.TimeOut > tmt.TimetableDate AndAlso CDate(tmt.TimeOut).Hour >= TimetableDriversNight2 Then
                            TriggerString &= String.Format("Λήξη μετά τις 0{0}:00 για {1} - {2} : {3}, {4}€", _
                                                           TimetableDriversNight2, tmt.EmployeeName, _
                                                           CDate(tmt.TimetableDate).ToShortDateString, _
                                                           CDate(tmt.TimeOut).ToShortTimeString, TimetableDriversNight2Amount) & "<br/>"
                        End If
                    Else

                        Dim NextExits = (From t In dataImported.Timetables _
                                         Where (t.TimeOut IsNot Nothing) _
                                         And t.EmployeeCode.Equals(tmt.EmployeeCode) _
                                         And t.TimetableDate > tmt.TimetableDate _
                                         Order By t.TimetableDate _
                                         Select t.EmployeeCode, t.EmployeeName, t.TimetableDate, t.TimeIn, t.TimeOut).Distinct

                        For Each ext In NextExits
                            If ext.TimeIn IsNot Nothing Then
                                Exit For
                            Else
                                If ext.TimeOut IsNot Nothing Then
                                    For i = 1 To DateDiff(DateInterval.Day, CDate(tmt.TimeIn), CDate(ext.TimeOut))
                                        If CDate(tmt.TimeOut).Hour >= TimetableDriversNight1 And CDate(tmt.TimeOut).Hour < TimetableDriversNight2 Then
                                            TriggerString &= String.Format("Λήξη μετά τις 0{0}:00 για {1} - {2} : {3}, {4}€", _
                                                                           TimetableDriversNight1, tmt.EmployeeName, _
                                                                           CDate(tmt.TimetableDate).AddDays(i - 1).ToShortDateString, _
                                                                           CDate(ext.TimeOut).ToShortTimeString, TimetableDriversNight1Amount) & "<br/>"
                                        ElseIf CDate(tmt.TimeOut).Hour >= TimetableDriversNight2 Then
                                            TriggerString &= String.Format("Λήξη μετά τις 0{0}:00 για {1} - {2} : {3}, {4}€", _
                                                                           TimetableDriversNight2, tmt.EmployeeName, _
                                                                           CDate(tmt.TimetableDate).AddDays(i - 1).ToShortDateString, _
                                                                           CDate(ext.TimeOut).ToShortTimeString, TimetableDriversNight2Amount) & "<br/>"
                                        End If
                                    Next

                                    Exit For
                                End If
                            End If
                        Next

                    End If
                Next
                LineList.Add(TriggerString)
                DataList.Add(LineList)
            Else
                Dim HeaderRow As New List(Of String)
                HeaderRow.Add("ΥΠΑΛΛΗΛΟΣ")
                HeaderRow.Add("ΗΜΕΡΟΜΗΝΙΑ")
                HeaderRow.Add("ΕΞΟΔΟΣ")
                HeaderRow.Add("ΑΠΟΖΗΜΙΩΣΗ")
                DataList.Add(HeaderRow)

                For Each tmt In AllTimetables.OrderBy(Function(f) f.EmployeeName).OrderBy(Function(p) p.TimetableDate)
                    If tmt.TimeOut IsNot Nothing Then
                        If tmt.TimeOut > tmt.TimetableDate AndAlso CDate(tmt.TimeOut).Hour >= TimetableDriversNight1 And CDate(tmt.TimeOut).Hour < TimetableDriversNight2 Then
                            Dim RowList As New List(Of String)
                            RowList.Add(tmt.EmployeeName)
                            RowList.Add(tmt.TimetableDate)
                            RowList.Add(CDate(tmt.TimeOut).ToShortTimeString)
                            RowList.Add(TimetableDriversNight1Amount & "€")
                            DataList.Add(RowList)
                        ElseIf tmt.TimeOut > tmt.TimetableDate AndAlso CDate(tmt.TimeOut).Hour >= TimetableDriversNight2 Then
                            Dim RowList As New List(Of String)
                            RowList.Add(tmt.EmployeeName)
                            RowList.Add(tmt.TimetableDate)
                            RowList.Add(CDate(tmt.TimeOut).ToShortTimeString)
                            RowList.Add(TimetableDriversNight2Amount & "€")
                            DataList.Add(RowList)
                        End If
                    Else


                        Dim NextExits = (From t In dataImported.Timetables _
                 Where (t.TimeOut IsNot Nothing) _
                 And t.EmployeeCode.Equals(tmt.EmployeeCode) _
                 And t.TimetableDate > tmt.TimetableDate _
                 Order By t.TimetableDate _
                 Select t.EmployeeCode, t.EmployeeName, t.TimetableDate, t.TimeIn, t.TimeOut).Distinct

                        For Each ext In NextExits
                            If ext.TimeIn IsNot Nothing Then
                                Exit For
                            Else
                                If ext.TimeOut IsNot Nothing Then
                                    For i = 1 To DateDiff(DateInterval.Day, CDate(tmt.TimeIn), CDate(ext.TimeOut))
                                        If CDate(tmt.TimeOut).Hour >= TimetableDriversNight1 And CDate(tmt.TimeOut).Hour < TimetableDriversNight2 Then
                                            Dim RowList As New List(Of String)
                                            RowList.Add(tmt.EmployeeName)
                                            RowList.Add(CDate(tmt.TimetableDate).AddDays(i - 1).ToShortDateString)
                                            RowList.Add(CDate(ext.TimeOut).ToShortTimeString)
                                            RowList.Add(TimetableDriversNight1Amount & "€")
                                            DataList.Add(RowList)
                                        ElseIf CDate(tmt.TimeOut).Hour >= TimetableDriversNight2 Then

                                            Dim RowList As New List(Of String)
                                            RowList.Add(tmt.EmployeeName)
                                            RowList.Add(CDate(tmt.TimetableDate).AddDays(i - 1).ToShortDateString)
                                            RowList.Add(CDate(ext.TimeOut).ToShortTimeString)
                                            RowList.Add(TimetableDriversNight2Amount & "€")
                                            DataList.Add(RowList)
                                        End If
                                    Next

                                    Exit For
                                End If
                            End If
                        Next

                    End If
                Next
            End If
        Catch ex As Exception
            SvSys.ErrorLog("ExporterData.TimetableDriversNights".PadRight(10) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
        End Try
        Return DataList
    End Function

    Private Function TimetableContinuous(ByVal EmplCodes As List(Of String)) As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))
        Try
            Dim AllTimetables = (From t In dataImported.Timetables _
                                Where t.SnapshotID.Equals(SnapID) _
                                And (t.TimeIn IsNot Nothing And t.TimeOut IsNot Nothing) _
                                Select t.EmployeeCode, t.EmployeeName, t.TimetableDate, t.TimeIn, t.TimeOut).Distinct

            If EmplCodes.Count > 0 Then
                AllTimetables = AllTimetables.Where(Function(f) EmplCodes.Contains(f.EmployeeCode))
            End If

            If JustAlert Then
                Dim LineList As New List(Of String)
                Dim TriggerString As String = ""

                For Each tmt In AllTimetables.OrderBy(Function(f) f.EmployeeName).OrderBy(Function(p) p.TimetableDate)

                    If dataImported.fContinuousWork(tmt.EmployeeCode, tmt.TimetableDate) Then
                        Dim CurrentDate As Date = CDate(tmt.TimetableDate)

                        Dim Day6 = (From t In dataImported.Timetables _
                        Where (t.TimeIn IsNot Nothing And t.TimeOut IsNot Nothing) _
                        And t.EmployeeCode.Equals(tmt.EmployeeCode) _
                        And t.TimetableDate.Equals(CurrentDate.AddDays(-1)) _
                        And t.TimetableDate.Equals(CurrentDate.AddDays(-2)) _
                        And t.TimetableDate.Equals(CurrentDate.AddDays(-3)) _
                        And t.TimetableDate.Equals(CurrentDate.AddDays(-4)) _
                        And t.TimetableDate.Equals(CurrentDate.AddDays(-5)) _
                        Select t.EmployeeCode, t.EmployeeName, t.TimetableDate, t.TimeIn, t.TimeOut).FirstOrDefault

                        If Day6 IsNot Nothing Then

                            Dim Day7 = (From t In dataImported.Timetables _
                            Where (t.TimeIn IsNot Nothing And t.TimeOut IsNot Nothing) _
                            And t.EmployeeCode.Equals(tmt.EmployeeCode) _
                            And t.TimetableDate.Equals(CurrentDate.AddDays(-6)) _
                            Select t.EmployeeCode, t.EmployeeName, t.TimetableDate, t.TimeIn, t.TimeOut).FirstOrDefault
                            If Day7 IsNot Nothing Then
                                TriggerString &= String.Format("Εργασία για 7η ημέρα για {0}", _
                                                               tmt.EmployeeName) & "<br/>"
                            Else
                                TriggerString &= String.Format("Εργασία για 6η ημέρα για {0}", _
                               tmt.EmployeeName) & "<br/>"
                            End If
                        End If
                    End If
                Next
                LineList.Add(TriggerString)
                DataList.Add(LineList)
            Else
                Dim HeaderRow As New List(Of String)
                HeaderRow.Add("ΥΠΑΛΛΗΛΟΣ")
                HeaderRow.Add("ΣΥΝΕΧΟΜΕΝΗ ΗΜΕΡΑ")
                HeaderRow.Add("ΗΜΕΡΟΜΗΝΙΑ")
                DataList.Add(HeaderRow)

                For Each tmt In AllTimetables.OrderBy(Function(f) f.EmployeeName).OrderBy(Function(p) p.TimetableDate)

                    If dataImported.fContinuousWork(tmt.EmployeeCode, tmt.TimetableDate) Then
                        Dim Day6 = (From t In dataImported.Timetables _
                        Where (t.TimeIn IsNot Nothing And t.TimeOut IsNot Nothing) _
                        And t.EmployeeCode.Equals(tmt.EmployeeCode) _
                        And t.TimetableDate.Equals(CDate(tmt.TimetableDate).AddDays(-5)) _
                        Select t.EmployeeCode, t.EmployeeName, t.TimetableDate, t.TimeIn, t.TimeOut).FirstOrDefault

                        If Day6 IsNot Nothing Then

                            Dim Day7 = (From t In dataImported.Timetables _
                            Where (t.TimeIn IsNot Nothing And t.TimeOut IsNot Nothing) _
                            And t.EmployeeCode.Equals(tmt.EmployeeCode) _
                            And t.TimetableDate.Equals(CDate(tmt.TimetableDate).AddDays(-6)) _
                            Select t.EmployeeCode, t.EmployeeName, t.TimetableDate, t.TimeIn, t.TimeOut).FirstOrDefault
                            If Day7 IsNot Nothing Then
                                Dim RowList7 As New List(Of String)
                                RowList7.Add(tmt.EmployeeName)
                                RowList7.Add("Εργασία για 7η ημέρα")
                                RowList7.Add(tmt.TimetableDate)
                                DataList.Add(RowList7)
                            Else
                                Dim RowList6 As New List(Of String)
                                RowList6.Add(tmt.EmployeeName)
                                RowList6.Add("Εργασία για 6η ημέρα")
                                RowList6.Add(tmt.TimetableDate)
                                DataList.Add(RowList6)
                            End If
                        End If

                    End If

                Next
            End If
        Catch ex As Exception
            SvSys.ErrorLog("ExporterData.TimetableContinuous".PadRight(10) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
        End Try
        Return DataList
    End Function

    Private Function TimetableDriversGap(ByVal EmplCodes As List(Of String)) As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))
        Try
            Dim Gap As Decimal = GetString("TimetableDriversGap")

            Dim AllTimetables = (From t In dataImported.Timetables _
                                Where t.SnapshotID.Equals(SnapID) _
                                And (t.TimeIn IsNot Nothing) _
                                And t.IsDriver = True _
                                Select t.EmployeeCode, t.EmployeeName, t.TimetableDate, t.TimeIn).Distinct

            If EmplCodes.Count > 0 Then
                AllTimetables = AllTimetables.Where(Function(f) EmplCodes.Contains(f.EmployeeCode))
            End If

            If JustAlert Then
                Dim LineList As New List(Of String)
                Dim TriggerString As String = ""

                For Each tmt In AllTimetables.OrderBy(Function(f) f.EmployeeName).OrderBy(Function(p) p.TimetableDate)
                    Dim PreviousExit = (From t In dataImported.Timetables _
                    Where (t.TimeOut IsNot Nothing) _
                    And t.EmployeeCode.Equals(tmt.EmployeeCode) _
                    And CDate(t.TimeOut) < CDate(tmt.TimeIn) _
                    And CDate(t.TimeOut).AddMinutes(60 * Gap) > CDate(tmt.TimeIn) _
                    Select t.EmployeeCode, t.EmployeeName, t.TimetableDate, t.TimeIn, t.TimeOut).FirstOrDefault

                    If PreviousExit IsNot Nothing Then
                        TriggerString &= String.Format("Εργασία σε λιγότερο από {0} ώρες : {1} - Έξοδος:{2} {3} , Είσοδος:{4} {5}", _
                       Gap, tmt.EmployeeName, _
                       CDate(PreviousExit.TimetableDate).ToShortDateString, CDate(PreviousExit.TimeOut).ToShortTimeString, _
                       CDate(tmt.TimetableDate).ToShortDateString, CDate(tmt.TimeIn).ToShortTimeString _
                       ) & "<br/>"
                    End If
                Next
                LineList.Add(TriggerString)
                DataList.Add(LineList)
            Else
                Dim HeaderRow As New List(Of String)
                HeaderRow.Add("ΥΠΑΛΛΗΛΟΣ")
                HeaderRow.Add("ΕΞΟΔΟΣ")
                HeaderRow.Add("ΕΙΣΟΔΟΣ")
                DataList.Add(HeaderRow)

                For Each tmt In AllTimetables.OrderBy(Function(f) f.EmployeeName).OrderBy(Function(p) p.TimetableDate)
                    Dim PreviousExit = (From t In dataImported.Timetables _
                    Where (t.TimeOut IsNot Nothing) _
                    And t.EmployeeCode.Equals(tmt.EmployeeCode) _
                    And CDate(t.TimeOut) < CDate(tmt.TimeIn) _
                    And CDate(t.TimeOut).AddMinutes(60 * Gap) > CDate(tmt.TimeIn) _
                    Select t.EmployeeCode, t.EmployeeName, t.TimetableDate, t.TimeIn, t.TimeOut).FirstOrDefault

                    If PreviousExit IsNot Nothing Then
                        Dim RowList As New List(Of String)
                        RowList.Add(tmt.EmployeeName)
                        RowList.Add(CDate(PreviousExit.TimetableDate).ToShortDateString & " " & CDate(PreviousExit.TimeOut).ToShortTimeString)
                        RowList.Add(CDate(tmt.TimetableDate).ToShortDateString & " " & CDate(tmt.TimeIn).ToShortTimeString)
                        DataList.Add(RowList)
                    End If
                Next
            End If
        Catch ex As Exception
            SvSys.ErrorLog("ExporterData.TimetableDriversGap".PadRight(10) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
        End Try
        Return DataList
    End Function

    Private Function TimetableDriversReport(ByVal EmplCodes As List(Of String), ByVal FullReport As Boolean) As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))
        Try

            Dim AllTimetables = (From t In dataImported.Timetables _
                                Where t.SnapshotID.Equals(SnapID) _
                                And t.TimeIn IsNot Nothing _
                                And t.IsDriver = True _
                                Select t.EmployeeCode, t.EmployeeName, t.TimetableDate, t.TimeIn, t.TimeOut).Distinct

            If EmplCodes.Count > 0 Then
                AllTimetables = AllTimetables.Where(Function(f) EmplCodes.Contains(f.EmployeeCode))
            End If

            If JustAlert Then
                Dim LineList As New List(Of String)
                Dim TriggerString As String = ""

                For Each tmt In AllTimetables.OrderBy(Function(f) f.EmployeeName).OrderBy(Function(p) p.TimetableDate)
                    If Not FullReport Then
                        TriggerString &= String.Format("{0}, είσοδος {1}", _
                                                       tmt.EmployeeName, CDate(tmt.TimeIn).ToShortTimeString) & "<br/>"
                    Else
                        TriggerString &= String.Format("{0}, {1} - {2}", _
                                                       tmt.EmployeeName, CDate(tmt.TimeIn).ToShortTimeString, CDate(tmt.TimeOut).ToShortTimeString) & "<br/>"
                    End If
                Next
                LineList.Add(TriggerString)
                DataList.Add(LineList)
            Else
                Dim HeaderRow As New List(Of String)
                HeaderRow.Add("ΥΠΑΛΛΗΛΟΣ")
                HeaderRow.Add("ΕΙΣΟΔΟΣ")
                If FullReport Then
                    HeaderRow.Add("ΕΞΟΔΟΣ")
                    HeaderRow.Add("ΩΡΕΣ")
                End If
                DataList.Add(HeaderRow)

                For Each tmt In AllTimetables.OrderBy(Function(f) f.EmployeeName).OrderBy(Function(p) p.TimetableDate)
                    Dim RowList As New List(Of String)
                    RowList.Add(tmt.EmployeeName)
                    RowList.Add(If(tmt.TimeIn Is Nothing, "", CDate(tmt.TimeIn).ToShortTimeString))
                    If FullReport Then
                        RowList.Add(If(tmt.TimeOut Is Nothing, "", CDate(tmt.TimeOut).ToShortTimeString))
                        RowList.Add(DurationDescription(tmt.TimeIn, tmt.TimeOut))
                    End If
                    DataList.Add(RowList)
                Next
            End If
        Catch ex As Exception
            SvSys.ErrorLog("ExporterData.TimetableDriversReport".PadRight(10) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
        End Try
        Return DataList
    End Function

    Private Function TimetableSameDayEntrances(ByVal EmplCodes As List(Of String)) As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))
        Try

            Dim AllTimetables = (From t In dataImported.Timetables _
                                Where t.SnapshotID.Equals(SnapID) _
                                And (t.TimeIn IsNot Nothing) _
                                Select t.EmployeeCode, t.EmployeeName, t.TimetableDate, t.TimeIn).Distinct

            If EmplCodes.Count > 0 Then
                AllTimetables = AllTimetables.Where(Function(f) EmplCodes.Contains(f.EmployeeCode))
            End If

            If JustAlert Then
                Dim LineList As New List(Of String)
                Dim TriggerString As String = ""

                For Each tmt In AllTimetables.OrderBy(Function(f) f.EmployeeName)
                    TriggerString &= String.Format("{0} : {1}", _
                                                   tmt.EmployeeName, CDate(tmt.TimeIn).ToShortTimeString) & "<br/>"
                Next
                LineList.Add(TriggerString)
                DataList.Add(LineList)
            Else
                Dim HeaderRow As New List(Of String)
                HeaderRow.Add("ΥΠΑΛΛΗΛΟΣ")
                HeaderRow.Add("ΕΙΣΟΔΟΣ")
                DataList.Add(HeaderRow)

                For Each tmt In AllTimetables.OrderBy(Function(f) f.EmployeeName)
                    Dim RowList As New List(Of String)
                    RowList.Add(tmt.EmployeeName)
                    RowList.Add(tmt.TimeIn)
                    DataList.Add(RowList)
                Next
            End If
        Catch ex As Exception
            SvSys.ErrorLog("ExporterData.TimetableSameDayEntrances".PadRight(10) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
        End Try
        Return DataList
    End Function

    Private Function DurationDescription(ByVal TimeIn As Date?, ByVal TimeOut As Date?) As String
        Dim Duration As String = ""
        If TimeIn IsNot Nothing AndAlso TimeOut IsNot Nothing Then
            Dim TimetableMinutes As Integer = DateDiff(DateInterval.Minute, CDate(TimeIn), CDate(TimeOut))
            Dim remainder As Integer
            Dim quotient As Integer = Math.DivRem(TimetableMinutes, 60, remainder)
            Duration = String.Format("{0}.{1}", quotient, remainder)
        End If
        Return Duration
    End Function

#End Region
#End Region
End Class
