﻿Public Class Globals

    Public Enum ScenarioTypes
        PRODUCTS = 1
        MEDIATORS = 2
        TRADERS = 3
        TRADERS_MEDS = 203
        AREAS = 4
        ENTRIES = 10
        ENTRIES_PRODS = 110
        ENTRIES_MEDS = 210
        ENTRIES_TRDS = 310
        ENTRIES_PRODS_MEDS = 1210
        ENTRIES_PRODS_TRDS = 1310
        ENTRIES_MEDS_TRDS = 2310
        ENTRIES_PRODS_MEDS_TRDS = 12310
        COMPANIES = 11
        AREABALANCE = 12
        AREABALANCE_AREAS = 412
        AREABALANCE_PRODS = 112
        AREABALANCE_AREAS_PRODS = 1412
        FISCALS = 13
        APPS = 14
        APPS_MEDS = 214
        APPS_TRDS = 314
        APPS_MEDS_TRDS = 2314
        ASSIGNS = 15
        ASSIGNS_MEDS = 215
        ASSIGNS_TRDS = 315
        ASSIGNS_MEDS_TRDS = 2315
        EXPENSES = 16
        EXPENSES_MEDS = 216
        EXPENSES_TRDS = 316
        EXPENSES_MEDS_TRDS = 2316
        TRANSACTIONS = 17
        TRANSACTIONS_TRDS = 317
        VAT = 18
        BANKBALANCE = 19
    End Enum

    Public Enum ReportTypes
        GROSS_PROFIT = 1
        TURNOVER = 2
        TURNOVER_MONTH_LY = 3
        TURNOVER_YEAR_LY = 4
        STOCK = 5
        STOCK_GREATER = 6
        STOCK_LESS = 7
        MED_ORDERS = 8
        MED_ORDERS_LY = 9
        SALES_BUYS = 10
        TRADERS_REST = 11
        TRADERS_REST_GREATER = 12
        SALES = 13
        TRANSIT = 14
        CUSTOMERS_ACTIVE = 15
        CUSTOMERS_ACTIVE_LY = 16
        CUSTOMERS_CONTRACT = 31
        CUSTOMERS_CONTRACT_LY = 32
        TURNOVER_SERVICE = 33
        TURNOVER_SERVICE_LY = 34
        TECH_HOURS = 35
        TECH_HOURS_LY = 36
        OFFERS = 37
        APPS_OFFERS_ORDERS = 38
        MEAN_HOUR_CHARGE = 39
        MEAN_HOUR_CHARGE_LESS = 40
        OPEN_REPAIRS_GREATER = 41
        TASK_COMPLETE_DAYS = 42
        DAILY_APPS = 43
        TURNOVER_DAILY = 44
        MONEY_COLLECTION = 61
        PAYMENTS = 62
        CHECKOUT = 63
        VAT = 64
        CUSTOMERS_OPEN_REST = 65
        CAP_LIMIT = 66
        CREDIT_LIMIT = 67
        BANK_REST = 68
        EXPENSES = 69
        TURNOVER_FINANCIAL = 70
        SUPPLIERS_ORDERS_GREATER = 71
    End Enum

    Private _Reports As Dictionary(Of Short, String)
    Public Property Reports() As Dictionary(Of Short, String)
        Get
            Return _Reports
        End Get
        Set(ByVal value As Dictionary(Of Short, String))
            _Reports = value
        End Set
    End Property

    Public Sub New()
        _Reports = New Dictionary(Of Short, String)
        Reports.Add(ReportTypes.GROSS_PROFIT, "Μικτό κέρδος")
        Reports.Add(ReportTypes.TURNOVER, "Τζίρος")
        Reports.Add(ReportTypes.TURNOVER_MONTH_LY, "Τζίρος μήνα συγκριτικά με πέρσι")
        Reports.Add(ReportTypes.TURNOVER_YEAR_LY, "Τζίρος έτος συγκριτικά με πέρσι")
        Reports.Add(ReportTypes.STOCK, "Stock")
        Reports.Add(ReportTypes.STOCK_GREATER, "Stock μεγάλο")
        Reports.Add(ReportTypes.STOCK_LESS, "Stock μικρό")
        Reports.Add(ReportTypes.MED_ORDERS, "Παραγγελίες ανά πωλητή")
        Reports.Add(ReportTypes.MED_ORDERS_LY, "Παραγγελίες ανά πωλητή συγκριτικά με πέρσι")
        Reports.Add(ReportTypes.SALES_BUYS, "Πωλήσεις-αγορές")
        Reports.Add(ReportTypes.TRADERS_REST, "Υπόλοιπα πελατών")
        Reports.Add(ReportTypes.TRADERS_REST_GREATER, "Υπόλοιπα πελατών μεγάλα")
        Reports.Add(ReportTypes.SALES, "Πωλήσεις")
        Reports.Add(ReportTypes.TRANSIT, "Παραγγελίες transit")
        Reports.Add(ReportTypes.CUSTOMERS_ACTIVE, "Ενεργοί πελάτες")
        Reports.Add(ReportTypes.CUSTOMERS_ACTIVE_LY, "Ενεργοί πελάτες συγκριτικά με πέρσι")
        Reports.Add(ReportTypes.CUSTOMERS_CONTRACT, "Ενεργοί πελάτες σε σύμβαση")
        Reports.Add(ReportTypes.CUSTOMERS_CONTRACT_LY, "Ενεργοί πελάτες σε σύμβαση συγκριτικά με πέρσι")
        Reports.Add(ReportTypes.TURNOVER_SERVICE, "Τζίρος υπηρεσιών")
        Reports.Add(ReportTypes.TURNOVER_SERVICE_LY, "Τζίρος υπηρεσιών συγκριτικά με πέρσι")
        Reports.Add(ReportTypes.TECH_HOURS, "Ώρες τεχνικών")
        Reports.Add(ReportTypes.TECH_HOURS_LY, "Ώρες τεχνικών συγκριτικά με πέρσι")
        Reports.Add(ReportTypes.OFFERS, "Ποσοστό θετικών-ανοικτών προσφορών")
        Reports.Add(ReportTypes.APPS_OFFERS_ORDERS, "Αριθμός Ραντεβού-Παραγγελιών-Προσφορών")
        Reports.Add(ReportTypes.MEAN_HOUR_CHARGE, "Μέση ωριαία χρέωση")
        Reports.Add(ReportTypes.MEAN_HOUR_CHARGE_LESS, "Μικρή μέση ωριαία χρέωση")
        Reports.Add(ReportTypes.OPEN_REPAIRS_GREATER, "Ανοικτές εντολές επισκευές με πολλές ημέρες")
        Reports.Add(ReportTypes.TASK_COMPLETE_DAYS, "Χρόνος ολοκλήρωσης εργασιών")
        Reports.Add(ReportTypes.DAILY_APPS, "Ραντεβού ημέρας")
        Reports.Add(ReportTypes.TURNOVER_DAILY, "Τζίρος ημερήσιος")
        Reports.Add(ReportTypes.MONEY_COLLECTION, "Εισπράξεις")
        Reports.Add(ReportTypes.PAYMENTS, "Πληρωμές")
        Reports.Add(ReportTypes.CHECKOUT, "Ταμείο")
        Reports.Add(ReportTypes.VAT, "ΦΠΑ")
        Reports.Add(ReportTypes.CUSTOMERS_OPEN_REST, "Ανοικτό υπόλοιπο πελατών")
        Reports.Add(ReportTypes.CAP_LIMIT, "Όρια πλαφόν πελατών")
        Reports.Add(ReportTypes.CREDIT_LIMIT, "Όρια πίστωσης πελατών")
        Reports.Add(ReportTypes.BANK_REST, "Υπόλοιπα τραπεζών")
        Reports.Add(ReportTypes.EXPENSES, "Έξοδα")
        Reports.Add(ReportTypes.TURNOVER_FINANCIAL, "Μικτό κέρδος")
        Reports.Add(ReportTypes.SUPPLIERS_ORDERS_GREATER, "Παραγγελίες προμηθευτών")
    End Sub

    Public Shared Sub AllScenarios(ByVal ScenarioType As Short, ByRef OtherScenarios As String, ByRef BasicScenario As Short)
        Dim Scenario As String = ScenarioType

        Do Until Scenario.Length <= 2
            OtherScenarios &= Scenario(0)
            Scenario = Scenario.Substring(1, Scenario.Length - 1)
        Loop

        BasicScenario = Convert.ToInt16(Scenario)
    End Sub
End Class
