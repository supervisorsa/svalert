﻿Imports svGlobal.Settings
Public Module modMain
    Public dataImported As dataImportedDataContext

    Public Function ImproveData(ByVal DataType As String, ByVal DataValue As String) As Object
        Select Case DataType
            Case "Numeric"
                If DataValue Is Nothing OrElse DataValue = "" OrElse Not IsNumeric(DataValue) Then
                    Return Convert.ToDecimal(0)
                Else
                    Return Convert.ToDecimal(DataValue)
                End If
            Case "Date"
                If DataValue Is Nothing OrElse DataValue = "" Then
                    Return Nothing
                Else
                    Try
                        Return CDate(DataValue)
                    Catch
                        Return Nothing
                    End Try
                End If
            Case "Boolean"
                If DataValue Is Nothing OrElse DataValue = "" Then
                    Return False
                ElseIf DataValue = "1" Or DataValue.ToLower = "true" Then
                    Return True
                Else
                    Return False
                End If
            Case Else
                If DataValue Is Nothing Then
                    Return ""
                Else
                    Return DataValue
                End If
        End Select
    End Function

    Public Function ImproveDateData(ByVal SvTime As DateTime, ByVal SvDate As DateTime) As DateTime
        If CDate(SvTime).Date <> Date.MinValue Then
            Return SvTime
        Else
            Return SvDate.Date.AddHours(SvTime.Hour).AddMinutes(SvTime.Minute).AddSeconds(SvTime.Second)
        End If
    End Function

    Private Sub CorrectMonth(ByRef PreviousMonth As Short, ByRef PreviousYear As Short)
        If PreviousMonth = 0 Then
            PreviousMonth = 12
            PreviousYear -= 1
        End If
    End Sub

    Public Sub LoadSnapshots(ByVal SnapshotID As Guid, ByVal TaskTypeSnapshots As List(Of Guid), _
                             ByRef CurrentSnap As Snapshot, ByRef PreviousSnap As Snapshot)
        Dim PreviousYear As Short
        Dim PreviousMonth As Short
        Dim PreviousDate As Date

        If CurrentSnap Is Nothing Then
            CurrentSnap = (From t In dataImported.Snapshots _
                          Where t.ID.Equals(SnapshotID) _
                          Select t).FirstOrDefault
        End If

        If TaskTypeSnapshots.Count = 0 Then Exit Sub

        Dim CurrentDate = CurrentSnap.ExecuteDate.AddDays(-CurrentSnap.Offset)

        If CurrentSnap.IsYearly Then
            PreviousSnap = (From t In dataImported.Snapshots _
              Where t.IsYearly _
              And t.ExecuteDate.AddDays(-t.Offset).Year = CurrentDate.Year - 1 _
              And TaskTypeSnapshots.Contains(t.ID) _
              Order By t.ExecuteDate Descending _
              Select t).FirstOrDefault
        ElseIf CurrentSnap.IsMonthly Then
            PreviousMonth = CurrentSnap.ExecuteDate.Month - 1
            PreviousYear = CurrentSnap.ExecuteDate.Year
            CorrectMonth(PreviousMonth, PreviousYear)
            PreviousSnap = (From t In dataImported.Snapshots _
                          Where t.IsMonthly _
                          And t.ExecuteDate.AddDays(-t.Offset).Month = PreviousMonth _
                          And t.ExecuteDate.AddDays(-t.Offset).Year = PreviousYear _
                          And TaskTypeSnapshots.Contains(t.ID) _
                          Order By t.ExecuteDate Descending _
                          Select t).FirstOrDefault
        ElseIf CurrentSnap.IsFortnightly Then
            Dim PreviousDay As Short
            PreviousYear = CurrentSnap.ExecuteDate.Year

            If CurrentSnap.ExecuteDate.Day < 14 Then
                PreviousMonth = CurrentSnap.ExecuteDate.Month - 1
                If PreviousMonth = 2 Then
                    PreviousDay = 14
                Else
                    PreviousDay = 15
                End If
                CorrectMonth(PreviousMonth, PreviousYear)
            Else
                PreviousDay = 1
                PreviousMonth = CurrentSnap.ExecuteDate.Month
            End If

            PreviousSnap = (From t In dataImported.Snapshots _
              Where t.IsFortnightly _
              And t.ExecuteDate.AddDays(-t.Offset).Month = PreviousMonth _
              And t.ExecuteDate.AddDays(-t.Offset).Year = PreviousYear _
              And t.ExecuteDate.AddDays(-t.Offset).Day = PreviousDay _
              And TaskTypeSnapshots.Contains(t.ID) _
              Order By t.ExecuteDate Descending _
              Select t).FirstOrDefault
        ElseIf CurrentSnap.IsWeekly Then
            PreviousDate = CurrentSnap.ExecuteDate.Date.AddDays(-7)
            PreviousSnap = (From t In dataImported.Snapshots _
              Where t.IsWeekly _
              And t.ExecuteDate.AddDays(-t.Offset).Date = PreviousDate _
              And TaskTypeSnapshots.Contains(t.ID) _
              Order By t.ExecuteDate Descending _
              Select t).FirstOrDefault
        ElseIf CurrentSnap.IsDaily Then
            PreviousDate = CurrentSnap.ExecuteDate.Date.AddDays(-1)
            PreviousSnap = (From t In dataImported.Snapshots _
              Where t.IsDaily _
              And t.ExecuteDate.AddDays(-t.Offset).Date = PreviousDate _
              And TaskTypeSnapshots.Contains(t.ID) _
              Order By t.ExecuteDate Descending _
              Select t).FirstOrDefault
        ElseIf CurrentSnap.IsWorkdays Then
            PreviousDate = CurrentSnap.ExecuteDate.Date.AddDays(-1)
            Do Until PreviousDate.DayOfWeek <> DayOfWeek.Sunday And PreviousDate.DayOfWeek <> DayOfWeek.Saturday
                PreviousDate = PreviousDate.Date.AddDays(-1)
            Loop
            PreviousSnap = (From t In dataImported.Snapshots _
              Where t.IsWorkdays _
              And t.ExecuteDate.AddDays(-t.Offset).Date = PreviousDate _
              And TaskTypeSnapshots.Contains(t.ID) _
              Order By t.ExecuteDate Descending _
              Select t).FirstOrDefault
        ElseIf CurrentSnap.IsMinutes Then
            PreviousSnap = (From t In dataImported.Snapshots _
              Where t.IsMinutes _
              And TaskTypeSnapshots.Contains(t.ID) _
              Order By t.ExecuteDate Descending _
              Select t).FirstOrDefault
        Else
            PreviousSnap = (From t In dataImported.Snapshots _
                            Where t.IsYearly = False And t.IsMonthly = False _
                            And t.ExecuteDate.AddDays(-t.Offset).Date < CurrentDate _
                            And TaskTypeSnapshots.Contains(t.ID) _
                            And Not t.ID.Equals(SnapshotID) _
                            Order By t.ExecuteDate Descending _
                            Select t).FirstOrDefault
        End If
    End Sub
End Module
