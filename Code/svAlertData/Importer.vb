﻿Imports svAlertReportAndScenario.Globals
Public Class Importer
    Private SnapStartDate As Date
    Private SnapEndDate As Date

    Public Sub New(ByVal ConnString As String, ByVal StartDate As Date, ByVal EndDate As Date)
        dataImported = New dataImportedDataContext(ConnString)
        SnapStartDate = StartDate
        SnapEndDate = EndDate
    End Sub

    Public Function Import(ByVal SnapshotID As Guid, ByVal ScheduleType As Short, ByVal ScheduleOffset As Short, _
                           ByVal ImportType As Short, ByVal ConnectionString As String, ByVal IncludeHeaders As Boolean, ByVal ScenarioType As Short) As Boolean
        Dim Success As Boolean = False
        Try
            Dim SnapshotExists = (From t In dataImported.Snapshots _
                               Where t.ID.Equals(SnapshotID) _
                               Select t).FirstOrDefault

            If SnapshotExists Is Nothing Then
                Dim NewSnap As New Snapshot With { _
                .ID = SnapshotID, _
                .SnapMonth = Now.AddDays(-ScheduleOffset).Month, _
                .SnapYear = Now.AddDays(-ScheduleOffset).Year, _
                .ExecuteDate = Now, _
                .IsMinutes = (ScheduleType = ScheduleTypes.Minutes), _
                .IsYearly = (ScheduleType = ScheduleTypes.Yearly), _
                .IsMonthly = (ScheduleType = ScheduleTypes.Monthly), _
                .IsFortnightly = (ScheduleType = ScheduleTypes.Fortnightly), _
                .IsWeekly = (ScheduleType = ScheduleTypes.Weekly), _
                .IsDaily = (ScheduleType = ScheduleTypes.Daily), _
                .IsWorkdays = (ScheduleType = ScheduleTypes.Workdays)}
                dataImported.Snapshots.InsertOnSubmit(NewSnap)
                dataImported.SubmitChanges()
                SnapshotExists = NewSnap
            End If

            Dim DataList As New List(Of List(Of String))
            Dim HeaderColumns As New List(Of String)

            If ImportType = ImportTypes.FILE Then
                Dim ImportFile As New File
                ImportFile.LoadExcel(ConnectionString, IncludeHeaders, DataList, HeaderColumns)
            End If

            Dim OtherScenarios As String = ""
            Dim BasicScenario As Short
            AllScenarios(ScenarioType, OtherScenarios, BasicScenario)

            Dim EnDecrypt As New svMerit.SvCrypto(svMerit.SvCrypto.SymmProvEnum.Rijndael)
            Dim ConnString As String
            Dim PwdIndex As Integer = ConnectionString.IndexOf("Password=")
            ConnString = ConnectionString.Substring(0, PwdIndex + 9) & EnDecrypt.Decrypting(ConnectionString.Substring(PwdIndex + 9), GlobalKey, "-")

            Select Case BasicScenario
                Case ScenarioTypes.PRODUCTS
                    If ImportType = ImportTypes.FOOTSTEPS Then
                        Dim CurrentFS As New FS(ConnString, SnapStartDate, SnapEndDate)
                        DataList = CurrentFS.ImportStock(SnapshotExists) 'IsMonthly, IsYearly)
                    End If
                    For Each row In DataList
                        Dim NewProduct As New Product
                        NewProduct.Import(row, SnapshotID, True)
                    Next
                Case ScenarioTypes.MEDIATORS
                    If ImportType = ImportTypes.FOOTSTEPS Then
                        Dim CurrentFS As New FS(ConnString, SnapStartDate, SnapEndDate)
                        DataList = CurrentFS.ImportEmpl(SnapshotExists) 'IsMonthly, IsYearly)
                    End If
                    For Each row In DataList
                        Dim NewMediator As New Mediator
                        NewMediator.Import(row, SnapshotID, True)
                    Next
                Case ScenarioTypes.TRADERS
                    If ImportType = ImportTypes.FOOTSTEPS Then
                        Dim CurrentFS As New FS(ConnString, SnapStartDate, SnapEndDate)
                        DataList = CurrentFS.ImportCustomers(SnapshotExists) 'IsMonthly, IsYearly)
                    End If
                    If OtherScenarios <> "" AndAlso OtherScenarios.IndexOf(ScenarioTypes.MEDIATORS) > -1 Then
                        Dim BaseList As List(Of List(Of String)) = GetBaseData(DataList, 4, False)
                        For Each row In BaseList
                            Dim MediatorExists = From t In dataImported.Mediators _
                                   Where t.MediatorID.Equals(row(0)) _
                                   And t.SnapshotID.Equals(SnapshotID) _
                                   Select t
                            If MediatorExists.Count = 0 Then
                                Dim NewMediator As New Mediator
                                NewMediator.Import(row, SnapshotID, False)
                            End If
                        Next
                    End If
                    For Each row In DataList
                        Dim NewTrader As New Trader
                        NewTrader.Import(row, SnapshotID, True)
                    Next
                Case ScenarioTypes.AREAS
                    For Each row In DataList
                        Dim NewArea As New Area
                        NewArea.Import(row, SnapshotID, True)
                    Next
                Case ScenarioTypes.AREABALANCE
                    If OtherScenarios <> "" AndAlso OtherScenarios.IndexOf(ScenarioTypes.AREAS) > -1 Then
                        Dim BaseList As List(Of List(Of String)) = GetBaseData(DataList, 0, False)
                        For Each row In BaseList
                            Dim AreaExists = From t In dataImported.Areas _
                                           Where t.AreaID.Equals(row(0)) _
                                   And t.SnapshotID.Equals(SnapshotID) _
                                           Select t
                            If AreaExists.Count = 0 Then
                                Dim NewArea As New Area
                                NewArea.Import(row, SnapshotID, False)
                            End If
                        Next
                    End If
                    If OtherScenarios <> "" AndAlso OtherScenarios.IndexOf(ScenarioTypes.PRODUCTS) > -1 Then
                        Dim BaseList As List(Of List(Of String)) = GetBaseData(DataList, 3, False)
                        For Each row In BaseList
                            Dim ProductExists = From t In dataImported.Products _
                                   Where t.ProductID.Equals(row(0)) _
                                   And t.SnapshotID.Equals(SnapshotID) _
                                   Select t
                            If ProductExists.Count = 0 Then
                                Dim NewProduct As New Product
                                NewProduct.Import(row, SnapshotID, False)
                            End If
                        Next
                    End If
                    For Each row In DataList
                        Dim NewBalance As New AreaBalance
                        NewBalance.Import(row, SnapshotID)
                    Next
                Case ScenarioTypes.APPS
                    If ImportType = ImportTypes.FOOTSTEPS Then
                        Dim CurrentFS As New FS(ConnString, SnapStartDate, SnapEndDate)
                        DataList = CurrentFS.ImportDiary(SnapshotExists) 'IsMonthly, IsYearly)
                    End If
                    If OtherScenarios <> "" AndAlso OtherScenarios.IndexOf(ScenarioTypes.TRADERS) > -1 Then
                        Dim BaseList As List(Of List(Of String)) = GetBaseData(DataList, 5, True)
                        For Each row In BaseList
                            Dim TraderExists = From t In dataImported.Traders _
                                   Where t.TraderID.Equals(row(0)) _
                                   And t.SnapshotID.Equals(SnapshotID) _
                                   Select t
                            If TraderExists.Count = 0 Then
                                Dim NewTrader As New Trader
                                NewTrader.Import(row, SnapshotID, False)
                            End If
                        Next
                    End If
                    If OtherScenarios <> "" AndAlso OtherScenarios.IndexOf(ScenarioTypes.MEDIATORS) > -1 Then
                        Dim BaseList As List(Of List(Of String)) = GetBaseData(DataList, 8, False)
                        For Each row In BaseList
                            Dim MediatorExists = From t In dataImported.Mediators _
                                   Where t.MediatorID.Equals(row(0)) _
                                   And t.SnapshotID.Equals(SnapshotID) _
                                   Select t
                            If MediatorExists.Count = 0 Then
                                Dim NewMediator As New Mediator
                                NewMediator.Import(row, SnapshotID, False)
                            End If
                        Next
                    End If
                    For Each row In DataList
                        Dim NewApp As New Appointment
                        NewApp.Import(row, SnapshotID)
                    Next
                Case ScenarioTypes.TIMETABLE
                    Dim CurrentBusinessSalary As New BusinessSalary(ConnString, SnapStartDate, SnapEndDate)
                    DataList = CurrentBusinessSalary.ImportTimetable(SnapshotExists)
                    For Each row In DataList
                        Dim NewTimetable As New Timetable
                        NewTimetable.Import(row, SnapshotID)
                    Next
                Case ScenarioTypes.ASSIGNS
                    If ImportType = ImportTypes.FOOTSTEPS Then
                        Dim CurrentFS As New FS(ConnString, SnapStartDate, SnapEndDate)
                        DataList = CurrentFS.ImportTasks(SnapshotExists) 'IsMonthly, IsYearly)
                    End If
                    If OtherScenarios <> "" AndAlso OtherScenarios.IndexOf(ScenarioTypes.TRADERS) > -1 Then
                        Dim BaseList As List(Of List(Of String)) = GetBaseData(DataList, 5, True)
                        For Each row In BaseList
                            Dim TraderExists = From t In dataImported.Traders _
                                   Where t.TraderID.Equals(row(0)) _
                                   And t.SnapshotID.Equals(SnapshotID) _
                                   Select t
                            If TraderExists.Count = 0 Then
                                Dim NewTrader As New Trader
                                NewTrader.Import(row, SnapshotID, False)
                            End If
                        Next
                    End If
                    If OtherScenarios <> "" AndAlso OtherScenarios.IndexOf(ScenarioTypes.MEDIATORS) > -1 Then
                        Dim BaseList As List(Of List(Of String)) = GetBaseData(DataList, 8, False)
                        For Each row In BaseList
                            Dim MediatorExists = From t In dataImported.Mediators _
                                   Where t.MediatorID.Equals(row(0)) _
                                   And t.SnapshotID.Equals(SnapshotID) _
                                   Select t
                            If MediatorExists.Count = 0 Then
                                Dim NewMediator As New Mediator
                                NewMediator.Import(row, SnapshotID, False)
                            End If
                        Next
                    End If
                    For Each row In DataList
                        Dim NewAssign As New Assignment
                        NewAssign.Import(row, SnapshotID)
                    Next
                Case ScenarioTypes.EXPENSES
                    If OtherScenarios <> "" AndAlso OtherScenarios.IndexOf(ScenarioTypes.TRADERS) > -1 Then
                        Dim BaseList As List(Of List(Of String)) = GetBaseData(DataList, 6, True)
                        For Each row In BaseList
                            Dim TraderExists = From t In dataImported.Traders _
                                   Where t.TraderID.Equals(row(0)) _
                                   And t.SnapshotID.Equals(SnapshotID) _
                                   Select t
                            If TraderExists.Count = 0 Then
                                Dim NewTrader As New Trader
                                NewTrader.Import(row, SnapshotID, False)
                            End If
                        Next
                    End If
                    If OtherScenarios <> "" AndAlso OtherScenarios.IndexOf(ScenarioTypes.MEDIATORS) > -1 Then
                        Dim BaseList As List(Of List(Of String)) = GetBaseData(DataList, 9, False)
                        For Each row In BaseList
                            Dim MediatorExists = From t In dataImported.Mediators _
                                   Where t.MediatorID.Equals(row(0)) _
                                   And t.SnapshotID.Equals(SnapshotID) _
                                   Select t
                            If MediatorExists.Count = 0 Then
                                Dim NewMediator As New Mediator
                                NewMediator.Import(row, SnapshotID, False)
                            End If
                        Next
                    End If
                    For Each row In DataList
                        Dim NewExpense As New Expense
                        NewExpense.Import(row, SnapshotID)
                    Next
                Case ScenarioTypes.TRANSACTIONS
                    If OtherScenarios <> "" AndAlso OtherScenarios.IndexOf(ScenarioTypes.TRADERS) > -1 Then
                        Dim BaseList As List(Of List(Of String)) = GetBaseData(DataList, 6, True)
                        For Each row In BaseList
                            Dim TraderExists = From t In dataImported.Traders _
                                   Where t.TraderID.Equals(row(0)) _
                                   And t.SnapshotID.Equals(SnapshotID) _
                                   Select t
                            If TraderExists.Count = 0 Then
                                Dim NewTrader As New Trader
                                NewTrader.Import(row, SnapshotID, False)
                            End If
                        Next
                    End If
                    For Each row In DataList
                        Dim NewTrans As New Transaction
                        NewTrans.Import(row, SnapshotID)
                    Next
                Case ScenarioTypes.COMPANIES
                    For Each row In DataList
                        Dim NewCompany As New Company
                        NewCompany.Import(row, SnapshotID)
                    Next
                Case ScenarioTypes.FISCALS
                    For Each row In DataList
                        Dim NewFiscal As New Fiscal
                        NewFiscal.Import(row, SnapshotID)
                    Next
                Case ScenarioTypes.VAT
                    For Each row In DataList
                        Dim NewVat As New Vat
                        NewVat.Import(row, SnapshotID)
                    Next
                Case ScenarioTypes.BANKBALANCE
                    For Each row In DataList
                        Dim NewBankBalance As New BankBalance
                        NewBankBalance.Import(row, SnapshotID)
                    Next
                Case ScenarioTypes.ENTRIES
                    If ImportType = ImportTypes.FOOTSTEPS Then
                        Dim CurrentFS As New FS(ConnString, SnapStartDate, SnapEndDate)
                        DataList = CurrentFS.ImportOffers(SnapshotExists) 'IsMonthly, IsYearly)
                    End If
                    Dim AllEntries As New List(Of SvEntry)
                    For Each row In DataList
                        Dim CurrentEntry As SvEntry
                        Dim CurrentEntries = AllEntries.Where(Function(f) f.EntryID = row(0))

                        If CurrentEntries.Count = 0 Then
                            CurrentEntry = New SvEntry(row(0))
                        Else
                            CurrentEntry = CurrentEntries.FirstOrDefault
                            AllEntries.Remove(CurrentEntry)
                        End If
                        CurrentEntry.AddData(row)
                        AllEntries.Add(CurrentEntry)
                    Next

                    For Each ent In AllEntries
                        If OtherScenarios <> "" AndAlso OtherScenarios.IndexOf(ScenarioTypes.TRADERS) > -1 Then
                            Dim BaseList As List(Of List(Of String)) = GetBaseData(ent.Data, 6, True)
                            For Each row In BaseList
                                Dim TraderExists = From t In dataImported.Traders _
                                       Where t.TraderID.Equals(row(0)) _
                                       And t.SnapshotID.Equals(SnapshotID) _
                                       Select t
                                If TraderExists.Count = 0 Then
                                    Dim NewTrader As New Trader
                                    NewTrader.Import(row, SnapshotID, False)
                                End If
                            Next
                        End If
                        If OtherScenarios <> "" AndAlso OtherScenarios.IndexOf(ScenarioTypes.MEDIATORS) > -1 Then
                            Dim BaseList As List(Of List(Of String)) = GetBaseData(ent.Data, 9, False)
                            For Each row In BaseList
                                Dim MediatorExists = From t In dataImported.Mediators _
                                       Where t.MediatorID.Equals(row(0)) _
                                       And t.SnapshotID.Equals(SnapshotID) _
                                       Select t
                                If MediatorExists.Count = 0 Then
                                    Dim NewMediator As New Mediator
                                    NewMediator.Import(row, SnapshotID, False)
                                End If
                            Next
                        End If
                        If OtherScenarios <> "" AndAlso OtherScenarios.IndexOf(ScenarioTypes.PRODUCTS) > -1 Then
                            Dim BaseList As List(Of List(Of String)) = GetBaseData(ent.Data, 17, False)
                            For Each row In BaseList
                                Dim ProductExists = From t In dataImported.Products _
                                       Where t.ProductID.Equals(row(0)) _
                                       And t.SnapshotID.Equals(SnapshotID) _
                                       Select t
                                If ProductExists.Count = 0 Then
                                    Dim NewProduct As New Product
                                    NewProduct.Import(row, SnapshotID, False)
                                End If
                            Next
                        End If

                        Dim NewEntry As New Entry
                        NewEntry.Import(ent.Data, SnapshotID)
                    Next
                Case ScenarioTypes.NEWOBJECTS
                    If ImportType = ImportTypes.CONTROL Then
                        Dim CurrentControl As New Control(ConnString, SnapStartDate, SnapEndDate)
                        DataList = CurrentControl.ImportStockNew(SnapshotExists, SnapStartDate, SnapEndDate) 'IsMonthly, IsYearly)
                    End If
                    For Each row In DataList
                        Dim NewProduct As New Product
                        NewProduct.Import(row, SnapshotID, True)
                    Next
                    If ImportType = ImportTypes.CONTROL Then
                        Dim CurrentControl As New Control(ConnString, SnapStartDate, SnapEndDate)
                        DataList = CurrentControl.ImportCustomersNew(SnapshotExists, SnapStartDate, SnapEndDate) 'IsMonthly, IsYearly)
                    End If
                    For Each row In DataList
                        Dim NewTrader As New Trader
                        NewTrader.Import(row, SnapshotID, True)
                    Next
                    If ImportType = ImportTypes.CONTROL Then
                        Dim CurrentControl As New Control(ConnString, SnapStartDate, SnapEndDate)
                        DataList = CurrentControl.ImportStockPrice(SnapshotExists, SnapStartDate, SnapEndDate) 'IsMonthly, IsYearly)
                    End If
                    For Each row In DataList
                        Dim NewProduct As New Product
                        NewProduct.Import(row, SnapshotID, True)
                    Next
                Case ScenarioTypes.LOTS
                    If ImportType = ImportTypes.GLX Then
                        Dim CurrentGlx As New GLX(ConnString, SnapStartDate, SnapEndDate)
                        DataList = CurrentGlx.ImportItem(SnapshotExists) 'IsMonthly, IsYearly)
                    End If
                    For Each row In DataList
                        Dim NewProduct As New Product
                        NewProduct.Import(row, SnapshotID, True)
                    Next

                    If ImportType = ImportTypes.GLX Then
                        Dim CurrentGlx As New GLX(ConnString, SnapStartDate, SnapEndDate)
                        DataList = CurrentGlx.ImportLots(SnapshotExists)
                    End If
                    For Each row In DataList
                        Dim NewLot As New ProductLot
                        NewLot.Import(row, SnapshotID)
                    Next
                Case ScenarioTypes.SEN
                    If ImportType = ImportTypes.SEN Then
                        
                        Dim CurrentSen As New SEN(dataImported, SnapStartDate)
                        CurrentSen.ImportSenSuppliers(SnapshotID)

                    End If
            End Select

            Success = True
        Catch ex As Exception
            svGlobal.SvSys.ErrorLog("Importer.Import".PadRight(10) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
        End Try
        Return Success
    End Function

    Private Function GetBaseData(ByVal DataList As List(Of List(Of String)), ByVal StartIndex As Short, ByVal Extended As Boolean) As List(Of List(Of String))
        Dim BaseList As New List(Of List(Of String))
        For Each row In DataList
            Dim RowList As New List(Of String)
            RowList.Add(row(StartIndex))
            RowList.Add(row(StartIndex + 1))
            RowList.Add(row(StartIndex + 2))
            If Extended Then
                RowList.Add(row(StartIndex - 1))
            End If
            BaseList.Add(RowList)
        Next
        Return BaseList
    End Function

    Private Structure SvEntry

        Private _EntryID As String
        Public Property EntryID() As String
            Get
                Return _EntryID
            End Get
            Set(ByVal value As String)
                _EntryID = value
            End Set
        End Property

        Private _Data As List(Of List(Of String))
        Public Property Data() As List(Of List(Of String))
            Get
                Return _Data
            End Get
            Set(ByVal value As List(Of List(Of String)))
                _Data = value
            End Set
        End Property

        Public Sub New(ByVal SvEntryID As String)
            _EntryID = SvEntryID
            _Data = New List(Of List(Of String))
        End Sub

        Public Sub AddData(ByVal NewData As List(Of String))
            _Data.Add(NewData)
        End Sub

    End Structure

End Class
