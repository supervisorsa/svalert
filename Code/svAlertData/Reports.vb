﻿Imports svAlertReportAndScenario.Globals
Public Class Reports
    Public Sub New(ByVal ConnString As String)
        dataImported = New dataImportedDataContext(ConnString)
    End Sub

    Public Sub ExportReport(ByVal ReportType As Short, ByVal ExportPath As String, ByVal SnapID As Guid)
        Dim DataList As New List(Of List(Of String))

        Dim NewGlobal As New svAlertReportAndScenario.Globals
        Dim ReportName As String = NewGlobal.Reports(ReportType)

        Select Case ReportType
            Case ReportTypes.GROSS_PROFIT_PER_ORDER
                DataList = GrossProfit(SnapID)
            Case ReportTypes.TURNOVER_LESS Or ReportTypes.TURNOVER_MONTH_LESS
                DataList = Turnover(SnapID)
            Case ReportTypes.TURNOVER Or ReportTypes.TURNOVER_LM Or ReportTypes.TURNOVER_LY
                Dim CurrentSnap, PreviousSnap As Snapshot
                Dim AllSnapshots = (From t In dataImported.Entries _
                Select t.SnapshotID).Distinct
                LoadSnapshots(SnapID, AllSnapshots.ToList, CurrentSnap, PreviousSnap)
                If PreviousSnap Is Nothing Then
                    DataList = Turnover(SnapID)
                Else
                    DataList = TurnoverCompare(CurrentSnap, PreviousSnap)
                End If
            Case ReportTypes.STOCK
                DataList = Stock(SnapID)
            Case ReportTypes.MED_ORDERS Or ReportTypes.MED_ORDERS_LM Or ReportTypes.MED_ORDERS_LY
                Dim CurrentSnap, PreviousSnap As Snapshot
                Dim AllSnapshots = (From t In dataImported.Entries _
                Select t.SnapshotID).Distinct
                LoadSnapshots(SnapID, AllSnapshots.ToList, CurrentSnap, PreviousSnap)
                If PreviousSnap Is Nothing Then
                    DataList = MediatorOrders(SnapID)
                Else
                    DataList = MediatorOrdersCompare(CurrentSnap, PreviousSnap)
                End If
            Case ReportTypes.MED_ORDERS_LESS
                DataList = MediatorOrders(SnapID)
            Case ReportTypes.SALES_BUYS
                DataList = SalesBuys(SnapID)
            Case ReportTypes.TRADERS_REST
                DataList = TraderRest(SnapID)
            Case ReportTypes.SALES
                DataList = Sales(SnapID)
            Case ReportTypes.TRANSIT
                DataList = Transit(SnapID)

            Case ReportTypes.CUSTOMERS_CONTRACT
                DataList = CustomerInContract(SnapID)
            Case ReportTypes.CUSTOMERS_CONTRACT_LY Or ReportTypes.CUSTOMERS_CONTRACT_LM
                Dim CurrentSnap, PreviousSnap As Snapshot
                Dim AllSnapshots = (From t In dataImported.Traders _
                Select t.SnapshotID).Distinct
                LoadSnapshots(SnapID, AllSnapshots.ToList, CurrentSnap, PreviousSnap)
                If PreviousSnap Is Nothing Then
                    DataList = CustomerInContract(SnapID)
                Else
                    DataList = CustomerInContract_Compare(CurrentSnap, PreviousSnap)
                End If
            Case ReportTypes.TURNOVER_SERVICE
                DataList = ServicesTurnover(SnapID)
            Case ReportTypes.TURNOVER_SERVICE_LY Or ReportTypes.TURNOVER_SERVICE_LM
                Dim CurrentSnap, PreviousSnap As Snapshot
                Dim AllSnapshots = (From t In dataImported.Appointments _
                Select t.SnapshotID).Distinct
                LoadSnapshots(SnapID, AllSnapshots.ToList, CurrentSnap, PreviousSnap)
                If PreviousSnap Is Nothing Then
                    DataList = ServicesTurnover(SnapID)
                Else
                    DataList = ServicesTurnover_Compare(CurrentSnap, PreviousSnap)
                End If
            Case ReportTypes.TECH_HOURS
                DataList = TechHours(SnapID)
            Case ReportTypes.TECH_HOURS_LY Or ReportTypes.TECH_HOURS_LM
                Dim CurrentSnap, PreviousSnap As Snapshot
                Dim AllSnapshots = (From t In dataImported.Appointments _
                Select t.SnapshotID).Distinct
                LoadSnapshots(SnapID, AllSnapshots.ToList, CurrentSnap, PreviousSnap)
                If PreviousSnap Is Nothing Then
                    DataList = TechHours(SnapID)
                Else
                    DataList = TechHours_Compare(CurrentSnap, PreviousSnap)
                End If
            Case ReportTypes.OFFERS
                DataList = Offers(SnapID)
            Case ReportTypes.APPS_OFFERS_ORDERS
                DataList = AppOrders(SnapID)
            Case ReportTypes.MEAN_HOUR_CHARGE
                DataList = MeanHours(SnapID)
            Case ReportTypes.OPEN_REPAIRS
                DataList = OpenTasks(SnapID)
            Case ReportTypes.TASK_COMPLETE_DAYS
                DataList = CompletionDays(SnapID)
            Case ReportTypes.DAILY_APPS
                DataList = DailyApps(SnapID)
            Case ReportTypes.TURNOVER_DAILY
                DataList = TurnOverDaily(SnapID)
            Case ReportTypes.NEW_SUPPLIER
                DataList = NewSupplier(SnapID)
        End Select

        Dim ExporterFile As New ExporterFile
        'Exporter.ExcelExport(String.Format("{0}\{1}{2).xls", ExportPath, "apps", Format(Now, "yyyyMMdd.hhmmss")), DataList)
        ExporterFile.ExcelExport(String.Format("{0}\{1}_{2}.xls", ExportPath, ReportName, Format(Now, "yyyyMMdd.HHmmss")), DataList)
        'String.Format("{0}\{1}{2}.xls", ExportPath,"apps",Format(Now, "yyyyMMdd.hhmmss"))
    End Sub

    Private Function GrossProfit(ByVal SnapshotID As Guid) As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))

        Dim AllOrders = (From e In dataImported.Entries _
                         Join t In dataImported.Traders _
                         On e.TraderID Equals t.TraderID _
                         And e.SnapshotID Equals t.SnapshotID _
                    Where e.SnapshotID.Equals(SnapshotID) _
                         And e.IsOrder = True _
                         Select e.EntryID, t.TraderCode, t.TraderDescription, e.EntryDate, e.EntryFinalValue).Distinct

        Dim HeaderRow As New List(Of String)
        HeaderRow.Add("ΚΩΔΙΚΟΣ ΠΕΛΑΤΗ")
        HeaderRow.Add("ΕΠΩΝΥΜΙΑ ΠΕΛΑΤΗ")
        HeaderRow.Add("ΗΜΕΡΟΜΗΝΙΑ")
        HeaderRow.Add("ΠΩΛΗΣΗ")
        HeaderRow.Add("ΚΟΣΤΟΣ")
        HeaderRow.Add("ΜΙΚΤΟ ΚΕΡΔΟΣ")
        DataList.Add(HeaderRow)

        For Each ord In AllOrders
            Dim OrderCost As Decimal = 0

            Dim AllItems = From t In dataImported.EntryLines _
                           Where t.EntryID.Equals(ord.EntryID) _
                           And t.SnapshotID.Equals(SnapshotID) _
                           Select t.ProductID, t.Quantity

            For Each item In AllItems
                Dim ProductCost = (From t In dataImported.Products _
                                Where t.ProductID.Equals(item.ProductID) _
                                And t.SnapshotID.Equals(SnapshotID) _
                                Select t.Cost).FirstOrDefault
                OrderCost += ProductCost * item.Quantity
            Next

            Dim RowList As New List(Of String)
            RowList.Add(ord.TraderCode)
            RowList.Add(ord.TraderDescription)
            RowList.Add(ord.EntryDate.ToShortDateString)
            RowList.Add(Convert.ToDecimal(ord.EntryFinalValue).ToString("#.00"))
            RowList.Add(Convert.ToDecimal(OrderCost).ToString("#.00"))
            RowList.Add(Convert.ToDecimal(ord.EntryFinalValue - OrderCost).ToString("#.00"))

            DataList.Add(RowList)
        Next

        Return DataList

    End Function

    Private Function Turnover(ByVal SnapshotID As Guid) As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))

        Dim AllOrders = (From e In dataImported.Entries _
                      Join t In dataImported.Traders _
                      On e.TraderID Equals t.TraderID _
                      And e.SnapshotID Equals t.SnapshotID _
                    Where e.SnapshotID.Equals(SnapshotID) _
                      And e.IsOrder = True _
                      Select e.EntryID, t.TraderCode, t.TraderDescription, e.EntryDate, e.EntryFinalValue).Distinct


        Dim Total As New List(Of String)
        Total.Add("ΣΥΝΟΛΟ ΤΖΙΡΟΥ")
        Total.Add(Convert.ToDecimal(AllOrders.Sum(Function(f) f.EntryFinalValue)).ToString("#.00"))
        DataList.Add(Total)

        DataList.Add(New List(Of String))

        Dim HeaderRow As New List(Of String)
        HeaderRow.Add("ΚΩΔΙΚΟΣ ΠΕΛΑΤΗ")
        HeaderRow.Add("ΕΠΩΝΥΜΙΑ ΠΕΛΑΤΗ")
        HeaderRow.Add("ΗΜΕΡΟΜΗΝΙΑ")
        HeaderRow.Add("ΠΩΛΗΣΗ")
        DataList.Add(HeaderRow)

        For Each ord In AllOrders
            Dim RowList As New List(Of String)
            RowList.Add(ord.TraderCode)
            RowList.Add(ord.TraderDescription)
            RowList.Add(ord.EntryDate.ToShortDateString)
            RowList.Add(Convert.ToDecimal(ord.EntryFinalValue).ToString("#.00"))

            DataList.Add(RowList)
        Next

        Return DataList
    End Function

    Private Function TurnoverCompare(ByVal CurrentSnap As Snapshot, ByVal PreviousSnap As Snapshot) As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))

        Dim CurrentOrders = (From e In dataImported.Entries _
                      Join t In dataImported.Traders _
                      On e.TraderID Equals t.TraderID _
                      And e.SnapshotID Equals t.SnapshotID _
                      Where e.SnapshotID.Equals(CurrentSnap.ID) _
                      And e.IsOrder = True _
                      Select e.EntryID, t.TraderCode, t.TraderDescription, e.EntryDate, e.EntryFinalValue).Distinct

        Dim PreviousOrders = (From e In dataImported.Entries _
                      Join t In dataImported.Traders _
                      On e.TraderID Equals t.TraderID _
                      And e.SnapshotID Equals t.SnapshotID _
                      Where e.SnapshotID.Equals(CurrentSnap.ID) _
                      And e.IsOrder = True _
                      Select e.EntryID, t.TraderCode, t.TraderDescription, e.EntryDate, e.EntryFinalValue).Distinct

        Dim Total1 As New List(Of String)
        Total1.Add("ΣΥΝΟΛΟ ΤΖΙΡΟΥ")
        Total1.Add(CurrentSnap.ExecuteDate.Month & "/" & CurrentSnap.ExecuteDate.Year)
        Total1.Add(Convert.ToDecimal(CurrentOrders.Sum(Function(f) f.EntryFinalValue)).ToString("#.00"))
        DataList.Add(Total1)

        Dim Total2 As New List(Of String)
        Total2.Add("ΣΥΝΟΛΟ ΤΖΙΡΟΥ")
        Total2.Add(PreviousSnap.ExecuteDate.Month & "/" & PreviousSnap.ExecuteDate.Year)
        Total2.Add(Convert.ToDecimal(PreviousOrders.Sum(Function(f) f.EntryFinalValue)).ToString("#.00"))
        DataList.Add(Total2)

        DataList.Add(New List(Of String))

        Dim HeaderRow As New List(Of String)
        HeaderRow.Add("ΚΩΔΙΚΟΣ ΠΕΛΑΤΗ")
        HeaderRow.Add("ΕΠΩΝΥΜΙΑ ΠΕΛΑΤΗ")
        HeaderRow.Add("ΗΜΕΡΟΜΗΝΙΑ")
        HeaderRow.Add("ΠΩΛΗΣΗ")
        DataList.Add(HeaderRow)

        For Each ord In CurrentOrders
            Dim RowList As New List(Of String)
            RowList.Add(ord.TraderCode)
            RowList.Add(ord.TraderDescription)
            RowList.Add(ord.EntryDate.ToShortDateString)
            RowList.Add(Convert.ToDecimal(ord.EntryFinalValue).ToString("#.00"))

            DataList.Add(RowList)
        Next

        DataList.Add(New List(Of String))

        For Each ord In PreviousOrders
            Dim RowList As New List(Of String)
            RowList.Add(ord.TraderCode)
            RowList.Add(ord.TraderDescription)
            RowList.Add(ord.EntryDate.ToShortDateString)
            RowList.Add(Convert.ToDecimal(ord.EntryFinalValue).ToString("#.00"))

            DataList.Add(RowList)
        Next

        Return DataList
    End Function

    Private Function MediatorOrders(ByVal SnapshotID As Guid) As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))

        Dim AllOrders = (From e In dataImported.Entries _
                      Join t In dataImported.Traders _
                      On e.TraderID Equals t.TraderID _
                      And e.SnapshotID Equals t.SnapshotID _
                      Join m In dataImported.Mediators _
                      On e.MediatorID Equals m.MediatorID _
                      And e.SnapshotID Equals m.SnapshotID _
                      Where e.SnapshotID.Equals(SnapshotID) _
                      Order By m.MediatorDescription, e.EntryDate _
                      Select t.TraderCode, t.TraderDescription, m.MediatorDescription, e.EntryDate, e.EntryFinalValue, e.IsOrder).Distinct

        Dim CurrentMediator As String = ""

        For Each ord In AllOrders
            If CurrentMediator <> ord.MediatorDescription Then
                CurrentMediator = ord.MediatorDescription

                DataList.Add(New List(Of String))

                Dim Total As New List(Of String)
                Total.Add("ΣΥΝΟΛΟ ΤΖΙΡΟΥ")
                Total.Add(Convert.ToDecimal(AllOrders.Where(Function(p) p.MediatorDescription = CurrentMediator).Sum(Function(f) f.EntryFinalValue)).ToString("#.00"))
                DataList.Add(Total)

                DataList.Add(New List(Of String))

                Dim HeaderRow As New List(Of String)
                HeaderRow.Add("ΚΩΔΙΚΟΣ ΠΕΛΑΤΗ")
                HeaderRow.Add("ΕΠΩΝΥΜΙΑ ΠΕΛΑΤΗ")
                HeaderRow.Add("ΗΜΕΡΟΜΗΝΙΑ")
                HeaderRow.Add("ΠΩΛΗΣΗ")
                DataList.Add(HeaderRow)
            End If

            Dim RowList As New List(Of String)
            RowList.Add(ord.TraderCode)
            RowList.Add(ord.TraderDescription)
            RowList.Add(ord.EntryDate.ToShortDateString)
            RowList.Add(Convert.ToDecimal(ord.EntryFinalValue).ToString("#.00"))

            DataList.Add(RowList)
        Next

        Return DataList
    End Function

    Private Function MediatorOrdersCompare(ByVal CurrentSnap As Snapshot, ByVal PreviousSnap As Snapshot) As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))

        Dim CurrentOrders = (From e In dataImported.Entries _
                      Join t In dataImported.Traders _
                      On e.TraderID Equals t.TraderID _
                      And e.SnapshotID Equals t.SnapshotID _
                      Join m In dataImported.Mediators _
                      On e.MediatorID Equals m.MediatorID _
                      And e.SnapshotID Equals m.SnapshotID _
                      Where e.SnapshotID.Equals(CurrentSnap.ID) _
                      Order By m.MediatorDescription, e.EntryDate _
                      Select t.TraderCode, t.TraderDescription, m.MediatorDescription, e.EntryDate, e.EntryFinalValue, e.IsOrder).Distinct

        Dim PreviousOrders 'As IQueryable(Of Object)

        Dim CurrentMediator As String = ""

        For Each ord In CurrentOrders
            If CurrentMediator <> ord.MediatorDescription Then
                CurrentMediator = ord.MediatorDescription

                PreviousOrders = (From e In dataImported.Entries _
                      Join t In dataImported.Traders _
                      On e.TraderID Equals t.TraderID _
                      And e.SnapshotID Equals t.SnapshotID _
                      Join m In dataImported.Mediators _
                      On e.MediatorID Equals m.MediatorID _
                      And e.SnapshotID Equals m.SnapshotID _
                      Where e.SnapshotID.Equals(CurrentSnap.ID) _
                      And m.MediatorDescription = CurrentMediator _
                      Order By m.MediatorDescription, e.EntryDate _
                      Select t.TraderCode, t.TraderDescription, m.MediatorDescription, e.EntryDate, e.EntryFinalValue, e.IsOrder).Distinct

                DataList.Add(New List(Of String))

                Dim Total1 As New List(Of String)
                Total1.Add("ΣΥΝΟΛΟ ΤΖΙΡΟΥ")
                Total1.Add(CurrentSnap.ExecuteDate.Month & "/" & CurrentSnap.ExecuteDate.Year)
                Total1.Add(Convert.ToDecimal(CurrentOrders.Where(Function(p) p.MediatorDescription = CurrentMediator).Sum(Function(f) f.EntryFinalValue)).ToString("#.00"))
                DataList.Add(Total1)

                Dim Total2 As New List(Of String)
                Total2.Add("ΣΥΝΟΛΟ ΤΖΙΡΟΥ")
                Total2.Add(PreviousSnap.ExecuteDate.Month & "/" & PreviousSnap.ExecuteDate.Year)
                Total2.Add(Convert.ToDecimal(PreviousOrders.Sum(Function(f) f.EntryFinalValue)).ToString("#.00"))
                DataList.Add(Total2)

                DataList.Add(New List(Of String))

                Dim HeaderRow As New List(Of String)
                HeaderRow.Add("ΚΩΔΙΚΟΣ ΠΕΛΑΤΗ")
                HeaderRow.Add("ΕΠΩΝΥΜΙΑ ΠΕΛΑΤΗ")
                HeaderRow.Add("ΗΜΕΡΟΜΗΝΙΑ")
                HeaderRow.Add("ΠΩΛΗΣΗ")
                DataList.Add(HeaderRow)
            End If

            Dim RowList As New List(Of String)
            RowList.Add(ord.TraderCode)
            RowList.Add(ord.TraderDescription)
            RowList.Add(ord.EntryDate.ToShortDateString)
            RowList.Add(Convert.ToDecimal(ord.EntryFinalValue).ToString("#.00"))

            DataList.Add(RowList)
        Next

        DataList.Add(New List(Of String))

        For Each ord In PreviousOrders
            Dim RowList As New List(Of String)
            RowList.Add(ord.TraderCode)
            RowList.Add(ord.TraderDescription)
            RowList.Add(ord.EntryDate.ToShortDateString)
            RowList.Add(Convert.ToDecimal(ord.EntryFinalValue).ToString("#.00"))

            DataList.Add(RowList)
        Next

        Return DataList
    End Function

    Private Function Stock(ByVal SnapshotID As Guid) As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))

        Dim AllStock = (From t In dataImported.Products _
                         Where t.SnapshotID.Equals(SnapshotID) _
                         Select t.ProductCode, t.ProductDescription, t.StockQuantity, t.Price).Distinct

        Dim Total As New List(Of String)
        Total.Add("ΑΞΙΑ STOCK")
        Total.Add(Convert.ToDecimal(AllStock.Sum(Function(f) f.StockQuantity * f.Price)).ToString("#.00"))
        DataList.Add(Total)

        DataList.Add(New List(Of String))

        Dim HeaderRow As New List(Of String)
        HeaderRow.Add("ΚΩΔΙΚΟΣ ΕΙΔΟΥΣ")
        HeaderRow.Add("ΕΙΔΟΣ")
        HeaderRow.Add("ΠΟΣΟΤΗΤΑ")
        HeaderRow.Add("ΑΞΙΑ STOCK")
        DataList.Add(HeaderRow)

        For Each stk In AllStock
            Dim RowList As New List(Of String)
            RowList.Add(stk.ProductCode)
            RowList.Add(stk.ProductDescription)
            RowList.Add(stk.StockQuantity)
            RowList.Add(Convert.ToDecimal(stk.StockQuantity * stk.Price).ToString("#.00"))

            DataList.Add(RowList)
        Next

        Return DataList
    End Function

    Private Function SalesBuys(ByVal SnapshotID As Guid) As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))

        Dim AllOrders = (From e In dataImported.Entries _
                      Join t In dataImported.Traders _
                      On e.TraderID Equals t.TraderID _
                      And e.SnapshotID Equals t.SnapshotID _
                    Where e.SnapshotID.Equals(SnapshotID) _
                      And e.IsOrder = True _
                      Select e.EntryID, t.TraderCode, t.TraderDescription, e.EntryDate, e.EntryFinalValue, e.IsSale).Distinct


        Dim Total1 As New List(Of String)
        Total1.Add("ΠΩΛΗΣΕΙΣ")
        Total1.Add(Convert.ToDecimal(AllOrders.Where(Function(p) p.IsSale = True).Sum(Function(f) f.EntryFinalValue)).ToString("#.00"))
        DataList.Add(Total1)

        Dim Total2 As New List(Of String)
        Total2.Add("ΑΓΟΡΕΣ")
        Total2.Add(Convert.ToDecimal(AllOrders.Where(Function(p) p.IsSale = False).Sum(Function(f) f.EntryFinalValue)).ToString("#.00"))
        DataList.Add(Total2)


        DataList.Add(New List(Of String))

        Dim HeaderRow As New List(Of String)
        HeaderRow.Add("")
        HeaderRow.Add("ΚΩΔΙΚΟΣ ΠΕΛΑΤΗ")
        HeaderRow.Add("ΕΠΩΝΥΜΙΑ ΠΕΛΑΤΗ")
        HeaderRow.Add("ΗΜΕΡΟΜΗΝΙΑ")
        HeaderRow.Add("ΠΩΛΗΣΗ")
        DataList.Add(HeaderRow)

        For Each ord In AllOrders
            Dim RowList As New List(Of String)
            RowList.Add(If(ord.IsSale, "ΠΩΛΗΣΗ", "ΑΓΟΡΑ"))
            RowList.Add(ord.TraderCode)
            RowList.Add(ord.TraderDescription)
            RowList.Add(ord.EntryDate.ToShortDateString)
            RowList.Add(Convert.ToDecimal(ord.EntryFinalValue).ToString("#.00"))

            DataList.Add(RowList)
        Next

        Return DataList
    End Function

    Private Function Sales(ByVal SnapshotID As Guid) As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))

        Dim AllOrders = (From e In dataImported.Entries _
                      Join t In dataImported.Traders _
                      On e.TraderID Equals t.TraderID _
                      And e.SnapshotID Equals t.SnapshotID _
                    Where e.SnapshotID.Equals(SnapshotID) _
                      And e.IsOrder = True _
                      Select e.EntryID, t.TraderCode, t.TraderDescription, e.EntryDate, e.EntryFinalValue).Distinct


        Dim Total As New List(Of String)
        Total.Add("ΠΩΛΗΣΕΙΣ")
        Total.Add(Convert.ToDecimal(AllOrders.Sum(Function(f) f.EntryFinalValue)).ToString("#.00"))
        DataList.Add(Total)


        DataList.Add(New List(Of String))

        Dim HeaderRow As New List(Of String)
        HeaderRow.Add("ΚΩΔΙΚΟΣ ΠΕΛΑΤΗ")
        HeaderRow.Add("ΕΠΩΝΥΜΙΑ ΠΕΛΑΤΗ")
        HeaderRow.Add("ΗΜΕΡΟΜΗΝΙΑ")
        HeaderRow.Add("ΠΩΛΗΣΗ")
        DataList.Add(HeaderRow)

        For Each ord In AllOrders
            Dim RowList As New List(Of String)
            RowList.Add(ord.TraderCode)
            RowList.Add(ord.TraderDescription)
            RowList.Add(ord.EntryDate.ToShortDateString)
            RowList.Add(Convert.ToDecimal(ord.EntryFinalValue).ToString("#.00"))

            DataList.Add(RowList)
        Next

        Return DataList
    End Function

    Private Function Transit(ByVal SnapshotID As Guid) As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))

        Dim AllOrders = (From e In dataImported.Entries _
                      Join t In dataImported.Traders _
                      On e.TraderID Equals t.TraderID _
                      And e.SnapshotID Equals t.SnapshotID _
                      Where e.SnapshotID.Equals(SnapshotID) _
                      And e.IsOrder = True _
                      And e.IsDelivered = False _
                      Select e.EntryID, t.TraderCode, t.TraderDescription, e.EntryDate, e.EntryFinalValue).Distinct

        Dim Total As New List(Of String)
        Total.Add("ΑΞΙΑ")
        Total.Add(Convert.ToDecimal(AllOrders.Sum(Function(f) f.EntryFinalValue)).ToString("#.00"))
        DataList.Add(Total)


        DataList.Add(New List(Of String))

        Dim HeaderRow As New List(Of String)
        HeaderRow.Add("ΚΩΔΙΚΟΣ ΠΕΛΑΤΗ")
        HeaderRow.Add("ΕΠΩΝΥΜΙΑ ΠΕΛΑΤΗ")
        HeaderRow.Add("ΗΜΕΡΟΜΗΝΙΑ")
        HeaderRow.Add("ΠΩΛΗΣΗ")
        DataList.Add(HeaderRow)

        For Each ord In AllOrders
            Dim RowList As New List(Of String)
            RowList.Add(ord.TraderCode)
            RowList.Add(ord.TraderDescription)
            RowList.Add(ord.EntryDate.ToShortDateString)
            RowList.Add(Convert.ToDecimal(ord.EntryFinalValue).ToString("#.00"))

            DataList.Add(RowList)
        Next

        Return DataList
    End Function

    Private Function TraderRest(ByVal SnapshotID As Guid) As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))

        Dim AllRests = (From t In dataImported.Traders _
                     Where t.SnapshotID.Equals(SnapshotID) _
                     And t.TraderDescription <> "" _
                     Select t.TraderCode, t.TraderDescription, t.Charge, t.Credit, t.Rest).Distinct

        Dim HeaderRow As New List(Of String)
        HeaderRow.Add("ΚΩΔΙΚΟΣ ΠΕΛΑΤΗ")
        HeaderRow.Add("ΕΠΩΝΥΜΙΑ ΠΕΛΑΤΗ")
        HeaderRow.Add("ΧΡΕΩΣΗ")
        HeaderRow.Add("ΠΙΣΤΩΣΗ")
        HeaderRow.Add("ΥΠΟΛΟΙΠΟ")
        DataList.Add(HeaderRow)

        For Each rest In AllRests
            Dim RowList As New List(Of String)
            RowList.Add(rest.TraderCode)
            RowList.Add(rest.TraderDescription)
            RowList.Add(rest.Charge)
            RowList.Add(rest.Credit)
            RowList.Add(rest.Rest)
            DataList.Add(RowList)
        Next

        Return DataList
    End Function


    Private Function CustomerInContract(ByVal SnapshotID As Guid) As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))

        Dim AllCustomers = (From t In dataImported.Traders _
                     Where t.SnapshotID.Equals(SnapshotID) _
                      And t.TraderDescription <> "" _
                    Select t.TraderCode, t.TraderDescription, t.Charge, t.Credit, t.Rest).Distinct

        Dim HeaderRow As New List(Of String)
        HeaderRow.Add("ΚΩΔΙΚΟΣ ΠΕΛΑΤΗ")
        HeaderRow.Add("ΕΠΩΝΥΜΙΑ ΠΕΛΑΤΗ")
        HeaderRow.Add("ΧΡΕΩΣΗ")
        HeaderRow.Add("ΠΙΣΤΩΣΗ")
        HeaderRow.Add("ΥΠΟΛΟΙΠΟ")
        DataList.Add(HeaderRow)

        For Each rest In AllCustomers
            Dim RowList As New List(Of String)
            RowList.Add(rest.TraderCode)
            RowList.Add(rest.TraderDescription)
            RowList.Add(rest.Charge)
            RowList.Add(rest.Credit)
            RowList.Add(rest.Rest)
            DataList.Add(RowList)
        Next

        Return DataList
    End Function
    Private Function CustomerInContract_Compare(ByVal CurrentSnap As Snapshot, ByVal PreviousSnap As Snapshot) As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))

        Dim CurrentCustomers = (From t In dataImported.Traders _
                                Where t.SnapshotID.Equals(CurrentSnap.ID) _
                                And t.TraderDescription <> "" _
                                Select t.TraderCode, t.TraderDescription, t.Charge, t.Credit, t.Rest).Distinct

        Dim PreviousCustomers = (From t In dataImported.Traders _
                                 Where t.SnapshotID.Equals(PreviousSnap.ID) _
                                 And t.TraderDescription <> "" _
                                 Select t.TraderCode, t.TraderDescription, t.Charge, t.Credit, t.Rest).Distinct

        Dim Total1 As New List(Of String)
        Total1.Add(CurrentSnap.ExecuteDate.Month & "/" & CurrentSnap.ExecuteDate.Year)
        Total1.Add(CurrentCustomers.Count)
        DataList.Add(Total1)

        Dim Total2 As New List(Of String)
        Total2.Add(PreviousSnap.ExecuteDate.Month & "/" & PreviousSnap.ExecuteDate.Year)
        Total2.Add(PreviousCustomers.Count)
        DataList.Add(Total2)

        DataList.Add(New List(Of String))

        Dim HeaderRow As New List(Of String)
        HeaderRow.Add("ΚΩΔΙΚΟΣ ΠΕΛΑΤΗ")
        HeaderRow.Add("ΕΠΩΝΥΜΙΑ ΠΕΛΑΤΗ")
        HeaderRow.Add("ΧΡΕΩΣΗ")
        HeaderRow.Add("ΠΙΣΤΩΣΗ")
        HeaderRow.Add("ΥΠΟΛΟΙΠΟ")
        DataList.Add(HeaderRow)

        For Each rest In CurrentCustomers
            Dim RowList As New List(Of String)
            RowList.Add(rest.TraderCode)
            RowList.Add(rest.TraderDescription)
            RowList.Add(rest.Charge)
            RowList.Add(rest.Credit)
            RowList.Add(rest.Rest)
            DataList.Add(RowList)
        Next

        DataList.Add(New List(Of String))

        For Each rest In PreviousCustomers
            Dim RowList As New List(Of String)
            RowList.Add(rest.TraderCode)
            RowList.Add(rest.TraderDescription)
            RowList.Add(rest.Charge)
            RowList.Add(rest.Credit)
            RowList.Add(rest.Rest)
            DataList.Add(RowList)
        Next

        Return DataList
    End Function


    Private Function DailyApps(ByVal SnapshotID As Guid) As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))

        Dim AllApps = (From a In dataImported.Appointments _
                      Join t In dataImported.Traders _
                      On a.TraderID Equals t.TraderID _
                      And a.SnapshotID Equals t.SnapshotID _
                      Join m In dataImported.Mediators _
                      On a.MediatorID Equals m.MediatorID _
                      And a.SnapshotID Equals m.SnapshotID _
                    Where a.SnapshotID.Equals(SnapshotID) _
                    And a.IsCanceled = False _
                    And a.IsService = False _
                    And a.IsService = False _
                    Select t.TraderCode, t.TraderDescription, m.MediatorDescription, a.AppointmentDate, a.StartTime, a.EndTime).Distinct

        Dim HeaderRow As New List(Of String)
        HeaderRow.Add("ΚΩΔΙΚΟΣ ΠΕΛΑΤΗ")
        HeaderRow.Add("ΕΠΩΝΥΜΙΑ ΠΕΛΑΤΗ")
        HeaderRow.Add("ΥΠΑΛΛΗΛΟΣ")
        HeaderRow.Add("ΗΜΕΡΟΜΗΝΙΑ")
        HeaderRow.Add("ΑΠΟ")
        HeaderRow.Add("ΕΩΣ")
        DataList.Add(HeaderRow)

        For Each app In AllApps
            Dim RowList As New List(Of String)
            RowList.Add(app.TraderCode)
            RowList.Add(app.TraderDescription)
            RowList.Add(app.MediatorDescription)
            RowList.Add(app.AppointmentDate.ToShortDateString)
            RowList.Add(CDate(app.StartTime).ToShortTimeString)
            RowList.Add(CDate(app.EndTime).ToShortTimeString)
            DataList.Add(RowList)
        Next
	
        Return DataList
    End Function

    Private Function ServicesTurnover(ByVal SnapshotID As Guid) As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))

        Dim AllCharges = (From a In dataImported.Appointments _
                      Join t In dataImported.Traders _
                      On a.TraderID Equals t.TraderID _
                      And a.SnapshotID Equals t.SnapshotID _
                      Join m In dataImported.Mediators _
                      On a.MediatorID Equals m.MediatorID _
                      And a.SnapshotID Equals m.SnapshotID _
                    Where a.SnapshotID.Equals(SnapshotID) _
                    And a.IsCanceled = False _
                    And a.IsService = True _
                    Order By a.AppointmentDate _
                    Select t.TraderCode, t.TraderDescription, m.MediatorDescription, a.AppointmentDate, a.AppointmentValue).Distinct

        Dim HeaderRow As New List(Of String)
        HeaderRow.Add("ΚΩΔΙΚΟΣ ΠΕΛΑΤΗ")
        HeaderRow.Add("ΕΠΩΝΥΜΙΑ ΠΕΛΑΤΗ")
        HeaderRow.Add("ΥΠΑΛΛΗΛΟΣ")
        HeaderRow.Add("ΗΜΕΡΟΜΗΝΙΑ")
        HeaderRow.Add("ΧΡΕΩΣΗ")
        DataList.Add(HeaderRow)

        For Each chrg In AllCharges
            Dim RowList As New List(Of String)
            RowList.Add(chrg.TraderCode)
            RowList.Add(chrg.TraderDescription)
            RowList.Add(chrg.MediatorDescription)
            RowList.Add(chrg.AppointmentDate.ToShortDateString)
            RowList.Add(chrg.AppointmentValue.ToString("#.00"))
            DataList.Add(RowList)
        Next

        Return DataList
    End Function

    Private Function ServicesTurnover_Compare(ByVal CurrentSnap As Snapshot, ByVal PreviousSnap As Snapshot) As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))

        Dim CurrentCharges = (From a In dataImported.Appointments _
                      Join t In dataImported.Traders _
                      On a.TraderID Equals t.TraderID _
                      And a.SnapshotID Equals t.SnapshotID _
                      Join m In dataImported.Mediators _
                      On a.MediatorID Equals m.MediatorID _
                      And a.SnapshotID Equals m.SnapshotID _
                      Where a.SnapshotID.Equals(CurrentSnap.ID) _
                      And a.IsCanceled = False _
                      And a.IsService = True _
                    Order By a.AppointmentDate _
                      Select t.TraderCode, t.TraderDescription, m.MediatorDescription, a.AppointmentDate, a.AppointmentValue).Distinct

        Dim PreviousCharges = (From a In dataImported.Appointments _
                      Join t In dataImported.Traders _
                      On a.TraderID Equals t.TraderID _
                      And a.SnapshotID Equals t.SnapshotID _
                      Join m In dataImported.Mediators _
                      On a.MediatorID Equals m.MediatorID _
                      And a.SnapshotID Equals m.SnapshotID _
                      Where a.SnapshotID.Equals(PreviousSnap.ID) _
                      And a.IsCanceled = False _
                      And a.IsService = True _
                    Order By a.AppointmentDate _
                      Select t.TraderCode, t.TraderDescription, m.MediatorDescription, a.AppointmentDate, a.AppointmentValue).Distinct

        Dim Total1 As New List(Of String)
        Total1.Add(CurrentSnap.ExecuteDate.Month & "/" & CurrentSnap.ExecuteDate.Year)
        If CurrentCharges.Count = 0 Then
            Total1.Add("0")
        Else
            Total1.Add(CurrentCharges.Sum(Function(f) f.AppointmentValue).ToString("#.00"))
        End If
        DataList.Add(Total1)

        Dim Total2 As New List(Of String)
        Total2.Add(PreviousSnap.ExecuteDate.Month & "/" & PreviousSnap.ExecuteDate.Year)
        If PreviousCharges.Count = 0 Then
            Total2.Add("0")
        Else
            Total2.Add(PreviousCharges.Sum(Function(f) f.AppointmentValue).ToString("#.00"))
        End If
        DataList.Add(Total2)

        DataList.Add(New List(Of String))

        Dim HeaderRow As New List(Of String)
        HeaderRow.Add("ΚΩΔΙΚΟΣ ΠΕΛΑΤΗ")
        HeaderRow.Add("ΕΠΩΝΥΜΙΑ ΠΕΛΑΤΗ")
        HeaderRow.Add("ΥΠΑΛΛΗΛΟΣ")
        HeaderRow.Add("ΗΜΕΡΟΜΗΝΙΑ")
        HeaderRow.Add("ΧΡΕΩΣΗ")
        DataList.Add(HeaderRow)

        For Each chrg In CurrentCharges
            Dim RowList As New List(Of String)
            RowList.Add(chrg.TraderCode)
            RowList.Add(chrg.TraderDescription)
            RowList.Add(chrg.MediatorDescription)
            RowList.Add(chrg.AppointmentDate.ToShortDateString)
            RowList.Add(chrg.AppointmentValue.ToString("#.00"))
            DataList.Add(RowList)
        Next

        DataList.Add(New List(Of String))

        For Each chrg In PreviousCharges
            Dim RowList As New List(Of String)
            RowList.Add(chrg.TraderCode)
            RowList.Add(chrg.TraderDescription)
            RowList.Add(chrg.MediatorDescription)
            RowList.Add(chrg.AppointmentDate.ToShortDateString)
            RowList.Add(chrg.AppointmentValue.ToString("#.00"))
            DataList.Add(RowList)
        Next

        Return DataList
    End Function

    Private Function TechHours(ByVal SnapshotID As Guid) As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))

        Dim AllHours = (From a In dataImported.Appointments _
                      Join t In dataImported.Traders _
                      On a.TraderID Equals t.TraderID _
                      And a.SnapshotID Equals t.SnapshotID _
                      Join m In dataImported.Mediators _
                      On a.MediatorID Equals m.MediatorID _
                      And a.SnapshotID Equals m.SnapshotID _
                    Where a.SnapshotID.Equals(SnapshotID) _
                    And a.IsCanceled = False _
                    And a.IsService = True _
                    Order By a.AppointmentDate _
                    Select t.TraderCode, t.TraderDescription, m.MediatorDescription, a.AppointmentDate, a.StartTime, a.EndTime, a.CalcHours).Distinct

        Dim HeaderRow As New List(Of String)
        HeaderRow.Add("ΚΩΔΙΚΟΣ ΠΕΛΑΤΗ")
        HeaderRow.Add("ΕΠΩΝΥΜΙΑ ΠΕΛΑΤΗ")
        HeaderRow.Add("ΥΠΑΛΛΗΛΟΣ")
        HeaderRow.Add("ΗΜΕΡΟΜΗΝΙΑ")
        HeaderRow.Add("ΩΡΕΣ")
        DataList.Add(HeaderRow)

        For Each hrs In AllHours
            Dim RowList As New List(Of String)
            RowList.Add(hrs.TraderCode)
            RowList.Add(hrs.TraderDescription)
            RowList.Add(hrs.MediatorDescription)
            RowList.Add(hrs.AppointmentDate.ToShortDateString)
            RowList.Add(hrs.CalcHours.ToString("#.00"))

            DataList.Add(RowList)
        Next

        Return DataList
    End Function

    Private Function TechHours_Compare(ByVal CurrentSnap As Snapshot, ByVal PreviousSnap As Snapshot) As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))

        Dim CurrentHours = (From a In dataImported.Appointments _
                      Join t In dataImported.Traders _
                      On a.TraderID Equals t.TraderID _
                      And a.SnapshotID Equals t.SnapshotID _
                      Join m In dataImported.Mediators _
                      On a.MediatorID Equals m.MediatorID _
                      And a.SnapshotID Equals m.SnapshotID _
                    Where a.SnapshotID.Equals(CurrentSnap.ID) _
                    And a.IsCanceled = False _
                    And a.IsService = True _
                    Order By a.AppointmentDate _
                    Select t.TraderCode, t.TraderDescription, m.MediatorDescription, a.AppointmentDate, a.StartTime, a.EndTime, a.CalcHours).Distinct

        Dim PreviousHours = (From a In dataImported.Appointments _
                      Join t In dataImported.Traders _
                      On a.TraderID Equals t.TraderID _
                      And a.SnapshotID Equals t.SnapshotID _
                      Join m In dataImported.Mediators _
                      On a.MediatorID Equals m.MediatorID _
                      And a.SnapshotID Equals m.SnapshotID _
                    Where a.SnapshotID.Equals(PreviousSnap.ID) _
                    And a.IsCanceled = False _
                    And a.IsService = True _
                    Order By a.AppointmentDate _
                    Select t.TraderCode, t.TraderDescription, m.MediatorDescription, a.AppointmentDate, a.StartTime, a.EndTime, a.CalcHours).Distinct

        Dim Total1 As New List(Of String)
        Total1.Add(CurrentSnap.ExecuteDate.Month & "/" & CurrentSnap.ExecuteDate.Year)
        If CurrentHours.Count = 0 Then
            Total1.Add("0")
        Else
            Dim TempHours1 As Decimal = 0
            For Each row In CurrentHours
                TempHours1 += row.CalcHours
            Next
            Total1.Add(TempHours1.ToString("#.00"))
        End If
        DataList.Add(Total1)

        Dim Total2 As New List(Of String)
        Total2.Add(PreviousSnap.ExecuteDate.Month & "/" & PreviousSnap.ExecuteDate.Year)
        If PreviousHours.Count = 0 Then
            Total2.Add("0")
        Else
            Dim TempHours2 As Decimal = 0
            For Each row In PreviousHours
                TempHours2 += row.CalcHours
            Next
            Total2.Add(TempHours2.ToString("#.00"))
        End If
        DataList.Add(Total2)

        DataList.Add(New List(Of String))


        Dim HeaderRow As New List(Of String)
        HeaderRow.Add("ΚΩΔΙΚΟΣ ΠΕΛΑΤΗ")
        HeaderRow.Add("ΕΠΩΝΥΜΙΑ ΠΕΛΑΤΗ")
        HeaderRow.Add("ΥΠΑΛΛΗΛΟΣ")
        HeaderRow.Add("ΗΜΕΡΟΜΗΝΙΑ")
        HeaderRow.Add("ΩΡΕΣ")
        DataList.Add(HeaderRow)


        For Each hrs In CurrentHours
            Dim RowList As New List(Of String)
            RowList.Add(hrs.TraderCode)
            RowList.Add(hrs.TraderDescription)
            RowList.Add(hrs.MediatorDescription)
            RowList.Add(hrs.AppointmentDate.ToShortDateString)
            RowList.Add(hrs.CalcHours.ToString("#.00"))

            DataList.Add(RowList)
        Next

        DataList.Add(New List(Of String))

        For Each hrs In PreviousHours
            Dim RowList As New List(Of String)
            RowList.Add(hrs.TraderCode)
            RowList.Add(hrs.TraderDescription)
            RowList.Add(hrs.MediatorDescription)
            RowList.Add(hrs.AppointmentDate.ToShortDateString)
            RowList.Add(hrs.CalcHours.ToString("#.00"))

            DataList.Add(RowList)
        Next

        Return DataList

    End Function

    Private Function Offers(ByVal SnapshotID As Guid) As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))

        Dim AllOffers = (From e In dataImported.Entries _
                      Join t In dataImported.Traders _
                      On e.TraderID Equals t.TraderID _
                      And e.SnapshotID Equals t.SnapshotID _
                      Join m In dataImported.Mediators _
                      On e.MediatorID Equals m.MediatorID _
                      And e.SnapshotID Equals m.SnapshotID _
                    Where e.SnapshotID.Equals(SnapshotID) _
                    And e.AfterOffer = True _
                    Order By e.EntryDate _
                    Select t.TraderCode, t.TraderDescription, m.MediatorDescription, e.EntryDate, e.EntryFinalValue, e.IsOrder).Distinct

        Dim HeaderRow As New List(Of String)
        HeaderRow.Add("ΚΩΔΙΚΟΣ ΠΕΛΑΤΗ")
        HeaderRow.Add("ΕΠΩΝΥΜΙΑ ΠΕΛΑΤΗ")
        HeaderRow.Add("ΠΩΛΗΤΗΣ")
        HeaderRow.Add("ΗΜΕΡΟΜΗΝΙΑ")
        HeaderRow.Add("ΑΞΙΑ")
        HeaderRow.Add("ΚΑΤΑΣΤΑΣΗ")
        DataList.Add(HeaderRow)

        For Each ofr In AllOffers
            Dim RowList As New List(Of String)
            RowList.Add(ofr.TraderCode)
            RowList.Add(ofr.TraderDescription)
            RowList.Add(ofr.MediatorDescription)
            RowList.Add(ofr.EntryDate.ToShortDateString)
            RowList.Add(Convert.ToDecimal(ofr.EntryFinalValue).ToString("#.00"))
            If ofr.IsOrder Then
                RowList.Add("ΑΠΟΔΟΧΗ")
            Else
                RowList.Add("ΕΚΚΡΕΜΕΙ")
            End If
            DataList.Add(RowList)
        Next

        If AllOffers.Count > 0 Then

            DataList.Add(New List(Of String))

            Dim ApprovedOffers = AllOffers.Where(Function(f) f.IsOrder = True).Count

            Dim Total1 As New List(Of String)
            Total1.Add("ΑΠΟΔΟΧΗ")
            Total1.Add(ApprovedOffers.ToString("#"))
            Total1.Add(Convert.ToDecimal((ApprovedOffers / AllOffers.Count) * 100).ToString("#.00") & "%")

            DataList.Add(Total1)

            Dim Total2 As New List(Of String)
            Total2.Add("ΑΝΟΙΚΤΕΣ")
            Total2.Add((AllOffers.Count - ApprovedOffers).ToString("#"))
            Total2.Add(Convert.ToDecimal(((AllOffers.Count - ApprovedOffers) / AllOffers.Count) * 100).ToString("#.00") & "%")

            DataList.Add(Total2)
        End If

        Return DataList
    End Function

    Private Function AppOrders(ByVal SnapshotID As Guid) As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))

        Dim AllApps = (From a In dataImported.Appointments _
                     Where a.SnapshotID.Equals(SnapshotID) _
                     And a.IsService = False _
                     Order By a.AppointmentDate _
                     Select a).Distinct

        Dim AllEntries = (From e In dataImported.Entries _
                          Where e.SnapshotID.Equals(SnapshotID) _
                          Order By e.EntryDate _
                          Select e).Distinct

        Dim Total1 As New List(Of String)
        Total1.Add("ΡΑΝΤΕΒΟΥ")
        Total1.Add(AllApps.Count.ToString("#"))
        DataList.Add(Total1)

        Dim Total2 As New List(Of String)
        Total2.Add("ΠΡΟΣΦΟΡΕΣ")
        Total2.Add(AllEntries.Where(Function(f) f.IsOrder = False).Count.ToString("#"))
        DataList.Add(Total2)

        Dim Total3 As New List(Of String)
        Total3.Add("ΠΑΡΑΓΓΕΛΙΕΣ")
        Total3.Add(AllEntries.Where(Function(f) f.IsOrder = True).Count.ToString("#"))
        DataList.Add(Total3)

        Return DataList
    End Function

    Private Function MeanHours(ByVal SnapshotID As Guid) As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))

        Dim AllHours = (From a In dataImported.Appointments _
                      Join t In dataImported.Traders _
                      On a.TraderID Equals t.TraderID _
                      And a.SnapshotID Equals t.SnapshotID _
                      Join m In dataImported.Mediators _
                      On a.MediatorID Equals m.MediatorID _
                      And a.SnapshotID Equals m.SnapshotID _
                    Where a.SnapshotID.Equals(SnapshotID) _
                    And a.IsCanceled = False _
                    And a.IsService = True _
                    Order By a.AppointmentDate _
                    Select t.TraderCode, t.TraderDescription, m.MediatorDescription, a.AppointmentDate, a.StartTime, a.EndTime, a.CalcHours, a.AppointmentValue).Distinct

        Dim HeaderRow As New List(Of String)
        HeaderRow.Add("ΚΩΔΙΚΟΣ ΠΕΛΑΤΗ")
        HeaderRow.Add("ΕΠΩΝΥΜΙΑ ΠΕΛΑΤΗ")
        HeaderRow.Add("ΥΠΑΛΛΗΛΟΣ")
        HeaderRow.Add("ΗΜΕΡΟΜΗΝΙΑ")
        HeaderRow.Add("ΧΡΕΩΣΗ")
        HeaderRow.Add("ΩΡΕΣ")
        HeaderRow.Add("ΜΕΣΗ ΧΡΕΩΣΗ")
        DataList.Add(HeaderRow)

        For Each hrs In AllHours

            Dim MeanCharge As Double = 0
            If hrs.CalcHours <> 0 Then
                MeanCharge = hrs.AppointmentValue / hrs.CalcHours
            Else
                MeanCharge = 0
            End If

            'If MaxLimit = 0 OrElse MeanCharge <= MaxLimit Then
            Dim RowList As New List(Of String)
            RowList.Add(hrs.TraderCode)
            RowList.Add(hrs.TraderDescription)
            RowList.Add(hrs.MediatorDescription)
            RowList.Add(hrs.AppointmentDate.ToShortDateString)
            RowList.Add(hrs.AppointmentValue.ToString("#.00"))
            RowList.Add(hrs.CalcHours.ToString("#.00"))
            RowList.Add(MeanCharge.ToString("#.00"))

            DataList.Add(RowList)
            'End If
        Next

        Return DataList
    End Function

    Private Function OpenTasks(ByVal SnapshotID As Guid) As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))

        Dim SnapDate = (From t In dataImported.Snapshots _
              Where t.ID.Equals(SnapshotID) _
              Select t.ExecuteDate).FirstOrDefault

        Dim AllTasks = (From g In dataImported.Assignments _
                      Join t In dataImported.Traders _
                      On g.TraderID Equals t.TraderID _
                      And g.SnapshotID Equals t.SnapshotID _
                      Join m In dataImported.Mediators _
                      On g.MediatorID Equals m.MediatorID _
                      And g.SnapshotID Equals m.SnapshotID _
                    Where g.SnapshotID.Equals(SnapshotID) _
                    And g.IsCompleted = False _
                    Order By g.StartDate _
                    Select t.TraderCode, t.TraderDescription, m.MediatorDescription, g.StartDate).Distinct

        Dim HeaderRow As New List(Of String)
        HeaderRow.Add("ΚΩΔΙΚΟΣ ΠΕΛΑΤΗ")
        HeaderRow.Add("ΕΠΩΝΥΜΙΑ ΠΕΛΑΤΗ")
        HeaderRow.Add("ΥΠΑΛΛΗΛΟΣ")
        HeaderRow.Add("ΗΜΕΡΟΜΗΝΙΑ ΕΝΑΡΞΗΣ")
        HeaderRow.Add("ΗΜΕΡΕΣ ΕΚΚΡΕΜΟΤΗΤΑΣ")
        DataList.Add(HeaderRow)

        For Each tsk In AllTasks
            Dim DaysPending As Short = DateDiff(DateInterval.Day, tsk.StartDate, SnapDate)
            'If DaysPending >= MinLimit Then
            Dim RowList As New List(Of String)
            RowList.Add(tsk.TraderCode)
            RowList.Add(tsk.TraderDescription)
            RowList.Add(tsk.MediatorDescription)
            RowList.Add(tsk.StartDate.ToShortDateString)
            RowList.Add(DaysPending.ToString("#.00"))
            DataList.Add(RowList)
            'End If
        Next

        Return DataList
    End Function

    Private Function CompletionDays(ByVal SnapshotID As Guid) As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))

        Dim AllTasks = (From g In dataImported.Assignments _
                      Join t In dataImported.Traders _
                      On g.TraderID Equals t.TraderID _
                      And g.SnapshotID Equals t.SnapshotID _
                      Join m In dataImported.Mediators _
                      On g.MediatorID Equals m.MediatorID _
                      And g.SnapshotID Equals m.SnapshotID _
                    Where g.SnapshotID.Equals(SnapshotID) _
                    And g.IsCompleted = True _
                    Order By g.StartDate _
                    Select t.TraderCode, t.TraderDescription, m.MediatorDescription, g.StartDate, g.EndDate).Distinct

        Dim HeaderRow As New List(Of String)
        HeaderRow.Add("ΚΩΔΙΚΟΣ ΠΕΛΑΤΗ")
        HeaderRow.Add("ΕΠΩΝΥΜΙΑ ΠΕΛΑΤΗ")
        HeaderRow.Add("ΥΠΑΛΛΗΛΟΣ")
        HeaderRow.Add("ΗΜΕΡΟΜΗΝΙΑ ΕΝΑΡΞΗΣ")
        HeaderRow.Add("ΗΜΕΡΟΜΗΝΙΑ ΛΗΞΗΣ")
        HeaderRow.Add("ΗΜΕΡΕΣ")
        DataList.Add(HeaderRow)

        For Each tsk In AllTasks
            If tsk.EndDate IsNot Nothing Then
                Dim DaysPending As Short = DateDiff(DateInterval.Day, tsk.StartDate, CDate(tsk.EndDate))
                If DaysPending >= 0 Then
                    Dim RowList As New List(Of String)
                    RowList.Add(tsk.TraderCode)
                    RowList.Add(tsk.TraderDescription)
                    RowList.Add(tsk.MediatorDescription)
                    RowList.Add(tsk.StartDate.ToShortDateString)
                    RowList.Add(CDate(tsk.EndDate).ToShortDateString)
                    RowList.Add(DaysPending.ToString("#.00"))
                    DataList.Add(RowList)
                End If
            End If
        Next

        Return DataList
    End Function

    Private Function TurnOverDaily(ByVal SnapshotID As Guid) As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))

        Dim AllOffers = (From e In dataImported.Entries _
                      Join t In dataImported.Traders _
                      On e.TraderID Equals t.TraderID _
                      And e.SnapshotID Equals t.SnapshotID _
                      Join m In dataImported.Mediators _
                      On e.MediatorID Equals m.MediatorID _
                      And e.SnapshotID Equals m.SnapshotID _
                    Where e.SnapshotID.Equals(SnapshotID) _
                    And e.IsOrder = True _
                    Order By e.EntryDate _
                    Select t.TraderCode, t.TraderDescription, m.MediatorDescription, e.EntryDate, e.EntryFinalValue).Distinct

        Dim HeaderRow As New List(Of String)
        HeaderRow.Add("ΚΩΔΙΚΟΣ ΠΕΛΑΤΗ")
        HeaderRow.Add("ΕΠΩΝΥΜΙΑ ΠΕΛΑΤΗ")
        HeaderRow.Add("ΠΩΛΗΤΗΣ")
        HeaderRow.Add("ΑΞΙΑ")
        DataList.Add(HeaderRow)

        For Each ofr In AllOffers
            Dim RowList As New List(Of String)
            RowList.Add(ofr.TraderCode)
            RowList.Add(ofr.TraderDescription)
            RowList.Add(ofr.MediatorDescription)
            RowList.Add(Convert.ToDecimal(ofr.EntryFinalValue).ToString("#.00"))
            DataList.Add(RowList)
        Next
        Return DataList
    End Function
    Private Function NewSupplier(ByVal SnapshotID As Guid) As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))

        Dim AllSupplier = (From t In dataImported.Traders _
                     Where t.SnapshotID.Equals(SnapshotID) _
                      And t.TraderDescription <> "" _
                    Select t.TraderCode, t.TraderDescription).Distinct

        Dim HeaderRow As New List(Of String)
        HeaderRow.Add("ΚΩΔΙΚΟΣ ΠΡΟΜΗΘΕΥΤΗ")
        HeaderRow.Add("ΕΠΩΝΥΜΙΑ ΠΡΟΜΗΘΕΥΤΗ")
        
        DataList.Add(HeaderRow)

        For Each rest In AllSupplier
            Dim RowList As New List(Of String)
            RowList.Add(rest.TraderCode)
            RowList.Add(rest.TraderDescription)        
            DataList.Add(RowList)
        Next

        Return DataList
    End Function
End Class
