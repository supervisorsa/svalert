﻿Imports System.Runtime.InteropServices
Public Class ExporterFile

    Private _ExplainText As String = ""
    Public WriteOnly Property ExplainText() As String
        Set(ByVal value As String)
            _ExplainText = value
        End Set
    End Property

    Private Function SaveToCsv(ByVal DataList As List(Of List(Of String)), ByVal filename As String) As String
        Try
            Dim sw As IO.StreamWriter = New IO.StreamWriter(filename & ".csv", False, System.Text.Encoding.UTF8)

            For Each row In DataList
                Dim RowData As String = ""
                For Each col In row
                    RowData &= col & ";"
                Next
                sw.WriteLine(RowData)
            Next
            sw.Close()
            Return filename & ".csv"
        Catch
            Return ""
        End Try
    End Function

    Private Function SaveToExcel(ByVal DataList As List(Of List(Of String)), ByVal filename As String, _
                                Optional ByVal SheetName As String = "") As Boolean
        Dim OldCulture As System.Globalization.CultureInfo = System.Threading.Thread.CurrentThread.CurrentCulture
        Try
            Dim newCulture As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("en-US")
            System.Threading.Thread.CurrentThread.CurrentCulture = newCulture
            If Not ExcelExport(DataList, filename, SheetName) Then
                Return ExcelExport(DataList, filename, SheetName, True)
            Else
                Return True
            End If
            'Kill(filename & ".xml")
        Catch ex As Exception
            SvSys.ErrorLog("SaveToExcel".PadRight(10) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
            Return False
        Finally
            System.Threading.Thread.CurrentThread.CurrentCulture = OldCulture
        End Try
    End Function

    Private Function ExcelExport(ByVal DataList As List(Of List(Of String)), ByVal filename As String, _
                                Optional ByVal SheetName As String = "", _
                                 Optional ByVal LoadObjects As Boolean = False) As Boolean
        Try
            'Dim excelApp As New Excel.Application
            'Dim excelBook As Excel.Workbook = excelApp.Workbooks.Add
            'Dim excelWorksheet As Excel.Worksheet = _
            '    CType(excelBook.Worksheets(1), Excel.Worksheet)

            Dim excelApp As Object
            Dim excelBook As Object
            Dim excelWorksheet As Object

            If LoadObjects Then
                excelApp = CreateObject("Excel.Application")
                excelBook = excelApp.Workbooks.Add
                excelWorksheet = excelBook.Worksheets(1)
                '    excelBook.Worksheets.Add(, , 6)
            Else
                excelApp = New Excel.Application

                excelBook = excelApp.Workbooks.Add
                excelWorksheet = CType(excelBook.Worksheets(1), Excel.Worksheet)
                '   excelBook.Worksheets.Add(, , 6)
            End If

            'If SheetName <> String.Empty Then excelWorksheet.Name = SheetName + 1

            'excelWorksheet.Columns.Style.NumberFormat = "@"

            Dim Level1 As Short = 0

            Dim row As Integer = 1

            'excelWorksheet.Cells(row, 1) = "ΑΡΘΡΟ"
            'excelWorksheet.Cells(row, 2) = "ΠΕΡΙΓΡΑΦΗ ΑΡΘΡΟΥ"
            'excelWorksheet.Cells(row, 3) = "ΜΟΝΑΔΑ ΜΕΤΡΗΣΗΣ"
            'excelWorksheet.Cells(row, 4) = "ΠΟΣΟΤΗΤΑ"
            'excelWorksheet.Cells(row, 5) = "ΤΙΜΗ ΜΟΝΑΔΑΣ"
            'excelWorksheet.Cells(row, 6) = "ΜΕΡΙΚΟ ΣΥΝΟΛΟ"
            'excelWorksheet.Cells(row, 7) = "ΠΕΡΙΓΡΑΦΗ ΑΡΘΡΟΥ"



            'Dim FirstRow As Short = 2
            'Dim RowsCount As Short = 0

            If _ExplainText <> "" Then
                excelWorksheet.Cells(1, 1) = _ExplainText
                row += 2
            End If

            For Each line In DataList

                For i = 1 To line.Count
                    excelWorksheet.Cells(row, i) = line(i - 1)
                Next

                row += 1

                'If Level1 <> node.Elements("Level1").FirstOrDefault.Value Then
                '    If Level1 > 0 Then
                '        excelWorksheet.Columns.AutoFit()
                '        excelWorksheet.Columns(7).ColumnWidth = 80

                '        Dim oRng = excelWorksheet.Range("1:500").Rows
                '        oRng.WrapText = True
                '        oRng.Style.VerticalAlignment = Excel.Constants.xlCenter ' VerticalAlignType.Center
                '        oRng.AutoFit()

                '        'For i = 2 To oRng.Count
                '        '    If oRng.Cells(i, 7).Value <> "" And oRng(i).RowHeight < 40 Then
                '        '        oRng.Cells(i, 7).WrapText = True
                '        '        oRng(i).AutoFit()
                '        '    End If
                '        'Next

                '    End If

                '    If LoadObjects Then
                '        excelWorksheet = excelBook.Worksheets(Level1 + 1)
                '    Else
                '        excelWorksheet = CType(excelBook.Worksheets(Level1 + 1), Excel.Worksheet)
                '    End If

                '    excelWorksheet.Name = node.Elements("Name").FirstOrDefault.Value.ToString.Trim

                '    excelWorksheet.Columns.Style.NumberFormat = "@"


                '    row = 1
                '    excelWorksheet.Cells(row, 1) = "ΑΡΘΡΟ"
                '    excelWorksheet.Cells(row, 2) = "ΠΕΡΙΓΡΑΦΗ ΑΡΘΡΟΥ"
                '    excelWorksheet.Cells(row, 3) = "ΜΟΝΑΔΑ ΜΕΤΡΗΣΗΣ"
                '    excelWorksheet.Cells(row, 4) = "ΠΟΣΟΤΗΤΑ"
                '    excelWorksheet.Cells(row, 5) = "ΤΙΜΗ ΜΟΝΑΔΑΣ"
                '    excelWorksheet.Cells(row, 6) = "ΜΕΡΙΚΟ ΣΥΝΟΛΟ"
                '    excelWorksheet.Cells(row, 7) = "ΠΕΡΙΓΡΑΦΗ ΑΡΘΡΟΥ"


                '    Dim oRng2 = excelWorksheet.Range("1:500").Rows
                '    oRng2.Style.VerticalAlignment = Excel.Constants.xlCenter ' VerticalAlignType.Center

                '    'FirstRow = 2
                '    'RowsCount = 0

                '    Level1 = node.Elements("Level1").FirstOrDefault.Value
                'End If


                'row += 1

                ''RowsCount += 1
                'excelWorksheet.Cells(row, 1) = node.Attributes("Code").FirstOrDefault.Value '
                'excelWorksheet.Cells(row, 2) = node.Elements("Name").FirstOrDefault.Value
                'excelWorksheet.Cells(row, 3) = node.Elements("UnitName").FirstOrDefault.Value 'If(node.Elements("Euro").FirstOrDefault.Value.Equals(""), "", FormatDateTime(node.Elements("Euro").FirstOrDefault.Value, DateFormat.ShortTime))
                'excelWorksheet.Cells(row, 4) = node.Elements("Quantity").FirstOrDefault.Value
                'excelWorksheet.Cells(row, 5) = If(IsNumeric(node.Elements("Price").FirstOrDefault.Value), node.Elements("Price").FirstOrDefault.Value.ToString.Replace(".", ","), node.Elements("Price").FirstOrDefault.Value)
                'excelWorksheet.Cells(row, 6) = node.Elements("TotalValue").FirstOrDefault.Value
                'excelWorksheet.Cells(row, 7) = node.Elements("Description").FirstOrDefault.Value


            Next

            excelWorksheet.Columns.AutoFit()
            'excelWorksheet.Columns(7).ColumnWidth = 80

            Dim oRng1
            If row > 490 Then
                oRng1 = excelWorksheet.Range("1:5000").Rows
            Else
                oRng1 = excelWorksheet.Range("1:500").Rows
            End If

            Dim rg As Excel.Range
            If _ExplainText <> "" Then
                rg = excelWorksheet.Range("1:1").Rows
                rg.Font.Italic = True

                rg = excelWorksheet.Range("3:3").Rows
                rg.Font.Bold = True
            Else
                rg = excelWorksheet.Range("1:1").Rows
                rg.Font.Bold = True
            End If

            oRng1.WrapText = True
            oRng1.Style.VerticalAlignment = Excel.Constants.xlCenter ' VerticalAlignType.Center
            oRng1.AutoFit()

            excelBook.SaveAs(filename.Replace("\\", "\") & ".xls")
            'excelApp.Visible = True


            excelBook.Close()
            excelApp.Quit()

            Marshal.ReleaseComObject(excelWorksheet)
            Marshal.ReleaseComObject(excelBook)
            Marshal.ReleaseComObject(excelApp)


            Return True
        Catch ex As Exception
            SvSys.ErrorLog("ExcelExport".PadRight(10) & vbTab & filename & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
            Return False
        End Try
    End Function

    Public Function ExcelExport(ByVal filename As String, ByVal DataList As List(Of List(Of String))) As String
        Dim ExcelCreated As Boolean = SaveToExcel(DataList, filename, Format(Now, "yyyyMMdd"))

        If Not ExcelCreated Then
            'elem.Save(filename & ".xml")
            Return SaveToCsv(DataList, filename)
            'SaveToExcelObj(elem, filename)
        Else
            Return filename & ".xls"
        End If

    End Function
End Class
