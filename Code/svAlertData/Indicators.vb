﻿Public Class Indicators
    Public Sub New(ByVal ConnString As String)
        dataImported = New dataImportedDataContext(ConnString)
    End Sub

    Public Function Rest(ByRef SnapDate As DateTime) As Decimal
        Dim LastSnapshot = From s In dataImported.Snapshots _
                            Join t In dataImported.Traders _
                            On s.ID Equals t.SnapshotID _
                            Order By s.ExecuteDate Descending _
                            Select s.ID, s.ExecuteDate

        If LastSnapshot.Count = 0 Then
            Return 0
        Else
            Dim LastSnapshotID As Guid = LastSnapshot.FirstOrDefault.ID
            Dim SumRest As Decimal = 0
            Dim AllTraders = From t In dataImported.Traders _
                         Where t.SnapshotID.Equals(LastSnapshotID) _
                         Select t
            For Each trd In AllTraders
                SumRest += trd.Rest
            Next
            SnapDate = LastSnapshot.FirstOrDefault.ExecuteDate
            Return SumRest
        End If
    End Function

    Public Function AppsNo(ByRef SnapDate As DateTime, ByVal AppDate As Date) As Integer
        Dim LastSnapshot = From s In dataImported.Snapshots _
                            Join t In dataImported.Appointments _
                            On s.ID Equals t.SnapshotID _
                            Where s.ExecuteDate.Date.Equals(AppDate.Date) _
                            Order By s.ExecuteDate Descending _
                            Select s.ID, s.ExecuteDate
        If LastSnapshot.Count = 0 Then
            LastSnapshot = From s In dataImported.Snapshots _
                                Join t In dataImported.Appointments _
                                On s.ID Equals t.SnapshotID _
                                Order By s.ExecuteDate Descending _
                                Select s.ID, s.ExecuteDate
        End If

        If LastSnapshot.Count = 0 Then
            Return 0
        Else
            Dim LastSnapshotID As Guid = LastSnapshot.FirstOrDefault.ID
            SnapDate = LastSnapshot.FirstOrDefault.ExecuteDate
            Dim AllApps = From t In dataImported.Appointments _
                         Where t.SnapshotID.Equals(LastSnapshotID) _
                         And t.AppointmentDate.Equals(AppDate) _
                         Select t
            Return AllApps.Count
        End If
    End Function
End Class
