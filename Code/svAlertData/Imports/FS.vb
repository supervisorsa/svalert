﻿Public Class FS
    Private dataFS As dataFSDataContext

    Private SnapStartDate As Date
    Private SnapEndDate As Date

    Public Sub New(ByVal ConnectionString As String, ByVal StartDate As Date, ByVal EndDate As Date)
        dataFS = New dataFSDataContext(ConnectionString)
        dataFS.CommandTimeout = 300
        SnapStartDate = StartDate
        SnapEndDate = EndDate
    End Sub

    Public Function ImportTasks(ByVal CurrentSnapshot As Snapshot) As List(Of List(Of String))
        If SnapStartDate = Date.MinValue Then
            Dim PreviousSnapshot As Snapshot
            Dim AllSnapshots = (From t In dataImported.Assignments _
                 Select t.SnapshotID).Distinct
            LoadSnapshots(Guid.Empty, AllSnapshots.ToList, CurrentSnapshot, PreviousSnapshot)
            If PreviousSnapshot Is Nothing Then
                SnapStartDate = Now.Date.AddDays(-8)
            Else
                SnapStartDate = PreviousSnapshot.ExecuteDate.Date
            End If
            SnapEndDate = Now.Date.AddDays(-1)
        End If

        Dim DataList As New List(Of List(Of String))

        Dim AllTasks = From t In dataFS.alertTasks _
                     Where t.FromDate IsNot Nothing And ( _
                     (t.ToDate >= SnapStartDate And t.ToDate <= SnapEndDate) Or t.Closed = False) _
                     Select t

        For Each tsk In AllTasks
            Try
                Dim RowList As New List(Of String)
                RowList.Add(tsk.TaskID.ToString)
                RowList.Add(If(tsk.FromDate Is Nothing, "", CDate(tsk.FromDate).ToShortDateString))
                RowList.Add(ImproveData("String", tsk.TaskType))
                RowList.Add(ImproveData("String", tsk.Status))
                RowList.Add(1)
                RowList.Add(tsk.ContactID)
                RowList.Add(tsk.ContactCode)
                RowList.Add(tsk.ContactName)
                RowList.Add(tsk.EmplID)
                RowList.Add(tsk.EmplID)
                RowList.Add(tsk.EmplName)
                RowList.Add(If(tsk.FromDate Is Nothing, "", CDate(tsk.FromDate).ToShortDateString))
                RowList.Add(If(tsk.ToDate Is Nothing, "", CDate(tsk.ToDate).ToShortDateString))
                RowList.Add(tsk.Closed)
                RowList.Add(0)
                DataList.Add(RowList)
            Catch ex As Exception
                SvSys.ErrorLog("FS.ImportTasks".PadRight(10) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
            End Try
        Next
        Return DataList
    End Function

    Public Function ImportDiary(ByVal CurrentSnapshot As Snapshot) As List(Of List(Of String))
        If SnapStartDate = Date.MinValue Then
            Dim PreviousSnapshot As Snapshot
            Dim AllSnapshots = (From t In dataImported.Appointments _
                 Select t.SnapshotID).Distinct
            LoadSnapshots(Guid.Empty, AllSnapshots.ToList, CurrentSnapshot, PreviousSnapshot)
            If PreviousSnapshot Is Nothing Then
                SnapStartDate = Now.Date.AddDays(-8)
            Else
                SnapStartDate = PreviousSnapshot.ExecuteDate.Date
            End If
            SnapEndDate = Now.Date.AddDays(-1)
        End If

        Dim DataList As New List(Of List(Of String))

        Dim AllDiary = From t In dataFS.alertDiaries _
                     Where t.Date >= SnapStartDate _
                     And t.Date <= SnapEndDate _
                     Select t

        For Each dry In AllDiary
            Try
                Dim RowList As New List(Of String)
                RowList.Add(dry.ID.ToString)
                RowList.Add(CDate(dry.Date).ToShortDateString)
                RowList.Add(ImproveData("String", dry.DiaryType))
                RowList.Add(0)
                RowList.Add(1)
                RowList.Add(dry.ContactID)
                RowList.Add(dry.ContactCode)
                RowList.Add(dry.ContactName)
                RowList.Add(dry.EmplID)
                RowList.Add(dry.EmplID)
                RowList.Add(dry.EmplName)
                If dry.Time Is Nothing Then
                    RowList.Add("")
                Else
                    RowList.Add(CDate(dry.Time).ToShortTimeString)
                End If

                If dry.ToTime Is Nothing Then
                    RowList.Add("")
                Else
                    RowList.Add(CDate(dry.ToTime).ToShortTimeString)
                End If

                If dry.Time Is Nothing Or dry.ToTime Is Nothing Then
                    RowList.Add(0)
                Else
                    RowList.Add(DateDiff(DateInterval.Minute, CDate(dry.Time), CDate(dry.ToTime)) / 60)
                End If
                RowList.Add(dry.Notes)
                RowList.Add(dry.Canceled)
                RowList.Add(0)

                DataList.Add(RowList)


            Catch ex As Exception
                SvSys.ErrorLog("FS.ImportDiary.Diary".PadRight(10) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
            End Try
        Next


        Dim AllResults = From t In dataFS.alertResults _
                         Where t.Date >= SnapStartDate _
                         And t.Date <= SnapEndDate _
                         Select t

        For Each rst In AllResults
            Try
                Dim RowList As New List(Of String)
                RowList.Add(rst.ID.ToString)
                RowList.Add(CDate(rst.Date).ToShortDateString)
                RowList.Add(ImproveData("String", rst.DiaryType))
                RowList.Add(rst.Value)
                RowList.Add(1)
                RowList.Add(rst.ContactID)
                RowList.Add(rst.ContactCode)
                RowList.Add(rst.ContactName)
                RowList.Add(rst.EmplID)
                RowList.Add(rst.EmplID)
                RowList.Add(rst.EmplName)
                If rst.FromTime Is Nothing Then
                    RowList.Add("")
                Else
                    RowList.Add(CDate(rst.FromTime).ToShortTimeString)
                End If

                If rst.ToTime Is Nothing Then
                    RowList.Add("")
                Else
                    RowList.Add(CDate(rst.ToTime).ToShortTimeString)
                End If

                If rst.FromTime Is Nothing Or rst.ToTime Is Nothing Then
                    RowList.Add(0)
                Else
                    RowList.Add(DateDiff(DateInterval.Minute, CDate(rst.FromTime), CDate(rst.ToTime)) / 60)
                End If
                RowList.Add(rst.Name)
                RowList.Add(rst.Canceled)
                RowList.Add(1)

                DataList.Add(RowList)
            Catch ex As Exception
                SvSys.ErrorLog("FS.ImportDiary.Results".PadRight(10) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
            End Try
        Next

        Return DataList
    End Function

    Public Function ImportCustomers(ByVal CurrentSnapshot As Snapshot) As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))

        Dim AllContacts = From t In dataFS.alertCustomers _
                          Order By t.ID _
                          Select t

        For Each cnt In AllContacts
            Try
                Dim RowList As New List(Of String)
                RowList.Add(cnt.ID.ToString)
                RowList.Add(cnt.Euro)
                RowList.Add(cnt.Name)
                RowList.Add(1)
                RowList.Add(cnt.EmplID)
                RowList.Add(cnt.EmplID)
                RowList.Add(cnt.EmplName)
                RowList.Add(cnt.Xreosi)
                RowList.Add(cnt.Pistosi)
                RowList.Add(cnt.Xreosi - cnt.Pistosi)
                RowList.Add(0)
                RowList.Add(0)
                RowList.Add(cnt.IsActive)
                RowList.Add(cnt.InContract)
                RowList.Add(cnt.IsCustomer)
                RowList.Add(cnt.IsSupplier)
                RowList.Add(0)
                RowList.Add(0)
                RowList.Add(0)
                RowList.Add(0)
                RowList.Add(0)
                RowList.Add(1)
                RowList.Add(0)
                RowList.Add(0)
                DataList.Add(RowList)

            Catch ex As Exception
                SvSys.ErrorLog("FS.ImportCustomers".PadRight(10) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
            End Try
        Next
        Return DataList
    End Function

    Public Function ImportStock(ByVal CurrentSnapshot As Snapshot) As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))

        Dim AllStock = From t In dataFS.alertStocks _
                          Select t

        For Each stk In AllStock
            Try
                Dim RowList As New List(Of String)
                RowList.Add(stk.ID.ToString)
                RowList.Add(stk.Euro)
                RowList.Add(stk.Name)
                RowList.Add(stk.Sale)
                RowList.Add(stk.Buy)
                RowList.Add(0)
                RowList.Add(0)

                DataList.Add(RowList)

            Catch ex As Exception
                SvSys.ErrorLog("FS.ImportStock".PadRight(10) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
            End Try
        Next
        Return DataList
    End Function

    Public Function ImportEmpl(ByVal CurrentSnapshot As Snapshot) As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))

        Dim AllEmpls = From t In dataFS.alertEmpls _
                          Select t

        For Each emp In AllEmpls
            Try
                Dim RowList As New List(Of String)
                RowList.Add(emp.ID.ToString)
                RowList.Add(emp.ID.ToString)
                RowList.Add(emp.Name)
                RowList.Add(emp.DepartmentName)
                RowList.Add(1)
                RowList.Add(1)
                RowList.Add(1)
                RowList.Add(1)
                RowList.Add(1)
                RowList.Add(1)

                DataList.Add(RowList)

            Catch ex As Exception
                SvSys.ErrorLog("FS.ImportEmpl".PadRight(10) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
            End Try
        Next
        Return DataList
    End Function

    Public Function ImportOffers(ByVal CurrentSnapshot As Snapshot) As List(Of List(Of String))
        If SnapStartDate = Date.MinValue Then
            Dim PreviousSnapshot As Snapshot
            Dim AllSnapshots = (From t In dataImported.Entries _
                             Select t.SnapshotID).Distinct
            LoadSnapshots(Guid.Empty, AllSnapshots.ToList, CurrentSnapshot, PreviousSnapshot)
            If PreviousSnapshot Is Nothing Then
                SnapStartDate = Now.Date.AddDays(-8)
            Else
                SnapStartDate = PreviousSnapshot.ExecuteDate.Date
            End If
            SnapEndDate = Now.Date.AddDays(-1)
        End If

        Dim DataList As New List(Of List(Of String))

        Dim AllOffers = From t In dataFS.alertOffers _
                     Where (t.Date >= SnapStartDate And t.Date <= SnapEndDate) _
                     Or (t.OrderDate >= SnapStartDate And t.OrderDate <= SnapEndDate) _
                     Select t

        For Each ofr In AllOffers
            Try
                Dim RowList As New List(Of String)
                RowList.Add(ofr.ID.ToString)
                RowList.Add(ofr.ID.ToString)
                RowList.Add(CDate(ofr.Date).ToShortDateString)
                RowList.Add(ImproveData("String", ofr.Status))
                RowList.Add(ofr.TotalValue)
                RowList.Add(1)
                RowList.Add(ofr.ContactID)
                RowList.Add(ofr.ContactEuro)
                RowList.Add(ofr.ContactName)
                RowList.Add(ofr.EmplID)
                RowList.Add(ofr.EmplID)
                RowList.Add(ofr.EmplName)
                RowList.Add(If(ofr.OrderDate Is Nothing, "", CDate(ofr.OrderDate).ToShortDateString))
                RowList.Add(1)
                RowList.Add(1)
                RowList.Add(ofr.IsOrder)
                RowList.Add(1)
                RowList.Add(ofr.IsOrder)

                If ofr.Date >= SnapStartDate And ofr.Date <= SnapEndDate Then
                    RowList.Add(1)
                Else
                    RowList.Add(0)
                End If

                If ofr.OrderDate IsNot Nothing AndAlso ofr.OrderDate >= SnapStartDate And ofr.OrderDate <= SnapEndDate Then
                    RowList.Add(1)
                Else
                    RowList.Add(0)
                End If

                RowList.Add(ofr.DetailID)
                RowList.Add(ImproveData("Numeric", ofr.StockID))
                RowList.Add(ImproveData("String", ofr.StockEuro))
                RowList.Add(ImproveData("String", ofr.StockName))
                RowList.Add(ofr.StockPreValue)
                RowList.Add(ofr.StockFinalValue)
                RowList.Add(ofr.Quantity)
                RowList.Add(ofr.DiscountPercentage)
                RowList.Add(ofr.LineDiscountValue)
                RowList.Add(0)
                RowList.Add(0)

                DataList.Add(RowList)
            Catch ex As Exception
                SvSys.ErrorLog("FS.ImportOffers".PadRight(10) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
            End Try
        Next
        Return DataList
    End Function
End Class
