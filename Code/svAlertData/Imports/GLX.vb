﻿
Imports svGlobal
Public Class GLX
    Private dataGlx As dataGLXDataContext

    Private SnapStartDate As Date
    Private SnapEndDate As Date

    Public Sub New(ByVal ConnectionString As String, ByVal StartDate As Date, ByVal EndDate As Date)
        dataGlx = New dataGLXDataContext(ConnectionString)
        dataGlx.CommandTimeout = 300
        SnapStartDate = StartDate
        SnapEndDate = EndDate
    End Sub

    Public Function ImportLots(ByVal CurrentSnapshot As Snapshot) As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))

        Dim AllLot = From t In dataGlx.usAlertLots _
                          Select t

        For Each lot In AllLot
            Try
                Dim RowList As New List(Of String)
                RowList.Add(lot.GXID.ToString)
                RowList.Add(lot.GXCODE)
                RowList.Add(lot.GXEXPIRATIONDATE)
                RowList.Add(lot.GXITEMID.ToString)

                DataList.Add(RowList)

            Catch ex As Exception
                SvSys.ErrorLog("FS.ImportLot".PadRight(10) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
            End Try
        Next
        Return DataList
    End Function

    Public Function ImportItem(ByVal CurrentSnapshot As Snapshot) As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))

        Dim AllItem = From t In dataGlx.usAlertItems _
                          Select t

        For Each stk In AllItem
            Try
                Dim RowList As New List(Of String)
                RowList.Add(stk.GXID.ToString)
                RowList.Add(stk.GXCODE)
                RowList.Add(stk.GXDESCRIPTION)
                RowList.Add(stk.PriceRetail)
                RowList.Add(stk.PriceWholesale)
                RowList.Add(stk.BalanceRest)
                RowList.Add(stk.NewAlert)

                DataList.Add(RowList)

            Catch ex As Exception
                SvSys.ErrorLog("FS.ImportItem".PadRight(10) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
            End Try
        Next
        Return DataList
    End Function


End Class
