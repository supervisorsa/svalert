﻿Imports svGlobal
Public Class Control
    Private dataControl As dataControlDataContext

    Private SnapStartDate As Date
    Private SnapEndDate As Date

    Public Sub New(ByVal ConnectionString As String, ByVal StartDate As Date, ByVal EndDate As Date)
        dataControl = New dataControlDataContext(ConnectionString)
        dataControl.CommandTimeout = 300
        SnapStartDate = StartDate
        SnapEndDate = EndDate
    End Sub

    Public Function ImportCustomersNew(ByVal CurrentSnapshot As Snapshot, ByVal OldDate As DateTime, ByVal NewDate As DateTime) As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))

        Dim AllContacts = From t In dataControl.SVLOGs _
                          Where t.ModuleID = "Client" _
                          And t.EntryDate >= OldDate _
                          And t.EntryDate < NewDate _
                          Order By t.ID _
                          Select t

        For Each cnt In AllContacts
            Try
                Dim RowList As New List(Of String)
                RowList.Add(cnt.ObjectID.ToString)
                RowList.Add(cnt.DLLMessage.Substring(cnt.DLLMessage.IndexOf(":") + 1).Trim)
                RowList.Add(cnt.ObjectDescr)
                RowList.Add(1)
                RowList.Add(0)
                RowList.Add(0)
                RowList.Add("")
                RowList.Add(0)
                RowList.Add(0)
                RowList.Add(0)
                RowList.Add(0)
                RowList.Add(0)
                RowList.Add(1)
                RowList.Add(0)
                RowList.Add(1)
                RowList.Add(0)
                RowList.Add(0)
                RowList.Add(0)
                RowList.Add(0)
                RowList.Add(0)
                RowList.Add(0)
                RowList.Add(1)
                RowList.Add(0)
                RowList.Add(0)
                RowList.Add(1)
                DataList.Add(RowList)

            Catch ex As Exception
                SvSys.ErrorLog("Control.ImportCustomers".PadRight(10) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
            End Try
        Next
        Return DataList
    End Function

    Public Function ImportStockNew(ByVal CurrentSnapshot As Snapshot, ByVal OldDate As DateTime, ByVal NewDate As DateTime) As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))

        Dim AllStock = From t In dataControl.SVLOGs _
                       Where t.ModuleID = "Stock" _
                       And t.EntryDate >= OldDate _
                       And t.EntryDate < NewDate _
                       And Not t.DLLMessage.Contains("Updated Purchase-Price changed") _
                       And Not t.DLLMessage.Contains("New price") _
                       Select t

        For Each stk In AllStock
            Try
                Dim RowList As New List(Of String)
                RowList.Add(stk.ObjectID.ToString)
                RowList.Add(stk.DLLMessage.Substring(stk.DLLMessage.IndexOf(":") + 1).Trim)
                RowList.Add(stk.ObjectDescr)
                RowList.Add(0)
                RowList.Add(0)
                RowList.Add(0)
                RowList.Add(1)

                DataList.Add(RowList)

            Catch ex As Exception
                SvSys.ErrorLog("Control.ImportStock".PadRight(10) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
            End Try
        Next
        Return DataList
    End Function

    Public Function ImportStockPrice(ByVal CurrentSnapshot As Snapshot, ByVal OldDate As DateTime, ByVal NewDate As DateTime) As List(Of List(Of String))
        Dim DataList As New List(Of List(Of String))

        Dim AllStock = From t In dataControl.SVLOGs _
                       Where t.ModuleID = "Stock" _
                       And t.EntryDate >= OldDate _
                       And t.EntryDate < NewDate _
                       And (t.DLLMessage.Contains("Updated Purchase-Price changed") _
                       Or t.DLLMessage.Contains("New price")) _
                       Select t

        For Each stk In AllStock
            Try
                Dim RowList As New List(Of String)
                RowList.Add(stk.ObjectID.ToString)
                RowList.Add(stk.DLLMessage.Substring(stk.DLLMessage.IndexOf(":") + 1).Trim)
                RowList.Add(stk.ObjectDescr)
                RowList.Add(stk.DLLMessage.Substring(stk.DLLMessage.IndexOf(":") + 1).Trim)
                RowList.Add(0)
                RowList.Add(0)
                RowList.Add(1)

                DataList.Add(RowList)

            Catch ex As Exception
                SvSys.ErrorLog("Control.ImportStock".PadRight(10) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
            End Try
        Next
        Return DataList
    End Function

End Class
