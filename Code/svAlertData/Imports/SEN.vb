﻿Imports Oracle.DataAccess.Client
Imports System.Configuration
Imports System.Threading

Public Class SEN
    Private SnapStartDate As DateTime
    Private SnapEndDate As Date
    Private UserId As String
    Private Password As String
    Private DataSource As String
    Private Data As dataImportedDataContext
    Private Const GETSUPPLIER As String = _
    "SELECT sv.LEEID as LEEID, sv.LEENAME as LEENAME ,sv.LEEAFM as LEEAFM,sv.PMTTITLE as PMTTITLE ,usr.FULLNAME ,sv.SYS_LUPD as LUPD ,sv.SYS_INS AS INS,sv.TRACODE AS TRACODE" & _
    " FROM SUPVIEW sv INNER JOIN SECUSR usr ON SV.SYS_USRID=usr.ID " & _
    " WHERE TO_NUMBER(TO_CHAR(sv.SYS_LUPD,'ddmmyy')) = {0} "

    Private Const SENCONNSTRING As String = "Data Source={0};User Id={1};Password={2};"

    Public Sub New(ByRef dataImported As dataImportedDataContext, ByVal startDate As DateTime)
        Data = dataImported
        GetUserPasswordDataSource()
        SnapStartDate = startDate

    End Sub

    Public Function ImportSenSuppliers(ByVal SnapshotID As Guid) As Boolean

        Try
            Dim dbMan = New DBManager(String.Format(SENCONNSTRING, DataSource, UserId, Password))

            Dim dt = Convert.ToInt64(Format(DateTime.Today, "ddMMyy"))

            Dim query As String = String.Format(GETSUPPLIER, dt)
            If dbMan.Connect() Then
                dbMan.ExecuteQuery(query)
                Dim Supplier = New Trader
                Dim dataList As New List(Of String)

                Do While dbMan.OracleReader.Read()
                    With dbMan                        
                        dataList.Add(.OracleReader("TRACODE"))    'TraderID '0
                        dataList.Add(.OracleReader("LEENAME"))  'TraderCode '1
                        dataList.Add(.OracleReader("LEEAFM"))   'TraderDescription '2
                        dataList.Add(.OracleReader("PMTTITLE")) 'CompanyID '3
                        dataList.Add(.OracleReader("FULLNAME")) 'MediatorID '4                        
                        dataList.Add(IIf(.OracleReader("LUPD").Equals(.OracleReader("INS")), "Εισαγωγή Nέου", "Μεταβολή")) 'MediatorCode '5
                        dataList.Add("") 'MediatorDescription  '6
                        dataList.Add("0") 'Charge '7
                        dataList.Add("0") 'Credit '8
                        dataList.Add("0") 'Rest '9
                        dataList.Add("0") 'CapLimit '10
                        dataList.Add("0") 'CreditLimit '11
                        dataList.Add("1") 'IsActive 12
                        dataList.Add("0") 'InContract '13
                        dataList.Add("0") 'IsCustomer '14
                        dataList.Add("1") 'IsSupplier '15
                        dataList.Add("0") 'IsDebtor '16
                        dataList.Add("0") 'IsCreditor  '17                      
                        dataList.Add("") 'UseInCommercial '18
                        dataList.Add("") 'UseInServices '19
                        dataList.Add("") 'UseInFinancial '20
                        dataList.Add("1") 'NewAlert '21
                        Supplier.Import(dataList, SnapshotID, True)
                        dataList = New List(Of String)
                        Supplier = New Trader

                    End With
                Loop
            End If

        Catch ex As Exception
            SvSys.ErrorLog("ImportSenSuppliers " & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
            Throw New Exception(ex.Message)
        End Try
    End Function
    Private Sub GetUserPasswordDataSource()

        Dim aConfig As ConfigurationSettings
        DataSource = aConfig.AppSettings("DataSource")
        UserId = aConfig.AppSettings("UserName")
        Password = aConfig.AppSettings("Password")
    End Sub
    ' ConnectSen()
    '"SEN_DB" ' svAlertReportAndScenario.Globals.SasAliasName
    'Dim sStrSelect As String = "SELECT * FROM LEE"
    '    "SELECT sv.LEENAME as LEENAME ,sv.LEEAFM as LEEAFM,sv.PMTTITLE as PMTTITLE FROM s01800.SUPVIEW sv"
    'sv INNER JOIN s01800.SECUSR usr ON SV.SYS_USRID=usr.ID
    'Dim kbmSup As New kbm
    'SacSession = New SacSessionX
    'SacSession.Host = "server2003" 'svAlertReportAndScenario.Globals.Host
    'SacSession.Port = "400" 'svAlertReportAndScenario.Globals.Port
    'SacSession.Application = "SEN"
    'SacSession.Open()
    'SacSession.Session.ToString()
    'Dim v As Object
    'Dim arr(5) As Object
    'Dim arr0(1) As Object
    'Dim VersionStr As String
    'oSenAppl = SacSession.CreateObject("TSenApplication", False)
    'arr0(0) = ""
    'VersionStr = oSenAppl.Call("ISenApplication_GetVersionString", arr0)(0)
    'arr(0) = "SNG" 'svAlertReportAndScenario.Globals.UserName
    'arr(1) = "" ' svAlertReportAndScenario.Globals.PassWord
    'arr(2) = "SEN_DB" 'svAlertReportAndScenario.Globals.SasAliasName
    'arr(3) = VersionStr
    'arr(4) = ""
    'v = oSenAppl.Call("ISenApplication_Login", arr)
    'Dim arr1(2) As Object
    'arr1(0) = "cmpcode"
    'arr1(1) = "800" ' GetFullCode(svAlertReportAndScenario.Globals.CmpCode)
    'v = oSenAppl.Call("ISenApplication_SetVariable", arr1)
    'arr1(0) = "fiyid"
    'arr1(1) = "201501001" 'svAlertReportAndScenario.Globals.currentfiyid
    'v = oSenAppl.Call("ISenApplication_SetVariable", arr1)
    'arr1(0) = "wrhid"
    'arr1(1) = "1" ' GetFullCode(svAlertReportAndScenario.Globals.WrhID)
    'v = oSenAppl.Call("ISenApplication_SetVariable", arr1)
    'arr1(0) = "workingdate"
    'arr1(1) = Now
    'v = oSenAppl.Call("ISenApplication_SetVariable", arr1)
    'SacSession.Timeout = 100000
    'sasAlias = oSenAppl.Call("ISenApplication_GetAliasName", Nothing)(0)
    'kbmSup.DataPacket = SacSession.Call("OpenSQL", New Object() {sasAlias, sStrSelect, 0})(0)
    '     Loop
    '    kbmSup.EmptyAll()
    '  SacSession = Nothing
    '  oSenAppl = Nothing

    'Public Sub ConnectSen()
    '    Try
    '        SacSession = New SacSessionX
    '        SacSession.Host = "SRVSENNEW" 'svAlertReportAndScenario.Globals.Host
    '        SacSession.Port = "400" 'svAlertReportAndScenario.Globals.Port
    '        SacSession.Application = "SEN"
    '        SacSession.Open()
    '        SacSession.Session.ToString()
    '        Dim v As Object
    '        Dim arr(5) As Object
    '        Dim arr0(1) As Object
    '        Dim VersionStr As String
    '        oSenAppl = SacSession.CreateObject("TSenApplication", False)
    '        arr0(0) = ""
    '        VersionStr = oSenAppl.Call("ISenApplication_GetVersionString", arr0)(0)
    '        arr(0) = "SNG" 'svAlertReportAndScenario.Globals.UserName
    '        arr(1) = "" ' svAlertReportAndScenario.Globals.PassWord
    '        arr(2) = "SEN_DB" 'svAlertReportAndScenario.Globals.SasAliasName
    '        arr(3) = ""
    '        arr(4) = ""
    '        v = oSenAppl.Call("ISenApplication_Login", arr)
    '        Dim arr1(2) As Object
    '        arr1(0) = "cmpcode"
    '        arr1(1) = "800" ' GetFullCode(svAlertReportAndScenario.Globals.CmpCode)
    '        v = oSenAppl.Call("ISenApplication_SetVariable", arr1)
    '        arr1(0) = "fiyid"
    '        arr1(1) = "201501001" 'svAlertReportAndScenario.Globals.currentfiyid
    '        v = oSenAppl.Call("ISenApplication_SetVariable", arr1)
    '        arr1(0) = "wrhid"
    '        arr1(1) = "1" ' GetFullCode(svAlertReportAndScenario.Globals.WrhID)
    '        v = oSenAppl.Call("ISenApplication_SetVariable", arr1)
    '        arr1(0) = "workingdate"
    '        arr1(1) = Now
    '        v = oSenAppl.Call("ISenApplication_SetVariable", arr1)
    '        SacSession.Timeout = 100000
    '        sasAlias = oSenAppl.Call("ISenApplication_GetAliasName", Nothing)(0)

    '    Catch ex As Exception
    '        SvSys.ErrorLog("ImportSingleTrader " & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
    '        Throw New Exception(ex.Message)
    '    End Try
    'End Sub
    'Private Function GetFullCode(ByVal code As String) As String
    '    Dim TempCode As String = code
    '    Do Until TempCode.Length >= 3
    '        TempCode = "0" & TempCode
    '    Loop
    '    Return TempCode
    'End Function
End Class
