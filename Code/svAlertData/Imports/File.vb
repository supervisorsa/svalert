﻿Imports System.IO
Public Class File

    Public Sub LoadExcel(ByVal filename As String, ByVal IncludeHeaders As Boolean, _
                               ByRef DataList As List(Of List(Of String)), _
                               ByRef HeaderColumns As List(Of String))
        Dim FirstRow As Short = If(IncludeHeaders, 2, 1)
        Dim ColumnsCount As Short = -1

        Dim xlApp As Excel.Application
        Dim xlWorkBook As Excel.Workbook
        Dim xlWorkSheet As Excel.Worksheet

        Dim ActualFile As String

        If filename.IndexOf("*") = -1 Then
            ActualFile = filename
        Else
            ActualFile = filename.Replace("*", "")
            Dim DirectoryPath As String
            Dim NakedFilename As String
            DirectoryPath = Path.GetDirectoryName(ActualFile)
            NakedFilename = Path.GetFileNameWithoutExtension(ActualFile)

            Dim dInfo As New IO.DirectoryInfo(DirectoryPath)
            Dim AllFiles = dInfo.GetFiles().ToList.OrderByDescending(Function(f) f.CreationTime)

            For Each imp In AllFiles
                If imp.Name.StartsWith(NakedFilename) Then
                    ActualFile = imp.FullName
                    Exit For
                End If
            Next
        End If

        If IO.File.Exists(ActualFile) Then
            xlApp = New Excel.ApplicationClass
            xlWorkBook = xlApp.Workbooks.Open(ActualFile)

            For Each xlWorkSheet In xlWorkBook.Worksheets
                Dim oRng = xlWorkSheet.UsedRange.Rows ' .Range("1:500").Rows

                If IncludeHeaders Then
                    For j = 1 To oRng.Cells.Count
                        If oRng.Cells(1, j).Value IsNot Nothing AndAlso oRng.Cells(1, j).Value <> "" Then
                            HeaderColumns.Add(oRng.Cells(1, j).Value)
                        End If
                    Next
                    ColumnsCount = HeaderColumns.Count
                Else
                    ColumnsCount = oRng.Columns.Count
                    If ColumnsCount < 20 Then
                        ColumnsCount = 20
                    End If
                End If

                For i = FirstRow To oRng.Count

                    If oRng.Cells(i, 1).Value Is Nothing OrElse oRng.Cells(i, 1).Value.ToString = "" Then

                    Else
                        Dim RowData As New List(Of String)

                        If IncludeHeaders Then
                            For j = 1 To ColumnsCount
                                If oRng.Cells(i, j).Value IsNot Nothing AndAlso oRng.Cells(i, j).Value <> "" Then
                                    RowData.Add(oRng.Cells(i, j).Value.ToString)
                                Else
                                    RowData.Add("")
                                End If
                            Next
                        Else
                            For j = 1 To ColumnsCount
                                If oRng.Cells(i, j).Value IsNot Nothing Then
                                    If oRng.Cells(i, j).NumberFormat.ToString.IndexOf(":") > -1 AndAlso _
                                        (oRng.Cells(i, j).NumberFormat.ToString.IndexOf("/") = 0 OrElse _
                                         oRng.Cells(i, j).NumberFormat.ToString.IndexOf("/") > oRng.Cells(i, j).NumberFormat.ToString.IndexOf(":")) Then
                                        RowData.Add(oRng.Cells(i, j).Text)
                                    Else
                                        RowData.Add(oRng.Cells(i, j).Value)
                                    End If
                                Else
                                    RowData.Add("")
                                End If
                            Next
                        End If

                        DataList.Add(RowData)
                    End If
                Next
                System.Runtime.InteropServices.Marshal.FinalReleaseComObject(xlWorkSheet)
            Next

            xlWorkBook.Close()
            xlApp.Quit()

            System.Runtime.InteropServices.Marshal.FinalReleaseComObject(xlWorkBook)
            System.Runtime.InteropServices.Marshal.FinalReleaseComObject(xlApp)

            xlWorkBook = Nothing
            xlApp = Nothing

            GC.GetTotalMemory(False)
            GC.Collect()
            GC.WaitForPendingFinalizers()
            GC.Collect()
            GC.GetTotalMemory(True)
        End If
    End Sub

End Class
