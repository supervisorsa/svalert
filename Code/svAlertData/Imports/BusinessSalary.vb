﻿Public Class BusinessSalary
    Private dataBusinessSalary As dataBusinessSalaryDataContext

    Private SnapStartDate As Date
    Private SnapEndDate As Date

    Public Sub New(ByVal ConnectionString As String, ByVal StartDate As Date, ByVal EndDate As Date)
        dataBusinessSalary = New dataBusinessSalaryDataContext(ConnectionString)
        dataBusinessSalary.CommandTimeout = 300
        SnapStartDate = StartDate
        SnapEndDate = EndDate
    End Sub

    Public Function ImportTimetable(ByVal CurrentSnapshot As Snapshot) As List(Of List(Of String))
        If SnapStartDate = Date.MinValue Then
            Dim PreviousSnapshot As Snapshot
            Dim AllSnapshots = (From t In dataImported.Timetables _
                 Select t.SnapshotID).Distinct
            LoadSnapshots(Guid.Empty, AllSnapshots.ToList, CurrentSnapshot, PreviousSnapshot)
            If PreviousSnapshot Is Nothing Then
                SnapStartDate = Now.Date.AddDays(-2)
            Else
                SnapStartDate = PreviousSnapshot.ExecuteDate.Date
            End If
            SnapEndDate = Now.Date.AddDays(-1)
        End If

        Dim DataList As New List(Of List(Of String))

        Dim AllTimetables = From t In dataBusinessSalary.svvTimetables _
                     Where t.CLOCK_DATE >= SnapStartDate And t.CLOCK_DATE <= SnapEndDate _
                     Select t

        For Each tmt In AllTimetables
            Try
                Dim RowList As New List(Of String)
                RowList.Add(tmt.CODE)
                RowList.Add(tmt.FullName)
                RowList.Add(tmt.ID_EMP.ToString)
                RowList.Add(tmt.CLOCK_DATE)
                RowList.Add(If(tmt.HOUR_FROM Is Nothing, "", CDate(tmt.HOUR_FROM).ToString))
                RowList.Add(If(tmt.HOUR_TO Is Nothing, "", CDate(tmt.HOUR_TO).ToString))
                RowList.Add(ImproveData("Boolean", tmt.IsDriver))
                DataList.Add(RowList)
            Catch ex As Exception
                SvSys.ErrorLog("BusinessSalary.ImportTimetable".PadRight(10) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
            End Try
        Next

        Dim MissingTimetables = From t In dataBusinessSalary.svvMissings _
             Where t.CLOCK_DATE >= SnapStartDate And t.CLOCK_DATE <= SnapEndDate _
             Select t

        For Each tmt In MissingTimetables
            Try
                Dim RowList As New List(Of String)
                RowList.Add(tmt.CODE)
                RowList.Add(tmt.FullName)
                RowList.Add(tmt.ID_EMP.ToString)
                RowList.Add(tmt.CLOCK_DATE)
                RowList.Add("")
                RowList.Add("")
                RowList.Add(ImproveData("Boolean", tmt.IsDriver))
                DataList.Add(RowList)
            Catch ex As Exception
                SvSys.ErrorLog("BusinessSalary.ImportTimetable".PadRight(10) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
            End Try
        Next

        Return DataList
    End Function

End Class
