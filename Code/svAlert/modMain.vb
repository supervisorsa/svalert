﻿Module modMain
    Public dataMain As svAlertSnap.dataMainDataContext
    Public ConnStringMain As String
    Public ConnStringData As String

    Public ReportsNames As Dictionary(Of Short, String)
    Public ImportsNames As Dictionary(Of Short, String)

    Public UserID As Guid

    Public EnDecrypt As New svMerit.SvCrypto(svMerit.SvCrypto.SymmProvEnum.Rijndael)

    Public Sub LoadReportNames()
        Dim NewGlobal As New svAlertReportAndScenario.Globals
        ReportsNames = NewGlobal.Reports
        ImportsNames = NewGlobal.Importes
    End Sub

    Public Function DbConnected() As Boolean
        Try
            ''  pk
            ''  Bypass this for Form1, return true
            If Not OpenDatabase() Then
                'GlobalsClass.HandleErrorMsg(MSG_DB_CONNECTED_FAILED)
                Dim frm As New DbConnForm
                frm.WriteToConfig = False
                frm.ShowDialog()
                If frm.DB <> "" Then
                    ConnStringMain = DbConn.ConstructConnString(frm.Server, frm.User, frm.Pwd, frm.DB, "")
                    Settings.SetStringValue("MainServer", frm.Server)
                    Settings.SetStringValue("MainUser", frm.User)
                    Settings.SetStringValue("MainPwd", EnDecrypt.Encrypting(frm.Pwd, svAlertReportAndScenario.Globals.GlobalKey, "-"))
                    Settings.SetStringValue("MainDB", frm.DB)
                    Settings.SetStringValue("DataServer", frm.Server)
                    Settings.SetStringValue("DataUser", frm.User)
                    Settings.SetStringValue("DataPwd", EnDecrypt.Encrypting(frm.Pwd, svAlertReportAndScenario.Globals.GlobalKey, "-"))

                    Return OpenDatabase()
                Else
                    Return False
                End If
            Else
                Return True
            End If
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Function OpenDatabase() As Boolean
        Try
            'Dim ConnClass As New GlobalsClass
            'ConnClass.Server = AppSettings.Settings.GetString("Server")
            'ConnClass.DB = AppSettings.Settings.GetString("DB")
            'ConnClass.User = AppSettings.Settings.GetString("User")
            'ConnClass.Password = AppSettings.Settings.GetString("Pwd")
            'MASTERConnectionString = ConnClass.ConnString()

            Dim connection As New System.Data.SqlClient.SqlConnection(ConnStringMain)
            connection.Open()
            'Dim CurrentOrg As Organization = New Organization
            'CurrentOrg.Load()
            Return True
        Catch ex As Exception
            Return False
        End Try

    End Function

End Module
