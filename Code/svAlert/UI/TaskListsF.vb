﻿Public Class TaskListsF
    Private SelectedList As svAlertSnap.TaskList

    Private Sub TaskListsF_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        GetDatasource()
    End Sub

    Private Sub GetDatasource()
        Dim AllTaskLists = From t In dataMain.TaskLists _
                     Order By t.ListName _
                     Select t

        SvDevExpress.GridDataSource(dgdTaskList, gdviewTaskList, AllTaskLists)
        InitGrid()
    End Sub

    Public Sub RefreshData()
        GetDatasource()
    End Sub

    Private Sub InitGrid()
        For Each col As GridColumn In gdviewTaskList.Columns
            If col.FieldName.ToLower.Contains("listname") Then
                col.Caption = "Όνομα λίστας"
            ElseIf col.FieldName.ToLower.Contains("lastrun") Then
                col.Caption = "Τελ.ενημέρωση"
                col.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
                col.DisplayFormat.FormatString = "g"
            ElseIf col.FieldName.ToLower.Contains("nextrun") Then
                col.Caption = "Επόμ.ενημέρωση"
                col.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
                col.DisplayFormat.FormatString = "g"
            ElseIf col.FieldName.ToLower.Contains("active") Then
                col.Caption = "Ενεργή"
            Else
                col.Visible = False
            End If
        Next
        gdviewTaskList.BestFitColumns()
    End Sub

    Private Sub btnAdd_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnAdd.ItemClick
        Dim frm As New TaskListWizardF
        frm.ShowDialog()
        GetDatasource()
    End Sub

    Private Sub btnExecute_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnExecute.ItemClick
        If gdviewTaskList.FocusedRowHandle > -1 Then
            If BackgroundWorker1.IsBusy Then
                MessageBox.Show("Γίνεται ήδη λήψη δεδομένων" & vbCrLf & "Παρακαλώ δοκιμάστε αργότερα", "", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                SelectedList = CType(gdviewTaskList.GetRow(gdviewTaskList.FocusedRowHandle), svAlertSnap.TaskList)
                AlertControl1.Show(Me, SelectedList.ListName, "Η λήψη δεδομένων ξεκίνησε")
                BackgroundWorker1.RunWorkerAsync()
            End If
        End If
    End Sub

    Private Sub btnDelete_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnDelete.ItemClick
        If gdviewTaskList.FocusedRowHandle > -1 Then
            If MessageBox.Show("Θέλετε σίγουρα να γίνει διαγραφή;", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                Delete()
                GetDatasource()
            End If
        End If
    End Sub

    Private Sub Delete()
        Dim TaskListID = CType(gdviewTaskList.GetFocusedRowCellValue("ID"), Guid)
        Dim TaskListInUse = From t In dataMain.Snapshots _
                  Where t.TaskListID.Equals(TaskListID) _
                  Select t
        If TaskListInUse.Count > 0 Then
            MessageBox.Show("Υπάρχει στιγμιότυπο με αυτήν τη λίστα", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Else
            Dim CurrentTaskList = (From t In dataMain.TaskLists _
                                 Where t.ID.Equals(TaskListID) _
                                 Select t).FirstOrDefault

            If CurrentTaskList.TaskListLines.Count > 0 Then
                dataMain.TaskListLines.DeleteAllOnSubmit(CurrentTaskList.TaskListLines)
            End If

            If CurrentTaskList.TaskListUsers.Count > 0 Then
                For Each usr In CurrentTaskList.TaskListUsers
                    If usr.TaskListUserReports.Count > 0 Then
                        dataMain.TaskListUserReports.DeleteAllOnSubmit(usr.TaskListUserReports)
                    End If
                Next
                dataMain.TaskListUsers.DeleteAllOnSubmit(CurrentTaskList.TaskListUsers)
            End If

            dataMain.TaskLists.DeleteOnSubmit(CurrentTaskList)
            dataMain.SubmitChanges()
        End If
    End Sub

    Private Sub btnEdit_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnEdit.ItemClick
        EditList()
    End Sub

    Private Sub EditList()
        If gdviewTaskList.FocusedRowHandle > -1 Then
            Dim frm As New TaskListWizardF
            frm.TaskListID = CType(gdviewTaskList.GetFocusedRowCellValue("ID"), Guid)
            frm.ShowDialog()
            GetDatasource()
        End If
    End Sub

    Private Sub dgdTaskList_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgdTaskList.DoubleClick
        EditList()
    End Sub

    Private Sub BackgroundWorker1_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker1.DoWork
        Dim NewSnap As New svAlertSnap.Run(ConnStringMain)
        NewSnap.RunSnapshot(ConnStringData, SelectedList.ID)
        BeginInvoke(New MethodInvoker(AddressOf CompleteAlert))
    End Sub

    Private Sub CompleteAlert()
        AlertControl1.Show(Me, SelectedList.ListName, "Η εκτέλεση της λίστας εργασιών ολοκληρώθηκε")
        dataMain = New svAlertSnap.dataMainDataContext(ConnStringMain)
        GetDatasource()
    End Sub
End Class