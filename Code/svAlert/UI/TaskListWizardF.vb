﻿Public Class TaskListWizardF
    Private CurrentTaskList As svAlertSnap.TaskList
    Private CurrentUser As svAlertSnap.User
    'Private ReportsNames As Dictionary(Of Short, String)

    Private UsersList As New List(Of svAlertSnap.User)

    Private Sub TaskWizardF_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        GetDatasource()

        'Dim Importer As New svAlertData.Importer(ConnStringData)
        'ReportsNames = Importer.Reports
    End Sub

    Public WriteOnly Property TaskListID() As Guid
        Set(ByVal value As Guid)
            CurrentTaskList = (From t In dataMain.TaskLists _
                             Where t.ID.Equals(value) _
                             Select t).FirstOrDefault
            txtTaskListName.Text = CurrentTaskList.ListName
            chkActive.Checked = CurrentTaskList.IsActive
        End Set
    End Property

    Private Sub GetDatasource()
        Dim AllSchedules = From t In dataMain.Schedules _
                         Order By t.ScheduleName _
                         Select t.ID, Name = t.ScheduleName

        If CurrentTaskList Is Nothing Then
            SvClassGuid.AddToCombo(AllSchedules, cboSchedule, Nothing)
        Else
            SvClassGuid.AddToCombo(AllSchedules, cboSchedule, CurrentTaskList.ScheduleID)
        End If

        Dim AllUsers = From t In dataMain.Users _
                       Where Not t.LoginName.Equals("admin") _
                       Order By t.UserName _
                       Select t

        For Each usr In AllUsers
            Dim ReportChecked As Boolean = False

            For Each rpt In usr.UserReports
                If CurrentTaskList Is Nothing Then
                    ReportChecked = False
                Else
                    Dim ReportExists = From t In dataMain.TaskListUserReports _
                                       Where t.ReportType.Equals(rpt.ReportType) _
                                       And t.TaskListUser.UserID.Equals(usr.ID) _
                                       And t.TaskListUser.TaskListID.Equals(CurrentTaskList.ID) _
                                       Select t
                    ReportChecked = (ReportExists.Count > 0)
                End If

                rpt.IsSelected = ReportChecked
            Next
            UsersList.Add(usr)
        Next

        bsUser.DataSource = UsersList

        Dim AllTasks = From t In dataMain.Tasks _
                     Order By t.TaskName _
                     Select t

        Dim TaskChecked As Boolean = False

        For Each tsk In AllTasks
            If CurrentTaskList Is Nothing Then
                TaskChecked = False
            Else
                Dim TaskExists = From t In CurrentTaskList.TaskListLines _
                                   Where t.TaskID.Equals(tsk.ID) _
                                   Select t
                TaskChecked = (TaskExists.Count > 0)
            End If
            lstTasks.Items.Add(tsk.ID, tsk.TaskName, If(TaskChecked, CheckState.Checked, CheckState.Unchecked), True)
        Next

    End Sub

    Private Sub WizardControl1_FinishClick(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles WizardControl1.FinishClick
        Save()
    End Sub

    Private Sub Save()
        Try
            Dim IsNew As Boolean = False
            If CurrentTaskList Is Nothing Then
                CurrentTaskList = New svAlertSnap.TaskList With {.ID = Guid.NewGuid}
                IsNew = True
            Else
                For Each usr In CurrentTaskList.TaskListUsers
                    dataMain.TaskListUserReports.DeleteAllOnSubmit(usr.TaskListUserReports)
                Next
                dataMain.TaskListUsers.DeleteAllOnSubmit(CurrentTaskList.TaskListUsers)
                dataMain.TaskListLines.DeleteAllOnSubmit(CurrentTaskList.TaskListLines)
            End If
            CurrentTaskList.IsActive = True
            CurrentTaskList.ListName = txtTaskListName.Text
            CurrentTaskList.IsActive = chkActive.Checked

            If SvClassGuid.ValidComboItem(cboSchedule) Then
                Dim ScheduleID As Guid = cboSchedule.SelectedItem.id
                If Not IsNew AndAlso ScheduleID <> CurrentTaskList.ScheduleID Then
                    CurrentTaskList.NextRun = Nothing
                End If
                Dim CurrentSchedule = (From t In dataMain.Schedules _
                                     Where t.ID.Equals(ScheduleID) _
                                     Select t).FirstOrDefault

                CurrentTaskList.Schedule = CurrentSchedule
                'CurrentTaskList.ScheduleID = cboSchedule.SelectedItem.id
            End If

            If IsNew Then
                dataMain.TaskLists.InsertOnSubmit(CurrentTaskList)
            End If

            For Each tsk In lstTasks.Items
                If tsk.CheckState = System.Windows.Forms.CheckState.Checked Then
                    Dim NewLine As New svAlertSnap.TaskListLine With { _
                    .ID = Guid.NewGuid, _
                    .TaskList = CurrentTaskList, _
                    .TaskID = tsk.Value}
                    dataMain.TaskListLines.InsertOnSubmit(NewLine)
                End If
            Next

            For Each usr As svAlertSnap.User In UsersList
                If usr.UserReports.Where(Function(f) f.IsSelected).Count > 0 Then
                    Dim NewUser As New svAlertSnap.TaskListUser With { _
                    .ID = Guid.NewGuid, _
                    .TaskList = CurrentTaskList, _
                    .UserID = usr.ID}
                    dataMain.TaskListUsers.InsertOnSubmit(NewUser)

                    For Each rpt In usr.UserReports
                        If rpt.IsSelected Then
                            Dim NewReport As New svAlertSnap.TaskListUserReport With { _
                            .ID = Guid.NewGuid, _
                            .TaskListUser = NewUser, _
                            .ReportType = rpt.ReportType}
                            dataMain.TaskListUserReports.InsertOnSubmit(NewReport)
                        End If
                    Next
                End If
            Next
            dataMain.SubmitChanges()

            If CurrentTaskList.NextRun Is Nothing Then
                Dim NewSnap As New svAlertSnap.Run(ConnStringMain)
                CurrentTaskList.NextRun = NewSnap.CalcNextRun(CurrentTaskList.ScheduleID, Now, CurrentTaskList.Schedule.Offset)
                dataMain.SubmitChanges()
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub UpdateUserReports()
        For Each rpt In CurrentUser.UserReports
            'Dim ReportName As String = ReportsNames.Item(rpt.ReportType)

            For Each nme In lstReports.Items
                If nme.value = rpt.ReportName Then
                    If lstReports.CheckedItems.IndexOf(nme.Value) > -1 Then
                        rpt.IsSelected = True
                    Else
                        rpt.IsSelected = False
                    End If
                End If
            Next
        Next
        Dim CurrentIndex As Short = UsersList.IndexOf(CurrentUser)
        UsersList.RemoveAt(CurrentIndex)
        UsersList.Insert(CurrentIndex, CurrentUser)
        CurrentUser = Nothing
        bsUser.DataSource = UsersList
    End Sub

    Private Sub lstUsers_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstUsers.SelectedIndexChanged
        Dim NewIndex As Short = lstUsers.SelectedIndex
        If CurrentUser IsNot Nothing Then
            UpdateUserReports()
        End If

        CurrentUser = lstUsers.SelectedItem
        lstReports.Items.Clear()
        For Each rpt In CurrentUser.UserReports.OrderBy(Function(f) f.ReportName)
            'Dim ReportName As String = ReportsNames.Item(rpt.ReportType)
            'rpt.ReportName = ReportName
            'lstReports.Items.Add(ReportName, rpt.IsSelected)
            lstReports.Items.Add(rpt.ReportName, rpt.IsSelected)
        Next
    End Sub

    Private Sub WizardControl1_NextClick(ByVal sender As Object, ByVal e As DevExpress.XtraWizard.WizardCommandButtonClickEventArgs) Handles WizardControl1.NextClick
        If WizardControl1.SelectedPage Is WelcomeWizardPage1 Then
            If txtTaskListName.Text = "" Then
                MessageBox.Show("Συμπληρώστε ονομασία", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                e.Handled = True
            ElseIf Not SvClassGuid.ValidComboItem(cboSchedule) Then
                MessageBox.Show("Συμπληρώστε προγραμματισμό", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                e.Handled = True
            End If
        ElseIf WizardControl1.SelectedPage Is wizpTasks Then
            Dim ItemChecked As Boolean = False
            For Each tsk In lstTasks.Items
                If tsk.CheckState = System.Windows.Forms.CheckState.Checked Then
                    ItemChecked = True
                End If
            Next
            If Not ItemChecked Then
                MessageBox.Show("Συμπληρώστε τουλάχιστον μία εργασία", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                e.Handled = True
            End If
        ElseIf WizardControl1.SelectedPage Is wizpReports Then
            UpdateUserReports()
        End If
    End Sub
End Class
