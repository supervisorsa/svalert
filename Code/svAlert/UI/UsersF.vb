﻿Public Class UsersF

    Private Sub UsersF_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        GetDatasource()
    End Sub

    Private Sub GetDatasource()
        Dim AllUsers = From t In dataMain.Users _
                       Where Not t.LoginName.Equals("admin") _
                       Order By t.UserName _
                       Select t

        SvDevExpress.GridDataSource(dgdUser, gdviewUser, AllUsers)
        InitGrid()
    End Sub

    Private Sub InitGrid()
        For Each col As GridColumn In gdviewUser.Columns
            If col.FieldName.ToLower.Contains("isadmin") Then
                col.Caption = "Administrator"
            ElseIf col.FieldName.ToLower.Contains("username") Then
                col.Caption = "Ονοματεπώνυμο"
            ElseIf col.FieldName.ToLower.Contains("active") Then
                col.Caption = "Ενεργός"
            Else
                col.Visible = False
            End If
        Next
        gdviewUser.BestFitColumns()
    End Sub

    Private Sub btnAdd_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnAdd.ItemClick
        Dim frm As New UserWizardF
        frm.ShowDialog()
        GetDatasource()
    End Sub

    Private Sub btnDelete_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnDelete.ItemClick
        If gdviewUser.FocusedRowHandle > -1 Then
            If MessageBox.Show("Θέλετε σίγουρα να γίνει διαγραφή;", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                Delete()
                GetDatasource()
            End If
        End If
    End Sub

    Private Sub Delete()
        Dim UserID = CType(gdviewUser.GetFocusedRowCellValue("ID"), Guid)
        Dim UserInUse = From t In dataMain.TaskListUsers _
                          Where t.UserID.Equals(UserID) _
                          Select t
        If UserInUse.Count > 0 Then
            MessageBox.Show("Ο χρήστης συμμετέχει σε λίστα", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Else
            Dim CurrentUser = (From t In dataMain.Users _
                                 Where t.ID.Equals(UserID) _
                                 Select t).FirstOrDefault

            If CurrentUser.UserReports.Count > 0 Then
                dataMain.UserReports.DeleteAllOnSubmit(CurrentUser.UserReports)
            End If
            dataMain.Users.DeleteOnSubmit(CurrentUser)
            dataMain.SubmitChanges()
        End If
    End Sub

    Private Sub btnEdit_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnEdit.ItemClick
        EditUser()
    End Sub

    Private Sub EditUser()
        If gdviewUser.FocusedRowHandle > -1 Then
            Dim frm As New UserWizardF
            frm.UserID = CType(gdviewUser.GetFocusedRowCellValue("ID"), Guid)
            frm.ShowDialog()
            GetDatasource()
        End If
    End Sub

    Private Sub dgdUser_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgdUser.DoubleClick
        EditUser()
    End Sub
End Class