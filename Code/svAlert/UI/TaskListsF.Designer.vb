﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class TaskListsF
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.dgdTaskList = New DevExpress.XtraGrid.GridControl
        Me.gdviewTaskList = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.BarManager1 = New DevExpress.XtraBars.BarManager(Me.components)
        Me.Bar1 = New DevExpress.XtraBars.Bar
        Me.btnAdd = New DevExpress.XtraBars.BarButtonItem
        Me.btnEdit = New DevExpress.XtraBars.BarButtonItem
        Me.btnDelete = New DevExpress.XtraBars.BarButtonItem
        Me.btnExecute = New DevExpress.XtraBars.BarButtonItem
        Me.BarDockControl1 = New DevExpress.XtraBars.BarDockControl
        Me.BarDockControl2 = New DevExpress.XtraBars.BarDockControl
        Me.BarDockControl3 = New DevExpress.XtraBars.BarDockControl
        Me.BarDockControl4 = New DevExpress.XtraBars.BarDockControl
        Me.BarButtonItem3 = New DevExpress.XtraBars.BarButtonItem
        Me.AlertControl1 = New DevExpress.XtraBars.Alerter.AlertControl(Me.components)
        Me.BackgroundWorker1 = New System.ComponentModel.BackgroundWorker
        CType(Me.dgdTaskList, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gdviewTaskList, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgdTaskList
        '
        Me.dgdTaskList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgdTaskList.Location = New System.Drawing.Point(0, 47)
        Me.dgdTaskList.MainView = Me.gdviewTaskList
        Me.dgdTaskList.Name = "dgdTaskList"
        Me.dgdTaskList.Size = New System.Drawing.Size(622, 323)
        Me.dgdTaskList.TabIndex = 4
        Me.dgdTaskList.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gdviewTaskList})
        '
        'gdviewTaskList
        '
        Me.gdviewTaskList.GridControl = Me.dgdTaskList
        Me.gdviewTaskList.Name = "gdviewTaskList"
        Me.gdviewTaskList.OptionsBehavior.Editable = False
        Me.gdviewTaskList.OptionsView.ShowDetailButtons = False
        Me.gdviewTaskList.OptionsView.ShowGroupPanel = False
        '
        'BarManager1
        '
        Me.BarManager1.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.Bar1})
        Me.BarManager1.DockControls.Add(Me.BarDockControl1)
        Me.BarManager1.DockControls.Add(Me.BarDockControl2)
        Me.BarManager1.DockControls.Add(Me.BarDockControl3)
        Me.BarManager1.DockControls.Add(Me.BarDockControl4)
        Me.BarManager1.Form = Me
        Me.BarManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.btnAdd, Me.BarButtonItem3, Me.btnDelete, Me.btnEdit, Me.btnExecute})
        Me.BarManager1.MaxItemId = 5
        '
        'Bar1
        '
        Me.Bar1.BarName = "Tools"
        Me.Bar1.DockCol = 0
        Me.Bar1.DockRow = 0
        Me.Bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.Bar1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnAdd, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnEdit, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnDelete, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnExecute, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)})
        Me.Bar1.Text = "Tools"
        '
        'btnAdd
        '
        Me.btnAdd.Caption = "Προσθήκη"
        Me.btnAdd.Glyph = Global.svAlert.My.Resources.Resources.list_add
        Me.btnAdd.Id = 0
        Me.btnAdd.Name = "btnAdd"
        '
        'btnEdit
        '
        Me.btnEdit.Caption = "Επεξεργασία"
        Me.btnEdit.Glyph = Global.svAlert.My.Resources.Resources.accessories_text_editor
        Me.btnEdit.Id = 3
        Me.btnEdit.Name = "btnEdit"
        '
        'btnDelete
        '
        Me.btnDelete.Caption = "Διαγραφή"
        Me.btnDelete.Glyph = Global.svAlert.My.Resources.Resources.dialog_error
        Me.btnDelete.Id = 2
        Me.btnDelete.Name = "btnDelete"
        '
        'btnExecute
        '
        Me.btnExecute.Caption = "Εκτέλεση"
        Me.btnExecute.Glyph = Global.svAlert.My.Resources.Resources.application_x_executable_script
        Me.btnExecute.Id = 4
        Me.btnExecute.Name = "btnExecute"
        '
        'BarDockControl1
        '
        Me.BarDockControl1.CausesValidation = False
        Me.BarDockControl1.Dock = System.Windows.Forms.DockStyle.Top
        Me.BarDockControl1.Location = New System.Drawing.Point(0, 0)
        Me.BarDockControl1.Size = New System.Drawing.Size(622, 47)
        '
        'BarDockControl2
        '
        Me.BarDockControl2.CausesValidation = False
        Me.BarDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.BarDockControl2.Location = New System.Drawing.Point(0, 370)
        Me.BarDockControl2.Size = New System.Drawing.Size(622, 0)
        '
        'BarDockControl3
        '
        Me.BarDockControl3.CausesValidation = False
        Me.BarDockControl3.Dock = System.Windows.Forms.DockStyle.Left
        Me.BarDockControl3.Location = New System.Drawing.Point(0, 47)
        Me.BarDockControl3.Size = New System.Drawing.Size(0, 323)
        '
        'BarDockControl4
        '
        Me.BarDockControl4.CausesValidation = False
        Me.BarDockControl4.Dock = System.Windows.Forms.DockStyle.Right
        Me.BarDockControl4.Location = New System.Drawing.Point(622, 47)
        Me.BarDockControl4.Size = New System.Drawing.Size(0, 323)
        '
        'BarButtonItem3
        '
        Me.BarButtonItem3.Caption = "2"
        Me.BarButtonItem3.Id = 1
        Me.BarButtonItem3.Name = "BarButtonItem3"
        '
        'AlertControl1
        '
        Me.AlertControl1.AutoFormDelay = 5000
        Me.AlertControl1.FormDisplaySpeed = DevExpress.XtraBars.Alerter.AlertFormDisplaySpeed.Slow
        Me.AlertControl1.ShowPinButton = False
        Me.AlertControl1.ShowToolTips = False
        '
        'BackgroundWorker1
        '
        '
        'TaskListsF
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(622, 370)
        Me.ControlBox = False
        Me.Controls.Add(Me.dgdTaskList)
        Me.Controls.Add(Me.BarDockControl3)
        Me.Controls.Add(Me.BarDockControl4)
        Me.Controls.Add(Me.BarDockControl2)
        Me.Controls.Add(Me.BarDockControl1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "TaskListsF"
        Me.Text = "Λίστες εργασιών"
        CType(Me.dgdTaskList, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gdviewTaskList, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents dgdTaskList As DevExpress.XtraGrid.GridControl
    Friend WithEvents gdviewTaskList As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents BarDockControl3 As DevExpress.XtraBars.BarDockControl
    Friend WithEvents BarDockControl4 As DevExpress.XtraBars.BarDockControl
    Friend WithEvents BarDockControl2 As DevExpress.XtraBars.BarDockControl
    Friend WithEvents BarDockControl1 As DevExpress.XtraBars.BarDockControl
    Friend WithEvents BarManager1 As DevExpress.XtraBars.BarManager
    Friend WithEvents Bar1 As DevExpress.XtraBars.Bar
    Friend WithEvents btnAdd As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem3 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnDelete As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnEdit As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnExecute As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents AlertControl1 As DevExpress.XtraBars.Alerter.AlertControl
    Friend WithEvents BackgroundWorker1 As System.ComponentModel.BackgroundWorker
End Class
