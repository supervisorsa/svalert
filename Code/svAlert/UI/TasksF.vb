﻿Public Class TasksF

    Private Sub TasksF_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        GetDatasource()
    End Sub

    Private Sub GetDatasource()
        Dim AllTasks = From t In dataMain.Tasks _
                     Order By t.TaskName _
                     Select t

        SvDevExpress.GridDataSource(dgdTask, gdviewTask, AllTasks)
        InitGrid()
    End Sub

    Private Sub btnAdd_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnAdd.ItemClick
        Dim frm As New TaskWizardF
        frm.ShowDialog()
        GetDatasource()
    End Sub

    Private Sub InitGrid()
        For Each col As GridColumn In gdviewTask.Columns
            If col.FieldName.ToLower.Contains("taskname") Then
                col.Caption = "Όνομα εργασίας"
            ElseIf col.FieldName.ToLower.Contains("active") Then
                col.Caption = "Ενεργή"
            Else
                col.Visible = False
            End If
        Next
        gdviewTask.BestFitColumns()
    End Sub

    Private Sub btnDelete_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnDelete.ItemClick
        If gdviewTask.FocusedRowHandle > -1 Then
            If MessageBox.Show("Θέλετε σίγουρα να γίνει διαγραφή;", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                Delete()
                GetDatasource()
            End If
        End If
    End Sub

    Private Sub Delete()
        Dim TaskID = CType(gdviewTask.GetFocusedRowCellValue("ID"), Guid)
        Dim TaskInUse = From t In dataMain.TaskListLines _
                          Where t.TaskID.Equals(TaskID) _
                          Select t
        If TaskInUse.Count > 0 Then
            MessageBox.Show("Η εργασία συμμετέχει σε λίστα", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Else
            Dim CurrentTask = (From t In dataMain.Tasks _
                                 Where t.ID.Equals(TaskID) _
                                 Select t).FirstOrDefault
            dataMain.Tasks.DeleteOnSubmit(CurrentTask)
            dataMain.SubmitChanges()
        End If
    End Sub

    Private Sub btnEdit_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnEdit.ItemClick
        EditTask()
    End Sub

    Private Sub EditTask()
        If gdviewTask.FocusedRowHandle > -1 Then
            Dim frm As New TaskWizardF
            frm.TaskID = CType(gdviewTask.GetFocusedRowCellValue("ID"), Guid)
            frm.ShowDialog()
            GetDatasource()
        End If
    End Sub

    Private Sub dgdTask_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgdTask.DoubleClick
        EditTask()
    End Sub
End Class