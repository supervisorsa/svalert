﻿Public Class ScheduleWizardF

    Private CheckChanging As Boolean = False

    Private CurrentSchedule As svAlertSnap.Schedule

    'Private ScheduleDates As List(Of Date)

    Public WriteOnly Property ScheduleID() As Guid
        Set(ByVal value As Guid)
            CurrentSchedule = (From t In dataMain.Schedules _
                        Where t.ID.Equals(value) _
                        Select t).FirstOrDefault

            txtScheduleName.Text = CurrentSchedule.ScheduleName
            If CurrentSchedule.IsMinutes Then
                chkMinutes.Checked = CurrentSchedule.IsMinutes
                nupMinutes.Value = CurrentSchedule.IntervalMinutes
            Else
                chkYearly.Checked = CurrentSchedule.IsYearly
                chkMonthly.Checked = CurrentSchedule.IsMonthly
                chkFortnightly.Checked = CurrentSchedule.IsFortnightly
                chkWeekly.Checked = CurrentSchedule.IsWeekly
                chkDaily.Checked = CurrentSchedule.IsDaily
                chkWorkdays.Checked = CurrentSchedule.IsWorkdays
                chkRepeated.Checked = CurrentSchedule.IsRepeated
                nupDays.Value = CurrentSchedule.IntervalDays
                nupOffset.Value = CurrentSchedule.Offset
                Dim ExecutionTime As DateTime = CDate(CurrentSchedule.ExecutionTime)
                dteTime.Time = Now.Date.AddHours(ExecutionTime.Hour).AddMinutes(ExecutionTime.Minute)

                For Each item In CurrentSchedule.ScheduleLines
                    lstDates.Items.Add(item.ScheduleDate)
                Next
            End If
        End Set
    End Property

    Private Sub CheckControl(ByVal EnableCheck As Boolean)
        chkYearly.Enabled = EnableCheck
        chkMonthly.Enabled = EnableCheck
        chkFortnightly.Enabled = EnableCheck
        chkWeekly.Enabled = EnableCheck
        chkDaily.Enabled = EnableCheck
        chkWorkdays.Enabled = EnableCheck
        chkRepeated.Enabled = EnableCheck
        chkYearly.Checked = False
        chkMonthly.Checked = False
        chkFortnightly.Checked = False
        chkWeekly.Checked = False
        chkDaily.Checked = False
        chkWorkdays.Checked = False
        chkRepeated.Checked = False
        nupDays.Enabled = False
    End Sub

    Private Sub chkYearFirst_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkYearly.CheckedChanged
        If Not CheckChanging Then
            CheckChanging = True
            If chkYearly.Checked Then
                CheckControl(False)
                chkYearly.Enabled = True
                chkYearly.Checked = True
            Else
                CheckControl(True)
            End If
            CheckChanging = False
        End If
    End Sub

    Private Sub chkMonthFirst_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkMonthly.CheckedChanged
        If Not CheckChanging Then
            CheckChanging = True
            If chkMonthly.Checked Then
                CheckControl(False)
                chkMonthly.Enabled = True
                chkMonthly.Checked = True
            Else
                CheckControl(True)
            End If
            CheckChanging = False
        End If
    End Sub

    Private Sub chkRepeated_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkRepeated.CheckedChanged
        If Not CheckChanging Then
            CheckChanging = True
            If chkRepeated.Checked Then
                CheckControl(False)
                chkRepeated.Enabled = True
                chkRepeated.Checked = True
                nupDays.Enabled = True
            Else
                CheckControl(True)
            End If
            CheckChanging = False
        End If
    End Sub

    Private Sub WizardControl1_FinishClick(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles WizardControl1.FinishClick
        Save()
    End Sub

    Private Sub Save()
        Try
            Dim IsNew As Boolean = False
            If CurrentSchedule Is Nothing Then
                CurrentSchedule = New svAlertSnap.Schedule With {.ID = Guid.NewGuid}
                IsNew = True
            End If
            CurrentSchedule.IsActive = True
            CurrentSchedule.ScheduleName = txtScheduleName.Text

            If chkMinutes.Checked Then
                CurrentSchedule.IsMinutes = chkMinutes.Checked
                CurrentSchedule.IntervalMinutes = nupMinutes.Value

                CurrentSchedule.IsYearly = False
                CurrentSchedule.IsMonthly = False
                CurrentSchedule.IsFortnightly = False
                CurrentSchedule.IsWeekly = False
                CurrentSchedule.IsDaily = False
                CurrentSchedule.IsWorkdays = False

                CurrentSchedule.IsRepeated = False

                CurrentSchedule.IntervalDays = 0

                CurrentSchedule.ExecutionTime = dteTime.Text

                CurrentSchedule.Offset = 0

            Else
                CurrentSchedule.IsMinutes = False
                CurrentSchedule.IntervalMinutes = 0

                CurrentSchedule.IsYearly = chkYearly.Checked
                CurrentSchedule.IsMonthly = chkMonthly.Checked
                CurrentSchedule.IsFortnightly = chkFortnightly.Checked
                CurrentSchedule.IsWeekly = chkWeekly.Checked
                CurrentSchedule.IsDaily = chkDaily.Checked
                CurrentSchedule.IsWorkdays = chkWorkdays.Checked

                CurrentSchedule.IsRepeated = chkRepeated.Checked

                CurrentSchedule.IntervalDays = If(chkRepeated.Checked, nupDays.Value, 0)

                CurrentSchedule.ExecutionTime = dteTime.Text

                CurrentSchedule.Offset = nupOffset.Value
            End If

            If IsNew Then
                dataMain.Schedules.InsertOnSubmit(CurrentSchedule)
            Else
                dataMain.ScheduleLines.DeleteAllOnSubmit(CurrentSchedule.ScheduleLines)
            End If

            For Each item In lstDates.Items
                Dim NewLine As New svAlertSnap.ScheduleLine With { _
                .ID = Guid.NewGuid, _
                .Schedule = CurrentSchedule, _
                .ScheduleDate = item}
                dataMain.ScheduleLines.InsertOnSubmit(NewLine)
            Next

            dataMain.SubmitChanges()

            If Not IsNew Then
                For Each lst In CurrentSchedule.TaskLists
                    Dim NewSnap As New svAlertSnap.Run(ConnStringMain)
                    lst.NextRun = NewSnap.CalcNextRun(CurrentSchedule.ID, Now, CurrentSchedule.Offset)
                    dataMain.SubmitChanges()
                Next
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub WizardControl1_NextClick(ByVal sender As Object, ByVal e As DevExpress.XtraWizard.WizardCommandButtonClickEventArgs) Handles WizardControl1.NextClick
        If WizardControl1.SelectedPage Is WelcomeWizardPage1 Then
            If txtScheduleName.Text = "" Then
                MessageBox.Show("Συμπληρώστε ονομασία", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                e.Handled = True
            End If
            'Try
            '    Dim CurrentTime As DateTime = CDate(txtTime.Text)
            'Catch ex As Exception
            '    MessageBox.Show("Συμπληρώστε σωστή ώρα εκτέλεσης", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            '    e.Handled = True
            'End Try
            wizpDates.Visible = Not (chkMinutes.Checked Or _
                                     chkRepeated.Checked Or _
                                     chkYearly.Checked Or _
                                     chkMonthly.Checked Or _
                                     chkFortnightly.Checked Or _
                                     chkWeekly.Checked Or _
                                     chkDaily.Checked Or _
                                     chkWorkdays.Checked)
            'If ScheduleDates Is Nothing Then ScheduleDates = New List(Of Date)
        ElseIf WizardControl1.SelectedPage Is wizpDates Then
            If lstDates.Items.Count = 0 Then
                MessageBox.Show("Συμπληρώστε ημερομηνίες", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Dim frm As New DateF
        frm.ShowDialog()
        If frm.ExecutionTime <> "" Then
            If lstDates.Items.IndexOf(frm.ExecutionTime) = -1 Then
                lstDates.Items.Add(frm.ExecutionTime)
            End If
        End If

    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lstDates.SelectedItem IsNot Nothing Then
            lstDates.Items.Remove(lstDates.SelectedItem)
        End If
    End Sub

    Private Sub chkFortnightly_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkFortnightly.CheckedChanged
        If Not CheckChanging Then
            CheckChanging = True
            If chkFortnightly.Checked Then
                CheckControl(False)
                chkFortnightly.Enabled = True
                chkFortnightly.Checked = True
            Else
                CheckControl(True)
            End If
            CheckChanging = False
        End If
    End Sub

    Private Sub chkWeekly_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkWeekly.CheckedChanged
        If Not CheckChanging Then
            CheckChanging = True
            If chkWeekly.Checked Then
                CheckControl(False)
                chkWeekly.Enabled = True
                chkWeekly.Checked = True
            Else
                CheckControl(True)
            End If
            CheckChanging = False
        End If
    End Sub

    Private Sub chkDaily_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkDaily.CheckedChanged
        If Not CheckChanging Then
            CheckChanging = True
            If chkDaily.Checked Then
                CheckControl(False)
                chkDaily.Enabled = True
                chkDaily.Checked = True
            Else
                CheckControl(True)
            End If
            CheckChanging = False
        End If
    End Sub

    Private Sub chkWorkdays_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkWorkdays.CheckedChanged
        If Not CheckChanging Then
            CheckChanging = True
            If chkWorkdays.Checked Then
                CheckControl(False)
                chkWorkdays.Enabled = True
                chkWorkdays.Checked = True
            Else
                CheckControl(True)
            End If
            CheckChanging = False
        End If
    End Sub

    Private Sub chkMinutes_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkMinutes.CheckedChanged
        nupMinutes.Enabled = chkMinutes.Checked

        dteTime.Enabled = Not chkMinutes.Checked
        nupDays.Enabled = Not chkMinutes.Checked
        nupOffset.Enabled = Not chkMinutes.Checked
        chkDaily.Enabled = Not chkMinutes.Checked
        chkFortnightly.Enabled = Not chkMinutes.Checked
        chkMonthly.Enabled = Not chkMinutes.Checked
        chkRepeated.Enabled = Not chkMinutes.Checked
        chkWeekly.Enabled = Not chkMinutes.Checked
        chkWorkdays.Enabled = Not chkMinutes.Checked
        chkYearly.Enabled = Not chkMinutes.Checked
    End Sub
End Class