﻿Public Class LoginForm

    Const MAX_RETRIES As Short = 3
    Private RetryCount As Short = 0

    Private Sub LoginForm_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'If Not svAlertReportAndScenario.Globals.ValidLicense(My.Application.Info.DirectoryPath & "\sv.lck") Then
            '    Dim frm As New svClientActivation.ApplyActivationForm
            '    frm.Version = "1.0"
            '    frm.Path = My.Application.Info.DirectoryPath + "\sv.lck"
            '    frm.ShowDialog()
            'End If

            ConnStringMain = DbConn.ConstructConnString(Settings.GetString("MainServer"), _
                                                        Settings.GetString("MainUser"), _
                                                        EnDecrypt.Decrypting(Settings.GetString("MainPwd"), svAlertReportAndScenario.Globals.GlobalKey, "-"), _
                                                        Settings.GetString("MainDB"), "")

            If DbConnected() Then
                dataMain = New svAlertSnap.dataMainDataContext(ConnStringMain)
                HandleAdminUser()
                ''  ****    DEMO    ****
                'Dim Splash As New SplashScreen
                'Splash.Show()
                'Close()
                ''  ****    DEMO    ****
            Else
                Application.Exit()
            End If
#If DEBUG Then
            CheckLogin()
#Else

#End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub btnEnter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEnter.Click
        CheckLogin()
    End Sub

    Private Sub CheckLogin()
#If DEBUG Then
        UserID = New Guid("12FC21E1-5DEE-4773-AE78-8C92E5C0234C")
        'Dim UserExists = From t In dataMain.Users _
        '                 Where t.LoginName.Equals(txtUsername.Text) _
        '                 And t.LoginPwd.Equals(txtPassword.Text) _
        '                 Select t
        'UserID = UserExists.FirstOrDefault.ID
        LoginSuccess()
#Else
                If Not svAlertReportAndScenario.Globals.ValidLicense(My.Application.Info.DirectoryPath & "\sv.lck") Then
            Dim frm As New svClientActivation.ApplyActivationForm
            frm.Version = "1.0"
            frm.Path = My.Application.Info.DirectoryPath + "\sv.lck"
            frm.ShowDialog()
        Else
            Dim UserExists = From t In dataMain.Users _
                             Where t.LoginName.Equals(txtUsername.Text) _
                             And t.LoginPwd.Equals(txtPassword.Text) _
                             Select t

            If UserExists.Count = 1 Then
                UserID = UserExists.FirstOrDefault.ID
                LoginSuccess()
            Else
                RetryCount += 1
                If RetryCount = MAX_RETRIES Then
                    tmrFailureDelay.Enabled = True
                    txtUsername.Enabled = False
                    txtPassword.Enabled = False
                    btnEnter.Enabled = False
                Else
                    MessageBox.Show("Δεν υπάρχει τέτοιος χρήστης/κωδικός" & vbCrLf & "Παρακαλώ δοκιμάστε ξανά", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    txtUsername.Focus()
                End If
            End If
        End If
#End If
    End Sub

    Private Sub LoginSuccess()
        ConnStringData = DbConn.ConstructConnString(Settings.GetString("DataServer"), _
                                                    Settings.GetString("DataUser"), _
                                                    EnDecrypt.Decrypting(Settings.GetString("DataPwd"), svAlertReportAndScenario.Globals.GlobalKey, "-"), _
                                                    Settings.GetString("DataDB") & Now.Date.Year, "")
        LoadReportNames()

        Dim Main As New MainF
        Main.Show()
        Close()
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Close()
    End Sub

    Private Sub tmrFailureDelay_Tick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles tmrFailureDelay.Tick
        ''  Timer for closing application after login failure
        Me.Close()
    End Sub

    Private Sub txtUsername_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtUsername.KeyDown
        If e.KeyCode = Keys.Enter Then CheckLogin()
    End Sub

    Private Sub txtPassword_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtPassword.KeyDown
        If e.KeyCode = Keys.Enter Then CheckLogin()
        txtPassword.Focus()
    End Sub

    Private Function HandleAdminUser() As Guid

        Dim AdminUser As Guid
        Dim CurrentUsers = From t In dataMain.Users _
                           Where t.IsActive = True _
                           And Not t.LoginName.Equals("admin") _
                           Select t.LoginName
        Dim AdminExists = (From t In dataMain.Users _
                           Where t.UserName.Equals("admin") _
                           And t.LoginName.Equals("admin") _
                           Select t).FirstOrDefault

        If CurrentUsers.Count = 0 Then
            If AdminExists Is Nothing Then
                Dim NewAdminUser As New svAlertSnap.User With { _
                                .ID = Guid.NewGuid, _
                                .UserName = "admin", _
                                .LoginName = "admin", _
                                .LoginPwd = "admin", _
                                .IsAdmin = True, _
                                .IsActive = True _
                                }

                dataMain.Users.InsertOnSubmit(NewAdminUser)
                dataMain.SubmitChanges()
                AdminUser = NewAdminUser.ID
            Else
                AdminExists.IsActive = True
                AdminExists.IsAdmin = True
                AdminExists.LoginPwd = "admin"
                AdminUser = AdminExists.ID
                dataMain.SubmitChanges()
            End If
            MessageBox.Show("Προσοχή! Υπάρχει μόνο ο admin χρήστης για τη διαχείριση του προγράμματος. Προσθέστε χρήστες από τη ""Διαχείριση""", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Else
            AdminExists.IsActive = False
            AdminExists.IsAdmin = False
            AdminExists.LoginPwd = "admin"
            AdminUser = AdminExists.ID
            dataMain.SubmitChanges()
        End If
        Return AdminUser
    End Function

End Class
