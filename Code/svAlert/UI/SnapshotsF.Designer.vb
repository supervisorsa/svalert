﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SnapshotsF
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.dgdSnapshot = New DevExpress.XtraGrid.GridControl
        Me.gdviewSnapshots = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.BarManager1 = New DevExpress.XtraBars.BarManager(Me.components)
        Me.Bar1 = New DevExpress.XtraBars.Bar
        Me.btnSnapshotReports = New DevExpress.XtraBars.BarButtonItem
        Me.btnUserReports = New DevExpress.XtraBars.BarButtonItem
        Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl
        Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl
        Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl
        Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl
        CType(Me.dgdSnapshot, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gdviewSnapshots, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgdSnapshot
        '
        Me.dgdSnapshot.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgdSnapshot.Location = New System.Drawing.Point(0, 47)
        Me.dgdSnapshot.MainView = Me.gdviewSnapshots
        Me.dgdSnapshot.Name = "dgdSnapshot"
        Me.dgdSnapshot.Size = New System.Drawing.Size(622, 323)
        Me.dgdSnapshot.TabIndex = 4
        Me.dgdSnapshot.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gdviewSnapshots})
        '
        'gdviewSnapshots
        '
        Me.gdviewSnapshots.GridControl = Me.dgdSnapshot
        Me.gdviewSnapshots.Name = "gdviewSnapshots"
        Me.gdviewSnapshots.OptionsBehavior.Editable = False
        Me.gdviewSnapshots.OptionsView.ShowDetailButtons = False
        Me.gdviewSnapshots.OptionsView.ShowGroupPanel = False
        '
        'BarManager1
        '
        Me.BarManager1.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.Bar1})
        Me.BarManager1.DockControls.Add(Me.barDockControlTop)
        Me.BarManager1.DockControls.Add(Me.barDockControlBottom)
        Me.BarManager1.DockControls.Add(Me.barDockControlLeft)
        Me.BarManager1.DockControls.Add(Me.barDockControlRight)
        Me.BarManager1.Form = Me
        Me.BarManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.btnSnapshotReports, Me.btnUserReports})
        Me.BarManager1.MaxItemId = 2
        '
        'Bar1
        '
        Me.Bar1.BarName = "Tools"
        Me.Bar1.DockCol = 0
        Me.Bar1.DockRow = 0
        Me.Bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.Bar1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnSnapshotReports, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnUserReports, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)})
        Me.Bar1.Text = "Tools"
        '
        'btnSnapshotReports
        '
        Me.btnSnapshotReports.Caption = "Προβολή αναφορών στιγμιότυπου"
        Me.btnSnapshotReports.Glyph = Global.svAlert.My.Resources.Resources.application_msword
        Me.btnSnapshotReports.Id = 0
        Me.btnSnapshotReports.Name = "btnSnapshotReports"
        '
        'btnUserReports
        '
        Me.btnUserReports.Caption = "Προβολή αναφορών χρήστη"
        Me.btnUserReports.Glyph = Global.svAlert.My.Resources.Resources.gnome_mime_text_x_authors
        Me.btnUserReports.Id = 1
        Me.btnUserReports.LargeGlyph = Global.svAlert.My.Resources.Resources.gnome_mime_text_x_authors
        Me.btnUserReports.Name = "btnUserReports"
        '
        'barDockControlTop
        '
        Me.barDockControlTop.CausesValidation = False
        Me.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.barDockControlTop.Location = New System.Drawing.Point(0, 0)
        Me.barDockControlTop.Size = New System.Drawing.Size(622, 47)
        '
        'barDockControlBottom
        '
        Me.barDockControlBottom.CausesValidation = False
        Me.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.barDockControlBottom.Location = New System.Drawing.Point(0, 370)
        Me.barDockControlBottom.Size = New System.Drawing.Size(622, 0)
        '
        'barDockControlLeft
        '
        Me.barDockControlLeft.CausesValidation = False
        Me.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left
        Me.barDockControlLeft.Location = New System.Drawing.Point(0, 47)
        Me.barDockControlLeft.Size = New System.Drawing.Size(0, 323)
        '
        'barDockControlRight
        '
        Me.barDockControlRight.CausesValidation = False
        Me.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right
        Me.barDockControlRight.Location = New System.Drawing.Point(622, 47)
        Me.barDockControlRight.Size = New System.Drawing.Size(0, 323)
        '
        'SnapshotsF
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(622, 370)
        Me.ControlBox = False
        Me.Controls.Add(Me.dgdSnapshot)
        Me.Controls.Add(Me.barDockControlLeft)
        Me.Controls.Add(Me.barDockControlRight)
        Me.Controls.Add(Me.barDockControlBottom)
        Me.Controls.Add(Me.barDockControlTop)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "SnapshotsF"
        Me.Text = "Στιγμιότυπα"
        CType(Me.dgdSnapshot, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gdviewSnapshots, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents dgdSnapshot As DevExpress.XtraGrid.GridControl
    Friend WithEvents gdviewSnapshots As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents BarManager1 As DevExpress.XtraBars.BarManager
    Friend WithEvents Bar1 As DevExpress.XtraBars.Bar
    Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
    Friend WithEvents btnSnapshotReports As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnUserReports As DevExpress.XtraBars.BarButtonItem
End Class
