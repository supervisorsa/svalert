﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ScheduleWizardF
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ScheduleWizardF))
        Me.WizardControl1 = New DevExpress.XtraWizard.WizardControl
        Me.WelcomeWizardPage1 = New DevExpress.XtraWizard.WelcomeWizardPage
        Me.nupMinutes = New DevExpress.XtraEditors.SpinEdit
        Me.chkMinutes = New DevExpress.XtraEditors.CheckEdit
        Me.nupOffset = New DevExpress.XtraEditors.SpinEdit
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl
        Me.chkWorkdays = New DevExpress.XtraEditors.CheckEdit
        Me.chkDaily = New DevExpress.XtraEditors.CheckEdit
        Me.chkWeekly = New DevExpress.XtraEditors.CheckEdit
        Me.chkFortnightly = New DevExpress.XtraEditors.CheckEdit
        Me.dteTime = New DevExpress.XtraEditors.TimeEdit
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
        Me.chkYearly = New DevExpress.XtraEditors.CheckEdit
        Me.chkMonthly = New DevExpress.XtraEditors.CheckEdit
        Me.nupDays = New DevExpress.XtraEditors.SpinEdit
        Me.chkRepeated = New DevExpress.XtraEditors.CheckEdit
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
        Me.txtScheduleName = New DevExpress.XtraEditors.TextEdit
        Me.CompletionWizardPage1 = New DevExpress.XtraWizard.CompletionWizardPage
        Me.wizpDates = New DevExpress.XtraWizard.WizardPage
        Me.lstDates = New DevExpress.XtraEditors.ListBoxControl
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip
        Me.btnAdd = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
        Me.btnDelete = New System.Windows.Forms.ToolStripButton
        Me.FolderBrowserDialog1 = New System.Windows.Forms.FolderBrowserDialog
        CType(Me.WizardControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.WizardControl1.SuspendLayout()
        Me.WelcomeWizardPage1.SuspendLayout()
        CType(Me.nupMinutes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkMinutes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nupOffset.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkWorkdays.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkDaily.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkWeekly.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkFortnightly.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dteTime.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkYearly.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkMonthly.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nupDays.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkRepeated.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtScheduleName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.wizpDates.SuspendLayout()
        CType(Me.lstDates, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ToolStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'WizardControl1
        '
        Me.WizardControl1.CancelText = "Ακύρωση"
        Me.WizardControl1.Controls.Add(Me.WelcomeWizardPage1)
        Me.WizardControl1.Controls.Add(Me.CompletionWizardPage1)
        Me.WizardControl1.Controls.Add(Me.wizpDates)
        Me.WizardControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.WizardControl1.FinishText = "Ολοκλήρωση"
        Me.WizardControl1.Location = New System.Drawing.Point(0, 0)
        Me.WizardControl1.Name = "WizardControl1"
        Me.WizardControl1.NavigationMode = DevExpress.XtraWizard.NavigationMode.Stacked
        Me.WizardControl1.NextText = "Συνέχεια >"
        Me.WizardControl1.Pages.AddRange(New DevExpress.XtraWizard.BaseWizardPage() {Me.WelcomeWizardPage1, Me.wizpDates, Me.CompletionWizardPage1})
        Me.WizardControl1.PreviousText = "< Επιστροφή"
        Me.WizardControl1.Size = New System.Drawing.Size(828, 461)
        Me.WizardControl1.Text = "Δημιουργία / Μεταβολή προγραμματισμού"
        Me.WizardControl1.WizardStyle = DevExpress.XtraWizard.WizardStyle.WizardAero
        '
        'WelcomeWizardPage1
        '
        Me.WelcomeWizardPage1.Controls.Add(Me.nupMinutes)
        Me.WelcomeWizardPage1.Controls.Add(Me.chkMinutes)
        Me.WelcomeWizardPage1.Controls.Add(Me.nupOffset)
        Me.WelcomeWizardPage1.Controls.Add(Me.LabelControl3)
        Me.WelcomeWizardPage1.Controls.Add(Me.chkWorkdays)
        Me.WelcomeWizardPage1.Controls.Add(Me.chkDaily)
        Me.WelcomeWizardPage1.Controls.Add(Me.chkWeekly)
        Me.WelcomeWizardPage1.Controls.Add(Me.chkFortnightly)
        Me.WelcomeWizardPage1.Controls.Add(Me.dteTime)
        Me.WelcomeWizardPage1.Controls.Add(Me.LabelControl2)
        Me.WelcomeWizardPage1.Controls.Add(Me.chkYearly)
        Me.WelcomeWizardPage1.Controls.Add(Me.chkMonthly)
        Me.WelcomeWizardPage1.Controls.Add(Me.nupDays)
        Me.WelcomeWizardPage1.Controls.Add(Me.chkRepeated)
        Me.WelcomeWizardPage1.Controls.Add(Me.LabelControl1)
        Me.WelcomeWizardPage1.Controls.Add(Me.txtScheduleName)
        Me.WelcomeWizardPage1.Name = "WelcomeWizardPage1"
        Me.WelcomeWizardPage1.Size = New System.Drawing.Size(768, 301)
        Me.WelcomeWizardPage1.Text = "Συμπληρώστε τα στοιχεία του προγραμματισμού"
        '
        'nupMinutes
        '
        Me.nupMinutes.EditValue = New Decimal(New Integer() {10, 0, 0, 0})
        Me.nupMinutes.Enabled = False
        Me.nupMinutes.Location = New System.Drawing.Point(447, 44)
        Me.nupMinutes.Name = "nupMinutes"
        Me.nupMinutes.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.nupMinutes.Properties.MaxValue = New Decimal(New Integer() {10000, 0, 0, 0})
        Me.nupMinutes.Properties.MinValue = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nupMinutes.Size = New System.Drawing.Size(54, 20)
        Me.nupMinutes.TabIndex = 13
        '
        'chkMinutes
        '
        Me.chkMinutes.Location = New System.Drawing.Point(191, 45)
        Me.chkMinutes.Name = "chkMinutes"
        Me.chkMinutes.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.chkMinutes.Properties.Appearance.Options.UseFont = True
        Me.chkMinutes.Properties.Caption = "Επαναλαμβανόμενο κάθε (λεπτά) :"
        Me.chkMinutes.Size = New System.Drawing.Size(250, 19)
        Me.chkMinutes.TabIndex = 12
        '
        'nupOffset
        '
        Me.nupOffset.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.nupOffset.Location = New System.Drawing.Point(689, 92)
        Me.nupOffset.Name = "nupOffset"
        Me.nupOffset.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.nupOffset.Properties.MaxValue = New Decimal(New Integer() {365, 0, 0, 0})
        Me.nupOffset.Properties.MinValue = New Decimal(New Integer() {365, 0, 0, -2147483648})
        Me.nupOffset.Size = New System.Drawing.Size(54, 20)
        Me.nupOffset.TabIndex = 2
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.LabelControl3.Location = New System.Drawing.Point(461, 94)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(222, 14)
        Me.LabelControl3.TabIndex = 11
        Me.LabelControl3.Text = "Eκτέλεση με καθυστέρηση ημερών :"
        '
        'chkWorkdays
        '
        Me.chkWorkdays.Location = New System.Drawing.Point(325, 214)
        Me.chkWorkdays.Name = "chkWorkdays"
        Me.chkWorkdays.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.chkWorkdays.Properties.Appearance.Options.UseFont = True
        Me.chkWorkdays.Properties.Caption = "Εργάσιμες ημέρες"
        Me.chkWorkdays.Size = New System.Drawing.Size(148, 19)
        Me.chkWorkdays.TabIndex = 10
        '
        'chkDaily
        '
        Me.chkDaily.Location = New System.Drawing.Point(181, 214)
        Me.chkDaily.Name = "chkDaily"
        Me.chkDaily.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.chkDaily.Properties.Appearance.Options.UseFont = True
        Me.chkDaily.Properties.Caption = "Καθημερινά"
        Me.chkDaily.Size = New System.Drawing.Size(113, 19)
        Me.chkDaily.TabIndex = 9
        '
        'chkWeekly
        '
        Me.chkWeekly.Location = New System.Drawing.Point(17, 214)
        Me.chkWeekly.Name = "chkWeekly"
        Me.chkWeekly.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.chkWeekly.Properties.Appearance.Options.UseFont = True
        Me.chkWeekly.Properties.Caption = "Κάθε εβδομάδα"
        Me.chkWeekly.Size = New System.Drawing.Size(139, 19)
        Me.chkWeekly.TabIndex = 8
        '
        'chkFortnightly
        '
        Me.chkFortnightly.Location = New System.Drawing.Point(325, 185)
        Me.chkFortnightly.Name = "chkFortnightly"
        Me.chkFortnightly.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.chkFortnightly.Properties.Appearance.Options.UseFont = True
        Me.chkFortnightly.Properties.Caption = "1 και 15 κάθε μήνα"
        Me.chkFortnightly.Size = New System.Drawing.Size(152, 19)
        Me.chkFortnightly.TabIndex = 7
        '
        'dteTime
        '
        Me.dteTime.EditValue = New Date(2013, 4, 26, 1, 0, 0, 0)
        Me.dteTime.Location = New System.Drawing.Point(193, 92)
        Me.dteTime.Name = "dteTime"
        Me.dteTime.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.dteTime.Properties.Mask.EditMask = "HH:mm"
        Me.dteTime.Size = New System.Drawing.Size(61, 20)
        Me.dteTime.TabIndex = 1
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.LabelControl2.Location = New System.Drawing.Point(87, 94)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(100, 14)
        Me.LabelControl2.TabIndex = 5
        Me.LabelControl2.Text = "Ώρα εκτέλεσης :"
        '
        'chkYearly
        '
        Me.chkYearly.Location = New System.Drawing.Point(17, 185)
        Me.chkYearly.Name = "chkYearly"
        Me.chkYearly.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.chkYearly.Properties.Appearance.Options.UseFont = True
        Me.chkYearly.Properties.Caption = "1η κάθε χρονιάς"
        Me.chkYearly.Size = New System.Drawing.Size(149, 19)
        Me.chkYearly.TabIndex = 5
        '
        'chkMonthly
        '
        Me.chkMonthly.Location = New System.Drawing.Point(181, 185)
        Me.chkMonthly.Name = "chkMonthly"
        Me.chkMonthly.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.chkMonthly.Properties.Appearance.Options.UseFont = True
        Me.chkMonthly.Properties.Caption = "1η κάθε μήνα"
        Me.chkMonthly.Size = New System.Drawing.Size(113, 19)
        Me.chkMonthly.TabIndex = 6
        '
        'nupDays
        '
        Me.nupDays.EditValue = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nupDays.Enabled = False
        Me.nupDays.Location = New System.Drawing.Point(273, 148)
        Me.nupDays.Name = "nupDays"
        Me.nupDays.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.nupDays.Properties.MaxValue = New Decimal(New Integer() {10000, 0, 0, 0})
        Me.nupDays.Properties.MinValue = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nupDays.Size = New System.Drawing.Size(54, 20)
        Me.nupDays.TabIndex = 4
        '
        'chkRepeated
        '
        Me.chkRepeated.Location = New System.Drawing.Point(17, 149)
        Me.chkRepeated.Name = "chkRepeated"
        Me.chkRepeated.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.chkRepeated.Properties.Appearance.Options.UseFont = True
        Me.chkRepeated.Properties.Caption = "Επαναλαμβανόμενο κάθε (ημέρες) :"
        Me.chkRepeated.Size = New System.Drawing.Size(250, 19)
        Me.chkRepeated.TabIndex = 3
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.LabelControl1.Location = New System.Drawing.Point(19, 21)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(168, 14)
        Me.LabelControl1.TabIndex = 2
        Me.LabelControl1.Text = "Όνομα προγραμματισμού :"
        '
        'txtScheduleName
        '
        Me.txtScheduleName.Location = New System.Drawing.Point(193, 19)
        Me.txtScheduleName.Name = "txtScheduleName"
        Me.txtScheduleName.Size = New System.Drawing.Size(550, 20)
        Me.txtScheduleName.TabIndex = 0
        '
        'CompletionWizardPage1
        '
        Me.CompletionWizardPage1.Name = "CompletionWizardPage1"
        Me.CompletionWizardPage1.Size = New System.Drawing.Size(768, 301)
        Me.CompletionWizardPage1.Text = "Ολοκλήρωση κι αποθήκευση αλλαγών"
        '
        'wizpDates
        '
        Me.wizpDates.Controls.Add(Me.lstDates)
        Me.wizpDates.Controls.Add(Me.ToolStrip1)
        Me.wizpDates.Name = "wizpDates"
        Me.wizpDates.Size = New System.Drawing.Size(768, 301)
        Me.wizpDates.Text = "Επιλεγμένες ημερομηνίες εκτέλεσης"
        '
        'lstDates
        '
        Me.lstDates.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lstDates.Location = New System.Drawing.Point(0, 25)
        Me.lstDates.Name = "lstDates"
        Me.lstDates.Size = New System.Drawing.Size(768, 276)
        Me.lstDates.TabIndex = 2
        '
        'ToolStrip1
        '
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnAdd, Me.ToolStripSeparator1, Me.btnDelete})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(768, 25)
        Me.ToolStrip1.TabIndex = 0
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'btnAdd
        '
        Me.btnAdd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.btnAdd.Image = CType(resources.GetObject("btnAdd.Image"), System.Drawing.Image)
        Me.btnAdd.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(60, 22)
        Me.btnAdd.Text = "Προσθήκη"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'btnDelete
        '
        Me.btnDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.btnDelete.Image = CType(resources.GetObject("btnDelete.Image"), System.Drawing.Image)
        Me.btnDelete.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(58, 22)
        Me.btnDelete.Text = "Διαγραφή"
        '
        'ScheduleWizardF
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(828, 461)
        Me.Controls.Add(Me.WizardControl1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "ScheduleWizardF"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Χρονοπρογραμματισμός"
        CType(Me.WizardControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.WizardControl1.ResumeLayout(False)
        Me.WelcomeWizardPage1.ResumeLayout(False)
        Me.WelcomeWizardPage1.PerformLayout()
        CType(Me.nupMinutes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkMinutes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nupOffset.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkWorkdays.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkDaily.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkWeekly.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkFortnightly.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dteTime.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkYearly.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkMonthly.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nupDays.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkRepeated.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtScheduleName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.wizpDates.ResumeLayout(False)
        Me.wizpDates.PerformLayout()
        CType(Me.lstDates, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents WizardControl1 As DevExpress.XtraWizard.WizardControl
    Friend WithEvents WelcomeWizardPage1 As DevExpress.XtraWizard.WelcomeWizardPage
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtScheduleName As DevExpress.XtraEditors.TextEdit
    Friend WithEvents CompletionWizardPage1 As DevExpress.XtraWizard.CompletionWizardPage
    Friend WithEvents wizpDates As DevExpress.XtraWizard.WizardPage
    Friend WithEvents FolderBrowserDialog1 As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents chkRepeated As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents chkYearly As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents chkMonthly As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents nupDays As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents btnAdd As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents btnDelete As System.Windows.Forms.ToolStripButton
    Friend WithEvents dteTime As DevExpress.XtraEditors.TimeEdit
    Friend WithEvents lstDates As DevExpress.XtraEditors.ListBoxControl
    Friend WithEvents chkFortnightly As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents chkWorkdays As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents chkDaily As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents chkWeekly As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents nupOffset As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents nupMinutes As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents chkMinutes As DevExpress.XtraEditors.CheckEdit
End Class
