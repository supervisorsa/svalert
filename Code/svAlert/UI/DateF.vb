﻿Public Class DateF

    Private _ExecutionTime As String
    Public ReadOnly Property ExecutionTime() As String
        Get
            Return _ExecutionTime
        End Get
    End Property

    Private Sub btnClose_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnClose.ItemClick
        Close()
    End Sub

    Private Sub btnSave_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnSave.ItemClick
        _ExecutionTime = dteDate.Text
        Close()
    End Sub
End Class