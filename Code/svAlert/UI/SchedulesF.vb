﻿Public Class SchedulesF

    Private Sub TasksF_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        GetDatasource()
    End Sub

    Private Sub GetDatasource()
        Dim AllSchedules = From t In dataMain.Schedules _
                     Order By t.ScheduleName _
                     Select t

        SvDevExpress.GridDataSource(dgdSchedule, gdviewSchedule, AllSchedules)
        InitGrid()
    End Sub

    Private Sub btnAdd_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnAdd.ItemClick
        Dim frm As New ScheduleWizardF
        frm.ShowDialog()
        GetDatasource()
    End Sub

    Private Sub InitGrid()
        For Each col As GridColumn In gdviewSchedule.Columns
            If col.FieldName.ToLower.Contains("schedulename") Then
                col.Caption = "Όνομα προγραμματισμού"
            ElseIf col.FieldName.ToLower.Contains("month") Then
                col.Caption = "Κάθε μήνα"
            ElseIf col.FieldName.ToLower.Contains("year") Then
                col.Caption = "Κάθε χρόνο"
            ElseIf col.FieldName.ToLower.Contains("days") Then
                col.Caption = "Κάθε x ημέρες"
            ElseIf col.FieldName.ToLower.Contains("active") Then
                col.Caption = "Ενεργός"
            Else
                col.Visible = False
            End If
        Next
        gdviewSchedule.BestFitColumns()
    End Sub

    Private Sub btnDelete_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnDelete.ItemClick
        If gdviewSchedule.FocusedRowHandle > -1 Then
            If MessageBox.Show("Θέλετε σίγουρα να γίνει διαγραφή;", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                Delete()
                GetDatasource()
            End If
        End If
    End Sub

    Private Sub Delete()
        Dim ScheduleID = CType(gdviewSchedule.GetFocusedRowCellValue("ID"), Guid)
        Dim ScheduleInUse = From t In dataMain.TaskLists _
                          Where t.ScheduleID.Equals(ScheduleID) _
                          Select t
        If ScheduleInUse.Count > 0 Then
            MessageBox.Show("Ο χρονοπρογραμματισμός είναι σε χρήση", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Else
            Dim CurrentSchedule = (From t In dataMain.Schedules _
                                 Where t.ID.Equals(ScheduleID) _
                                 Select t).FirstOrDefault

            If CurrentSchedule.ScheduleLines.Count > 0 Then
                dataMain.ScheduleLines.DeleteAllOnSubmit(CurrentSchedule.ScheduleLines)
            End If
            dataMain.Schedules.DeleteOnSubmit(CurrentSchedule)
            dataMain.SubmitChanges()
        End If
    End Sub

    Private Sub btnEdit_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnEdit.ItemClick
        EditSchedule()
    End Sub

    Private Sub EditSchedule()
        If gdviewSchedule.FocusedRowHandle > -1 Then
            Dim frm As New ScheduleWizardF
            frm.ScheduleID = CType(gdviewSchedule.GetFocusedRowCellValue("ID"), Guid)
            frm.ShowDialog()
            GetDatasource()
        End If
    End Sub

    Private Sub dgdSchedule_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgdSchedule.DoubleClick
        EditSchedule()
    End Sub
End Class