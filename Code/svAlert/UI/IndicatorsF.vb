﻿Public Class IndicatorsF

    Private Sub IndicatorsF_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim Indicator As New svAlertData.Indicators(ConnStringData)

        Dim IndicDate As DateTime = Nothing

        ArcScaleComponent1.MaxValue = Settings.GetString("Rest") 'My.Settings.Rest
        ArcScaleComponent1.Value = Indicator.Rest(IndicDate)
        lblRest.Text = IndicDate.ToShortDateString

        IndicDate = Nothing

        ArcScaleComponent2.MaxValue = Settings.GetString("AppsNo")
        ArcScaleComponent2.Value = Indicator.AppsNo(IndicDate, Now.Date)
        lblAppsNo.Text = IndicDate.ToShortDateString
    End Sub

End Class