﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MainF
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim SuperToolTip1 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip
        Dim ToolTipTitleItem1 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem
        Dim SuperToolTip2 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip
        Dim ToolTipTitleItem2 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem
        Dim SuperToolTip3 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip
        Dim ToolTipTitleItem3 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem
        Dim SuperToolTip4 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip
        Dim ToolTipTitleItem4 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem
        Dim SuperToolTip5 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip
        Dim ToolTipTitleItem5 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem
        Dim SuperToolTip6 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip
        Dim ToolTipTitleItem6 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem
        Dim SuperToolTip7 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip
        Dim ToolTipTitleItem7 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem
        Dim SuperToolTip8 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip
        Dim ToolTipTitleItem8 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MainF))
        Me.BarManager1 = New DevExpress.XtraBars.BarManager(Me.components)
        Me.Bar1 = New DevExpress.XtraBars.Bar
        Me.btnIndicators = New DevExpress.XtraBars.BarButtonItem
        Me.btnSnapshots = New DevExpress.XtraBars.BarButtonItem
        Me.btnTaskLists = New DevExpress.XtraBars.BarButtonItem
        Me.btnTasks = New DevExpress.XtraBars.BarButtonItem
        Me.btnUsers = New DevExpress.XtraBars.BarButtonItem
        Me.btnSchedules = New DevExpress.XtraBars.BarButtonItem
        Me.btnConfiguration = New DevExpress.XtraBars.BarButtonItem
        Me.btnExit = New DevExpress.XtraBars.BarButtonItem
        Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl
        Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl
        Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl
        Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl
        Me.DefaultLookAndFeel1 = New DevExpress.LookAndFeel.DefaultLookAndFeel(Me.components)
        Me.pnlMain = New DevExpress.XtraEditors.PanelControl
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pnlMain, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'BarManager1
        '
        Me.BarManager1.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.Bar1})
        Me.BarManager1.DockControls.Add(Me.barDockControlTop)
        Me.BarManager1.DockControls.Add(Me.barDockControlBottom)
        Me.BarManager1.DockControls.Add(Me.barDockControlLeft)
        Me.BarManager1.DockControls.Add(Me.barDockControlRight)
        Me.BarManager1.Form = Me
        Me.BarManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.btnUsers, Me.btnSnapshots, Me.btnTaskLists, Me.btnTasks, Me.btnExit, Me.btnSchedules, Me.btnIndicators, Me.btnConfiguration})
        Me.BarManager1.MaxItemId = 8
        '
        'Bar1
        '
        Me.Bar1.BarName = "Tools"
        Me.Bar1.DockCol = 0
        Me.Bar1.DockRow = 0
        Me.Bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Left
        Me.Bar1.FloatLocation = New System.Drawing.Point(166, 221)
        Me.Bar1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.btnIndicators), New DevExpress.XtraBars.LinkPersistInfo(Me.btnSnapshots), New DevExpress.XtraBars.LinkPersistInfo(Me.btnTaskLists), New DevExpress.XtraBars.LinkPersistInfo(Me.btnTasks), New DevExpress.XtraBars.LinkPersistInfo(Me.btnUsers), New DevExpress.XtraBars.LinkPersistInfo(Me.btnSchedules), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnConfiguration, DevExpress.XtraBars.BarItemPaintStyle.Standard), New DevExpress.XtraBars.LinkPersistInfo(Me.btnExit)})
        Me.Bar1.Text = "Tools"
        '
        'btnIndicators
        '
        Me.btnIndicators.Caption = "Δείκτες"
        Me.btnIndicators.Glyph = Global.svAlert.My.Resources.Resources.target
        Me.btnIndicators.Id = 6
        Me.btnIndicators.Name = "btnIndicators"
        ToolTipTitleItem1.Text = "Δείκτες"
        SuperToolTip1.Items.Add(ToolTipTitleItem1)
        Me.btnIndicators.SuperTip = SuperToolTip1
        '
        'btnSnapshots
        '
        Me.btnSnapshots.Caption = "Στιγμιότυπα"
        Me.btnSnapshots.Glyph = Global.svAlert.My.Resources.Resources.camera
        Me.btnSnapshots.Id = 1
        Me.btnSnapshots.Name = "btnSnapshots"
        ToolTipTitleItem2.Text = "Στιγμιότυπα"
        SuperToolTip2.Items.Add(ToolTipTitleItem2)
        Me.btnSnapshots.SuperTip = SuperToolTip2
        '
        'btnTaskLists
        '
        Me.btnTaskLists.Caption = "Λίστες εργασιών"
        Me.btnTaskLists.Glyph = Global.svAlert.My.Resources.Resources.restore
        Me.btnTaskLists.Id = 2
        Me.btnTaskLists.Name = "btnTaskLists"
        ToolTipTitleItem3.Text = "Λίστα εργασιών"
        SuperToolTip3.Items.Add(ToolTipTitleItem3)
        Me.btnTaskLists.SuperTip = SuperToolTip3
        '
        'btnTasks
        '
        Me.btnTasks.Caption = "Εργασίες"
        Me.btnTasks.Glyph = Global.svAlert.My.Resources.Resources.window
        Me.btnTasks.Id = 3
        Me.btnTasks.Name = "btnTasks"
        ToolTipTitleItem4.Text = "Εργασίες"
        SuperToolTip4.Items.Add(ToolTipTitleItem4)
        Me.btnTasks.SuperTip = SuperToolTip4
        '
        'btnUsers
        '
        Me.btnUsers.Caption = "Χρήστες"
        Me.btnUsers.Glyph = Global.svAlert.My.Resources.Resources.users
        Me.btnUsers.Id = 0
        Me.btnUsers.Name = "btnUsers"
        ToolTipTitleItem5.Text = "Χρήστες"
        SuperToolTip5.Items.Add(ToolTipTitleItem5)
        Me.btnUsers.SuperTip = SuperToolTip5
        '
        'btnSchedules
        '
        Me.btnSchedules.Caption = "Προγραμματισμός"
        Me.btnSchedules.Glyph = Global.svAlert.My.Resources.Resources.time
        Me.btnSchedules.Id = 5
        Me.btnSchedules.Name = "btnSchedules"
        ToolTipTitleItem6.Text = "Χρονοπρογραμματισμοί"
        SuperToolTip6.Items.Add(ToolTipTitleItem6)
        Me.btnSchedules.SuperTip = SuperToolTip6
        '
        'btnConfiguration
        '
        Me.btnConfiguration.Caption = "Παραμετροποίηση"
        Me.btnConfiguration.Glyph = Global.svAlert.My.Resources.Resources.configuration
        Me.btnConfiguration.Id = 7
        Me.btnConfiguration.Name = "btnConfiguration"
        ToolTipTitleItem7.Text = "Παραμετροποίηση"
        SuperToolTip7.Items.Add(ToolTipTitleItem7)
        Me.btnConfiguration.SuperTip = SuperToolTip7
        '
        'btnExit
        '
        Me.btnExit.Caption = "Έξοδος"
        Me.btnExit.Glyph = Global.svAlert.My.Resources.Resources._error
        Me.btnExit.Id = 4
        Me.btnExit.Name = "btnExit"
        ToolTipTitleItem8.Text = "Έξοδος"
        SuperToolTip8.Items.Add(ToolTipTitleItem8)
        Me.btnExit.SuperTip = SuperToolTip8
        '
        'barDockControlTop
        '
        Me.barDockControlTop.CausesValidation = False
        Me.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.barDockControlTop.Location = New System.Drawing.Point(0, 0)
        Me.barDockControlTop.Size = New System.Drawing.Size(817, 0)
        '
        'barDockControlBottom
        '
        Me.barDockControlBottom.CausesValidation = False
        Me.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.barDockControlBottom.Location = New System.Drawing.Point(0, 664)
        Me.barDockControlBottom.Size = New System.Drawing.Size(817, 0)
        '
        'barDockControlLeft
        '
        Me.barDockControlLeft.CausesValidation = False
        Me.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left
        Me.barDockControlLeft.Location = New System.Drawing.Point(0, 0)
        Me.barDockControlLeft.Size = New System.Drawing.Size(77, 664)
        '
        'barDockControlRight
        '
        Me.barDockControlRight.CausesValidation = False
        Me.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right
        Me.barDockControlRight.Location = New System.Drawing.Point(817, 0)
        Me.barDockControlRight.Size = New System.Drawing.Size(0, 664)
        '
        'DefaultLookAndFeel1
        '
        Me.DefaultLookAndFeel1.LookAndFeel.SkinName = "Office 2010 Black"
        '
        'pnlMain
        '
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(77, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(740, 664)
        Me.pnlMain.TabIndex = 5
        '
        'MainF
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(817, 664)
        Me.Controls.Add(Me.pnlMain)
        Me.Controls.Add(Me.barDockControlLeft)
        Me.Controls.Add(Me.barDockControlRight)
        Me.Controls.Add(Me.barDockControlBottom)
        Me.Controls.Add(Me.barDockControlTop)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.IsMdiContainer = True
        Me.Name = "MainF"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Supervisor Alert Server"
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pnlMain, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents BarManager1 As DevExpress.XtraBars.BarManager
    Friend WithEvents Bar1 As DevExpress.XtraBars.Bar
    Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
    Friend WithEvents pnlMain As DevExpress.XtraEditors.PanelControl
    Friend WithEvents DefaultLookAndFeel1 As DevExpress.LookAndFeel.DefaultLookAndFeel
    Friend WithEvents btnUsers As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnSnapshots As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnTaskLists As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnTasks As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnExit As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnSchedules As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnIndicators As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnConfiguration As DevExpress.XtraBars.BarButtonItem
End Class
