﻿Public Class SnapshotsF

    Private Sub gdviewSnapshots_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles gdviewSnapshots.DoubleClick
        ShowSnapshot()
    End Sub

    Private Sub ShowSnapshot()
        If gdviewSnapshots.FocusedRowHandle > -1 Then
            Dim SnapshotID = gdviewSnapshots.GetFocusedRowCellValue("ID")
            Dim frm As New ReportsF
            frm.SnapshotID = SnapshotID
            frm.ShowDialog()
        End If
    End Sub

    Private Sub SnapshotsF_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        GetDatasource()
    End Sub

    Private Sub GetDatasource()
        Dim AllTaskLists = From t In dataMain.Snapshots _
                     Order By t.ExecuteDate Descending _
                     Select t

        SvDevExpress.GridDataSource(dgdSnapshot, gdviewSnapshots, AllTaskLists)
        InitGrid()
    End Sub

    Public Sub RefreshData()
        GetDatasource()
    End Sub

    Private Sub InitGrid()
        For Each col As GridColumn In gdviewSnapshots.Columns
            If col.FieldName.ToLower.Contains("executedate") Then
                col.Caption = "Ημερομηνία"
                col.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
                col.DisplayFormat.FormatString = "g"
            ElseIf col.FieldName.ToLower.Contains("name") Then
                col.Caption = "Λίστα εργασιών"
            Else
                col.Visible = False
            End If
        Next
        gdviewSnapshots.BestFitColumns()
    End Sub

    Private Sub btnSnapshotReports_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnSnapshotReports.ItemClick
        ShowSnapshot()
    End Sub

    Private Sub btnUserReports_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnUserReports.ItemClick
        Dim frm As New ReportsF
        frm.ShowDialog()
    End Sub
End Class