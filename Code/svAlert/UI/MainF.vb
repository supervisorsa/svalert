﻿Public Class MainF

    Private IsExiting As Boolean = False

    Public Sub ShowNewForm(ByVal frm As Form)
        Dim IsAdded As Boolean = False
        For Each ctrl In pnlMain.Controls
            'If TypeOf ctrl Is TaskListsF Or TypeOf ctrl Is TasksF Or TypeOf ctrl Is SnapshotsF Then
            'ctrl.Visible = ctrl.Equals(frm)
            If (TypeOf ctrl Is TaskListsF And TypeOf frm Is TaskListsF) Or _
            (TypeOf ctrl Is TasksF And TypeOf frm Is TasksF) Or _
            (TypeOf ctrl Is SnapshotsF And TypeOf frm Is SnapshotsF) Or _
            (TypeOf ctrl Is IndicatorsF And TypeOf frm Is IndicatorsF) Or _
            (TypeOf ctrl Is SchedulesF And TypeOf frm Is SchedulesF) Or _
            (TypeOf ctrl Is ConfigurationF And TypeOf frm Is ConfigurationF) Or _
            (TypeOf ctrl Is UsersF And TypeOf frm Is UsersF) Then
                ctrl.Visible = True
                If TypeOf ctrl Is SnapshotsF Or TypeOf ctrl Is TaskListsF Then
                    ctrl.RefreshData()
                End If
                IsAdded = True
                'End If
            Else
                ctrl.Visible = False
                'pnlMain.Controls.Remove(ctrl)
            End If
        Next
        ' pnlMain.Controls.Clear()
        If Not IsAdded Then
            If frm.Parent IsNot pnlMain Then
                frm.MdiParent = Me
                'frm.Text = ""
                frm.Dock = DockStyle.Fill
                frm.ControlBox = False
                frm.FormBorderStyle = Windows.Forms.FormBorderStyle.None
                frm.Show()
            End If
            pnlMain.Controls.Add(frm)
        End If
        Me.Text = frm.Text

    End Sub

    Private Sub MainF_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            Dim frmIndicator As New IndicatorsF
            ShowNewForm(frmIndicator)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub btnTaskLists_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnTaskLists.ItemClick
        Dim frmTaskList As New TaskListsF
        ShowNewForm(frmTaskList)
    End Sub

    Private Sub btnTasks_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnTasks.ItemClick
        Dim frmTask As New TasksF
        ShowNewForm(frmTask)
    End Sub

    Private Sub btnSnapshots_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnSnapshots.ItemClick
        Dim frmSnapshot As New SnapshotsF
        ShowNewForm(frmSnapshot)
    End Sub

    Private Sub MainF_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If Not IsExiting AndAlso Not FormCloseQuestion() Then
            e.Cancel = True
        Else
            IsExiting = True
            Application.Exit()
        End If
    End Sub

    Private Function FormCloseQuestion() As Boolean
        Return (MessageBox.Show("Θέλετε σίγουρα να κλείσετε την εφαρμογή;", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes)
    End Function

    Private Sub btnExit_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnExit.ItemClick
        Close()
    End Sub

    Private Sub btnUsers_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnUsers.ItemClick
        Dim frmUser As New UsersF
        ShowNewForm(frmUser)
    End Sub

    Private Sub btnSchedules_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnSchedules.ItemClick
        Dim frmSchedules As New SchedulesF
        ShowNewForm(frmSchedules)
    End Sub

    Private Sub btnIndicators_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnIndicators.ItemClick
        Dim frmIndicators As New IndicatorsF
        ShowNewForm(frmIndicators)
    End Sub

    Private Sub btnConfiguration_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnConfiguration.ItemClick
        Dim frmConfig As New ConfigurationF
        ShowNewForm(frmConfig)
    End Sub
End Class