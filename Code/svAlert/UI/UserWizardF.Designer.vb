﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class UserWizardF
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(UserWizardF))
        Me.WizardControl1 = New DevExpress.XtraWizard.WizardControl
        Me.WelcomeWizardPage1 = New DevExpress.XtraWizard.WelcomeWizardPage
        Me.chkActive = New DevExpress.XtraEditors.CheckEdit
        Me.txtCode = New DevExpress.XtraEditors.TextEdit
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl
        Me.chkAdmin = New DevExpress.XtraEditors.CheckEdit
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl
        Me.txtMobile = New DevExpress.XtraEditors.TextEdit
        Me.txtEmail = New DevExpress.XtraEditors.TextEdit
        Me.txtPassword2 = New DevExpress.XtraEditors.TextEdit
        Me.txtUsername = New DevExpress.XtraEditors.TextEdit
        Me.txtPassword1 = New DevExpress.XtraEditors.TextEdit
        Me.txtExportPath = New DevExpress.XtraEditors.ButtonEdit
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
        Me.txtLoginName = New DevExpress.XtraEditors.TextEdit
        Me.CompletionWizardPage1 = New DevExpress.XtraWizard.CompletionWizardPage
        Me.wizpReports = New DevExpress.XtraWizard.WizardPage
        Me.lstReports = New DevExpress.XtraEditors.CheckedListBoxControl
        Me.WizardPage1 = New DevExpress.XtraWizard.WizardPage
        Me.lstUsers = New System.Windows.Forms.CheckedListBox
        Me.FolderBrowserDialog1 = New System.Windows.Forms.FolderBrowserDialog
        CType(Me.WizardControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.WizardControl1.SuspendLayout()
        Me.WelcomeWizardPage1.SuspendLayout()
        CType(Me.chkActive.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkAdmin.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMobile.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEmail.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPassword2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtUsername.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPassword1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtExportPath.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLoginName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.wizpReports.SuspendLayout()
        CType(Me.lstReports, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.WizardPage1.SuspendLayout()
        Me.SuspendLayout()
        '
        'WizardControl1
        '
        Me.WizardControl1.CancelText = "Ακύρωση"
        Me.WizardControl1.Controls.Add(Me.WelcomeWizardPage1)
        Me.WizardControl1.Controls.Add(Me.CompletionWizardPage1)
        Me.WizardControl1.Controls.Add(Me.wizpReports)
        Me.WizardControl1.Controls.Add(Me.WizardPage1)
        Me.WizardControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.WizardControl1.FinishText = "Ολοκλήρωση"
        Me.WizardControl1.Location = New System.Drawing.Point(0, 0)
        Me.WizardControl1.Name = "WizardControl1"
        Me.WizardControl1.NavigationMode = DevExpress.XtraWizard.NavigationMode.Stacked
        Me.WizardControl1.NextText = "Συνέχεια >"
        Me.WizardControl1.Pages.AddRange(New DevExpress.XtraWizard.BaseWizardPage() {Me.WelcomeWizardPage1, Me.WizardPage1, Me.wizpReports, Me.CompletionWizardPage1})
        Me.WizardControl1.PreviousText = "< Επιστροφή"
        Me.WizardControl1.Size = New System.Drawing.Size(828, 415)
        Me.WizardControl1.Text = "Δημιουργία / Μεταβολή χρήστη"
        Me.WizardControl1.WizardStyle = DevExpress.XtraWizard.WizardStyle.WizardAero
        '
        'WelcomeWizardPage1
        '
        Me.WelcomeWizardPage1.Controls.Add(Me.chkActive)
        Me.WelcomeWizardPage1.Controls.Add(Me.txtCode)
        Me.WelcomeWizardPage1.Controls.Add(Me.LabelControl8)
        Me.WelcomeWizardPage1.Controls.Add(Me.LabelControl7)
        Me.WelcomeWizardPage1.Controls.Add(Me.LabelControl6)
        Me.WelcomeWizardPage1.Controls.Add(Me.LabelControl5)
        Me.WelcomeWizardPage1.Controls.Add(Me.LabelControl4)
        Me.WelcomeWizardPage1.Controls.Add(Me.chkAdmin)
        Me.WelcomeWizardPage1.Controls.Add(Me.LabelControl3)
        Me.WelcomeWizardPage1.Controls.Add(Me.txtMobile)
        Me.WelcomeWizardPage1.Controls.Add(Me.txtEmail)
        Me.WelcomeWizardPage1.Controls.Add(Me.txtPassword2)
        Me.WelcomeWizardPage1.Controls.Add(Me.txtUsername)
        Me.WelcomeWizardPage1.Controls.Add(Me.txtPassword1)
        Me.WelcomeWizardPage1.Controls.Add(Me.txtExportPath)
        Me.WelcomeWizardPage1.Controls.Add(Me.LabelControl2)
        Me.WelcomeWizardPage1.Controls.Add(Me.LabelControl1)
        Me.WelcomeWizardPage1.Controls.Add(Me.txtLoginName)
        Me.WelcomeWizardPage1.Name = "WelcomeWizardPage1"
        Me.WelcomeWizardPage1.Size = New System.Drawing.Size(768, 255)
        Me.WelcomeWizardPage1.Text = "Συμπληρώστε τα στοιχεία του χρήστη"
        '
        'chkActive
        '
        Me.chkActive.EditValue = True
        Me.chkActive.Location = New System.Drawing.Point(666, 224)
        Me.chkActive.Name = "chkActive"
        Me.chkActive.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.chkActive.Properties.Appearance.Options.UseFont = True
        Me.chkActive.Properties.Caption = "Ενεργός"
        Me.chkActive.Size = New System.Drawing.Size(84, 19)
        Me.chkActive.TabIndex = 19
        '
        'txtCode
        '
        Me.txtCode.Location = New System.Drawing.Point(150, 223)
        Me.txtCode.Name = "txtCode"
        Me.txtCode.Size = New System.Drawing.Size(510, 20)
        Me.txtCode.TabIndex = 18
        '
        'LabelControl8
        '
        Me.LabelControl8.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.LabelControl8.Location = New System.Drawing.Point(86, 226)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(58, 14)
        Me.LabelControl8.TabIndex = 17
        Me.LabelControl8.Text = "Κωδικός :"
        '
        'LabelControl7
        '
        Me.LabelControl7.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.LabelControl7.Location = New System.Drawing.Point(485, 43)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(86, 14)
        Me.LabelControl7.TabIndex = 16
        Me.LabelControl7.Text = "Επιβεβαίωση :"
        '
        'LabelControl6
        '
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.LabelControl6.Location = New System.Drawing.Point(105, 150)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(39, 14)
        Me.LabelControl6.TabIndex = 15
        Me.LabelControl6.Text = "Email :"
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.LabelControl5.Location = New System.Drawing.Point(20, 185)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(124, 14)
        Me.LabelControl5.TabIndex = 14
        Me.LabelControl5.Text = "Φάκελος εξαγωγής :"
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.LabelControl4.Location = New System.Drawing.Point(95, 115)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(49, 14)
        Me.LabelControl4.TabIndex = 13
        Me.LabelControl4.Text = "Κινητό :"
        '
        'chkAdmin
        '
        Me.chkAdmin.Location = New System.Drawing.Point(637, 7)
        Me.chkAdmin.Name = "chkAdmin"
        Me.chkAdmin.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.chkAdmin.Properties.Appearance.Options.UseFont = True
        Me.chkAdmin.Properties.Caption = "Administrator"
        Me.chkAdmin.Size = New System.Drawing.Size(113, 19)
        Me.chkAdmin.TabIndex = 1
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.LabelControl3.Location = New System.Drawing.Point(33, 79)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(111, 14)
        Me.LabelControl3.TabIndex = 11
        Me.LabelControl3.Text = "Ονοματεπώνυμο :"
        '
        'txtMobile
        '
        Me.txtMobile.Location = New System.Drawing.Point(150, 113)
        Me.txtMobile.Name = "txtMobile"
        Me.txtMobile.Size = New System.Drawing.Size(600, 20)
        Me.txtMobile.TabIndex = 5
        '
        'txtEmail
        '
        Me.txtEmail.Location = New System.Drawing.Point(150, 148)
        Me.txtEmail.Name = "txtEmail"
        Me.txtEmail.Size = New System.Drawing.Size(600, 20)
        Me.txtEmail.TabIndex = 6
        '
        'txtPassword2
        '
        Me.txtPassword2.Location = New System.Drawing.Point(577, 41)
        Me.txtPassword2.Name = "txtPassword2"
        Me.txtPassword2.Properties.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtPassword2.Size = New System.Drawing.Size(173, 20)
        Me.txtPassword2.TabIndex = 3
        '
        'txtUsername
        '
        Me.txtUsername.Location = New System.Drawing.Point(150, 77)
        Me.txtUsername.Name = "txtUsername"
        Me.txtUsername.Size = New System.Drawing.Size(600, 20)
        Me.txtUsername.TabIndex = 4
        '
        'txtPassword1
        '
        Me.txtPassword1.Location = New System.Drawing.Point(150, 41)
        Me.txtPassword1.Name = "txtPassword1"
        Me.txtPassword1.Properties.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtPassword1.Size = New System.Drawing.Size(173, 20)
        Me.txtPassword1.TabIndex = 2
        '
        'txtExportPath
        '
        Me.txtExportPath.Location = New System.Drawing.Point(150, 183)
        Me.txtExportPath.Name = "txtExportPath"
        Me.txtExportPath.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.txtExportPath.Size = New System.Drawing.Size(600, 20)
        Me.txtExportPath.TabIndex = 7
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.LabelControl2.Location = New System.Drawing.Point(86, 43)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(58, 14)
        Me.LabelControl2.TabIndex = 4
        Me.LabelControl2.Text = "Κωδικός :"
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.LabelControl1.Location = New System.Drawing.Point(83, 10)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(61, 14)
        Me.LabelControl1.TabIndex = 2
        Me.LabelControl1.Text = "Χρήστης :"
        '
        'txtLoginName
        '
        Me.txtLoginName.Location = New System.Drawing.Point(150, 8)
        Me.txtLoginName.Name = "txtLoginName"
        Me.txtLoginName.Size = New System.Drawing.Size(481, 20)
        Me.txtLoginName.TabIndex = 0
        '
        'CompletionWizardPage1
        '
        Me.CompletionWizardPage1.Name = "CompletionWizardPage1"
        Me.CompletionWizardPage1.Size = New System.Drawing.Size(768, 255)
        Me.CompletionWizardPage1.Text = "Ολοκλήρωση κι αποθήκευση αλλαγών"
        '
        'wizpReports
        '
        Me.wizpReports.Controls.Add(Me.lstReports)
        Me.wizpReports.Name = "wizpReports"
        Me.wizpReports.Size = New System.Drawing.Size(768, 255)
        Me.wizpReports.Text = "Αναφορές χρήστη"
        '
        'lstReports
        '
        Me.lstReports.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lstReports.Location = New System.Drawing.Point(0, 0)
        Me.lstReports.Name = "lstReports"
        Me.lstReports.Size = New System.Drawing.Size(768, 255)
        Me.lstReports.TabIndex = 1
        '
        'WizardPage1
        '
        Me.WizardPage1.Controls.Add(Me.lstUsers)
        Me.WizardPage1.Name = "WizardPage1"
        Me.WizardPage1.Size = New System.Drawing.Size(768, 255)
        Me.WizardPage1.Text = "Υφιστάμενοι"
        '
        'lstUsers
        '
        Me.lstUsers.ColumnWidth = 230
        Me.lstUsers.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lstUsers.FormattingEnabled = True
        Me.lstUsers.Location = New System.Drawing.Point(0, 0)
        Me.lstUsers.MultiColumn = True
        Me.lstUsers.Name = "lstUsers"
        Me.lstUsers.Size = New System.Drawing.Size(768, 244)
        Me.lstUsers.Sorted = True
        Me.lstUsers.TabIndex = 5
        '
        'UserWizardF
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(828, 415)
        Me.Controls.Add(Me.WizardControl1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "UserWizardF"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Χρήστης"
        CType(Me.WizardControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.WizardControl1.ResumeLayout(False)
        Me.WelcomeWizardPage1.ResumeLayout(False)
        Me.WelcomeWizardPage1.PerformLayout()
        CType(Me.chkActive.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkAdmin.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMobile.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEmail.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPassword2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtUsername.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPassword1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtExportPath.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLoginName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.wizpReports.ResumeLayout(False)
        CType(Me.lstReports, System.ComponentModel.ISupportInitialize).EndInit()
        Me.WizardPage1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents WizardControl1 As DevExpress.XtraWizard.WizardControl
    Friend WithEvents WelcomeWizardPage1 As DevExpress.XtraWizard.WelcomeWizardPage
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtLoginName As DevExpress.XtraEditors.TextEdit
    Friend WithEvents CompletionWizardPage1 As DevExpress.XtraWizard.CompletionWizardPage
    Friend WithEvents wizpReports As DevExpress.XtraWizard.WizardPage
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lstReports As DevExpress.XtraEditors.CheckedListBoxControl
    Friend WithEvents txtUsername As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtPassword1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtExportPath As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents FolderBrowserDialog1 As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents txtPassword2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents chkAdmin As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtMobile As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtEmail As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtCode As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents chkActive As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents WizardPage1 As DevExpress.XtraWizard.WizardPage
    Friend WithEvents lstUsers As System.Windows.Forms.CheckedListBox
End Class
