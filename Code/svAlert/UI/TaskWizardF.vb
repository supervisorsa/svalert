﻿
Imports svAlertReportAndScenario.Globals

Public Class TaskWizardF
    Private CurrentTask As svAlertSnap.Task

    Private Sub TaskWizardF_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        For Each imp In ImportsNames
            svGlobal.SvClass.InsertToCombo(imp.Key, imp.Value, cboImportTypes)
        Next
        GetDatasource()
    End Sub

    Public WriteOnly Property TaskID() As Guid
        Set(ByVal value As Guid)
            CurrentTask = (From t In dataMain.Tasks _
                        Where t.ID.Equals(value) _
                        Select t).FirstOrDefault
            txtTaskName.Text = CurrentTask.TaskName
            If CurrentTask.ImportType = ImportTypes.FILE Then
                txtFilepath.Text = CurrentTask.FilePath
                txtFilename.Text = CurrentTask.FileName
            Else
                txtConnectionString.Text = CurrentTask.FilePath
            End If
        End Set
    End Property

    Private Sub GetDatasource()
        SvClass.InsertToCombo(ScenarioTypes.PRODUCTS, "Είδη", cboScenarios)
        SvClass.InsertToCombo(ScenarioTypes.TRANSACTIONS, "Εισπράξεις/Πληρωμές", cboScenarios)
        SvClass.InsertToCombo(ScenarioTypes.EXPENSES, "Έξοδα", cboScenarios)
        SvClass.InsertToCombo(ScenarioTypes.ASSIGNS, "Εργασίες/Επισκευές", cboScenarios)
        SvClass.InsertToCombo(ScenarioTypes.TIMETABLE, "Ωράριο", cboScenarios)
        SvClass.InsertToCombo(ScenarioTypes.COMPANIES, "Εταιρίες", cboScenarios)
        SvClass.InsertToCombo(ScenarioTypes.ENTRIES, "Παραγγελίες", cboScenarios)
        SvClass.InsertToCombo(ScenarioTypes.MEDIATORS, "Πωλητές/Υπάλληλοι", cboScenarios)
        SvClass.InsertToCombo(ScenarioTypes.APPS, "Ραντεβού/Υπηρεσίες", cboScenarios)
        SvClass.InsertToCombo(ScenarioTypes.TRADERS, "Συναλλασσόμενοι", cboScenarios)
        SvClass.InsertToCombo(ScenarioTypes.BANKBALANCE, "Υπόλοιπα τραπεζών", cboScenarios)
        SvClass.InsertToCombo(ScenarioTypes.AREABALANCE, "Υπόλοιπα χώρων", cboScenarios)
        SvClass.InsertToCombo(ScenarioTypes.VAT, "ΦΠΑ", cboScenarios)
        SvClass.InsertToCombo(ScenarioTypes.FISCALS, "Χρήσεις", cboScenarios)
        SvClass.InsertToCombo(ScenarioTypes.AREAS, "Χώροι", cboScenarios)
        SvClass.InsertToCombo(ScenarioTypes.NEWOBJECTS, "Εισαγωγή νέων", cboScenarios)
        SvClass.InsertToCombo(ScenarioTypes.LOTS, "Παρτίδες", cboScenarios)
        SvClass.InsertToCombo(ScenarioTypes.SEN, "Προμηθέυτες", cboScenarios)

        If CurrentTask IsNot Nothing Then
            Dim OtherScenarios As String = ""
            Dim BasicScenario As Short
            svAlertReportAndScenario.Globals.AllScenarios(CurrentTask.ScenarioType, OtherScenarios, BasicScenario)
            cboScenarios.SelectedItem = SvClass.GetComboItemByID(BasicScenario, cboScenarios)

            If OtherScenarios <> "" Then
                If OtherScenarios.IndexOf(ScenarioTypes.PRODUCTS) > -1 Then chkProduct.Checked = True
                If OtherScenarios.IndexOf(ScenarioTypes.MEDIATORS) > -1 Then chkMediator.Checked = True
                If OtherScenarios.IndexOf(ScenarioTypes.TRADERS) > -1 Then chkTrader.Checked = True
                If OtherScenarios.IndexOf(ScenarioTypes.AREAS) > -1 Then chkArea.Checked = True
            End If

            cboImportTypes.SelectedItem = SvClass.GetComboItemByID(CurrentTask.ImportType, cboImportTypes)
        
        End If

    End Sub

    Private Sub txtFilepath_ButtonClick(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) Handles txtFilepath.ButtonClick
        FolderBrowserDialog1.ShowDialog()
        If FolderBrowserDialog1.SelectedPath <> "" Then
            txtFilepath.Text = FolderBrowserDialog1.SelectedPath
        End If
    End Sub

    Private Sub WizardControl1_FinishClick(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles WizardControl1.FinishClick
        Save()
    End Sub

    Private Sub Save()
        Try
            Dim IsNew As Boolean = False
            If CurrentTask Is Nothing Then
                CurrentTask = New svAlertSnap.Task With {.ID = Guid.NewGuid}
                IsNew = True
            End If
            CurrentTask.IsActive = True
            CurrentTask.TaskName = txtTaskName.Text

            If cboImportTypes.SelectedItem.ID = ImportTypes.FILE Then
                CurrentTask.FilePath = txtFilepath.Text
                CurrentTask.FileName = txtFilename.Text
            Else
                CurrentTask.FilePath = txtConnectionString.Text
                CurrentTask.FileName = ""
            End If

            CurrentTask.ImportType = cboImportTypes.SelectedItem.ID

            If SvClass.ValidComboItem(cboScenarios) Then
                Select Case cboScenarios.SelectedItem.ID
                    Case ScenarioTypes.SEN
                        CurrentTask.ScenarioType = ScenarioTypes.SEN
                    Case ScenarioTypes.TRADERS
                        If chkMediator.Checked Then
                            CurrentTask.ScenarioType = ScenarioTypes.TRADERS_MEDS
                        Else
                            CurrentTask.ScenarioType = ScenarioTypes.TRADERS
                        End If
                    Case ScenarioTypes.AREABALANCE
                        If chkProduct.Checked And chkArea.Checked Then
                            CurrentTask.ScenarioType = ScenarioTypes.AREABALANCE_AREAS_PRODS
                        ElseIf chkProduct.Checked Then
                            CurrentTask.ScenarioType = ScenarioTypes.AREABALANCE_PRODS
                        ElseIf chkArea.Checked Then
                            CurrentTask.ScenarioType = ScenarioTypes.AREABALANCE_AREAS
                        Else
                            CurrentTask.ScenarioType = ScenarioTypes.AREABALANCE
                        End If
                    Case ScenarioTypes.ENTRIES
                        If chkMediator.Checked And chkTrader.Checked And chkProduct.Checked Then
                            CurrentTask.ScenarioType = ScenarioTypes.ENTRIES_PRODS_MEDS_TRDS
                        ElseIf chkMediator.Checked And chkTrader.Checked Then
                            CurrentTask.ScenarioType = ScenarioTypes.ENTRIES_MEDS_TRDS
                        ElseIf chkMediator.Checked And chkProduct.Checked Then
                            CurrentTask.ScenarioType = ScenarioTypes.ENTRIES_PRODS_MEDS
                        ElseIf chkTrader.Checked And chkProduct.Checked Then
                            CurrentTask.ScenarioType = ScenarioTypes.ENTRIES_PRODS_TRDS
                        ElseIf chkMediator.Checked Then
                            CurrentTask.ScenarioType = ScenarioTypes.ENTRIES_MEDS
                        ElseIf chkTrader.Checked Then
                            CurrentTask.ScenarioType = ScenarioTypes.ENTRIES_TRDS
                        ElseIf chkProduct.Checked Then
                            CurrentTask.ScenarioType = ScenarioTypes.ENTRIES_PRODS
                        Else
                            CurrentTask.ScenarioType = ScenarioTypes.ENTRIES
                        End If
                    Case ScenarioTypes.APPS
                        If chkMediator.Checked And chkTrader.Checked Then
                            CurrentTask.ScenarioType = ScenarioTypes.APPS_MEDS_TRDS
                        ElseIf chkMediator.Checked Then
                            CurrentTask.ScenarioType = ScenarioTypes.APPS_MEDS
                        ElseIf chkTrader.Checked Then
                            CurrentTask.ScenarioType = ScenarioTypes.APPS_TRDS
                        Else
                            CurrentTask.ScenarioType = ScenarioTypes.APPS
                        End If
                    Case ScenarioTypes.ASSIGNS
                        If chkMediator.Checked And chkTrader.Checked Then
                            CurrentTask.ScenarioType = ScenarioTypes.ASSIGNS_MEDS_TRDS
                        ElseIf chkMediator.Checked Then
                            CurrentTask.ScenarioType = ScenarioTypes.ASSIGNS_MEDS
                        ElseIf chkTrader.Checked Then
                            CurrentTask.ScenarioType = ScenarioTypes.ASSIGNS_TRDS
                        Else
                            CurrentTask.ScenarioType = ScenarioTypes.ASSIGNS
                        End If
                    Case ScenarioTypes.EXPENSES
                        If chkMediator.Checked And chkTrader.Checked Then
                            CurrentTask.ScenarioType = ScenarioTypes.EXPENSES_MEDS_TRDS
                        ElseIf chkMediator.Checked Then
                            CurrentTask.ScenarioType = ScenarioTypes.EXPENSES_MEDS
                        ElseIf chkTrader.Checked Then
                            CurrentTask.ScenarioType = ScenarioTypes.EXPENSES_TRDS
                        Else
                            CurrentTask.ScenarioType = ScenarioTypes.EXPENSES
                        End If
                    Case ScenarioTypes.TRANSACTIONS
                        If chkTrader.Checked Then
                            CurrentTask.ScenarioType = ScenarioTypes.TRANSACTIONS_TRDS
                        Else
                            CurrentTask.ScenarioType = ScenarioTypes.TRANSACTIONS
                        End If
                    Case Else
                        CurrentTask.ScenarioType = cboScenarios.SelectedItem.ID
                End Select
            End If

            If IsNew Then
                dataMain.Tasks.InsertOnSubmit(CurrentTask)
            End If

            dataMain.SubmitChanges()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub cboScenarios_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboScenarios.TextChanged
        If SvClass.ValidComboItem(cboScenarios) Then
            chkArea.Enabled = (cboScenarios.SelectedItem.ID = ScenarioTypes.AREABALANCE)
            chkProduct.Enabled = (cboScenarios.SelectedItem.ID = ScenarioTypes.ENTRIES) Or _
                               (cboScenarios.SelectedItem.ID = ScenarioTypes.AREABALANCE)
            chkMediator.Enabled = (cboScenarios.SelectedItem.ID = ScenarioTypes.TRADERS) Or _
                                 (cboScenarios.SelectedItem.ID = ScenarioTypes.ENTRIES) Or _
                                 (cboScenarios.SelectedItem.ID = ScenarioTypes.APPS) Or _
                                 (cboScenarios.SelectedItem.ID = ScenarioTypes.ASSIGNS) Or _
                                 (cboScenarios.SelectedItem.ID = ScenarioTypes.EXPENSES)
            chkTrader.Enabled = (cboScenarios.SelectedItem.ID = ScenarioTypes.ENTRIES) Or _
                                (cboScenarios.SelectedItem.ID = ScenarioTypes.APPS) Or _
                                 (cboScenarios.SelectedItem.ID = ScenarioTypes.ASSIGNS) Or _
                                 (cboScenarios.SelectedItem.ID = ScenarioTypes.EXPENSES) Or _
                                 (cboScenarios.SelectedItem.ID = ScenarioTypes.TRANSACTIONS)
            chkTrader.Enabled = (cboScenarios.SelectedItem.id = ScenarioTypes.SEN)
        End If
    End Sub

    Private Sub WizardControl1_NextClick(ByVal sender As Object, ByVal e As DevExpress.XtraWizard.WizardCommandButtonClickEventArgs) Handles WizardControl1.NextClick
        If WizardControl1.SelectedPage Is WelcomeWizardPage1 Then
            If txtTaskName.Text = "" Then
                MessageBox.Show("Συμπληρώστε ονομασία", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                e.Handled = True
            End If
            If Not SvClass.ValidComboItem(cboImportTypes) Then
                MessageBox.Show("Συμπληρώστε εισαγωγή", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                e.Handled = True
            Else
                If cboImportTypes.SelectedItem.ID = ImportTypes.FILE Then
                    wizpImportScenario.Visible = False
                    wizpImportFile.Visible = True
                Else
                    wizpImportFile.Visible = False
                    wizpImportScenario.Visible = True
                End If
            End If
        ElseIf WizardControl1.SelectedPage Is wizpImportFile Then
            If txtFilepath.Text = "" Then
                MessageBox.Show("Συμπληρώστε φάκελο αποθήκευσης", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                e.Handled = True
            ElseIf txtFilename.Text = "" Then
                MessageBox.Show("Συμπληρώστε όνομα αρχείου", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                e.Handled = True
            End If
        ElseIf WizardControl1.SelectedPage Is wizpImportScenario Then
            If txtConnectionString.Text = "" Then
                MessageBox.Show("Συμπληρώστε στοιχεία σύνδεσης", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                e.Handled = True
            End If
        ElseIf WizardControl1.SelectedPage Is wizpScenario Then
            If Not SvClass.ValidComboItem(cboScenarios) Then
                MessageBox.Show("Συμπληρώστε σενάριο", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub txtConnectionString_ButtonClick(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) Handles txtConnectionString.ButtonClick
        Dim frm As New svGlobal.DbConnForm
        frm.WriteToConfig = False
        'SEN NIKOS
        If cboImportTypes.SelectedIndex = 5 Then           
            txtConnectionString.Text = String.Format("Data Source=(DESCRIPTION=(CID=GTU_APP)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=)(PORT=)))(CONNECT_DATA=(SID=)(SERVER=DEDICATED)));User Id=;Password=;SasAlias=;WHID=;FYID=")
        Else
            frm.ShowDialog()
        End If

        If frm.DB <> "" Then
            txtConnectionString.Text = svGlobal.DbConn.ConstructConnString(frm.Server, frm.User, EnDecrypt.Encrypting(frm.Pwd, svAlertReportAndScenario.Globals.GlobalKey, "-"), frm.DB, "")
        End If
    End Sub

End Class
