﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class TaskWizardF
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(TaskWizardF))
        Me.WizardControl1 = New DevExpress.XtraWizard.WizardControl
        Me.WelcomeWizardPage1 = New DevExpress.XtraWizard.WelcomeWizardPage
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl
        Me.cboImportTypes = New DevExpress.XtraEditors.ComboBoxEdit
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
        Me.txtTaskName = New DevExpress.XtraEditors.TextEdit
        Me.wizpImportFile = New DevExpress.XtraWizard.WizardPage
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
        Me.txtFilepath = New DevExpress.XtraEditors.ButtonEdit
        Me.txtFilename = New DevExpress.XtraEditors.TextEdit
        Me.CompletionWizardPage1 = New DevExpress.XtraWizard.CompletionWizardPage
        Me.wizpImportScenario = New DevExpress.XtraWizard.WizardPage
        Me.txtConnectionString = New DevExpress.XtraEditors.ButtonEdit
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl
        Me.wizpScenario = New DevExpress.XtraWizard.WizardPage
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl
        Me.chkTrader = New DevExpress.XtraEditors.CheckEdit
        Me.chkMediator = New DevExpress.XtraEditors.CheckEdit
        Me.chkProduct = New DevExpress.XtraEditors.CheckEdit
        Me.chkArea = New DevExpress.XtraEditors.CheckEdit
        Me.cboScenarios = New DevExpress.XtraEditors.ComboBoxEdit
        Me.FolderBrowserDialog1 = New System.Windows.Forms.FolderBrowserDialog
        CType(Me.WizardControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.WizardControl1.SuspendLayout()
        Me.WelcomeWizardPage1.SuspendLayout()
        CType(Me.cboImportTypes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTaskName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.wizpImportFile.SuspendLayout()
        CType(Me.txtFilepath.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtFilename.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.wizpImportScenario.SuspendLayout()
        CType(Me.txtConnectionString.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.wizpScenario.SuspendLayout()
        CType(Me.chkTrader.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkMediator.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkProduct.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkArea.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboScenarios.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'WizardControl1
        '
        Me.WizardControl1.CancelText = "Ακύρωση"
        Me.WizardControl1.Controls.Add(Me.WelcomeWizardPage1)
        Me.WizardControl1.Controls.Add(Me.wizpImportFile)
        Me.WizardControl1.Controls.Add(Me.CompletionWizardPage1)
        Me.WizardControl1.Controls.Add(Me.wizpImportScenario)
        Me.WizardControl1.Controls.Add(Me.wizpScenario)
        Me.WizardControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.WizardControl1.FinishText = "Ολοκλήρωση"
        Me.WizardControl1.Location = New System.Drawing.Point(0, 0)
        Me.WizardControl1.Name = "WizardControl1"
        Me.WizardControl1.NextText = "Συνέχεια >"
        Me.WizardControl1.Pages.AddRange(New DevExpress.XtraWizard.BaseWizardPage() {Me.WelcomeWizardPage1, Me.wizpImportFile, Me.wizpImportScenario, Me.wizpScenario, Me.CompletionWizardPage1})
        Me.WizardControl1.PreviousText = "< Επιστροφή"
        Me.WizardControl1.Size = New System.Drawing.Size(774, 463)
        Me.WizardControl1.Text = "Δημιουργία / Μεταβολή εργασίας"
        Me.WizardControl1.WizardStyle = DevExpress.XtraWizard.WizardStyle.WizardAero
        '
        'WelcomeWizardPage1
        '
        Me.WelcomeWizardPage1.Controls.Add(Me.LabelControl6)
        Me.WelcomeWizardPage1.Controls.Add(Me.cboImportTypes)
        Me.WelcomeWizardPage1.Controls.Add(Me.LabelControl1)
        Me.WelcomeWizardPage1.Controls.Add(Me.txtTaskName)
        Me.WelcomeWizardPage1.Name = "WelcomeWizardPage1"
        Me.WelcomeWizardPage1.Size = New System.Drawing.Size(714, 303)
        Me.WelcomeWizardPage1.Text = "Συμπληρώστε τα στοιχεία της εργασίας"
        '
        'LabelControl6
        '
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.LabelControl6.Location = New System.Drawing.Point(29, 73)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(95, 14)
        Me.LabelControl6.TabIndex = 4
        Me.LabelControl6.Text = "Εισαγωγή από :"
        '
        'cboImportTypes
        '
        Me.cboImportTypes.Location = New System.Drawing.Point(144, 71)
        Me.cboImportTypes.Name = "cboImportTypes"
        Me.cboImportTypes.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboImportTypes.Size = New System.Drawing.Size(550, 20)
        Me.cboImportTypes.TabIndex = 3
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.LabelControl1.Location = New System.Drawing.Point(29, 25)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(109, 14)
        Me.LabelControl1.TabIndex = 2
        Me.LabelControl1.Text = "Όνομα εργασίας :"
        '
        'txtTaskName
        '
        Me.txtTaskName.Location = New System.Drawing.Point(144, 23)
        Me.txtTaskName.Name = "txtTaskName"
        Me.txtTaskName.Size = New System.Drawing.Size(550, 20)
        Me.txtTaskName.TabIndex = 0
        '
        'wizpImportFile
        '
        Me.wizpImportFile.Controls.Add(Me.LabelControl3)
        Me.wizpImportFile.Controls.Add(Me.LabelControl2)
        Me.wizpImportFile.Controls.Add(Me.txtFilepath)
        Me.wizpImportFile.Controls.Add(Me.txtFilename)
        Me.wizpImportFile.Name = "wizpImportFile"
        Me.wizpImportFile.Size = New System.Drawing.Size(714, 303)
        Me.wizpImportFile.Text = "Συμπληρώστε τα στοιχεία του αρχείου της εργασίας"
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.LabelControl3.Location = New System.Drawing.Point(121, 71)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(103, 14)
        Me.LabelControl3.TabIndex = 4
        Me.LabelControl3.Text = "Όνομα αρχείου :"
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.LabelControl2.Location = New System.Drawing.Point(24, 25)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(200, 14)
        Me.LabelControl2.TabIndex = 3
        Me.LabelControl2.Text = "Φάκελος αποθήκευσης αρχείου :"
        '
        'txtFilepath
        '
        Me.txtFilepath.Location = New System.Drawing.Point(230, 23)
        Me.txtFilepath.Name = "txtFilepath"
        Me.txtFilepath.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.txtFilepath.Size = New System.Drawing.Size(469, 20)
        Me.txtFilepath.TabIndex = 4
        '
        'txtFilename
        '
        Me.txtFilename.Location = New System.Drawing.Point(230, 69)
        Me.txtFilename.Name = "txtFilename"
        Me.txtFilename.Size = New System.Drawing.Size(469, 20)
        Me.txtFilename.TabIndex = 5
        '
        'CompletionWizardPage1
        '
        Me.CompletionWizardPage1.Name = "CompletionWizardPage1"
        Me.CompletionWizardPage1.Size = New System.Drawing.Size(714, 303)
        Me.CompletionWizardPage1.Text = "Ολοκλήρωση κι αποθήκευση αλλαγών"
        '
        'wizpImportScenario
        '
        Me.wizpImportScenario.Controls.Add(Me.txtConnectionString)
        Me.wizpImportScenario.Controls.Add(Me.LabelControl7)
        Me.wizpImportScenario.Name = "wizpImportScenario"
        Me.wizpImportScenario.Size = New System.Drawing.Size(714, 303)
        Me.wizpImportScenario.Text = "Σενάριο εισαγωγής"
        '
        'txtConnectionString
        '
        Me.txtConnectionString.Location = New System.Drawing.Point(80, 21)
        Me.txtConnectionString.Name = "txtConnectionString"
        Me.txtConnectionString.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.txtConnectionString.Size = New System.Drawing.Size(620, 20)
        Me.txtConnectionString.TabIndex = 7
        '
        'LabelControl7
        '
        Me.LabelControl7.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.LabelControl7.Location = New System.Drawing.Point(15, 23)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(59, 14)
        Me.LabelControl7.TabIndex = 6
        Me.LabelControl7.Text = "Σύνδεση :"
        '
        'wizpScenario
        '
        Me.wizpScenario.Controls.Add(Me.LabelControl5)
        Me.wizpScenario.Controls.Add(Me.LabelControl4)
        Me.wizpScenario.Controls.Add(Me.chkTrader)
        Me.wizpScenario.Controls.Add(Me.chkMediator)
        Me.wizpScenario.Controls.Add(Me.chkProduct)
        Me.wizpScenario.Controls.Add(Me.chkArea)
        Me.wizpScenario.Controls.Add(Me.cboScenarios)
        Me.wizpScenario.Name = "wizpScenario"
        Me.wizpScenario.Size = New System.Drawing.Size(714, 303)
        Me.wizpScenario.Text = "Σενάριο εισαγωγής"
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.LabelControl5.Location = New System.Drawing.Point(31, 73)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(146, 14)
        Me.LabelControl5.TabIndex = 6
        Me.LabelControl5.Text = "Ταυτόχρονη εισαγωγή :"
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.LabelControl4.Location = New System.Drawing.Point(31, 24)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(123, 14)
        Me.LabelControl4.TabIndex = 5
        Me.LabelControl4.Text = "Σενάρια εισαγωγής :"
        '
        'chkTrader
        '
        Me.chkTrader.Enabled = False
        Me.chkTrader.Location = New System.Drawing.Point(29, 172)
        Me.chkTrader.Name = "chkTrader"
        Me.chkTrader.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.chkTrader.Properties.Appearance.Options.UseFont = True
        Me.chkTrader.Properties.Caption = "Συναλλασσόμενων"
        Me.chkTrader.Size = New System.Drawing.Size(142, 19)
        Me.chkTrader.TabIndex = 4
        '
        'chkMediator
        '
        Me.chkMediator.Enabled = False
        Me.chkMediator.Location = New System.Drawing.Point(29, 137)
        Me.chkMediator.Name = "chkMediator"
        Me.chkMediator.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.chkMediator.Properties.Appearance.Options.UseFont = True
        Me.chkMediator.Properties.Caption = "Πωλητών / Υπαλλήλων"
        Me.chkMediator.Size = New System.Drawing.Size(177, 19)
        Me.chkMediator.TabIndex = 3
        '
        'chkProduct
        '
        Me.chkProduct.Enabled = False
        Me.chkProduct.Location = New System.Drawing.Point(29, 102)
        Me.chkProduct.Name = "chkProduct"
        Me.chkProduct.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.chkProduct.Properties.Appearance.Options.UseFont = True
        Me.chkProduct.Properties.Caption = "Ειδών"
        Me.chkProduct.Size = New System.Drawing.Size(75, 19)
        Me.chkProduct.TabIndex = 2
        '
        'chkArea
        '
        Me.chkArea.Enabled = False
        Me.chkArea.Location = New System.Drawing.Point(29, 206)
        Me.chkArea.Name = "chkArea"
        Me.chkArea.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.chkArea.Properties.Appearance.Options.UseFont = True
        Me.chkArea.Properties.Caption = "Χώρων"
        Me.chkArea.Size = New System.Drawing.Size(129, 19)
        Me.chkArea.TabIndex = 5
        '
        'cboScenarios
        '
        Me.cboScenarios.Location = New System.Drawing.Point(160, 22)
        Me.cboScenarios.Name = "cboScenarios"
        Me.cboScenarios.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboScenarios.Size = New System.Drawing.Size(537, 20)
        Me.cboScenarios.TabIndex = 0
        '
        'TaskWizardF
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(774, 463)
        Me.Controls.Add(Me.WizardControl1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "TaskWizardF"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Εργασία"
        CType(Me.WizardControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.WizardControl1.ResumeLayout(False)
        Me.WelcomeWizardPage1.ResumeLayout(False)
        Me.WelcomeWizardPage1.PerformLayout()
        CType(Me.cboImportTypes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTaskName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.wizpImportFile.ResumeLayout(False)
        Me.wizpImportFile.PerformLayout()
        CType(Me.txtFilepath.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtFilename.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.wizpImportScenario.ResumeLayout(False)
        Me.wizpImportScenario.PerformLayout()
        CType(Me.txtConnectionString.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.wizpScenario.ResumeLayout(False)
        Me.wizpScenario.PerformLayout()
        CType(Me.chkTrader.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkMediator.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkProduct.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkArea.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboScenarios.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents WizardControl1 As DevExpress.XtraWizard.WizardControl
    Friend WithEvents WelcomeWizardPage1 As DevExpress.XtraWizard.WelcomeWizardPage
    Friend WithEvents wizpImportFile As DevExpress.XtraWizard.WizardPage
    Friend WithEvents CompletionWizardPage1 As DevExpress.XtraWizard.CompletionWizardPage
    Friend WithEvents txtTaskName As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtFilename As DevExpress.XtraEditors.TextEdit
    Friend WithEvents FolderBrowserDialog1 As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents txtFilepath As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents wizpImportScenario As DevExpress.XtraWizard.WizardPage
    Friend WithEvents wizpScenario As DevExpress.XtraWizard.WizardPage
    Friend WithEvents cboScenarios As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents chkTrader As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents chkMediator As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents chkProduct As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents chkArea As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cboImportTypes As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtConnectionString As DevExpress.XtraEditors.ButtonEdit

End Class
