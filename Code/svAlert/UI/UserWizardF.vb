﻿Public Class UserWizardF
    Private CurrentUser As svAlertSnap.User

    Private Sub UserWizardF_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try

            GetDatasource()

            'Dim Importer As New svAlertData.Importer(ConnStringData)
            'ReportsNames = Importer.Reports
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

    End Sub

    Public WriteOnly Property UserID() As Guid
        Set(ByVal value As Guid)
            CurrentUser = (From t In dataMain.Users _
                         Where t.ID.Equals(value) _
                         Select t).FirstOrDefault

            txtLoginName.Text = CurrentUser.LoginName
            'txtPassword1.Text = CurrentUser.LoginPwd
            chkAdmin.Checked = CurrentUser.IsAdmin
            txtUsername.Text = CurrentUser.UserName
            txtMobile.Text = CurrentUser.MobilePhone
            txtEmail.Text = CurrentUser.Email
            txtExportPath.Text = CurrentUser.ExportPath
            txtCode.Text = CurrentUser.ExternalCode
            chkActive.Checked = CurrentUser.IsActive
            'txtCodeSupervisor.Text = CurrentUser.ExternalSupervisorCode
        End Set
    End Property

    Private Sub GetDatasource()
        Dim ReportChecked As Boolean = False

        For Each rpt In ReportsNames.ToList.OrderBy(Function(f) f.Value)
            If CurrentUser Is Nothing Then
                ReportChecked = False
            Else
                Dim ReportExists = From t In CurrentUser.UserReports _
                                   Where t.ReportType.Equals(rpt.Key) _
                                   Select t
                ReportChecked = (ReportExists.Count > 0)
            End If

            lstReports.Items.Add(rpt.Key, rpt.Value, If(ReportChecked, CheckState.Checked, CheckState.Unchecked), True)
        Next

        Dim SelectedUsers = From t In dataMain.Users _
                        Where t.IsActive = True _
                        Select t.ID, Name = t.UserName

        If CurrentUser IsNot Nothing Then
            SelectedUsers = SelectedUsers.Where(Function(f) Not f.ID.Equals(CurrentUser.ID))
        End If

        Dim IsSelected As svAlertSnap.UsersAssociate

        For Each usr In SelectedUsers
            If CurrentUser IsNot Nothing Then
                IsSelected = (From t In dataMain.UsersAssociates _
                                  Where t.SupervisorUserID.Equals(CurrentUser.ID) _
                                  And t.AssociateUserID.Equals(usr.ID) _
                                  Select t).FirstOrDefault
            End If

            Dim SvItem As New SvStructure(usr.ID, usr.Name)

            lstUsers.Items.Add(SvItem, IsSelected IsNot Nothing)
        Next

    End Sub

    Private Sub WizardControl1_FinishClick(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles WizardControl1.FinishClick
        Save()
    End Sub

    Private Sub Save()
        Try
            Dim IsNew As Boolean = False
            If CurrentUser Is Nothing Then
                CurrentUser = New svAlertSnap.User With {.ID = Guid.NewGuid}
                IsNew = True
            Else
                dataMain.UserReports.DeleteAllOnSubmit(CurrentUser.UserReports)
            End If
            If IsNew Or txtPassword1.Text <> "" Then
                CurrentUser.LoginPwd = txtPassword1.Text
            End If
            CurrentUser.IsActive = True
            CurrentUser.LoginName = txtLoginName.Text
            CurrentUser.IsAdmin = chkAdmin.Checked
            CurrentUser.UserName = txtUsername.Text
            CurrentUser.MobilePhone = txtMobile.Text
            CurrentUser.Email = txtEmail.Text
            CurrentUser.ExportPath = txtExportPath.Text
            CurrentUser.ExternalCode = txtCode.Text
            CurrentUser.IsActive = chkActive.Checked
            'CurrentUser.ExternalSupervisorCode = txtCodeSupervisor.Text

            If IsNew Then
                dataMain.Users.InsertOnSubmit(CurrentUser)
            End If

            For Each tsk In lstReports.Items
                If tsk.CheckState = System.Windows.Forms.CheckState.Checked Then
                    Dim NewReport As New svAlertSnap.UserReport With { _
                    .ID = Guid.NewGuid, _
                    .User = CurrentUser, _
                    .ReportType = tsk.Value}
                    dataMain.UserReports.InsertOnSubmit(NewReport)
                End If
            Next









            For i As Short = 0 To lstUsers.Items.Count - 1
                Dim NewUserID = lstUsers.Items(i).ID

                Dim ExistingUser = From t In dataMain.UsersAssociates _
                       Where t.AssociateUserID.Equals(NewUserID) _
                       And t.SupervisorUserID = CurrentUser.ID _
                       Select t
                If lstUsers.CheckedItems.IndexOf(lstUsers.Items(i)) > -1 Then
                    If ExistingUser.Count = 0 Then
                        Dim NewPrivileged As New svAlertSnap.UsersAssociate
                        NewPrivileged.ID = Guid.NewGuid
                        NewPrivileged.User = CurrentUser
                        NewPrivileged.AssociateUserID = NewUserID
                        dataMain.UsersAssociates.InsertOnSubmit(NewPrivileged)
                    End If
                Else
                    If ExistingUser.Count > 0 Then
                        dataMain.UsersAssociates.DeleteAllOnSubmit(ExistingUser)
                    End If
                End If
            Next

            dataMain.SubmitChanges()

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub txtExportPath_ButtonClick(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) Handles txtExportPath.ButtonClick
        FolderBrowserDialog1.ShowDialog()
        If FolderBrowserDialog1.SelectedPath <> "" Then
            txtExportPath.Text = FolderBrowserDialog1.SelectedPath
        End If
    End Sub

    Private Sub WizardControl1_NextClick(ByVal sender As Object, ByVal e As DevExpress.XtraWizard.WizardCommandButtonClickEventArgs) Handles WizardControl1.NextClick
        If WizardControl1.SelectedPage Is WelcomeWizardPage1 Then
            If CurrentUser Is Nothing AndAlso txtPassword1.Text = "" Then
                MessageBox.Show("Συμπληρώστε κωδικό", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                e.Handled = True
            ElseIf txtPassword1.Text <> txtPassword2.Text Then
                MessageBox.Show("Οι κωδικοί δεν ταιριάζουν", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                e.Handled = True
            ElseIf txtLoginName.Text = "" Then
                MessageBox.Show("Συμπληρώστε χρήστη", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                e.Handled = True
            ElseIf txtUsername.Text = "" Then
                MessageBox.Show("Συμπληρώστε ονοματεπώνυμο", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                e.Handled = True
            ElseIf txtEmail.Text = "" Then
                MessageBox.Show("Συμπληρώστε email", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                e.Handled = True
            ElseIf txtExportPath.Text = "" Then
                MessageBox.Show("Συμπληρώστε φάκελο εξαγωγής", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                e.Handled = True
            Else
                Dim CurrentID As Guid = Guid.Empty

                If CurrentUser IsNot Nothing Then
                    CurrentID = CurrentUser.ID
                End If

                Dim LoginExists = From t In dataMain.Users _
                                  Where t.LoginName.Equals(txtLoginName.Text) _
                                  And Not t.ID.Equals(CurrentID) _
                                  Select t

                If LoginExists.Count > 0 Then
                    MessageBox.Show("Υπάρχει ήδη χρήστης με αυτό το όνομα", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    e.Handled = True
                End If
            End If
        End If
    End Sub

    Private Structure SvStructure

        Private svItem As KeyValuePair(Of Guid, String)

        Public Property Name() As String
            Get
                Return svItem.Value
            End Get
            Set(ByVal value As String)
                svItem = New KeyValuePair(Of Guid, String)(ID, value)
            End Set
        End Property

        Public Property ID() As Guid
            Get
                Return svItem.Key
            End Get
            Set(ByVal value As Guid)
                svItem = New KeyValuePair(Of Guid, String)(value, Name)
            End Set
        End Property

        Public Sub New(ByVal svID As Guid, ByVal svName As String)
            svItem = New KeyValuePair(Of Guid, String)(svID, svName)
        End Sub

        Public Overrides Function ToString() As String
            Return Name
        End Function
    End Structure
End Class
