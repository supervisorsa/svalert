﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class TaskListWizardF
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(TaskListWizardF))
        Me.WizardControl1 = New DevExpress.XtraWizard.WizardControl
        Me.WelcomeWizardPage1 = New DevExpress.XtraWizard.WelcomeWizardPage
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
        Me.cboSchedule = New DevExpress.XtraEditors.ComboBoxEdit
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
        Me.txtTaskListName = New DevExpress.XtraEditors.TextEdit
        Me.CompletionWizardPage1 = New DevExpress.XtraWizard.CompletionWizardPage
        Me.wizpReports = New DevExpress.XtraWizard.WizardPage
        Me.SplitContainerControl1 = New DevExpress.XtraEditors.SplitContainerControl
        Me.lstUsers = New DevExpress.XtraEditors.ListBoxControl
        Me.bsUser = New System.Windows.Forms.BindingSource(Me.components)
        Me.lstReports = New DevExpress.XtraEditors.CheckedListBoxControl
        Me.wizpTasks = New DevExpress.XtraWizard.WizardPage
        Me.lstTasks = New DevExpress.XtraEditors.CheckedListBoxControl
        Me.chkActive = New DevExpress.XtraEditors.CheckEdit
        CType(Me.WizardControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.WizardControl1.SuspendLayout()
        Me.WelcomeWizardPage1.SuspendLayout()
        CType(Me.cboSchedule.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTaskListName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.wizpReports.SuspendLayout()
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainerControl1.SuspendLayout()
        CType(Me.lstUsers, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bsUser, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lstReports, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.wizpTasks.SuspendLayout()
        CType(Me.lstTasks, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkActive.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'WizardControl1
        '
        Me.WizardControl1.CancelText = "Ακύρωση"
        Me.WizardControl1.Controls.Add(Me.WelcomeWizardPage1)
        Me.WizardControl1.Controls.Add(Me.CompletionWizardPage1)
        Me.WizardControl1.Controls.Add(Me.wizpReports)
        Me.WizardControl1.Controls.Add(Me.wizpTasks)
        Me.WizardControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.WizardControl1.FinishText = "Ολοκλήρωση"
        Me.WizardControl1.Location = New System.Drawing.Point(0, 0)
        Me.WizardControl1.Name = "WizardControl1"
        Me.WizardControl1.NextText = "Συνέχεια >"
        Me.WizardControl1.Pages.AddRange(New DevExpress.XtraWizard.BaseWizardPage() {Me.WelcomeWizardPage1, Me.wizpTasks, Me.wizpReports, Me.CompletionWizardPage1})
        Me.WizardControl1.PreviousText = "< Επιστροφή"
        Me.WizardControl1.Size = New System.Drawing.Size(828, 371)
        Me.WizardControl1.Text = "Δημιουργία / Μεταβολή λίστας"
        Me.WizardControl1.WizardStyle = DevExpress.XtraWizard.WizardStyle.WizardAero
        '
        'WelcomeWizardPage1
        '
        Me.WelcomeWizardPage1.Controls.Add(Me.chkActive)
        Me.WelcomeWizardPage1.Controls.Add(Me.LabelControl2)
        Me.WelcomeWizardPage1.Controls.Add(Me.cboSchedule)
        Me.WelcomeWizardPage1.Controls.Add(Me.LabelControl1)
        Me.WelcomeWizardPage1.Controls.Add(Me.txtTaskListName)
        Me.WelcomeWizardPage1.Name = "WelcomeWizardPage1"
        Me.WelcomeWizardPage1.Size = New System.Drawing.Size(768, 211)
        Me.WelcomeWizardPage1.Text = "Συμπληρώστε τα στοιχεία της λίστας"
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.LabelControl2.Location = New System.Drawing.Point(29, 72)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(158, 14)
        Me.LabelControl2.TabIndex = 4
        Me.LabelControl2.Text = "Χρονοπρογραμματισμός :"
        '
        'cboSchedule
        '
        Me.cboSchedule.Location = New System.Drawing.Point(193, 70)
        Me.cboSchedule.Name = "cboSchedule"
        Me.cboSchedule.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboSchedule.Size = New System.Drawing.Size(460, 20)
        Me.cboSchedule.TabIndex = 3
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.LabelControl1.Location = New System.Drawing.Point(93, 25)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(94, 14)
        Me.LabelControl1.TabIndex = 2
        Me.LabelControl1.Text = "Όνομα λίστας :"
        '
        'txtTaskListName
        '
        Me.txtTaskListName.Location = New System.Drawing.Point(193, 23)
        Me.txtTaskListName.Name = "txtTaskListName"
        Me.txtTaskListName.Size = New System.Drawing.Size(557, 20)
        Me.txtTaskListName.TabIndex = 0
        '
        'CompletionWizardPage1
        '
        Me.CompletionWizardPage1.Name = "CompletionWizardPage1"
        Me.CompletionWizardPage1.Size = New System.Drawing.Size(768, 211)
        Me.CompletionWizardPage1.Text = "Ολοκλήρωση κι αποθήκευση αλλαγών"
        '
        'wizpReports
        '
        Me.wizpReports.Controls.Add(Me.SplitContainerControl1)
        Me.wizpReports.Name = "wizpReports"
        Me.wizpReports.Size = New System.Drawing.Size(768, 211)
        Me.wizpReports.Text = "Αναφορές ανά χρήστη"
        '
        'SplitContainerControl1
        '
        Me.SplitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainerControl1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainerControl1.Name = "SplitContainerControl1"
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.lstUsers)
        Me.SplitContainerControl1.Panel1.Text = "Panel1"
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.lstReports)
        Me.SplitContainerControl1.Panel2.Text = "Panel2"
        Me.SplitContainerControl1.Size = New System.Drawing.Size(768, 211)
        Me.SplitContainerControl1.SplitterPosition = 339
        Me.SplitContainerControl1.TabIndex = 2
        Me.SplitContainerControl1.Text = "SplitContainerControl1"
        '
        'lstUsers
        '
        Me.lstUsers.DataSource = Me.bsUser
        Me.lstUsers.DisplayMember = "UserName"
        Me.lstUsers.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lstUsers.Location = New System.Drawing.Point(0, 0)
        Me.lstUsers.Name = "lstUsers"
        Me.lstUsers.Size = New System.Drawing.Size(339, 211)
        Me.lstUsers.TabIndex = 0
        '
        'bsUser
        '
        Me.bsUser.Sort = ""
        '
        'lstReports
        '
        Me.lstReports.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lstReports.Location = New System.Drawing.Point(0, 0)
        Me.lstReports.Name = "lstReports"
        Me.lstReports.Size = New System.Drawing.Size(424, 211)
        Me.lstReports.TabIndex = 0
        '
        'wizpTasks
        '
        Me.wizpTasks.Controls.Add(Me.lstTasks)
        Me.wizpTasks.Name = "wizpTasks"
        Me.wizpTasks.Size = New System.Drawing.Size(768, 211)
        Me.wizpTasks.Text = "Εργασίες"
        '
        'lstTasks
        '
        Me.lstTasks.DisplayMember = "TaskName"
        Me.lstTasks.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lstTasks.Location = New System.Drawing.Point(0, 0)
        Me.lstTasks.Name = "lstTasks"
        Me.lstTasks.Size = New System.Drawing.Size(768, 211)
        Me.lstTasks.TabIndex = 0
        Me.lstTasks.ValueMember = "TaskID"
        '
        'chkActive
        '
        Me.chkActive.Location = New System.Drawing.Point(674, 69)
        Me.chkActive.Name = "chkActive"
        Me.chkActive.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.chkActive.Properties.Appearance.Options.UseFont = True
        Me.chkActive.Properties.Caption = "Ενεργή"
        Me.chkActive.Size = New System.Drawing.Size(76, 19)
        Me.chkActive.TabIndex = 20
        '
        'TaskListWizardF
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(828, 371)
        Me.Controls.Add(Me.WizardControl1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "TaskListWizardF"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Λίστα εργασιών"
        CType(Me.WizardControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.WizardControl1.ResumeLayout(False)
        Me.WelcomeWizardPage1.ResumeLayout(False)
        Me.WelcomeWizardPage1.PerformLayout()
        CType(Me.cboSchedule.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTaskListName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.wizpReports.ResumeLayout(False)
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainerControl1.ResumeLayout(False)
        CType(Me.lstUsers, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bsUser, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lstReports, System.ComponentModel.ISupportInitialize).EndInit()
        Me.wizpTasks.ResumeLayout(False)
        CType(Me.lstTasks, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkActive.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents WizardControl1 As DevExpress.XtraWizard.WizardControl
    Friend WithEvents WelcomeWizardPage1 As DevExpress.XtraWizard.WelcomeWizardPage
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtTaskListName As DevExpress.XtraEditors.TextEdit
    Friend WithEvents CompletionWizardPage1 As DevExpress.XtraWizard.CompletionWizardPage
    Friend WithEvents wizpReports As DevExpress.XtraWizard.WizardPage
    Friend WithEvents SplitContainerControl1 As DevExpress.XtraEditors.SplitContainerControl
    Friend WithEvents lstUsers As DevExpress.XtraEditors.ListBoxControl
    Friend WithEvents lstReports As DevExpress.XtraEditors.CheckedListBoxControl
    Friend WithEvents bsUser As System.Windows.Forms.BindingSource
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cboSchedule As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents wizpTasks As DevExpress.XtraWizard.WizardPage
    Friend WithEvents lstTasks As DevExpress.XtraEditors.CheckedListBoxControl
    Friend WithEvents chkActive As DevExpress.XtraEditors.CheckEdit
End Class
