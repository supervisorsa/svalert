﻿Public Class ReportsF

    Private _SnapshotID As Guid
    Public Property SnapshotID() As Guid
        Get
            Return _SnapshotID
        End Get
        Set(ByVal value As Guid)
            _SnapshotID = value
        End Set
    End Property

    Private Sub ReportsF_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        GetDatasource()
    End Sub

    Private Sub GetDatasource()
        Dim AllReports = From r In dataMain.SnapshotReports _
                         Join s In dataMain.Snapshots _
                         On r.SnapshotID Equals s.ID _
                         Where r.UserID.Equals(UserID) _
                         Select r.ReportName, SnapshotID = s.ID, s.ExecuteDate, r.User.ExportPath, r.ReportType

        If Not _SnapshotID.Equals(Guid.Empty) Then
            AllReports = AllReports.Where(Function(f) f.SnapshotID.Equals(_SnapshotID))
        End If

        'Dim NamedReports As New List(Of Report)

        'For Each rpt In AllReports
        '    NamedReports.Add(New Report(rpt.ExecuteDate, rpt.ReportName))
        'Next

        'SvDevExpress.GridDataSource(dgdReports, gdviewReports, NamedReports)
        SvDevExpress.GridDataSource(dgdReports, gdviewReports, AllReports)
        InitGrid()
    End Sub

    'Private Structure Report
    '    Private _SnapshotDate As Date
    '    Public Property SnapshotDate() As Date
    '        Get
    '            Return _SnapshotDate
    '        End Get
    '        Set(ByVal value As Date)
    '            _SnapshotDate = value
    '        End Set
    '    End Property

    '    Private _ReportName As String
    '    Public Property ReportName() As String
    '        Get
    '            Return _ReportName
    '        End Get
    '        Set(ByVal value As String)
    '            _ReportName = value
    '        End Set
    '    End Property

    '    Public Sub New(ByVal SvDate As Date, ByVal SvReportName As String)
    '        _SnapshotDate = SvDate
    '        _ReportName = SvReportName
    '    End Sub
    'End Structure

    Private Sub dgdReports_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgdReports.DoubleClick
        ExportReport(New List(Of String))
    End Sub

    Private Sub ExportReport(ByVal EmplCodes As List(Of String))
        If gdviewReports.FocusedRowHandle > -1 Then
            Dim SelectedReport = gdviewReports.GetRow(gdviewReports.FocusedRowHandle)
            Dim ReportExporter As New svAlertData.ExporterData(ConnStringData, SelectedReport.SnapshotID)
            ReportExporter.ExportPath = SelectedReport.ExportPath
            ReportExporter.Data(SelectedReport.ReportType, "TEMP", EmplCodes)
        End If
    End Sub

    Private Sub InitGrid()
        For Each col As GridColumn In gdviewReports.Columns
            If col.FieldName.ToLower.Contains("reportname") Then
                col.Caption = "Αναφορά"
            ElseIf col.FieldName.ToLower.Contains("executedate") Then
                col.Caption = "Ημερομηνία"
                col.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
                col.DisplayFormat.FormatString = "g"
            Else
                col.Visible = False
            End If
        Next
        gdviewReports.BestFitColumns()
    End Sub

    Private Sub btnExport_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnExport.ItemClick
        ExportReport(New List(Of String))
    End Sub

    Private Sub btnClose_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnClose.ItemClick
        Close()
    End Sub
End Class